<?php
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$aMenuLinks = array(
    Array(
        Loc::getMessage('L_MENU_SUB_ITEM_SETTINGS'),
        "settings/",
        Array(),
        Array('GROUPS' => array(U_GROUP_CODE_SUPER_ADMIN, U_GROUP_CODE_CLIENT_S_USER)),
        ""
    ),
    Array(
        Loc::getMessage('L_MENU_SUB_ITEM_USERS'),
        "users/",
        Array(),
        Array('GROUPS' => array(U_GROUP_CODE_SUPER_ADMIN)),
        ''
    ),
    Array(
        Loc::getMessage('L_MENU_SUB_ITEM_ORGANIZATION'),
        "company/",
        Array(),
        Array('GROUPS' => array(U_GROUP_CODE_SUPER_ADMIN, U_GROUP_CODE_CLIENT_S_USER)),
        '',
    ),
    Array(
        Loc::getMessage('L_MENU_SUB_ITEM_PRICES'),
        "orders/",
        Array(),
        Array('GROUPS' => array(U_GROUP_CODE_SUPER_ADMIN, U_GROUP_CODE_CLIENT_S_USER)),
        '',

    ),
    Array(
        Loc::getMessage('L_MENU_SUB_ITEM_STATISTICS'),
        "statistics/",
        Array(),
        Array('GROUPS' => array(U_GROUP_CODE_SUPER_ADMIN)),
        '',
    )
);
<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");?>

<?use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
global $APPLICATION, $USER;?>

<div class="page__frame">
    <div class="page__title">
        <h1><?= Loc::getMessage('TITLE_PAGE') ?></h1>
    </div>
    <div class="order">
        <?$APPLICATION->IncludeComponent(
            "box:user.cabinet",
            '',
            array(
                'SEF_MODE' => "Y",
                'SEF_FOLDER' => '/client/cabinet/',
                'SEF_URL_TEMPLATES' => array(
                    'settings' => 'settings/',
                    'company' => 'company/',
                    'orders' => 'orders/',
                    'users' => 'users/',
                    'user' => 'users/#ID#/',
                    'statistics' => 'statistics/',
                ),
                "VARIABLE_ALIASES" => array(
                    'settings' => array(
                        'NAME' => Loc::getMessage('SECTION_SETTINGS'),
                        'DEFAULT' => 'settings',
                        'VARIABLES' => array()
                    ),
                    'company' => array(
                        'NAME' => Loc::getMessage('SECTION_COMPANY'),
                        'DEFAULT' => 'company',
                        'VARIABLES' => array()
                    ),
                    'orders' => array(
                        'NAME' => Loc::getMessage('SECTION_ORDERS'),
                        'DEFAULT' => 'orders',
                        'VARIABLES' => array()
                    ),
                    'users' => array(
                        'NAME' => Loc::getMessage('SECTION_USERS'),
                        'DEFAULT' => 'users',
                        'VARIABLES' => array()
                    ),
                    'user' => array(
                        'NAME' => Loc::getMessage('SECTION_USERS'),
                        'DEFAULT' => 'user',
                        'VARIABLES' => array()
                    ),
                    'statistics' => array(
                        'NAME' => Loc::getMessage('SECTION_STATISTICS'),
                        'DEFAULT' => 'statistics',
                        'VARIABLES' => array()
                    ),
                ),
                'USER_GROUP_ROLE' => 'Y',
                'USER_FULL_NAME' => 'Y',
                'EDIT_DATA_URL' => 'settings/',
                'DATA_PROPERTY_FIELDS' => array(
                    'NAME',
                    'SECOND_NAME',
                    'LAST_NAME',
                    'WORK_POSITION',
                    //'WORK_COMPANY',
                    'LOGIN',
                    'ACTIVE',
                    'EMAIL',
                    'PERSONAL_PHONE',
                    'PERSONAL_MOBILE',
                ),
                'PHONE_FORMAT' => '+9 999 999 9999',
                'COMPANY_FIELDS' => array(                                          // массив со свойствами которые можно редактировать и какие нельзя
                    'NAME' => false,
                    'PROP_FORM_VALUE' => false,
                    'PROP_INN_VALUE' => false,
                    'PROP_KPP_VALUE' => false,
                    'PROP_OGRN_VALUE' => false,
                    'PROP_PHONE_VALUE' => true,
                    'PROP_FAX_VALUE' => true,
                    'PROP_ADDITIONAL_INFORM_VALUE' => true
                ),
                'IBLOCK_TYPE_LISTS' => IBLOCK_TYPE_LISTS,
                'IBLOCK_CODE_CONTRACTS' => IBLOCK_CODE_CONTRACTS,
                'IBLOCK_CODE_COMPANY' => IBLOCK_CODE_COMPANY,
                'IBLOCK_CODE_CLIENTS' => IBLOCK_CODE_CLIENTS,
                'IBLOCK_TYPE_REGISTRY' => IBLOCK_TYPE_REGISTRY,
                'COUNT_CONTRACTS_ON_PAGE' => 15,
                'NAV_TEMPLATE' => 'design',
                'COUNT_SYMBOL_PASSWORD' => 10
            )
        )?>
    </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
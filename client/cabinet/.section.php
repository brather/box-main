<?
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$sSectionName = Loc::getMessage('SECTION_NAME');
$arDirProperties = Array(
    'TITLE' => Loc::getMessage('SECTION_TITLE'),
    "description" => Loc::getMessage('SECTION_DESCRIPTION'),
    "keywords" => Loc::getMessage('SECTION_KEYWORDS')
);
?>
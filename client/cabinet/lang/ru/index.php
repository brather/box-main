<?php
$MESS['TITLE_PAGE'] = 'Личный кабинет';
$MESS['SECTION_SETTINGS'] = 'Настройки';
$MESS['SECTION_COMPANY'] = 'Организация';
$MESS['SECTION_ORDERS'] = 'Оплата';
$MESS['SECTION_USERS'] = 'Пользователи';
$MESS['SECTION_STATISTICS'] = 'Статистика';
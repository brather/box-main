<?php
$MESS['TITLE_PAGE'] = 'Personal cabinet';
$MESS['SECTION_SETTINGS'] = 'Settings';
$MESS['SECTION_COMPANY'] = 'Company';
$MESS['SECTION_ORDERS'] = 'Orders';
$MESS['SECTION_USERS'] = 'Users';
$MESS['SECTION_STATISTICS'] = 'Statistics';
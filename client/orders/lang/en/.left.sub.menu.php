<?php
$MESS['L_MENU_SUB_ITEM_ORDER_ADD'] = 'Create order';
$MESS['L_MENU_SUB_ITEM_ORDER_DRAFTS'] = 'Draft orders';
$MESS['L_MENU_SUB_ITEM_ORDER_COMPLETED'] = 'Completed orders';
$MESS['L_MENU_SUB_ITEM_ORDER_DELETED'] = 'Deleted orders';
$MESS['L_MENU_SUB_ITEM_REQUESTS'] = 'Requests';
<?php
$MESS['L_MENU_SUB_ITEM_ORDER_ADD'] = 'Создать заказ';
$MESS['L_MENU_SUB_ITEM_ORDER_DRAFTS'] = 'Черновики';
$MESS['L_MENU_SUB_ITEM_ORDER_COMPLETED'] = 'Завершенные';
$MESS['L_MENU_SUB_ITEM_ORDER_DELETED'] = 'Аннулированные';
$MESS['L_MENU_SUB_ITEM_REQUESTS'] = 'Запросы';
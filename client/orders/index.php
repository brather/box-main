<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

global $APPLICATION;

$APPLICATION->IncludeComponent(
    'box:user.orders',
    '',
    array(
        'IBLOCK_CODE_CONTRACTS' => IBLOCK_CODE_CONTRACTS,           // код инфоблока с договорами
        'IBLOCK_CODE_ORDERS' => IBLOCK_CODE_ORDERS,                 // код инфоблока заказы
        'IBLOCK_CODE_CLIENTS' => IBLOCK_CODE_CLIENTS,               // код инфоблока клиенты
        'SEF_MODE' => "Y",                                          // ЧПУ включен
        'SEF_FOLDER' => '/client/orders/',                          // Корневая директория
        'SEF_URL_TEMPLATES' => array(                               // Параметры ЧПУ
            'list' => '/',                                          // Страница со списком заказов, статус "processing", "execution", "prepared"
            'drafts' => 'drafts/',                                  // Страница со списком заказов, статус "formation"
            'completed' => 'completed/',                                  // Страница со списком заказов, статус "formation"
            'deleted' => 'deleted/',                                  // Страница со списком заказов, статус "formation"
            'detail' => '#ID#/',                                    // Детальная страница заказа (#ID# - id инфоблока реестра)
            'create' => 'create/',                                  // Страница создания заказа
            'requests' => 'requests/',                              // Список запросов
            'drequest' => 'requests/#ID#/',                         // Детальная страница запроса
        ),
        'EXCEPTION_TYPE_ORDER_CREATE' => array('first_placement'),  // Массив с исключенными для показа пользователю типами заказов
        'COUNT_ITEM_ON_PAGE' => 20,                                 // Количество ЕУ на странице реестра
        'MAX_COUNT_ITEM_ON_PAGE' => 100,                            // Максимальное количество ЕУ на странице реестра
        'COUNT_INPUT_IN_ROW' => 4,                                  // Количество input в строке
        'NAV_TEMPLATE' => 'design',                                 // Шаблон постраничной навигации
        'LIST_ORDERS' => array(                                     // Свойства списка заказов
            'SORT_FIELDS' => array(                                 // Список свойств для формирования таблицы и фильтра
                'ID',
                'DATE_CREATE',
                'PROPERTY_DATE',
                'PROPERTY_STATUS',
                'PROPERTY_TYPE_ORDER',
                'NAME',
                'CREATED_BY'
            ),
            'HL_CODE_STATUS'        => HL_BLOCK_CODE_STATUS_ORDER,         // код hl блока со статусами заказов
            'HL_CODE_ADRESS_CLIENT' => HL_BLOCK_CODE_ADRESS_ALIAS,  // код hl блока с адресами клиентов
            'HL_CODE_STATUS_OF_WMS' => HL_BLOCK_CODE_STATUS_WMS // код hl блока со статусами wms
        ),
        'USE_REQUESTS_MENU' => 'N',                                 // показ / скрытие ссылки на запросы в верхнем блоке меню
        'SET_SORT_FILTER' => array(
            'IBLOCK_NAME',
            'NAME',
            'PROPERTY_PROP_SSCC',
            'PROPERTY_PROP_WMS_STATUS'
        ),
    )
);?>

<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
<?php
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$aMenuLinks = array(
    Array(
        Loc::getMessage('L_MENU_SUB_ITEM_ORDER_ADD'),
        "create/",
        Array(),
        Array(),
        ""
    ),
    Array(
        Loc::getMessage('L_MENU_SUB_ITEM_ORDER_DRAFTS'),
        "drafts/",
        Array(),
        Array(),
        ""
    ),
    Array(
        Loc::getMessage('L_MENU_SUB_ITEM_ORDER_COMPLETED'),
        "completed/",
        Array(),
        Array(),
        ""
    ),
    Array(
        Loc::getMessage('L_MENU_SUB_ITEM_ORDER_DELETED'),
        "deleted/",
        Array(),
        Array(),
        ""
    ),
    Array(
        Loc::getMessage('L_MENU_SUB_ITEM_REQUESTS'),
        "requests/",
        Array(),
        Array('GROUPS' => array(U_GROUP_CODE_SUPER_ADMIN)),
        ""
    ),
);
<?php
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$aMenuLinks = array(
    Array(
        Loc::getMessage('L_MENU_SUB_ITEM_LOAD_LIST'),
        "list/",
        Array(),
        Array('GROUPS' => array(U_GROUP_CODE_SUPER_ADMIN)),
        ""
    ),
);
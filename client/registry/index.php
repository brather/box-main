<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");?>

<?use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
global $APPLICATION, $USER;
// отвечает за показ блока с просмотровщиком документов и разбивки страницы на две вертикальные части
$showViewer = 'N';
?>

<div class="page__col _left">
    <div id="horizontal-left" class="content">
        <?$APPLICATION->IncludeComponent(
            'box:user.registry',
            '',
            array(
                'SEF_MODE' => "Y",                                          // ЧПУ включен
                'SEF_FOLDER' => '/client/registry/',                        // Корневая директория
                'SEF_URL_TEMPLATES' => array(                               // Параметры ЧПУ
                    'list' => '/',                                          // Страница со списком всех ЕУ из всех доступных пользователю реестров
                    'detail' => '#ID#/',                                    // Страница с ЕУ конкретного реестра (#ID# - id инфоблока реестра)
                ),
                'COUNT_ITEM_ON_PAGE' => 15,                                 // Количество ЕУ на странице реестра
                'MAX_COUNT_ITEM_ON_PAGE' => 100,                            // Максимальное количество ЕУ на странице
                'COUNT_ON_PAGE_REGISTRY' => 15,                             // Количество реестров в списке
                'COUNT_IN_ROW_PARAMS' => 5,                                 // Количество выводимых параметров для фильтра в одной строке
                'NAV_TEMPLATE' => 'design',                                 // Шаблон постраничной навигации
                'UNIT_FILES' => '/upload/dig.documents/',                   // Путь к папке с оцифрованными файлами
                'OPTIONS_TO_UNIT' => array(                                 // Опции для работы с ЕУ реестра
                    'DOWNLOAD' => false,                                    // Скачать
                    'VIEW' => false,                                        // Просмотреть
                    'EDIT' => false,                                        // Редактировать
                    'PRINT' => false,                                       // Распечатать
                    'DELETE' => false                                       // Удалить
                ),
                'USE_VIEWER' => $showViewer,                                // На странице списка ЕУ включение просмотровщика
                'USE_ORDER_TYPE' => 'Y',                                    // Если == Y то есть возможность добавления в заказ
                'IBLOCK_CODE_ORDERS' => IBLOCK_CODE_ORDERS,
                'IBLOCK_CODE_CLIENTS' => IBLOCK_CODE_CLIENTS,
                'IBLOCK_CODE_CONTRACTS' => IBLOCK_CODE_CONTRACTS,
                'IBLOCK_TYPE_LISTS' => IBLOCK_TYPE_LISTS,
            )
        );?>
    </div>
</div>
<div class="page__col _right" data-right="<?= $showViewer ?>">
    <div class="content" id="horizontal-right">
        <div class="content__item _top">
            <div class="scrollbar-outer">

            </div>
        </div>
        <div class="content__item _bottom">
            <div class="scrollbar-outer">

            </div>
        </div>
    </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;

$aMenuLinksExt = $APPLICATION->IncludeComponent(
    'box:user.registry.list',
    '',
    Array(
        'MENU_EXT' => 'Y',
        'IBLOCK_CODE_CLIENTS' => IBLOCK_CODE_CLIENTS,
        'IBLOCK_CODE_CONTRACTS' => IBLOCK_CODE_CONTRACTS,
    )
);

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);?>
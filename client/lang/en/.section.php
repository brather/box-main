<?php
$MESS['SECTION_NAME'] = 'Personal cabinet';
$MESS['SECTION_TITLE'] = 'Personal cabinet';
$MESS['SECTION_DESCRIPTION'] = 'Personal user account is used to edit your settings, statistics and other data.';
$MESS['SECTION_KEYWORDS'] = 'Personal account, user settings, statistics, contract, order, organization profile, a list of users';
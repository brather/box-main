<?php
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$aMenuLinks = array(
    Array(
        Loc::getMessage('L_MENU_ITEM_FAVORITE'),
        "/",
        Array(),
        Array('GROUPS' => array(U_GROUP_CODE_SUPER_ADMIN)),
        ""
    ),
    Array(
        Loc::getMessage('L_MENU_ITEM_ORDERS'),
        "orders/",
        Array(),
        Array('GROUPS' => array(U_GROUP_CODE_SUPER_ADMIN, U_GROUP_CODE_CLIENT_S_USER)),
        ""
    ),
    Array(
        Loc::getMessage('L_MENU_ITEM_REQ'),
        "requests/",
        Array(),
        Array('GROUPS' => array(U_GROUP_CODE_SUPER_ADMIN, U_GROUP_CODE_CLIENT_USER)),
        ""
    ),
    Array(
        Loc::getMessage('L_MENU_ITEM_REGISTRY'),
        "registry/",
        Array(),
        Array(),
        ""
    ),
    Array(
        Loc::getMessage('L_MENU_ITEM_CIRCULATION'),
        "circulation/",
        Array(),
        Array('GROUPS' => array(U_GROUP_CODE_SUPER_ADMIN)),
        ""
    ),
    Array(
        Loc::getMessage('L_MENU_ITEM_CABINET'),
        "cabinet/",
        Array(),
        Array(),
        ""
    ),
);
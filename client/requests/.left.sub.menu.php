<?php
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$aMenuLinks = array(
    Array(
        Loc::getMessage('L_MENU_SUB_ITEM_REQ_CREATE'),
        "create/",
        Array(),
        Array(),
        ""
    ),
);
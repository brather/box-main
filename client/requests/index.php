<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
global $APPLICATION, $USER;

$APPLICATION->IncludeComponent(
    'box:user.requests',
    '',
    array(
        'SEF_MODE'          => "Y",                             // ЧПУ включен
        'SEF_FOLDER'        => '/client/requests/',             // Корневая директория
        'SEF_URL_TEMPLATES' => array(                               // Параметры ЧПУ
            'list'      => '/',                                     // Страница со списком запросов
            'detail'    => '#ID#/',                                 // Детальная страница запроса
            'create'    => 'create/',                               // Страница создания запроса
        ),
        'IBLOCK_CODE'           => array(
            'ORDERS'    => IBLOCK_CODE_ORDERS,
            'CONTRACTS' => IBLOCK_CODE_CONTRACTS,
            'CLIENTS'   => IBLOCK_CODE_CLIENTS
        ),
        'COUNT_ITEM_ON_PAGE'=> 20,                                // Количество ЕУ на странице реестра
        'COUNT_INPUT_IN_ROW'=> 3,                                 // Количество input в строке
        'NAV_TEMPLATE'      => 'design',                          // Шаблон постраничной навигации
        'LIST_REQUESTS' => array(                                     // Свойства списка заказов
            'SORT_FIELDS' => array(                                 // Список свойств для формирования таблицы и фильтра
                'ID',
                'DATE_CREATE',
                'PROPERTY_DATE',
                'PROPERTY_STATUS',
                'PROPERTY_TYPE_ORDER',
                'NAME',
                'CREATED_BY'
            ),
            'HL_CODE_STATUS' => HL_BLOCK_CODE_STATUS_ORDER,                // код hl блока со статусами заказов
        ),
        'SET_SORT_FILTER' => array(
            'ID',
            'IBLOCK_NAME',
            'NAME',
        ),
    )
);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
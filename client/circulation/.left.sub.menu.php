<?php
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$aMenuLinks = array(
    Array(
        Loc::getMessage('L_MENU_SUB_ITEM_CIRCULATION_ADD'),
        "add/",
        Array(),
        Array(),
        ""
    ),
    Array(
        Loc::getMessage('L_MENU_SUB_ITEM_CIRCULATION_HISTORY'),
        "history/",
        Array(),
        Array(),
        ""
    ),
);
<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/client/cabinet/statistics/(/?)([^/]*)#",
		"RULE" => "",
		"ID" => "box:user.cabinet",
		"PATH" => "/client/cabinet/index.php",
	),
	array(
		"CONDITION" => "#^/online/([\\.\\-0-9a-zA-Z]+)(/?)([^/]*)#",
		"RULE" => "alias=\$1",
		"ID" => "bitrix:im.router",
		"PATH" => "/desktop_app/router.php",
	),
	array(
		"CONDITION" => "#^/client/cabinet/settings/(/?)([^/]*)#",
		"RULE" => "",
		"ID" => "box:user.cabinet",
		"PATH" => "/client/cabinet/index.php",
	),
	array(
		"CONDITION" => "#^/client/cabinet/company/(/?)([^/]*)#",
		"RULE" => "",
		"ID" => "box:user.cabinet",
		"PATH" => "/client/cabinet/index.php",
	),
	array(
		"CONDITION" => "#^/client/cabinet/orders/(/?)([^/]*)#",
		"RULE" => "",
		"ID" => "box:user.cabinet",
		"PATH" => "/client/cabinet/index.php",
	),
	array(
		"CONDITION" => "#^/client/cabinet/users/(/?)([^/]*)#",
		"RULE" => "",
		"ID" => "box:user.cabinet",
		"PATH" => "/client/cabinet/index.php",
	),
	array(
		"CONDITION" => "#^/client/requests/(/?)([^/]*)#",
		"RULE" => "",
		"ID" => "box:user.cabinet",
		"PATH" => "/client/requests/index.php",
	),
	array(
		"CONDITION" => "#^/client/registry/(/?)([^/]*)#",
		"RULE" => "",
		"ID" => "box:user.cabinet",
		"PATH" => "/client/registry/index.php",
	),
	array(
		"CONDITION" => "#^/client/orders/(/?)([^/]*)#",
		"RULE" => "",
		"ID" => "box:user.cabinet",
		"PATH" => "/client/orders/index.php",
	),
	array(
		"CONDITION" => "#^/elar/orders/(/?)([^/]*)#",
		"RULE" => "",
		"ID" => "box:elar.supervisor",
		"PATH" => "/elar/orders/index.php",
	),
	array(
		"CONDITION" => "#^/elar/users/([^/]*)(/?)#",
		"RULE" => "USER_ID=$1",
		"ID" => "box:elar.user.detail",
		"PATH" => "/elar/users/detail.php",
	),
	array(
		"CONDITION" => "#^/elar/company/([^/]*)(/?)#",
		"RULE" => "COMPANY_ID=$1",
		"ID" => "box:elar.company.detail",
		"PATH" => "/elar/company/detail.php",
	),
	array(
		"CONDITION" => "#^/bitrix/services/ymarket/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/bitrix/services/ymarket/index.php",
	),
	array(
		"CONDITION" => "#^/online/(/?)([^/]*)#",
		"RULE" => "",
		"ID" => "bitrix:im.router",
		"PATH" => "/desktop_app/router.php",
	),
);

?>
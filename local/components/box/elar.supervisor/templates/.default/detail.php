<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 04.04.2017
 * Time: 15:59
 *
 * @var array $arResult
 * @var array $arParams
 */
global $APPLICATION;

$APPLICATION->IncludeComponent(
    'box:elar.supervisor.order.detail',
    '.default',
    array(
        "CACHE_TYPE" => "N",
        "USER_ID" => $arResult["USER_ID"],
        "SEF_FOLDER" => $arParams['SEF_FOLDER'],
        "USER_GROUPS" => $arResult["USER_GROUPS"],
        "ORDER_ID" => $arResult["VARIABLES"]["ID"],
        "LIST_URL" => $arParams["SEF_FOLDER"],
        "IBLOCK_CODE_ORDERS" => $arParams["IBLOCK_CODE_ORDERS"],
        "IBLOCK_CODE_CONTRACTS" => $arParams["IBLOCK_CODE_CONTRACTS"],
        "IBLOCK_CODE_CLIENTS" => $arParams["IBLOCK_CODE_CLIENTS"],
        "COUNT_ITEM_ON_PAGE" => $arParams["COUNT_ITEM_ON_PAGE"],
        "SET_SORT_FILTER" => $arParams["SET_SORT_FILTER"],
        "NAV_PRINT" => "Y",
        "NAV_TEMPLATE" => $arParams["NAV_TEMPLATE"],
        "HTML_PAGE_TITLE" => urlencode($arResult["PAGE_TITLE"]),
        "HL_CODE_STATUS" => HL_BLOCK_CODE_STATUS_ORDER,
        "HL_CODE_ADRESS_CLIENT" => HL_BLOCK_CODE_ADRESS_ALIAS,
        "PATH_COMPANY_ID" => '/elar/company/',
        "PATH_CLIENT" => '/elar/users/',
    )
);
/**
 * Created by AChechel on 30.03.2017.
 */
$(document).ready(function () {
    $('.orderLoaderStatus').each(function(i, v) {
        var self = v;
        $.ajax({
            url: '/local/components/box/user.orders/orderstatus.php',
            method: 'POST',
            dataType: 'JSON',
            data: {
                ORDER_ID: self.dataset.orderId
                //ORDER_ID: "1212"
            },
            success: function (res) {
                if (typeof res === 'object') {
                    if ("status" in res) {
                        $('#orderStatus_' + self.dataset.orderId).removeClass('fa fa-icon fa-spin fa-spinner').text(res.status);
                    }
                }
            },
            error: function (xhr, st, msg) {
                $('#orderStatus_' + self.dataset.orderId).removeClass('fa fa-icon fa-spin fa-spinner').html("<span class='frn-danger'>" + xhr.responseJSON.error + "</span>");
            }
        });
    });
});
<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 30.03.2017
 * Time: 11:16
 *
 * @var $arResult Array
 */
use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Application;

Loc::loadLanguageFile(__FILE__);
$arGet = Application::getInstance()->getContext()->getRequest()->getQueryList()->toArray();
?>

<div class="content__item _top top_panel">
    <div class="page__title">
        <h1><a onclick="return false;"><?= $arResult['PAGE_TITLE']?></a></h1>
    </div>
    <? if (is_array($arResult['FILTER_ROW']) && count($arResult['FILTER_ROW'])): ?>
    <div class="scrollbar-outer ">
        <form action="<?= $arResult['FORM_PATH_DEFAULT'] ?>" class="form form__fl-left" method="get" id="form-filter">
            <input type="hidden" name="ORDER_FILTER" value="Y">
            <div class="form__row form__row__sortable" id="sortable">
                <?
                $i = 0;
                foreach ( $arResult['FILTER_ROW'] as $code => $row )
                { ?>
                    <div class="column form__row__col _size4 ui-sortable">
                        <div class="portlet" data-portlet="<?= $i ?>">
                            <? switch ( $row['PROPERTY_TYPE'] )
                            {
                                case 'S': ?>
                                    <div class="portlet-header"><?= $row['NAME'] ?></div>
                                    <div class="portlet-content"><input
                                            class="form__field<?= ($row['USER_TYPE'] == 'Date' || $row['USER_TYPE'] == 'DateTime' )? ' js-datepicker':'' ?>" type="text"
                                            name="<?= $code ?>" value="<?= $arResult['SAVED_GET_PARAMS'][$code] ? $arResult['SAVED_GET_PARAMS'][$code] : "" ?>"/></div>
                                    <? break;
                                case 'L': ?>
                                    <div class="portlet-header"><?= $row['NAME'] ?></div>
                                    <div class="portlet-content"><select
                                            type="text" name="<?= $code ?>[]"
                                            multiple>
                                            <option value="">-</option>
                                            <? if ($row['HB_VALUES'] != null):
                                                foreach ($row['HB_VALUES'] as $val) : ?>
                                                     <option value="<?= $val['UF_XML_ID']?>" <?= (in_array($val['UF_XML_ID'], $arResult['SAVED_GET_PARAMS'][$code]))? "selected='selected'":''?>><?= $val['UF_NAME']?></option>
                                                <? endforeach; ?>
                                            <? elseif ($row['PROP_VALUES'] != null):
                                                foreach ($row['PROP_VALUES'] as $val):
                                                    ?><option value="<?= $val['ID']?>" <?= (in_array($val['ID'], $arResult['SAVED_GET_PARAMS'][$code]))? "selected='selected'":''?>><?= $val['VALUE']?></option><?
                                                endforeach;
                                            endif; ?>
                                        </select></div>
                                    <? break;
                            }
                            $i++;
                            ?>
                        </div>
                    </div>
                <? } ?>
            </div>
            <div class="form__row">
                <button type="submit" class="btn">Применить</button>
            </div>
        </form>
    </div>
    <? endif; ?>
</div>
<div class="content__item _bottom content__item__table">
    <div class="scrollbar-outer">
        <table class="table js-table">
            <thead>
                <tr>
                    <th><?= Loc::getMessage('ELAR_SUPERVISION_NUMBER_ORDER')?></th>
                    <th><?= Loc::getMessage('ELAR_SUPERVISION_TYPE_ORDER')?></th>
                    <th><?= Loc::getMessage('ELAR_SUPERVISION_STATUS_ORDER')?></th>
                    <th><?= Loc::getMessage('ELAR_SUPERVISION_ID_CUSTOMERS')?></th>
                    <th><?= Loc::getMessage('ELAR_SUPERVISION_CUSTOMER')?></th>
                    <th><?= Loc::getMessage('ELAR_SUPERVISION_DATE_OF_CREATE')?></th>
                    <th><?= Loc::getMessage('ELAR_SUPERVISION_DATE_OF_ISSUE')?></th>
                    <th><?= Loc::getMessage('ELAR_SUPERVISION_COMMENTS')?></th>
                </tr>
            </thead>
            <tbody><?
            if (isset($arResult['ORDERS']) && count($arResult['ORDERS'])):
                foreach ($arResult['ORDERS'] as $order): ?>
                    <tr>
                        <td><a class="order-detail" href="<?= $arParams['SEF_FOLDER'] . $order['ID']?>/">№<?= $order['ID']?></a></td>
                        <td><?= $order['PROPERTY_TYPE_ORDER_VALUE']?></td>
                        <td><span data-order-id="<?= $order['ID']?>" id="orderStatus_<?=$order['ID']?>" class="fa fa-icon fa-spin fa-spinner orderLoaderStatus"></span></td>
                        <td><?= $order['CREATED_BY']?></td>
                        <td><?= $order['CREATED_USER_NAME']?></td>
                        <td><?= $order['PROPERTY_DATE_VALUE']?></td>
                        <td><?= $order['PROPERTY_DATE_OF_ISSUE_VALUE']?></td>
                        <td><?= $order['PROPERTY_COMMENT_VALUE']?></td>
                    </tr>
            <?  endforeach;
            else: ?>
               <tr>
                   <td colspan="8" align="center"><?= Loc::getMessage('ELAR_SUPERVISION_EMPTY_DATA')?></td>
               </tr>
            <? endif;?>
            </tbody>
        </table>
        <div class="navigation">
            <?= $arResult['NAV_PRINT'] ?>
            <div class="pager">
                <div class="pager__current">
                    <span><?= Loc::getMessage('ELAR_SUPERVISION_COUNT_ORDERS_PER_PAGE')?></span>
                    <form action="" method="GET" class="nav-form">
                        <?= getHTMLInputExcludeParam($arGet, 'PER_PAGE') ?>
                        <input type="text" name="PER_PAGE" value="<?= $arResult['PER_PAGE'] ?>" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


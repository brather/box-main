<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 30.03.2017
 * Time: 11:17
 */
$MESS['ELAR_SUPERVISION_APPLY'] = 'Применить';
$MESS['ELAR_SUPERVISION_NUMBER_ORDER'] = 'Номер заказа';
$MESS['ELAR_SUPERVISION_TYPE_ORDER'] = 'Тип заказа';
$MESS['ELAR_SUPERVISION_STATUS_ORDER'] = 'Статус заказа';
$MESS['ELAR_SUPERVISION_ID_CUSTOMERS'] = 'ID Заказчика';
$MESS['ELAR_SUPERVISION_CUSTOMER'] = 'Заказчик';
$MESS['ELAR_SUPERVISION_DATE_OF_CREATE'] = 'Дата оформления';
$MESS['ELAR_SUPERVISION_DATE_OF_ISSUE'] = 'Срок исполнения';
$MESS['ELAR_SUPERVISION_COMMENTS'] = 'Комментарий';

$MESS['ELAR_SUPERVISION_EMPTY_DATA'] = 'Нет данных';

$MESS['ELAR_SUPERVISION_COUNT_ORDERS_PER_PAGE'] = 'Заказов на странице: ';



<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 30.03.2017
 * Time: 11:13
 */
use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader;

Loc::loadMessages(__FILE__);

class ElarSupervisor extends CBitrixComponent {
    /* Поля фильтров */
    private $filterRow = array(
        "PROPERTY" => array(
            'STATUS', 'TYPE_ORDER', 'DATE_OF_ISSUE', 'DATE'
        ),
        "ELEMENT" => array(
            'CREATED_BY'
        ),
    );
    private $arGet = array();

    /* Default limit order in table */
    private $orderPerPage = 10;

    /**
     * Проверяем принадлежность пользоватея к группе Супервайзеров
     * @return bool
     */
    private function _inGroup(){
        global $USER;

        $allowGroup = U_GROUP_CODE_ELAR_SUPERVISOR;
        $arGroup = $USER->GetUserGroupArray();

        if ( is_array($arGroup) && count($arGroup) > 0 ) {
            foreach ( $arGroup as $group) {
                $dbGroup = CGroup::GetByID($group)->Fetch();
                //var_dump($dbGroup['STRING_ID']);
                if ($dbGroup['STRING_ID'] == $allowGroup)
                    return true;
            }
        }

        return false;
    }

    public function executeComponent()
    {
        if ( $this->_inGroup() === false ){
            //if(defined("ERROR_404") ) {
                LocalRedirect("/404.php", "404 Not Found");
            //}
        }

        $this->arGet = Application::getInstance()->getContext()->getRequest()->getQueryList()->toArray();

        Loader::includeModule('iblock');
        Loader::includeModule('highloadblock');

        $this->_setArResult();
        $this->_getOrders( $this->_ifGetFilterParam() );
        $this->includeComponentTemplate($this->arParams['COMPONENT_TEMPLATE_FILE']);
    }

    private function _setArResult()
    {
        $this->arResult["PAGE_TITLE"] = Loc::getMessage('ELAR_SUPERVISION_PAGE_TITLE');
        $this->arResult["FILTER_ROW"] = $this->_getFilterRowProperty();
        $this->arResult["SAVED_GET_PARAMS"] = $this->arGet;
    }

    /** Возвращает массив данных свойсва типа HighLoadBlock
     *
     * Данные кешуруются на 1 Час !
     *
     * @param $tableName
     *
     * @return array
     */
    private function _getPropertyFromHL ($tableName)
    {
        $hbArValue = [];

        $cache  = new CPHPCache();
        $cTime  = 60 * 60; // 1 hour
        $cID    = md5('PROPERTY_HB_' . $tableName);

        /* Start cache */
        if ($cache->InitCache($cTime, $cID, 'supervisor')) {

            $tmp = $cache->GetVars();
            $hbArValue = $tmp[$cID];
            /* Return cache values */

        } else {

            if ($cache->StartDataCache($cTime, $cID, 'supervisor')) {

                $hb = $this->_getHLDataClass($tableName);
                $dbHb = $hb::getList(array(
                    'select' => array("ID", "UF_NAME", "UF_XML_ID"),
                    'order' => array("UF_SORT" => "ASC")
                ));

                while ($resHb = $dbHb->fetch())
                    $hbArValue[$resHb['UF_XML_ID']] = $resHb;

                /* Make cache values */
                $cache->EndDataCache(array($cID => $hbArValue));
            }

        }
        /* End cache */

        return $hbArValue;
    }

    /** Формирует массив знаечний для узказанного свойства, определенного
     * инфоблока.
     *
     * Данные кешурются на 1 Час !
     *
     * @param     $propertyID
     * @param int $IBLOCK_ID default = 6
     *
     * @return array
     */
    private function _getPropertyFromList($propertyID, $IBLOCK_ID = 6)
    {
        $propValue = array();

        $cache  = new CPHPCache();
        $cTime  = 60 * 60; // 1 hour
        $cID    = md5('PROPERTY_' . $propertyID . "_" . $IBLOCK_ID);
        /* Start cache */
        if ($cache->InitCache($cTime, $cID, 'supervisor')) {

            $tmp = $cache->GetVars();
            $propValue = $tmp[$cID];
            /* Return cache values */
        } else {

            if ($cache->StartDataCache($cTime, $cID, 'supervisor')) {

                $dbRes = CIBlockPropertyEnum::GetList(
                    array("SORT" => "ASC", "VALUE" => "ASC"),
                    array("IBLOCK_ID" => $IBLOCK_ID, "PROPERTY_ID" => (int)$propertyID)
                );

                while ($resProp = $dbRes->Fetch())
                    $propValue[] = $resProp;

                /* Make cache values */
                $cache->EndDataCache(array($cID => $propValue));
            }
        }
        /* End cache */

        return $propValue;
    }

    /**
     * @return array
     */
    private function _getFilterRowProperty ()
    {
        $db = CIBlockProperty::GetList(array("SORT" => "ASC"), array('IBLOCK_ID' => 6));

        $arRes = array();

        while ($res = $db->Fetch())
        {
            /* Пропускаем когда код свойства не найден в предустановленном массиве */
            if (!in_array($res['CODE'], $this->filterRow['PROPERTY'])) continue;

            /* Получим данные если свойство ссылается на HighLoadBlock */
            if ($res['USER_TYPE'] !== null && is_array($res['USER_TYPE_SETTINGS']) && $res['USER_TYPE_SETTINGS']['TABLE_NAME'])
            {
                /* Вернем массив данных по HighLoadBlock */
                $hbValue = $this->_getPropertyFromHL($res['USER_TYPE_SETTINGS']['TABLE_NAME']);
            }

            /* Получим значения свойства Типа список */
            if ($res['PROPERTY_TYPE'] == "L" && $res['LIST_TYPE'] == "L")
            {
                $propValue = $this->_getPropertyFromList($res['ID']);
            }

            $arRes[ $res['CODE'] ] = array(
                "ID" => $res['ID'],
                "NAME" => $res['NAME'],
                "PROPERTY_TYPE" => (($res["USER_TYPE_SETTINGS"]) ? 'L' : $res["PROPERTY_TYPE"]),
                "LIST_TYPE" => $res["LIST_TYPE"],
                "MULTIPLE" => $res["MULTIPLE"],
                "USER_TYPE" => $res["USER_TYPE"],
                "USER_TYPE_SETTINGS" => $res["USER_TYPE_SETTINGS"],
                "HB_VALUES" => ((isset($hbValue)) ? $hbValue : null),
                "PROP_VALUES" => ((isset($propValue)) ? $propValue : null),
            );
        }

        return $arRes;
    }

    public function onPrepareComponentParams( $arParams )
    {
        $arParams = $this->_getSef( $arParams );
        return parent::onPrepareComponentParams($arParams);
    }

    private function _getSef( $arParams ) {

        $arComponentVariables = array(
            'detail',
        );
        $arDefaultUrlTemplates404 = array(
            'detail' => '#ID#/',
        );
        $arDefaultVariableAliases404 = array();
        $arDefaultVariableAliases = array();
        $SEF_FOLDER = "";
        $arUrlTemplates = array();
        $path = '';
        if($arParams['SEF_MODE'] == 'Y') {
            $arVariables = array();

            $arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams['SEF_URL_TEMPLATES']);
            $arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases404, $arParams['VARIABLE_ALIASES']);
            $componentPage = CComponentEngine::ParseComponentPath($arParams["SEF_FOLDER"], $arUrlTemplates, $arVariables);
            if (mb_strlen($componentPage) <= 0) {
                $componentPage = 'template';
            }
            CComponentEngine::initComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);
            $SEF_FOLDER = $arParams['SEF_FOLDER'];

            $arPath = explode('/', $arUrlTemplates[$componentPage]);
            $path = $SEF_FOLDER;
            foreach($arPath as $tPath) {
                if(stristr($tPath, '#')) {
                    $path .= $arVariables[str_replace('#', '', $tPath)] . '/';
                }
            }
        } else {
            $arVariables = array();
            $arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases, $arParams['VARIABLE_ALIASES']);
            CComponentEngine::InitComponentVariables(false, $arComponentVariables, $arVariableAliases, $arVariables);
            $componentPage = 'template';
        }
        $this->arResult['FOLDER'] = $SEF_FOLDER;
        $this->arResult['URL_TEMPLATES'] = $arUrlTemplates;
        $this->arResult['VARIABLES'] = $arVariables;
        $this->arResult['ALIASES'] = $arVariableAliases;
        $this->arResult['CURRENT_PATH'] = $path;

        $arParams['COMPONENT_TEMPLATE_FILE'] = $componentPage;

        return $arParams;
    }

    private function _getHLDataClass($code)
    {
        $connection = Application::getConnection();
        $sql = "SELECT `ID` FROM b_hlblock_entity WHERE `TABLE_NAME` = '{$code}';";
        $res = $connection->query($sql)->fetch();
        $id = $res['ID'];
        unset($connection,$sql,$res);

        $hlblock = HighloadBlockTable::getById($id)->fetch();
        $dClass = HighloadBlockTable::compileEntity($hlblock);

        return $dClass->getDataClass();
    }

    /** Возврат массив данных заказов для таблицы
     *
     * @param array $filter
     */
    private function _getOrders($filter = array())
    {
        $arOrders = array();
        $order = array("ID" => "DESC");

        /* make sort data */
        if ($this->arGet['sort'] && !empty($this->arGet['sort'])){

            if ($this->arGet['order'] && !empty($this->arGet['order'])) {

                $order = array(
                    mb_strtoupper($this->arGet['sort']) => mb_strtoupper($this->arGet['order'])
                );

            } else {

                $order = array(
                    mb_strtoupper($this->arGet['sort']) => "DESC"
                );
            }
        }

        /* nPage number */
        if ( $this->arGet['PAGEN_1'] && !empty($this->arGet['PAGEN_1']) )
            $nPage = array('iNumPage' => (int)$this->arGet['PAGEN_1']);
        else
            $nPage = array('iNumPage' => 1);

        /* per page */
        if ( $this->arGet['PER_PAGE'] && !empty($this->arGet['PER_PAGE']) ){

            $this->orderPerPage = (int)$this->arGet['PER_PAGE'];
        }

        $arFilter = array(
            "IBLOCK_ID" => 6,
        );
        /* get data with filter param */
        if (is_array($filter) && count($filter) > 0) {
            $arFilter = array_merge($arFilter, $filter);
        }

        $db = CIBlockElement::GetList(
            // Order
            $order,
            // Filter
            $arFilter,
            // Group
            false,
            array_merge(array("nPageSize"=> $this->orderPerPage ), $nPage),
            // Select
            array(
                "ID", "NAME", "CREATED_BY", "CREATED_USER_NAME",
                "PROPERTY_TYPE_ORDER", "PROPERTY_STATUS", "PROPERTY_ADRESS_CLIENT", "PROPERTY_DATE", "PROPERTY_DATE_OF_ISSUE", "PROPERTY_COMMENT"
            )
        );

        $this->arResult['COUNT_ORDERS'] = $db->SelectedRowsCount();
        $this->arResult['PER_PAGE'] = $this->orderPerPage;
        $this->arResult['NAV_PRINT'] = $db->GetPageNavStringEx($navComponentOB, Loc::getMessage('ELAR_SUPERVISION_PAGE_TITLE'), $this->arParams['NAV_TEMPLATE']);

        while ($res = $db->Fetch())
        {
            $arOrders[] = $res;
        }

        $this->arResult['ORDERS'] = $arOrders;
    }

    private function _ifGetFilterParam () {

        /* Не фильтруем параметры если нет указателя на фильтр */
        if (!isset($this->arGet['ORDER_FILTER']) || $this->arGet['ORDER_FILTER'] != 'Y')
            return false;

        /**
         * DT = DataTime
         * D = Date
         * A = Array|List
         * LA = HighLoadBlock Elements Array
         * S = String
         * I = Integer
         * */
        $arMap = array(
            "DATE_OF_ISSUE" => 'DT',
            "DATE" => 'D',
            "STATUS" => 'LA',
            "TYPE_ORDER" => 'A'
        );

        $arResult = array();

        foreach ($this->arGet as $code => $val) {
            /* Пропустим параметр без значения */
            if (empty($val)) continue;

            switch ($arMap[$code]) {
                case 'DT':
                    $arResult['>=PROPERTY_' . $code] = date('Y-m-d H:i:s', strtotime($val));
                    $arResult['<=PROPERTY_' . $code] = date('Y-m-d', strtotime($val)) . " 23:59:59";
                    break;
                case 'D':
                    $arResult['=PROPERTY_' . $code] = date('Y-m-d', strtotime($val));
                    break;
                case 'A':
                    if (is_array($val)) {
                        $logic = array("LOGIC" => "OR");
                        foreach ( $val as $item )
                        {
                            $logic[] = array("=PROPERTY_" . $code => $item);
                        }
                        $arResult[] = $logic;
                    }
                    break;
                case 'LA':
                    if (is_array($val)) {
                        $logic = array("LOGIC" => "OR");
                        foreach ( $val as $item )
                        {
                            $logic[] = array("=PROPERTY_" . $code => $item);
                        }
                        $arResult[] = $logic;
                    }
                    break;
            }
        }

        return $arResult;

    }
}
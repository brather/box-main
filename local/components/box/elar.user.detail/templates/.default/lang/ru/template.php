<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 06.04.2017
 * Time: 18:00
 */
$MESS['E_USER_PROP_FULL_NAME'] = "ФИО";
$MESS['E_USER_BODY_TITLE'] = "Пользователь ";
$MESS['E_USER_PAGE_TITLE'] = "Пользователь - ";
$MESS['E_USER_PROP_ACTIVE'] = "Профиль активен";
$MESS['E_USER_PROP_ID_USER'] = "ID пользовтеля";
$MESS['E_USER_PROP_LOGIN'] = "Логин";
$MESS['E_USER_PROP_LOGIN'] = "Логин";
$MESS['E_USER_PROP_EMAIL'] = "Email";
$MESS['E_USER_PROP_LAST_LOGIN'] = "Последний визит";
$MESS['E_USER_PROP_WORK_COMPANY'] = "Организация";
$MESS['E_USER_PROP_WORK_POSITION'] = "Должность";
$MESS['E_USER_PROP_CREATED_DATE'] = "Дата создания";
$MESS['E_USER_PROP_VALUE_Y'] = "Да";
$MESS['E_USER_PROP_VALUE_N'] = "Нет";
$MESS['E_USER_PROP_PHONE'] = "Телефон";
$MESS['E_USER_PROP_MOBILE'] = "Мобильный телефон";
$MESS['E_CLIENT_DETAIL_ACTUAL_ORDERS'] = "Текущие заказы пользователя";

$MESS['E_ORDER_STATUS_processing'] = "Обработка";
$MESS['E_ORDER_STATUS_execution'] = "Исполнение";
$MESS['E_ORDER_STATUS_prepared'] = "Подготовлен";
$MESS['E_ORDER_STATUS_TITLE'] = "Статус: ";
$MESS['E_ORDER_DATE_OF_ISSUE_TITLE'] = "Дата исполнения: ";

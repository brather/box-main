<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 06.04.2017
 * Time: 12:23
 * @var array $arResult
 */

use Bitrix\Main\Localization\Loc;

global $APPLICATION;


Loc::loadLanguageFile(__FILE__);
$APPLICATION->SetTitle( Loc::getMessage('E_USER_PAGE_TITLE') . $arResult['USER']['FULL_NAME']);
?>
<div class="page__frame">
    <div class="page__title">
        <h1><?= Loc::getMessage('E_USER_BODY_TITLE')?> &laquo;<?= $arResult['USER']['FULL_NAME']?>&raquo;</h1>
    </div>
    <div class="page__body">
        <div class="order__col">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-6">
                        <ul class="order__list">
                            <? /* Full name */?>
                            <li>
                                <div><?= Loc::getMessage( "E_USER_PROP_FULL_NAME"); ?></div>
                                <div><?= $arResult['USER']['FULL_NAME'] ?></div>
                            </li>
                            <? /* Work position */?>
                            <li>
                                <div><?= Loc::getMessage( "E_USER_PROP_WORK_POSITION"); ?></div>
                                <div><?= $arResult['USER']['WORK_POSITION'] ?></div>
                            </li>
                            <? /* user login  */?>
                            <li>
                                <div><?= Loc::getMessage( "E_USER_PROP_LOGIN"); ?></div>
                                <div><?= $arResult['USER']['LOGIN'] ?></div>
                            </li>
                            <? /* user login  */?>
                            <li>
                                <div><?= Loc::getMessage( "E_USER_PROP_LOGIN"); ?></div>
                                <div><?= $arResult['USER']['LOGIN'] ?></div>
                            </li>
                            <? /* Active profile */?>
                            <li>
                                <div><?= Loc::getMessage( "E_USER_PROP_ACTIVE"); ?></div>
                                <div><?= Loc::getMessage( "E_USER_PROP_VALUE_{$arResult['USER']['ACTIVE']}"); ?></div>
                            </li>
                            <? /* user email  */?>
                            <li>
                                <div><?= Loc::getMessage( "E_USER_PROP_EMAIL"); ?></div>
                                <div><?= $arResult['USER']['EMAIL'] ?></div>
                            </li>
                            <? /* Phone */?>
                            <li>
                                <div><?= Loc::getMessage( "E_USER_PROP_PHONE"); ?></div>
                                <div><?= $arResult['USER']['PHONE'] ?></div>
                            </li>
                            <? /* Mobile phone */?>
                            <li>
                                <div><?= Loc::getMessage( "E_USER_PROP_MOBILE"); ?></div>
                                <div><?= $arResult['USER']['MOBILE'] ?></div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-xs-6">
                        <? if (count($arResult['ACTUAL_ORDERS']) > 0): ?>
                            <h2><?= Loc::getMessage('E_CLIENT_DETAIL_ACTUAL_ORDERS')?></h2>
                            <div class="form">
                                <ul class="order__list">
                                    <? foreach($arResult['ACTUAL_ORDERS'] as $order): ?>
                                        <? if ($order['ID'] == $arResult['ORDER']['ID']): ?>
                                            <li class="this-order">
                                            <div style="width: auto;">№ <?= $order['ID']?></div>
                                        <? else: ?>
                                            <li>
                                            <div style="width: auto;"><a href="/elar/orders/<?= $order['ID'] ?>/" target="_blank">№ <?= $order['ID']?></a></div>
                                        <? endif; ?>
                                        <div style="width: auto;">
                                            <ul>
                                                <li><strong><?= $order['NAME']?></strong></li>
                                                <li><?= Loc::getMessage('E_ORDER_STATUS_TITLE')?><?= Loc::getMessage("E_ORDER_STATUS_{$order['PROPERTY_STATUS_VALUE']}");?></li>
                                                <li><?= Loc::getMessage('E_ORDER_DATE_OF_ISSUE_TITLE')?><?= $order['PROPERTY_DATE_OF_ISSUE_VALUE']?></li>
                                            </ul>
                                        </div>
                                        </li>
                                    <? endforeach; ?>
                                </ul>
                            </div>
                        <? endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

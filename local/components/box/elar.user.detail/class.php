<?php
use Bitrix\Main\Application;
use Bitrix\Main\Loader;

/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 06.04.2017
 * Time: 12:21
 *
 *  /elar/client/#CLIENT_ID#/
 *  $_GET['CLIENT_ID']
 */


class ElarUserDetail extends CBitrixComponent {

    private $get;
    private $post;

    public function executeComponent()
    {
        Loader::includeModule("iblock");

        $this->get  = Application::getInstance()->getContext()->getRequest()->getQueryList()->toArray();
        $this->post = Application::getInstance()->getContext()->getRequest()->getPostList()->toArray();

        $this->_getUserData( (int)$this->get['USER_ID'] );
        $this->_getActualOrders( (int)$this->get['USER_ID'] );
        $this->includeComponentTemplate();
    }

    private function _getUserData( $USER_ID )
    {
        /* Когда придет не верное значение или пустое отправим к списку пользователей */
        if(!$USER_ID)
            LocalRedirect('/elar/users/');

        if ($u = CUser::GetByID($USER_ID)) {
            /* Получим и сформируем данные по пользовтелю */
            $db = CIBlockElement::GetList(
                array("SORT" => "ASC"),
                array("IBLOCK_ID" => IBLOCK_CODE_CLIENTS_ID, "PROPERTY_PROP_CLIENT" => $USER_ID)
            );


            $arUser = $db->Fetch();
            $arrUser = $u->Fetch();

            /* Full name */
            $this->arResult['USER']['FULL_NAME']      = $arUser['NAME'];

            /* todo нужен указать на Роль, куда ссылаться ? */

            /* Work position */
            $this->arResult['USER']['WORK_POSITION']    = $arrUser['WORK_POSITION'];

            /* Login */
            $this->arResult['USER']['LOGIN']            = $arrUser['LOGIN'];

            /* Active profile */
            $this->arResult['USER']['ACTIVE']           = $arUser['ACTIVE'];

            /* User email */
            $this->arResult['USER']['EMAIL']            = $arrUser['EMAIL'];

            /* Personal phone */
            $this->arResult['USER']['PHONE']            = $arrUser['PERSONAL_PHONE'];

            /* Personal mobile phone */
            $this->arResult['USER']['MOBILE']            = $arrUser['PERSONAL_MOBILE'];



            // todo в последующей Задаче эти значения отсутсвуют. Вдруг пригодятся
            /* Date of create profile */
            //$this->arResult['USER']['CREATED_DATE']    = $arUser['CREATED_DATE'];

            /* Identification on IBLOCK table  */
            //$this->arResult['USER']['ID_CUSTOM']      = $arUser['ID'];

            /* Identification on Table of User */
            //$this->arResult['USER']['ID_USER']        = $arrUser['ID'];

            /* Last login datetime */
            //$this->arResult['USER']['LAST_LOGIN']     = $arrUser['LAST_LOGIN'];

            /* Work company name */
            //$this->arResult['USER']['WORK_COMPANY']   = $arrUser['WORK_COMPANY'];

        } else {
            /* Если пользователя с таким ID не существует, к списку пользователей */
            LocalRedirect('/elar/users/');
        }
    }

    private  function _getActualOrders($USER_ID)
    {
        $db = CIBlockElement::GetList(
            array("ID" => "DESC"),
            array("IBLOCK_ID" => IBLOCK_CODE_ORDERS_ID, "CREATED_BY" => $USER_ID,
                  "PROPERTY_STATUS" => array(
                      'processing', 'execution', 'prepared')
            ),
            false,
            false,
            array("ID", "NAME", "PROPERTY_DATE_OF_ISSUE", "PROPERTY_STATUS")
        );

        while ($res = $db->Fetch()) {
            $this->arResult['ACTUAL_ORDERS'][$res['ID']] = $res;
        }
    }
}
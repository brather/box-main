$(document).ready(function() {
    var i = {
            form        : '#form',
            fullpage    : '#vertical',
            content     : '#contents',
            scontract   : '#unit-contract',
            sregistry   : '#unit-registry',
            dcontain    : '#data-contain'
        },
        c = {
            title   : '.page__title',
            blkorder: '.order',
            order   : '.order__col',
            scouter : '.scrollbar-outer',
            pole    : '.form__row__col__pole',
            selpole : '.pole__field',
            submit  : '.form__btn__submit',
            disable : 'btn-disabled'
        },
        heightTitle = $(i.fullpage).find(c.title).outerHeight(),
        marginOrder = getNumberFromString($(i.fullpage).find(c.blkorder).css('margin-top')),
        height = getPageSize().pageHeight - heightTitle - marginOrder;

    //region Установка значения max-height для scrollbar
    $(i.content).find(c.scouter).css({ 'height' : height + 'px' });
    $(window).on('resize', function(){
        height = getPageSize().pageHeight - heightTitle - marginOrder;
        $(i.content).find(c.scouter).css('height', height);
    });
    //endregion
    //region При выборе договора разблокируются реестры доступные для конкретного договора
    if($(i.scontract).find('option:selected').length > 0) {
        resetBlockRegistry(i.scontract);
        getPropertyList();
    }

    $(i.scontract).on('change', function (e) {
        resetBlockRegistry(this);
    });
    function resetBlockRegistry(obj) {
        var t = $(obj),
            v = t.val(),
            arRegistry = [];
        if(t.find('option:selected').data('registry')) {
            var s = t.find('option:selected').data('registry');
            if($.isNumeric(s)) {
                arRegistry = [s + ''];
            } else {
                arRegistry = s.split('|');
            }

            if(v != '') {
                $(i.sregistry).attr('disabled', false);
                $(i.sregistry).find('option').each(function(i,y) {
                    var vSub = $(y).val();
                    if($.inArray(vSub, arRegistry) != -1
                        || vSub == 'new'
                        || vSub == '') {
                        $(y).attr('disabled', false);
                    } else {
                        if($(y).is(':selected')) {
                            $(y).attr('selected', false);
                            $(i.sregistry).find('option').first().attr('selected', true);
                            getPropertyList();
                        }
                        $(y).attr('disabled', true);
                    }
                });
            } else {
                disableForm();
            }
        } else {
            disableForm();
        }
        $(i.sregistry).trigger('refresh');
    }
    //region Функция сброса выбранных данных
    function disableForm() {
        $(i.dcontain).html('');
        if(!$(i.form).find(c.submit).hasClass(c.disable)) {
            $(i.form).find(c.submit).toggleClass(c.disable);
        }
        if($(i.sregistry).find('option:selected').val() != '') {
            $(i.sregistry).find('option:selected').attr('selected', false);
            $(i.sregistry).find('option').first().attr('selected', true);
        }
        $(i.sregistry).attr('disabled', true);
    }
    //endregion
    //endregion
    //region Подгрузка полей со свойствами инфоблока и наименованиями колонок файла csv
    $(i.sregistry).on('change', function(e) {
        getPropertyList();
    });
    function getPropertyList() {
        var t = $(i.sregistry),
            v = t.val(),
            dataSend = {
                REG_ID      : t.val(),
                GET_PROP    : 'Y',
                FILE_NAME   : t.data('fname')
            };
        $(i.dcontain).html('');
        startLoader(i.form);
        if(v) {
            $.ajax({
                method  : 'post',
                url     : t.data('ajax'),
                data    : dataSend,
                success : function(data) {
                    $(i.dcontain).html(data);
                    if($(i.dcontain).find('select').length > 0) {
                        $(i.dcontain).find('select').each(function(is, ys) {
                            if($(ys).data('select')) {
                                $(ys).find('option[value=' + $(ys).data('select') + ']').attr('selected', true);
                            }
                            $(ys).styler();
                        });
                        $(i.dcontain).find(c.pole).tooltip({
                            position    : { my  : 'left top' },
                            tooltipClass: 'row__tooltip'
                        });
                        checkSelectProperties();
                        $(i.dcontain).find('select').on('change', function(){
                            checkSelectProperties();
                        });
                    }
                    stopLoader();
                }
            });
        } else {
            stopLoader();
        }
    }
    //endregion
    //region Функция проверки выборки вариантов опций в селекторах свойств инфоблока
    function checkSelectProperties() {
        var bsubmit = $(i.form).find(c.submit),
            flag = true;
        $(i.dcontain).find('select').each(function(is,ys) {
            if($(ys).val() == '') {
                flag = false;
            }
        });
        if((flag && bsubmit.hasClass(c.disable))
            || (!flag && !bsubmit.hasClass(c.disable))) {
            bsubmit.toggleClass(c.disable);
        }
    }
    //endregion
});
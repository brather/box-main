<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

class UserSetSectionComponent extends CBitrixComponent {
    //region Подготовка входных параметров компонента
    /** Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        return parent::onPrepareComponentParams($arParams);
    }
    //endregion
    //region Выполнение компонента
    /**
     * Выполнение компонента
     */
    public function executeComponent() {
        self::getUserGroup();
    }
    //endregion
    //region Метод определяет принадлежность авторизовавшегося пользователя к группе пользователей и осуществляет
    // редирект согласно параметрам компонента
    /**
     * Метод определяет принадлежность авторизовавшегося пользователя к группе пользователей и осуществляет редирект
     * согласно параметрам компонента
     */
    public function getUserGroup() {
        global $USER;
        $uGroup = $USER->GetUserGroupArray();
        $arGroups = self::getArrayGroupsId();
        $urlToRedirect = '';
        ksort($arGroups);
        foreach($uGroup as $idGroup) {
            if(!empty($arGroups[$idGroup])) {
                $urlToRedirect = $this->arParams['GROUPS_URL'][$arGroups[$idGroup]];
                break;
            }
        }
        if($urlToRedirect != '/' && $urlToRedirect != '') {
            LocalRedirect($urlToRedirect);
        }
    }
    //endregion
    //region Метод возвращает массив id активных групп пользователей по фильтру кодов заданных в параметрах компонента
    /** Метод возвращает массив id активных групп пользователей по фильтру кодов заданных в параметрах компонента
     * @return array
     */
    private function getArrayGroupsId() {
        $arCode = array();
        foreach($this->arParams['GROUPS_URL'] as $cGroup => $uGroup) {
            $arCode[] = $cGroup;
        }
        $arFilterGroups = array('ACTIVE' => 'Y');
        $rsGroups = CGroup::GetList($by = 'SORT', $order = 'ASC', $arFilterGroups);
        $arGroups = array();
        while($obGroup = $rsGroups->Fetch()) {
            if(in_array($obGroup['STRING_ID'], $arCode) && $obGroup['STRING_ID'] != U_GROUP_CODE_ALL) {
                $arGroups[$obGroup['ID']] = $obGroup['STRING_ID'];
            }
        }
        return $arGroups;
    }
    //endregion
}
$(document).ready(function(){
   var animationSpeed = 200,
       cls = {
           form     : '.js-edit-list-registry',
           table    : '.table',
           checkbox : '.js-table-checkbox',
           checkall : '.js-table-checkbox-all',
           btn      : '.btn',
           input    : '.js-input-disabled',
           disabled : 'btn-disabled'
       };

   $(cls.checkbox).on('click', function(e){
        var $this = $(this),
            row = $this.closest('tr'),
            table = row.closest(cls.table),
            button = table.closest(cls.form).find(cls.btn),
            countCheck = 0,
            flag = true;
        table.find(cls.checkbox).each(function(i,y){
            if($(y).prop('checked')) {
                countCheck = countCheck + 1;
            }
        });

        if($this.prop('checked')) {
            flag = false;
        }
        row.find(cls.input).prop('disabled', flag);
        row.find('select').prop('disabled', flag).trigger('refresh');

        if(countCheck == 0) {
            button.addClass(cls.disabled);
        } else if(countCheck == 1) {
            button.removeClass(cls.disabled);
        }
   });

   $(cls.checkall).on('click', function(e){
       var $this = $(this),
           table = $this.closest(cls.table),
           button = table.closest(cls.form).find(cls.btn),
           flag = true;
       if($this.prop('checked')) {
           flag = false;
           button.removeClass(cls.disabled);
       } else {
           button.addClass(cls.disabled);
       }
       table.find(cls.input).prop('disabled', flag);
       table.find('select').prop('disabled', flag).trigger('refresh');
   });

    $(cls.btn).on('click', function(e){
        e.preventDefault();
        var $this = $(this),
            form = $this.closest(cls.form);
        if(!$this.hasClass(cls.disabled)) {
            form.submit();
        }
    });

});
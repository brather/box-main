<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application;

$request = Application::getInstance()->getContext()->getRequest();
$arGet = $request->getQueryList();
$arPost = $request->getPostList(); ?>

<div class="content__item">
    <div class="page__frame">
        <div class="page__title">
            <h1><?= Loc::getMessage('USER_REG_T_TITLE') ?></h1>
        </div>
        <div class="order__col">
            <h4 class="backurl">
                <a href="<?= $arParams['SEF_FOLDER'] ?>">
                    <?= Loc::getMessage('USER_REG_T_BACK_TO_LIST_CONTRACTS') ?>
                </a>
            </h4>
            <hr>
            <form action="<?= $arParams['SEF_FOLDER'] ?><?= $arParams['VARIABLES']['CODE'] ?>/"
                  method="post"
                  class="js-edit-list-registry">
                <input type="hidden" name="EDIT_REGISTRY_LIST" value="Y">
                <?foreach($arResult['LIST_SUB_USERS'] as $arSubUser) {?>
                    <input type="hidden"
                           name="SUB_USER_<?= $arSubUser['ID'] ?>"
                           value="<?= implode(",", $arSubUser['PROP_REGISTRY']); ?>">
                    <?$arSubUsersID[] = $arSubUser['ID'];?>
                <?}?>
                <input type="hidden" name="SUB_USERS" value="<?= implode(',', $arSubUsersID) ?>">
                <table class="table js-table">
                    <thead>
                    <tr>
                        <th class="js-checkbox-td">
                            <label class="checkbox">
                                <input type="checkbox" name="L_UNITS_CHECK_ALL" class="js-table-checkbox-all">
                                <span></span>
                            </label>
                        </th>
                        <?foreach($arResult['ROW_SORT'] as $cSort => $arSort) {?>
                            <th>
                                <?if($arSort['USE_URI']) {?>
                                    <a href="<?= $arSort['URL'] ?>" class="filtr">
                                <?}?>
                                <?= Loc::getMessage('F_EDIT_REGISTRY_TH_' . $arSort['CODE']) ?>
                                <?if($arSort['USE_URI']) {?>
                                    </a>
                                <?}?>
                            </th>
                        <?}?>
                    </tr>
                    </thead>
                    <?foreach($arResult['LIST_REGISTRY'] as $iRegistry => $arRegistry) {?>
                        <tr>
                            <td class="js-checkbox-td">
                                <label class="checkbox">
                                    <input type="checkbox"
                                           name="L_CHECK_UNIT[]"
                                           value="<?= $arRegistry['ID'] ?>"
                                           class="js-table-checkbox">
                                    <span></span>
                                </label>
                            </td>
                            <td>
                                <input type="hidden"
                                       class="js-input-disabled"
                                       name="REGISTRY_ID[]"
                                       value="<?= $arRegistry['ID'] ?>"
                                       disabled>
                                <?= $arRegistry['ID'] ?>
                            </td>
                            <td>
                                <input type="text"
                                       name="REGISTRY_NAME_<?= $arRegistry['ID'] ?>"
                                       class="form__field js-input-disabled"
                                       value="<?= $arRegistry['NAME'] ?>"
                                       disabled
                                       placeholder="<?= Loc::getMessage('F_EDIT_REGISTRY_PLACEHOLDER_NAME') ?>">
                            </td>
                            <td>
                                <select name="REGISTRY_SUB_USER_<?= $arRegistry['ID'] ?>[]"
                                        id="reg-sub-user"
                                        multiple size="2"
                                        disabled>
                                    <option value="">-</option>
                                    <?foreach($arResult['LIST_SUB_USERS'] as $subUser) {?>
                                        <?$select = '';
                                        if(in_array($arRegistry['ID'], $subUser['PROP_REGISTRY'])) {
                                            $select = ' selected';
                                        }?>
                                        <option value="<?= $subUser['ID'] ?>"<?= $select ?>>
                                            <?= $subUser['NAME'] ?>
                                        </option>
                                    <?}?>
                                </select>
                            </td>
                            <td>
                                <a href="<?= $arRegistry['ID'] ?>/">
                                    <?= Loc::getMessage('F_EDIT_REGISTRY_URL_TO_DETAIL') ?>
                                </a>
                            </td>
                            <td><?= $arRegistry['COUNT_ELEMENTS'] ?></td>
                        </tr>
                    <?}?>
                </table>
                <hr>
                <button type="submit" class="btn btn-disabled"><?= Loc::getMessage('F_EDIT_REGISTRY_BUTTON_SAVE') ?></button>
            </form>
            <?
            $APPLICATION->IncludeComponent(
                "bitrix:main.pagenavigation",
                "box.pagenav",
                array(
                    "NAV_OBJECT" => $arResult['NAV_PRINT'],
                    "SEF_MODE" => "N",
                    "SHOW_COUNT" => "N",
                    'TITLE_UNIT' => Loc::getMessage('USER_REG_T_TITLE_PAGENAV')
                ),
                false
            );
            ?>
        </div>
    </div>
</div>
<?php
$MESS['USER_REG_T_LIST_REGISTRY'] = 'Список реестров: ';
$MESS['USER_REG_T_BACK_TO_LIST_CONTRACTS'] = 'Вернуться к списку договоров';
$MESS['USER_REG_T_TITLE'] = 'Реестр. Список реестров договора.';
$MESS['USER_REG_T_TITLE_PAGENAV'] = 'Реестры';

$MESS['F_EDIT_REGISTRY_TH_ID'] = 'ID';
$MESS['F_EDIT_REGISTRY_TH_NAME'] = 'Название';
$MESS['F_EDIT_REGISTRY_TH_SUB_USERS'] = 'Менеджер';
$MESS['F_EDIT_REGISTRY_TH_COUNT_ELEMENTS'] = 'Количество ЕУ';
$MESS['F_EDIT_REGISTRY_TH_DETAIL_VIEW'] = 'Детальный просмотр';

$MESS['F_EDIT_REGISTRY_PLACEHOLDER_NAME'] = 'текст...';
$MESS['F_EDIT_REGISTRY_BUTTON_SAVE'] = 'Изменить';
$MESS['F_EDIT_REGISTRY_URL_TO_DETAIL'] = 'Перейти';
<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Loader,
    \Bitrix\Main\UI\PageNavigation,
    \Bitrix\Iblock\IblockTable,
    \Bitrix\Iblock\ElementTable,
    \Bitrix\Main\Application;

class UserRegistryListComponent extends CBitrixComponent {
    //region Задание свойств класса
    private $page = 'template';
    private $arGet = array();
    private $arPost = array();
    private $arUserGroups = array();
    //endregion
    //region Подключение файлов перевода
    /**
     * Подключение файлов перевода
     */
    public function onIncludeComponentLang() {
        Loc::loadMessages(__FILE__);
    }
    //endregion
    //region Подготовка входных параметров компонента
    /** Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        return parent::onPrepareComponentParams($arParams);
    }
    //endregion
    //region Выполнение компонента
    /**
     * Выполнение компонента
     */
    public function executeComponent() {
        $this->getRequest();                                    // Получение данных request
        $this->submitForm();
        $this->includeModule();                                 // Подключение модулей
        if($this->arParams['MENU_EXT'] != 'Y') {
            $this->getSubUsers();                                   // Возвращает список пользователей (менеджеров) у суперпользователя
        }
        $this->getListRegistry();                               // Возвращает список реестров
        if($this->arParams['MENU_EXT'] != 'Y') {
            $this->setSortList();                                   // Сортировка результирующего массива
            $this->includeComponentTemplate($this->page);           // Подключение шаблона
        } else {
            $aMenuLinksNew = $this->arResult['ELEMENT_LINKS'];
            return $aMenuLinksNew;
        }
    }
    //endregion
    //region Получение данных request
    /**
     * Получение данных request
     */
    private function getRequest() {
        $request = Application::getInstance()->getContext()->getRequest();
        $this->arGet = $request->getQueryList()->toArray();
        $this->arPost = $request->getPostList()->toArray();
    }
    //endregion
    //region Подключаем список модулей
    /**
     * Подключаем список модулей
     */
    private function includeModule() {
        Loader::includeModule('iblock');
    }
    //endregion
    //region Устанавливает значения для измененного реестра
    /**
     * Устанавливает значения для измененного реестра
     */
    private function submitForm() {
        if($this->arPost['EDIT_REGISTRY_LIST'] == 'Y') {
            $arPost = $this->arPost;
            $arUsers = explode(',', $arPost['SUB_USERS']);
            $iBlockIDClients = $this->getIBlockID($this->arParams['IBLOCK_CODE_CLIENTS'], $this->arParams['IBLOCK_TYPE_LISTS']);
            foreach($arPost['REGISTRY_ID'] as $regID) {
                if(!empty($arPost['REGISTRY_NAME_' . $regID])){
                    $ib = new CIBlock;
                    $ib->Update($regID, array('NAME' => $arPost['REGISTRY_NAME_' . $regID]));
                }
                foreach($arUsers as $uID) {
                    $arUserRegID = explode(',', $arPost['SUB_USER_' . $uID]);
                    //region Если у пользователя присутсвовал в свойстве ID реестров $regID
                    if(in_array($regID, $arUserRegID)) {
                        //region Если в id пользователя был удален из списка менеджеров
                        if(!in_array($uID, $arPost['REGISTRY_SUB_USER_' . $regID])) {
                            // удаляем у пользователя из свойства id инфоблока реестра
                            unset($arUserRegID[array_search($regID, $arUserRegID)]);
                            CIBlockElement::SetPropertyValuesEx(
                                $uID,
                                $iBlockIDClients,
                                array('PROP_REGISTRY' => $arUserRegID)
                            );
                        }
                        //endregion
                    }
                    //endregion
                    //region
                    else {
                        //region Если id пользователя был добален в список менеджеров
                        if(in_array($uID, $arPost['REGISTRY_SUB_USER_' . $regID])){
                            // добавляем пользователю id инфоблока реестра
                            $arUserRegID[] = $regID;
                            CIBlockElement::SetPropertyValuesEx(
                                $uID,
                                $iBlockIDClients,
                                array('PROP_REGISTRY' => $arUserRegID)
                            );
                        }
                        //endregion
                    }
                    //endregion
                }
            }
        }
    }
    //endregion
    //region Возвращает ID инфоблока по его коду
    /** Возвращает ID инфоблока по его коду
     * @param $code - код инфоблока
     * @param $type - тип инфоблока
     * @return mixed
     */
    private function getIBlockID($code, $type) {
        $arIBlock = CIBlock::GetList(array(), array('CODE' => $code, 'TYPE' => $type))->Fetch();
        return $arIBlock['ID'];
    }
    //endregion
    //region Возвращает список пользователей (менеджеров) у суперпользователя
    /** Возвращает список пользователей (менеджеров) у суперпользователя
     * @param $sUserID
     */
    private function getSubUsers() {
        if(!empty($this->arParams['USER_ID'])) {
            $sUserID = $this->arParams['USER_ID'];
        } else {
            global $USER;
            $sUserID = $USER->GetID();
        }
        $arOrderUsers = array('NAME' => 'ASC');
        $arFilterUsers = array('IBLOCK_CODE' => $this->arParams['IBLOCK_CODE_CLIENTS'], 'PROPERTY_PROP_CLIENTS_OF_SUPERUSER' => $sUserID);
        $arSelectUsers = array('NAME', 'ID', 'PROPERTY_PROP_CLIENT', 'PROPERTY_PROP_REGISTRY');
        $rsUsers = CIBlockElement::GetList($arOrderUsers, $arFilterUsers, false, false, $arSelectUsers);
        while($obUsers = $rsUsers-> Fetch()) {
            $this->arResult['LIST_SUB_USERS'][] = array(
                'ID' => $obUsers['ID'],
                'NAME' => $obUsers['NAME'],
                'PROP_CLIENT' => $obUsers['PROPERTY_PROP_CLIENT_VALUE'],
                'PROP_REGISTRY' => $obUsers['PROPERTY_PROP_REGISTRY_VALUE'],
            );
        }
    }
    //endregion
    //region Возвращает список реестров
    /**
     * Возвращает список реестров
     */
    private function getListRegistry() {
        if(!empty($this->arParams['REGISTRY_ID'])) {
            $arListRegistry = $this->arParams['REGISTRY_ID'];
        } else {
            $arListRegistry = $this->getRegistryList();
        }
        if(empty($this->arGet['SORT']) || $this->arParams['MENU_EXT'] == 'Y') {
            $arOrderRegistry = array('NAME' => 'ASC');
        } else {
            $arOrderRegistry = array($this->arGet['SORT'] => $this->arGet['ORDER']);
        }

        $arIBlockTable = array(
            'filter' => array('ID' => $arListRegistry),
            'select' => array('*'),
            'order' => $arOrderRegistry,
            "count_total" => true,
        );

        if($this->arParams['NAV_PRINT'] == 'Y') {
            $nav = new PageNavigation('list-registry');
            $nav->allowAllRecords(true)->setPageSize($this->arParams['COUNT_ON_PAGE_REGISTRY'])->initFromUri();
            $arIBlockTable['offset'] = $nav->getOffset();
            $arIBlockTable['limit'] = $nav->getLimit();
        }

        $rsRegistry = IblockTable::getList($arIBlockTable);
        if($this->arParams['NAV_PRINT'] == 'Y'){
            $nav->setRecordCount($rsRegistry->getCount())->getPageCount();
            $this->arResult['NAV_PRINT'] = $nav;
        }
        if($this->arParams['MENU_EXT'] == 'Y') {
            $this->arResult["SECTIONS"] = array();
            $this->arResult["ELEMENT_LINKS"] = array();
        }

        while($obRegistry = $rsRegistry->fetch()) {
            $countUnits = ElementTable::getCount(array('IBLOCK_ID' => $obRegistry['ID']));
            if($this->arParams['MENU_EXT'] == 'Y') {
                $this->arResult['ELEMENT_LINKS'][] = array(
                    $obRegistry['NAME'] . Loc::getMessage('USER_REG_C_LIST_BRACKET_S') . $countUnits . Loc::getMessage('USER_REG_C_LIST_BRACKET_E'),
                    $obRegistry['ID'] . '/',
                    array(),
                    array(),
                    ''
                );
            } else {
                $this->arResult['LIST_REGISTRY'][] = array(
                    'ID' => $obRegistry['ID'],
                    'ACTIVE' => $obRegistry['ACTIVE'],
                    'CODE' => $obRegistry['CODE'],
                    'NAME' => $obRegistry['NAME'],
                    'PICTURE' => $obRegistry['PICTURE'],
                    'COUNT_ELEMENTS' => $countUnits
                );
            }
        }
    }
    //endregion
    //region Возвращает список ID инфоблоков реестров доступных пользователю
    /** Возвращает список ID инфоблоков реестров доступных пользователю
     * @return array
     */
    private function getRegistryList() {
        if(!empty($this->arParams['USER_ID'])) {
            $userID = $this->arParams['USER_ID'];
        } else {
            global $USER;
            $userID = $USER->GetID();
            $this->arParams['USER_ID'] = $userID;
        }
        $arRegistryList = array();
        // Возвращаем список реестров из инфоблока "Клиенты" (даже если пользователь == суперпользователь,
        // то если у него указаны id инфоблоков реестров то список ему доступных им и ограничивается)
        $arFilterUsers = array(
            'IBLOCK_CODE' => $this->arParams['IBLOCK_CODE_CLIENTS'],
            'PROPERTY_PROP_CLIENT' => $userID);
        $arSelectUsers = array('ID', 'IBLOCK_ID', 'PROPERTY_PROP_REGISTRY');
        $rsUsers = CIBlockElement::GetList(array(), $arFilterUsers, false, false, $arSelectUsers);
        while($obUsers = $rsUsers->Fetch()) {
            foreach($obUsers['PROPERTY_PROP_REGISTRY_VALUE'] as $idRegistry) {
                if(!in_array($idRegistry, $arRegistryList)) {
                    $arRegistryList[] = $idRegistry;
                }
            }
        }
        // Если список реестров в инфоблоке клиентов пуст, проверяем, является ли пользователь суперпользователем
        if(empty($arRegistryList)) {
            $this->getUserGroup();
            // Для суперпользователя
            if(in_array(U_GROUP_CODE_CLIENT_S_USER, $this->arUserGroups)) {
                $arOrderContracts = array('ID' => 'ASC');
                $arFilterContracrs = array(
                    'IBLOCK_CODE' => $this->arParams['IBLOCK_CODE_CONTRACTS'],
                    'PROPERTY_PROP_SUPER_USER' => $userID);
                $arSelectContracts = array('ID', 'NAME', 'IBLOCK_ID', 'PROPERTY_PROP_ID_REGISTRY');
                $rsContracts = CIBlockElement::GetList(
                    $arOrderContracts,
                    $arFilterContracrs,
                    false,
                    false,
                    $arSelectContracts);
                while($obContracts = $rsContracts->Fetch()) {
                    foreach($obContracts['PROPERTY_PROP_ID_REGISTRY_VALUE'] as $idRegistry) {
                        if(!in_array($idRegistry, $arRegistryList)) {
                            $arRegistryList[] = $idRegistry;
                        }
                    }
                }
            }
        }
        return $arRegistryList;
    }
    //endregion
    //region Возвращает список групп пользователя
    /**
     * Возвращает список групп пользователя
     */
    private function getUserGroup() {
        global $USER;
        $userID = $this->arParams['USER_ID'];
        if(empty($userID)) {
            $userID = $USER->GetID();
            $this->arParams['USER_ID'] = $userID;
        }
        $arGroupID = array();
        $rsGroups = $USER->GetUserGroupList($userID);
        while($obUG = $rsGroups->Fetch()) {
            $arGroupID[] = $obUG['GROUP_ID'];
        }
        $arFilterGroup = array('ID' => implode('|', $arGroupID), 'ACTIVE' => 'Y');
        $rsGroup = CGroup::GetList($by = 'ID', $order = 'ASC', $arFilterGroup);
        while($obGroup = $rsGroup->Fetch()) {
            $this->arUserGroups[] = $obGroup['STRING_ID'];
        }
    }
    //endregion
    //region Сортировка результирующего массива
    /**
     * Сортировка результирующего массива
     */
    private function setSortList() {
        if($this->arParams['MENU_EXT'] != 'Y') {
            $arSelectRegItem = array(
                'ID'                => true,
                'NAME'              => true,
                'SUB_USERS'         => false,
                'DETAIL_VIEW'       => false,
                'COUNT_ELEMENTS'    => false
            );
            $this->setSort($arSelectRegItem);
            if($this->arGet['SORT'] == 'COUNT_ELEMENTS') {
                $arListReg = $this->arResult['LIST_REGISTRY'];
                $arSort = array();
                foreach($arListReg as $arReg) {
                    $arSort[] = $arReg['COUNT_ELEMENTS'];
                }
                if($this->arGet['ORDER'] == 'ASC') {
                    $typeSort = SORT_ASC;
                } else {
                    $typeSort = SORT_DESC;
                }
                array_multisort($arSort, $typeSort, $arListReg);
                $this->arResult['LIST_REGISTRY'] = $arListReg;
            }
        }
    }
    //endregion
    //region Возвращает массив с полями для сортировки
    /** Возвращает массив с полями для сортировки
     * @param $arSort
     */
    private function setSort($arSort) {
        $arSetSort = $arSort;
        $arRowSort = array();
        $preffixGet = '';
        if(!empty($this->arGet)) {
            $iGet = 0;
            foreach($this->arGet as $cGet => $vGet) {
                if($cGet != 'SORT' && $cGet != 'ORDER') {
                    if($iGet == 0) {
                        $preffixGet .= '?';
                    } else {
                        $preffixGet .= '&';
                    }
                    $preffixGet .= $cGet . '=' . $vGet;
                    $iGet++;
                }
            }
        }
        foreach($arSetSort as $cSort => $vSort) {
            $order = 'ASC';
            if(!empty($this->arGet['SORT']) && $cSort == $this->arGet['SORT'] && $this->arGet['ORDER'] == $order) {
                $order = 'DESC';
            }
            if($preffixGet == '') {
                $preffixGet = '?';
            }
            $arRowSort[$cSort] = array(
                'USE_URI' => $vSort,
                'CODE' => $cSort,
                'URL' => $preffixGet . 'SORT=' . $cSort . '&ORDER=' . $order
            );
        }
        if($this->arParams['MENU_EXT'] != 'Y') {
            $this->arResult['ROW_SORT'] = $arRowSort;
        }
    }
    //endregion
}
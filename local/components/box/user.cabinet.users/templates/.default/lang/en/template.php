<?php
$MESS['T_USERS_TITLE_ADD_USER'] = 'Add user';
$MESS['T_USERS_FORM_NAME'] = 'Name: ';
$MESS['T_USERS_FORM_LAST_NAME'] = 'Last name: ';
$MESS['T_USERS_FORM_SECOND_NAME'] = 'Second name: ';
$MESS['T_USERS_FORM_EMAIL'] = 'E-mail: ';
$MESS['T_USERS_FORM_PERSONAL_PHONE'] = 'Phone: ';
$MESS['T_USERS_FORM_WORK_POSITION'] = 'Work position: ';
$MESS['T_USERS_FORM_WORK_COMPANY'] = 'Company: ';
$MESS['T_USERS_FORM_REGISTRY'] = 'Registry: ';
$MESS['T_USERS_FORM_PASSWORD'] = 'Password: ';
$MESS['T_USERS_FORM_LOGIN'] = 'Login: ';
$MESS['T_USERS_FORM_PLACEHOLDER_EMAIL'] = 'e-mail...';
$MESS['T_USERS_FORM_PLACEHOLDER_PERSONAL_PHONE'] = 'phone...';
$MESS['T_USERS_FORM_PLACEHOLDER_WORK_POSITION'] = 'position...';
$MESS['T_USERS_FORM_PLACEHOLDER_WORK_COMPANY'] = 'company... ';
$MESS['T_USERS_FORM_PLACEHOLDER_PASSWORD'] = 'password...';
$MESS['T_USERS_FORM_PLACEHOLDER_NAME'] = 'name...';
$MESS['T_USERS_FORM_PLACEHOLDER_LAST_NAME'] = 'last name...';
$MESS['T_USERS_FORM_PLACEHOLDER_SECOND_NAME'] = 'second name...';
$MESS['T_USERS_FORM_NO'] = 'Not';
$MESS['T_USERS_FORM_ADD_USER'] = 'Add user';
$MESS['T_USERS_FORM_EMPTY_CONTRACTS'] = 'You have not yet signed a single contract.';
$MESS['T_USERS_FORM_SELECT_CONTRACT'] = 'Select contract: ';
$MESS['T_USERS_LIST_SUB_USERS'] = 'List of users';
$MESS['T_USERS_TABLE_ID'] = 'ID';
$MESS['T_USERS_TABLE_FIO'] = 'Full name';
$MESS['T_USERS_TABLE_COMPANY'] = 'Company';
$MESS['T_USERS_TABLE_LOGIN'] = 'Login';
$MESS['T_USERS_TABLE_ACTIVE'] = 'Active';
$MESS['T_USERS_TABLE_EMAIL'] = 'E-mail';
$MESS['T_USERS_TABLE_PERSONAL_PHONE'] = 'Phone';
$MESS['T_USERS_TABLE_REGISTRY'] = 'Registry';
$MESS['T_USERS_TABLE_LAST_AUTHENTIFICATE'] = 'Last authentificate';
$MESS['T_USERS_TABLE_DELETE_USER'] = 'Delete selected';
$MESS['T_USERS_TABLE_DELETE_USER_CONFIRM_TITLE'] = 'Delete?';
$MESS['T_USERS_TABLE_DELETE_USER_CONFIRM_QESTION'] = 'Are you sure you want to delete selected users?';
$MESS['T_USERS_TABLE_DELETE_USER_CONFIRM_ANSWER_Y'] = 'Delete';
$MESS['T_USERS_TABLE_DELETE_USER_CONFIRM_ANSWER_n'] = 'Cancel';
$MESS['T_USERS_TABLE_BLOCK_USER'] = 'Block selected';
$MESS['T_USERS_TABLE_BLOCK_USER_CONFIRM_TITLE'] = 'Block?';
$MESS['T_USERS_TABLE_BLOCK_USER_CONFIRM_QESTION'] = 'Are you sure you want to block selected users?';
$MESS['T_USERS_TABLE_BLOCK_USER_CONFIRM_ANSWER_Y'] = 'Block';
$MESS['T_USERS_TABLE_BLOCK_USER_CONFIRM_ANSWER_N'] = 'Cancel';
$MESS['T_USERS_TABLE_UNBLOCK_USER'] = 'Unlock selected';
$MESS['T_USERS_TABLE_UNBLOCK_USER_CONFIRM_TITLE'] = 'Unlock?';
$MESS['T_USERS_TABLE_UNBLOCK_USER_CONFIRM_QESTION'] = 'Are you sure you want to unlock selected users?';
$MESS['T_USERS_TABLE_UNBLOCK_USER_CONFIRM_ANSWER_Y'] = 'Unlock';
$MESS['T_USERS_TABLE_UNBLOCK_USER_CONFIRM_ANSWER_N'] = 'Cancel';
$MESS['T_USERS_TABLE_NO_SELECT_USER'] = 'You have not selected any user';
$MESS['T_USERS_TABLE_ACTIVE_Y'] = 'Active';
$MESS['T_USERS_TABLE_ACTIVE_N'] = 'Blocked';
$MESS['T_USERS_TABLE_SUBUSERS_EMPTY'] = 'List of Establishments user is empty. Add users to work through the form above';
$MESS['T_USERS_SUCCESS_APPLY'] = 'Apply';
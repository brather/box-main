<?php
$MESS['T_USERS_TITLE_ADD_USER'] = 'Добавление пользователя';
$MESS['T_USERS_FORM_NAME'] = 'Имя: ';
$MESS['T_USERS_FORM_LAST_NAME'] = 'Фамилия: ';
$MESS['T_USERS_FORM_SECOND_NAME'] = 'Отчество: ';
$MESS['T_USERS_FORM_EMAIL'] = 'E-mail: ';
$MESS['T_USERS_FORM_PERSONAL_PHONE'] = 'Телефон: ';
$MESS['T_USERS_FORM_WORK_POSITION'] = 'Должность: ';
$MESS['T_USERS_FORM_WORK_COMPANY'] = 'Организация: ';
$MESS['T_USERS_FORM_REGISTRY'] = 'Реестр: ';
$MESS['T_USERS_FORM_PASSWORD'] = 'Пароль: ';
$MESS['T_USERS_FORM_LOGIN'] = 'Логин: ';
$MESS['T_USERS_FORM_PLACEHOLDER_EMAIL'] = 'e-mail...';
$MESS['T_USERS_FORM_PLACEHOLDER_PERSONAL_PHONE'] = 'телефон...';
$MESS['T_USERS_FORM_PLACEHOLDER_WORK_POSITION'] = 'должность...';
$MESS['T_USERS_FORM_PLACEHOLDER_WORK_COMPANY'] = 'организация...';
$MESS['T_USERS_FORM_PLACEHOLDER_PASSWORD'] = 'пароль...';
$MESS['T_USERS_FORM_PLACEHOLDER_NAME'] = 'имя...';
$MESS['T_USERS_FORM_PLACEHOLDER_LAST_NAME'] = 'фамилия...';
$MESS['T_USERS_FORM_PLACEHOLDER_SECOND_NAME'] = 'отчество...';
$MESS['T_USERS_FORM_NO'] = 'Нет';
$MESS['T_USERS_FORM_ADD_USER'] = 'Добавить пользователя';
$MESS['T_USERS_LIST_SUB_USERS'] = 'Список пользователей';
$MESS['T_USERS_FORM_EMPTY_CONTRACTS'] = 'У вас не заключено пока ни одного договора.';
$MESS['T_USERS_FORM_SELECT_CONTRACT'] = 'Выберите договор: ';
$MESS['T_USERS_TABLE_ID'] = 'ID';
$MESS['T_USERS_TABLE_FIO'] = 'ФИО';
$MESS['T_USERS_TABLE_COMPANY'] = 'Организация';
$MESS['T_USERS_TABLE_LOGIN'] = 'Логин';
$MESS['T_USERS_TABLE_ACTIVE'] = 'Активность';
$MESS['T_USERS_TABLE_EMAIL'] = 'E-mail';
$MESS['T_USERS_TABLE_PERSONAL_PHONE'] = 'Телефон';
$MESS['T_USERS_TABLE_REGISTRY'] = 'Реестры';
$MESS['T_USERS_TABLE_LAST_AUTHENTIFICATE'] = 'Последняя авторизация';
$MESS['T_USERS_TABLE_DELETE_USER'] = 'Удалить выбранных';
$MESS['T_USERS_TABLE_DELETE_USER_CONFIRM_TITLE'] = 'Удалить?';
$MESS['T_USERS_TABLE_DELETE_USER_CONFIRM_QESTION'] = 'Вы действительно хотите удалить выбранных пользователей?';
$MESS['T_USERS_TABLE_DELETE_USER_CONFIRM_ANSWER_Y'] = 'Удалить';
$MESS['T_USERS_TABLE_DELETE_USER_CONFIRM_ANSWER_N'] = 'Отмена';
$MESS['T_USERS_TABLE_BLOCK_USER'] = 'Блокировать выбранных';
$MESS['T_USERS_TABLE_BLOCK_USER_CONFIRM_TITLE'] = 'Заблокировать?';
$MESS['T_USERS_TABLE_BLOCK_USER_CONFIRM_QESTION'] = 'Вы действительно хотите заблокировать выбранных пользователей?';
$MESS['T_USERS_TABLE_BLOCK_USER_CONFIRM_ANSWER_Y'] = 'Заблокировать';
$MESS['T_USERS_TABLE_BLOCK_USER_CONFIRM_ANSWER_N'] = 'Отмена';
$MESS['T_USERS_TABLE_UNBLOCK_USER'] = 'Разблокировать выбранных';
$MESS['T_USERS_TABLE_UNBLOCK_USER_CONFIRM_TITLE'] = 'Разблокировать?';
$MESS['T_USERS_TABLE_UNBLOCK_USER_CONFIRM_QESTION'] = 'Вы действительно хотите разблокировать выбранных пользователей?';
$MESS['T_USERS_TABLE_UNBLOCK_USER_CONFIRM_ANSWER_Y'] = 'Разблокировать';
$MESS['T_USERS_TABLE_UNBLOCK_USER_CONFIRM_ANSWER_N'] = 'Отмена';
$MESS['T_USERS_TABLE_NO_SELECT_USER'] = 'Вы не выбрали ни одного пользователя';
$MESS['T_USERS_TABLE_ACTIVE_Y'] = 'Активен';
$MESS['T_USERS_TABLE_ACTIVE_N'] = 'Заблокированный';
$MESS['T_USERS_TABLE_SUBUSERS_EMPTY'] = 'Список заведенных пользователей пуст. Добавьте для работы пользователей через форму выше';
$MESS['T_USERS_SUCCESS_APPLY'] = 'Принять';
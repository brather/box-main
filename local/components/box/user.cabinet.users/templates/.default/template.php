<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application;
$request = Application::getInstance()->getContext()->getRequest();
$arRequest = $request->toArray();
$arPost = $request->getPostList()->toArray();
$arGet = $request->getQueryList()->toArray();
$sectionUrl = $arParams['FOLDER'] . $arParams['URL_SUB_SECTION'];?>

<div class="order__col__frame">
    <h2><?= Loc::getMessage('T_USERS_TITLE_ADD_USER') ?></h2>
    <form action="<?= $arResult['GET_STRING_FULL'] ?>"
          method="POST"
          class="form">
        <input type="hidden" name="F_ADD_USER" value="Y">
        <?if(!empty($arResult['F_ADD_USER_SUCCESS'])){?>
            <div class="form__row hidden"
                 id="js-success"
                 data-mess="<?= $arResult['F_ADD_USER_SUCCESS'] ?>"
                 data-apply="<?= Loc::getMessage('T_USERS_SUCCESS_APPLY') ?>"></div>
        <?}?>
        <div class="form__row">
            <?$countInLine = 4;
            foreach($arParams['FIELD_SUB_USER'] as $iCode => $code) {?>
                <?$val = '';
                if(!empty($arPost['F_ADD_USER_' . $code]) && empty($arResult['F_ADD_USER_SUCCESS'])) {
                    $val = $arPost['F_ADD_USER_' . $code];
                } elseif($code == 'PASSWORD' && !empty($arResult['F_ADD_USER_SUCCESS']) || $code == 'PASSWORD') {
                    $val = $arResult['F_USER_ADD_PASS'];
                } elseif($code == 'WORK_COMPANY' && !empty($arResult['F_ADD_USER_SUCCESS']) || $code == 'WORK_COMPANY') {
                    $val = $arParams['USER_DATA']['WORK_COMPANY'];
                }
                $errorCls = '';
                $placeHolder = Loc::getMessage('T_USERS_FORM_PLACEHOLDER_' . $code);
                $erMess = '';
                if(!empty($arResult['F_USER_ADD_ERROR'][$code])) {
                    $errorCls = ' placeholder-danger';
                    $erMess = $arResult['F_USER_ADD_ERROR'][$code]['MESS'];
                }?>
                <?if(($iCode + 1) % $countInLine == 1) {?>
                    </div>
                    <div class="form__row">
                <?}?>
                        <div class="form__row__col   _size6">
                            <div class="form__row__name">
                                <?= Loc::getMessage('T_USERS_FORM_' . $code) ?>
                                <?if(!empty($erMess)){?>
                                    <span class="text-danger"><?= $erMess ?></span>
                                <?}?>
                            </div>
                            <?if(stristr($code, 'PHONE') || stristr($code, 'MOBILE')) {?>
                                <input type="text"
                                       class="form__field js-maskedinput<?= $errorCls ?>"
                                       name="F_ADD_USER_<?= $code ?>"
                                       value="<?= $val ?>"
                                       data-maskedinput="+9 999 999 9999">
                            <?} else {?>
                                <input type="text"
                                       value="<?= $val ?>"
                                       class="form__field<?= $errorCls ?>"
                                       name="F_ADD_USER_<?= $code ?>"
                                       placeholder="<?//= $placeHolder ?>">
                            <?}?>
                        </div>
                <?if(count($arParams['FIELD_SUB_USER']) == ($iCode + 1) || ($iCode + 1) % $countInLine == 0){?>
                    </div>
                    <div class="form__row">
                <?}?>
            <?}?>

            <?if(count($arResult['COMPANY_LIST']) > 0) {
                $size = '';
                $multiple = '';
                if(count($arResult['COMPANY_LIST']) > 1) {
                    $size = ' size="3"';
                    $multiple = ' multiple';
                }?>
                <div class="form__row__col  _size6">
                    <div class="form__row__name"><?= Loc::getMessage('T_USERS_FORM_WORK_COMPANY') ?></div>
                    <?if(count($arResult['COMPANY_LIST']) > 1) {?>
                        <select name="F_ADD_USER_WORK_COMPANY[]"
                                id="prop-company"
                                data-sub-select="#contract-list"
                                class="js-select-sub"<?= $size ?><?= $multiple ?>>
                            <?foreach($arResult['COMPANY_LIST'] as $idCompany => $nCompany) {?>
                                <option value="<?= $idCompany ?>"><?= $nCompany ?></option>
                                <!--<option value="--><?//= $idCompany ?><!--">--><?//= $nCompany ?><!--</option>-->
                            <?}?>
                        </select>
                    <?} else {
                        $keysCompany = array_keys($arResult['COMPANY_LIST']);?>
                        <!--<input type="hidden" name="WORK_COMPANY" value="<?= $keysCompany[0] ?>">-->
                        <input type="hidden" name="F_ADD_USER_WORK_COMPANY" value="<?= $arResult['COMPANY_LIST'][$keysCompany[0]] ?>">
                        <p><b><?= $arResult['COMPANY_LIST'][$keysCompany[0]] ?></b></p>
                    <?}?>
                </div>
            <?}?>
            <?if(count($arResult['CONTRACT_LIST']) > 0) {?>
                <div class="form__row__col  _size6">
                    <div class="form__row__name"><?= Loc::getMessage('T_USERS_FORM_SELECT_CONTRACT') ?></div>
                    <?if(count($arResult['CONTRACT_LIST']) > 1){?>
                        <select name="F_ADD_USER_CONTRACT_ID[]"
                                class="js-select-sub"
                                id="contract-list"
                                multiple
                                size="3"
                                data-sub-select="#user-add-reg">
                            <?$disabled = '';
                            if(count($arResult['COMPANY_LIST']) > 1) {
                                $disabled = ' disabled';
                            }
                            foreach($arResult['CONTRACT_LIST'] as $idContract => $arContract) {?>
                                <?$selected = '';
                                if(in_array($idContract, $arPost['F_ADD_USER_CONTRACT_ID']) && empty($arResult['F_ADD_USER_SUCCESS'])) {
                                    $selected = ' selected';
                                }?>
                                <option data-sub="<?= $arContract['PROPERTY_PROP_COMPANY_VALUE'] ?>" value="<?= $idContract ?>"<?= $selected ?><?= $disabled ?>><?= $arContract['NAME'] ?></option>
                            <?}?>
                        </select>
                    <?} else {
                        $keysContract = array_keys($arResult['CONTRACT_LIST']);?>
                        <input type="hidden" name="F_ADD_USER_CONTRACT_ID[]" value="<?= $keysContract[0] ?>">
                        <p><b><?= $arResult['CONTRACT_LIST'][$keysContract[0]]['NAME'] ?></b></p>
                    <?}?>
                </div>
                <div class="form__row__col  _size6">
                    <div class="form__row__name"><?= Loc::getMessage('T_USERS_FORM_REGISTRY') ?></div>
                    <select name="F_ADD_USER_REGISTRY[]" id="user-add-reg" multiple size="3">
                        <?foreach($arResult['CONTRACT_LIST'] as $idContract => $arContract) {?>
                            <?if(count($arContract['PROPERTY_PROP_ID_REGISTRY_VALUE'])) {?>
                                <?foreach ($arContract['PROPERTY_PROP_ID_REGISTRY_VALUE'] as $idRegistry) {?>
                                    <?$disabled = ' disabled';
                                    if(in_array($idContract, $arPost['F_ADD_USER_CONTRACT_ID']) || count($arResult['CONTRACT_LIST']) == 1) {
                                        $disabled = '';
                                    }
                                    $arCode = $arResult['REG_CODE_LIST'][$idRegistry];
                                    $value = 'TITLE-' . urlencode($arCode['NAME']) . '-ID-' . $arCode['ID'];
                                    $selected = '';
                                    if(in_array($value, $arPost['F_ADD_USER_REGISTRY']) && empty($arResult['F_ADD_USER_SUCCESS'])) {
                                        $selected = ' selected';
                                    }?>
                                    <option data-sub="<?= $idContract ?>" value="<?= $value ?>"<?= $selected ?><?= $disabled ?>>
                                        <?= $arCode['NAME'] ?>
                                    </option>
                                <?}?>
                            <?}?>
                        <?}?>
                    </select>
                </div>
            <?} else {?>
                <p><?= Loc::getMessage('T_USERS_FORM_EMPTY_CONTRACTS') ?></p>
            <?}?>
        </div>
        <div class="form__button">
            <button class="btn" type="submit"><?= Loc::getMessage('T_USERS_FORM_ADD_USER') ?></button>
        </div>
    </form>
</div>
<div class="order__col__frame">
    <p><br></p>
    <h2><?= Loc::getMessage('T_USERS_LIST_SUB_USERS') ?></h2>
    <?if(!empty($arResult['LIST_SUB_USERS'])) {?>
        <form action="<?= $arResult['GET_STRING_FULL'] ?>"
              id="js-user-list"
              method="POST">
            <input type="hidden" name="ACTION_USER_LIST" value="Y">
            <input type="hidden" name="DELETE_USER" value="N">
            <input type="hidden" name="BLOCK_USER" value="N">
            <input type="hidden" name="UNBLOCK_USER" value="N">
            <table class="table js-table">
                <tr>
                    <th class="js-checkbox-td">
                        <label class="checkbox">
                            <input type="checkbox" name="L_USERS_CHECK_ALL" class="js-table-checkbox-all">
                            <span></span>
                        </label>
                    </th>
                    <?foreach($arResult['ROW_SORT'] as $iSort => $arSort) {?>
                        <th>
                            <a href="<?= $arResult['ROOT_URL'] ?><?= $arSort['URL'] ?>" class="filtr">
                                <?= $arSort['TITLE'] ?>
                            </a>
                        </th>
                    <?}?>
                </tr>
                <?foreach($arResult['LIST_SUB_USERS'] as $arUser) {?>
                    <?$clsNoAct = '';
                    if($arUser['ACTIVE'] == 'N') {
                        $clsNoAct = ' class="no-active-row"';
                    }?>
                    <tr<?= $clsNoAct ?>>
                        <td class="js-checkbox-td">
                            <input type="hidden" name="ID_USER_IBLOCK_<?= $arUser['ID'] ?>" value="<?= $arUser['ID_USER_IN_IBLOCK'] ?>">
                            <label class="checkbox">
                                <input type="checkbox"
                                       name="L_CHECK_USER[]"
                                       value="<?= $arUser['ID'] ?>"
                                       class="js-table-checkbox">
                                <span></span>
                            </label>
                        </td>
                        <!--<td><?//= $arUser['ID'] ?></td>-->
                        <td>
                            <a href="<?= $arParams['FOLDER'] . $arParams['URL_SUB_SECTION'] . $arUser['ID'] ?>/">
                                <?= $arUser['FULL_NAME'] ?>
                            </a>
                        </td>
                        <td><?= $arUser['WORK_COMPANY'] ?></td>
                        <td><?= $arUser['LOGIN'] ?></td>
                        <td><?= Loc::getMessage('T_USERS_TABLE_ACTIVE_' . $arUser['ACTIVE']) ?></td>
                        <td><?= $arUser['EMAIL'] ?></td>
                        <td><?= $arUser['PERSONAL_PHONE'] ?></td>
                        <td><?= $arUser['REGISTRY'] ?></td>
                        <td><?= $arUser['LAST_ACTIVITY_DATE'] ?></td>
                    </tr>
                <?}?>
            </table>
            <hr>
            <p>
                <button class="btn js-btn-confirm"
                        data-confirm-title="<?= Loc::getMessage('T_USERS_TABLE_DELETE_USER_CONFIRM_TITLE') ?>"
                        data-confirm-question="<?= Loc::getMessage('T_USERS_TABLE_DELETE_USER_CONFIRM_QESTION') ?>"
                        data-confirm-answer-y="<?= Loc::getMessage('T_USERS_TABLE_DELETE_USER_CONFIRM_ANSWER_Y') ?>"
                        data-confirm-answer-n="<?= Loc::getMessage('T_USERS_TABLE_DELETE_USER_CONFIRM_ANSWER_N') ?>"
                        data-confirm-no-selected="<?= Loc::getMessage('T_USERS_TABLE_NO_SELECT_USER') ?>"
                        data-action="DELETE_USER"
                        type="submit" >
                    <?= Loc::getMessage('T_USERS_TABLE_DELETE_USER') ?>
                </button>
                <button class="btn js-btn-confirm"
                        data-confirm-title="<?= Loc::getMessage('T_USERS_TABLE_BLOCK_USER_CONFIRM_TITLE') ?>"
                        data-confirm-question="<?= Loc::getMessage('T_USERS_TABLE_BLOCK_USER_CONFIRM_QESTION') ?>"
                        data-confirm-answer-y="<?= Loc::getMessage('T_USERS_TABLE_BLOCK_USER_CONFIRM_ANSWER_Y') ?>"
                        data-confirm-answer-n="<?= Loc::getMessage('T_USERS_TABLE_BLOCK_USER_CONFIRM_ANSWER_N') ?>"
                        data-confirm-no-selected="<?= Loc::getMessage('T_USERS_TABLE_NO_SELECT_USER') ?>"
                        data-action="BLOCK_USER"
                        type="submit">
                    <?= Loc::getMessage('T_USERS_TABLE_BLOCK_USER') ?>
                </button>
                <button class="btn js-btn-confirm"
                        data-confirm-title="<?= Loc::getMessage('T_USERS_TABLE_UNBLOCK_USER_CONFIRM_TITLE') ?>"
                        data-confirm-question="<?= Loc::getMessage('T_USERS_TABLE_UNBLOCK_USER_CONFIRM_QESTION') ?>"
                        data-confirm-answer-y="<?= Loc::getMessage('T_USERS_TABLE_UNBLOCK_USER_CONFIRM_ANSWER_Y') ?>"
                        data-confirm-answer-n="<?= Loc::getMessage('T_USERS_TABLE_UNBLOCK_USER_CONFIRM_ANSWER_N') ?>"
                        data-confirm-no-selected="<?= Loc::getMessage('T_USERS_TABLE_NO_SELECT_USER') ?>"
                        data-action="UNBLOCK_USER"
                        type="submit">
                    <?= Loc::getMessage('T_USERS_TABLE_UNBLOCK_USER') ?>
                </button>
            </p>
        </form>
    <?} else {?>
        <?= Loc::getMessage('T_USERS_TABLE_SUBUSERS_EMPTY') ?>
    <?}?>
</div>
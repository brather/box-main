<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Application;

$request = Application::getInstance()->getContext()->getRequest();
$arRequest = $request->toArray();
$arPost = $request->getPostList()->toArray();
$arGet = $request->getQueryList()->toArray();

$stGet = '';
$iGet = 0;
if(!empty($arGet)) {
    $stGet .= '?';
    foreach($arGet as $cGet => $vGet) {
        if($iGet != 0) {
            $stGet .= '&';
        }
        $stGet .= $cGet . '=' . $vGet;
    }
}
$arResult['ROOT_URL'] = $arParams['FOLDER'] . $arParams['URL_SUB_SECTION'];
$arResult['GET_STRING'] = $stGet;
$arResult['GET_STRING_FULL'] = $arResult['ROOT_URL'] . $stGet;
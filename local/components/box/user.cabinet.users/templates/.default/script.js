$(document).ready(function(){
    var selectContract = $('#contract-list'),
        successMess = $('#js-success'),
        c = {
            active      : '._active',
            btn         : '.btn.js-btn-confirm',
            btndel      : '.js-user-delete',
            btnblock    : '.js-user.block',
            jqselect    : '.jqselect',
            selsub      : '.js-select-sub'
        };
    if(successMess.length > 0) {
        $.confirm({
            theme               : 'light',
            backgroundDismiss   : true,
            animation           : 'top',
            closeAnimation      : 'bottom',
            title               : '',
            content             : successMess.data('mess'),
            buttons             : {
                confirm: {
                    keys    : ['enter', 'esc'],
                    text    : successMess.data('apply'),
                    action  : function() {
                        window.location.href = successMess.closest('form').attr('action');
                    }
                }
            }
        });
    }

    //region Выбор договора и реестра
    $(c.selsub).on('change', function(e) {
        var t = $(this),
            subSelect = $(t.data('sub-select'));
        subSelect.find('option').attr({ 'selected' : false, 'disabled' : "disabled" });
        t.find('option:selected').each(function(i,y) {
            var contract = $(y).val();
            subSelect.find('option[data-sub="' + contract + '"]').attr({ 'disabled' : false });
        });
        subSelect.trigger('refresh');
    });
    //endregion

    $(c.btn).on('click', function(e){
        e.preventDefault();
        var $this = $(this),
            form = $this.closest('form'),
            actionInput = form.find('[name="' + $this.data('action') + '"]'),
            confirm = {
                title           : $this.data('confirm-title'),
                text            : $this.data('confirm-question'),
                answery         : $this.data('confirm-answer-y'),
                answern         : $this.data('confirm-answer-n'),
                noselected     : $this.data('confirm-no-selected')
            },
            checkedExist = false;
        if(form.find('tr' + c.active).length > 0) {
            checkedExist = true;
        }

        if(checkedExist) {
            $.confirm({
                theme               : 'light',
                backgroundDismiss   : true,
                animation           : 'top',
                closeAnimation      : 'bottom',
                title               : confirm.title,
                content             : confirm.text,
                buttons             : {
                    confirm: {
                        keys : ['enter'],
                        text : confirm.answery,
                        action : function () {

                            actionInput.val('Y');
                            form.submit();
                        }
                    },
                    cancel: {
                        keys : ['esc'],
                        text : confirm.answern,
                        action : function () {
                            actionInput.val('N');
                        }
                    }
                }
            });
        } else {
            $.confirm({
                theme               : 'light',
                backgroundDismiss   : true,
                animation           : 'top',
                closeAnimation      : 'bottom',
                title               : confirm.noselected,
                content             : '',
                buttons             : {
                    cancel: {
                        keys : ['esc'],
                        text : confirm.answern
                    }
                }
            });
        }

    });
});
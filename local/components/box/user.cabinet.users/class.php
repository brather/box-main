<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application,
    \Bitrix\Iblock\IblockTable,
    \Bitrix\Highloadblock as HL;

class UserCabinetUsersComponent extends CBitrixComponent {
    //region Задание свойств класса
    private $page = 'template';
    private $arRequest = array();
    private $arGet = array();
    private $arPost = array();
    private $idIBlockClients = '';
    //endregion
    //region Подготовка входных параметров компонента
    /** Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        return parent::onPrepareComponentParams($arParams);
    }
    //endregion
    //region Подключение файлов перевода
    /**
     * Подключение файлов перевода
     */
    public function onIncludeComponentLang() {
        Loc::loadMessages(__FILE__);
    }
    //endregion
    //region Выполнение компонента
    /**
     * Выполнение компонента
     */
    public function executeComponent() {
        $this->getRequest();                                    // Получение данных request
        $this->includeModule();                                 // Подключение модулей
        if($this->arPost['F_ADD_USER'] == 'Y') {
            $this->addNewUser();                                // Добавление нового пользователя
        } elseif($this->arPost['ACTION_USER_LIST'] == 'Y') {
            if($this->arPost['DELETE_USER'] == 'Y') {
                $this->deleteUser();                            // Удаление пользователя
            } elseif($this->arPost['BLOCK_USER'] == 'Y') {
                $this->updateUser( 'N');                        // Блокировка пользователя
            } elseif($this->arPost['UNBLOCK_USER'] == 'Y') {
                $this->updateUser( 'Y');                        // Разблокировка пользователя
            }
        }
        $this->getContractList();                               // Возвращаем список договоров суперпользователя
        $this->generatePassword();                              // Генерация автоматического пароля
        $this->getSubUsers();                                   // Получение списка пользователей для текущего суперпользователя
        $this->includeComponentTemplate($this->page);           // Подключение шаблона
    }
    //endregion
    //region Получение данных request
    /**
     * Получение данных request
     */
    private function getRequest() {
        $request = Application::getInstance()->getContext()->getRequest();
        $this->arGet = $request->getQueryList()->toArray();
        $this->arPost = $request->getPostList()->toArray();
    }
    //endregion
    //region Подключаем список модулей
    /**
     * Подключаем список модулей
     */
    private function includeModule() {
        CModule::IncludeModule("highloadblock");
        CModule::IncludeModule('iblock');
    }
    //endregion
    //region Возвращаем список договоров суперпользователя
    /**
     * Возвращаем список договоров суперпользователя
     */
    private function getContractList() {
        $arFilterContracts = array(
            'IBLOCK_CODE' => $this->arParams['IBLOCK_CODE_CONTRACTS'],
            'PROPERTY_PROP_SUPER_USER' => array($this->arParams['USER_ID'])
        );
        $arSelectContracts = array(
            'ID',
            'IBLOCK_ID',
            'NAME',
            'PROPERTY_PROP_ID_REGISTRY',
            'PROPERTY_PROP_COMPANY'
        );
        $rsContracts = CIBlockElement::GetList(array(), $arFilterContracts, false, false, $arSelectContracts);
        $arIdRegistry = array();
        $arIdCompany = array();
        while($obContracts = $rsContracts->Fetch()) {
            $this->arResult['CONTRACT_LIST'][$obContracts['ID']] = $obContracts;
            if(!empty($obContracts['PROPERTY_PROP_COMPANY_VALUE'])
                && !in_array($obContracts['PROPERTY_PROP_COMPANY_VALUE'], $arIdCompany)) {
                $arIdCompany[] = $obContracts['PROPERTY_PROP_COMPANY_VALUE'];
            }
            foreach($obContracts['PROPERTY_PROP_ID_REGISTRY_VALUE'] as $idReg) {
                $this->arResult['REGISTRY_LIST'][$idReg] = $obContracts['ID'];
                if(!in_array($idReg, $arIdRegistry)) {
                    $arIdRegistry[] = $idReg;
                }
            }
        }
        $this->getCompanyList($arIdCompany);
        $this->getRegistryList($arIdRegistry);
    }
    //endregion
    //region Возвращает список организаций по массиву с id
    /** Возвращает список организаций по массиву с id
     * @param $arIdCompany
     */
    private function getCompanyList($arIdCompany) {
        $arFilterCompany = array('IBLOCK_CODE' => $this->arParams['IBLOCK_CODE_COMPANY'], 'ID' => $arIdCompany);
        $arSelectCompany = array(
            'ID',
            'IBLOCK_ID',
            'NAME',
            'PROPERTY_PROP_FORM'
        );
        $rsCompany = CIBlockElement::GetList(array(), $arFilterCompany, false, false, $arSelectCompany);
        while($obCompany = $rsCompany->Fetch()) {
            $nameOfCompany = '&laquo;' . $obCompany['NAME'] . '&raquo;';
            if(!empty($obCompany['PROPERTY_PROP_FORM_VALUE'])) {
                $nameOfCompany = $obCompany['PROPERTY_PROP_FORM_VALUE'] . ' ' . $nameOfCompany;
            }
            $this->arResult['COMPANY_LIST'][$obCompany['ID']] = $nameOfCompany;
        }
    }
    //endregion
    //region Возвращает массив реестров
    /** Возвращает массив реестров
     * @param $arIdRegistry
     */
    private function getRegistryList($arIdRegistry) {
        $rsRegistry = IblockTable::getList(array(
            'filter' => array('ID' => $arIdRegistry),
            'select' => array('ID', 'NAME', 'CODE')
        ));
        $this->arResult['REG_CODE_LIST'] = array();
        while($obRegistry = $rsRegistry->Fetch()) {
            $this->arResult['REG_CODE_LIST'][$obRegistry['ID']] = $obRegistry;
        }
    }
    //endregion
    //region Получение списка пользователей для текущего суперпользователя
    /**
     * Получение списка пользователей для текущего суперпользователя
     */
    private function getSubUsers() {
        $this->arSortSubUsers();
        $arFilterUser = array(
            'IBLOCK_CODE' => $this->arParams['IBLOCK_CODE_CLIENTS'],
            'PROPERTY_PROP_CLIENTS_OF_SUPERUSER' => $this->arParams['USER_ID']
        );
        $arSelectUser = array(
            'ID',
            'NAME',
            'ACTIVE',
            'PROPERTY_PROP_CLIENT',
            'PROPERTY_PROP_REGISTRY'
        );

        $rsUsers = CIBlockElement::GetList(array('NAME' => 'ASC'), $arFilterUser, false, false, $arSelectUser);
        $arIdUsers = array();
        $strIdUsers = '';
        $iUser = 0;
        while($obUser = $rsUsers->Fetch()) {
            $arIdUsers[$obUser['PROPERTY_PROP_CLIENT_VALUE']] = $obUser;
            if($iUser != 0) {
                $strIdUsers .= ' | ';
            }
            $strIdUsers .= $obUser['PROPERTY_PROP_CLIENT_VALUE'];
            $iUser++;
        }

        if(!empty($this->arGet['SORT'])) {
            $by = $this->arGet['SORT'];
            $order = $this->arGet['ORDER'];
        } else {
            $by = "LAST_NAME";
            $order = "desc";
        }
        if(!empty($strIdUsers)) {
            $rsUsers = CUser::GetList($by, $order, array('ID' => $strIdUsers));
            $arRegistry = $this->arResult['REG_CODE_LIST'];

            while($obUsers = $rsUsers->Fetch()) {
                $arRegistryFull = array();
                $arContractsUser = array();
                //$arCompanyUser = array();
                foreach($arIdUsers[$obUsers['ID']]['PROPERTY_PROP_REGISTRY_VALUE'] as $iReg => $idRegistry) {
                    if(empty($arRegistry[$idRegistry]['ID'])) {
                        $arRegistry[$idRegistry] = $this->getRegistryById($idRegistry);
                    }
                    $arRegistryFull[] = $arRegistry[$idRegistry]['NAME'];
                    $idContract = $this->arResult['REGISTRY_LIST'][$idRegistry];
                    if(empty($arContractsUser[$idContract])) {
                        $arContractsUser[$idContract] = $this->arResult['CONTRACT_LIST'][$idContract];
                    }
                    //$companyID = $arContractsUser[$idContract]['PROPERTY_PROP_COMPANY_VALUE'];
                    //$companyName = $this->arResult['COMPANY_LIST'][$companyID];
                    //if(!in_array($companyName, $arCompanyUser)) {
                    //    $arCompanyUser[] = $companyName;
                    //}
                }
                $this->arResult['LIST_SUB_USERS'][] = array(
                    'ID'                => $obUsers['ID'],
                    'FULL_NAME'         => $arIdUsers[$obUsers['ID']]['NAME'],
                    'LOGIN'             => $obUsers['LOGIN'],
                    'ACTIVE'            => $arIdUsers[$obUsers['ID']]['ACTIVE'],
                    'NAME'              => $obUsers['NAME'],
                    'LAST_NAME'         => $obUsers['LAST_NAME'],
                    'SECOND_NAME'       => $obUsers['LAST_NAME'],
                    'EMAIL'             => $obUsers['EMAIL'],
                    'PERSONAL_PHONE'    => $obUsers['PERSONAL_PHONE'],
                    'WORK_COMPANY'      => $obUsers['WORK_COMPANY'],
                    'WORK_POSITION'     => $obUsers['WORK_POSITION'],
                    'IS_ONLINE'         => $obUsers['IS_ONLINE'],
                    'LAST_ACTIVITY_DATE'=> $obUsers['LAST_LOGIN'],
                    'REGISTRY'          => implode(',<br/>', $arRegistryFull),
                    'ID_USER_IN_IBLOCK' => $arIdUsers[$obUsers['ID']]['ID'],
                );
            }
            if(!empty($this->arGet['SORT'])) {
                if($this->arGet['SORT'] == 'WORK_COMPANY' || $this->arGet['SORT'] == 'REGISTRY') {
                    $this->arResult['LIST_SUB_USERS'] = $this->sortArrayByParam(
                        $this->arResult['LIST_SUB_USERS'],
                        $this->arGet['SORT'],
                        $this->arGet['ORDER']);
                }
            }
        } else {
            $this->arResult['LIST_SUB_USERS'] = array();
        }
    }
    //endregion
    private function sortArrayByParam($array, $sort, $order) {
        $arListSort = $array;
        $arSort = array();
        foreach($arListSort as $arReg) {
            $arSort[] = $arReg[$sort];
        }
        if($order == 'ASC') {
            $typeSort = SORT_ASC;
        } else {
            $typeSort = SORT_DESC;
        }
        array_multisort($arSort, $typeSort, $arListSort);
        return $arListSort;
    }
    //region Возвращает инфоблок реестра по id
    /** Возвращает инфоблок реестра по id
     * @param $idRegistry - id инфоблока
     * @return array
     */
    private function getRegistryById($idRegistry) {
        $arFilterRegistry = array('TYPE' => $this->arParams['IBLOCK_TYPE_REGISTRY'], 'ID' => $idRegistry);
        $arRegistry = CIBlock::GetList(array(), $arFilterRegistry)->Fetch();
        return $arRegistry;
    }
    //endregion
    //region Возвращает массив полей для заголовков таблицы
    /**
     * Возвращает массив полей для заголовков таблицы
     */
    private function arSortSubUsers() {
        $arSetSort = $this->arParams['FIELDS_LIST_USERS'];
        $arRowSort = array();
        $preffixGet = '';
        if(!empty($this->arGet)) {
            $iGet = 0;
            foreach($this->arGet as $cGet => $vGet) {
                if($cGet != 'SORT' && $cGet != 'ORDER') {
                    if($iGet == 0) {
                        $preffixGet .= '?';
                    } else {
                        $preffixGet .= '&';
                    }
                    $preffixGet .= $cGet . '=' . $vGet;
                    $iGet++;
                }
            }
        }
        foreach($arSetSort as $cSort) {
            $order = 'ASC';
            if(!empty($this->arGet['SORT']) && $cSort == $this->arGet['SORT'] && $this->arGet['ORDER'] == $order) {
                $order = 'DESC';
            }
            if($preffixGet == '') {
                $preffixGet = '?';
            }
            $arRowSort[] = array(
                'TITLE' => Loc::getMessage('T_USERS_TABLE_' . $cSort),
                'URL' => $preffixGet . 'SORT=' . $cSort . '&ORDER=' . $order
            );
        }
        $this->arResult['ROW_SORT'] = $arRowSort;
    }
    //endregion
    //region Генерация автоматического пароля
    /**
     * Генерация автоматического пароля
     */
    private function generatePassword() {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $max = $this->arParams['COUNT_SYMBOL_PASSWORD'];
        $size = strlen($chars)-1;
        $password = null;
        while($max--) {
            $password .= $chars[rand(0,$size)];
        }
        $this->arResult['F_USER_ADD_PASS'] = $password;
    }
    //endregion
    //region Добавление нового пользователя
    /**
     * Добавление нового пользователя
     */
    private function addNewUser() {
        $arRequest = $this->arPost;
        $arError = array();
        $arFields = array();
        if($arRequest['F_ADD_USER'] == 'Y') {
            foreach($arRequest as $cCode => $vCode) {
                if(!is_array($vCode)) {
                    $vCode = trim($vCode);
                }
                if($cCode != 'F_ADD_USER') {
                    $codeWithoutPref = str_replace('F_ADD_USER_', '', $cCode);
                    if($codeWithoutPref == 'WORK_COMPANY') {
                        if(is_array($vCode)) {
                            $this->getCompanyList($vCode);
                            $companyName = '';
                            $iCompany = 0;
                            foreach($this->arResult['COMPANY_LIST'] as $idCompany => $nameCompany) {
                                if($iCompany != 0) {
                                    $companyName .= ', ';
                                }
                                $companyName .= $nameCompany;
                                $iCompany++;
                            }
                        } else {
                            $companyName = $vCode;
                        }
                        $arFields['WORK_COMPANY'] = $companyName;
                    }
                    elseif(is_string($vCode)
                        && empty($vCode)
                        && $codeWithoutPref != 'REGISTRY'
                        //&& $codeWithoutPref != 'WORK_POSITION'
                        && $codeWithoutPref != 'PERSONAL_PHONE') {
                        $arError[$codeWithoutPref]['MESS'] = Loc::getMessage('F_ADD_USER_ERROR_EMPTY_'
                            . $codeWithoutPref);
                    } else {
                        if($codeWithoutPref == 'EMAIL') {
                            if (!checkEmail($vCode)) {
                                $arError[$codeWithoutPref]['MESS'] = Loc::getMessage('F_ADD_USER_ERROR_INCORECT_EMAIL');
                            }
                        } elseif($codeWithoutPref == 'PERSONAL_PHONE') {
                            if(!checkPhone($vCode)) {
                                $arError[$codeWithoutPref]['MESS'] = Loc::getMessage('F_ADD_USER_ERROR_INCORECT_PERSONAL_PHONE');
                            }
                        } elseif($codeWithoutPref == 'CONTRACT_ID') {
                            if(count($arRequest['F_ADD_USER_CONTRACT_ID']) == 0) {
                                $arError[$codeWithoutPref]['MESS'] = Loc::getMessage('F_ADD_USER_ERROR_EMPTY_CONTRACT_ID');
                            }
                        } elseif($codeWithoutPref == 'REGISTRY') {
                            $codeWithoutPref = 'UF_REGISTRY';
                            if(empty($arRequest['F_ADD_USER_REGISTRY'])) {
                                $arError[$codeWithoutPref]['MESS'] = Loc::getMessage('F_ADD_USER_ERROR_EMPTY_REGISTRY');
                            }
                            $vCode = $this->getParamFromSelect($arRequest['F_ADD_USER_REGISTRY']);
                        } elseif($codeWithoutPref != 'PASSWORD'
                            && $codeWithoutPref != 'WORK_POSITION'
                            && $codeWithoutPref != 'ORGANIZATION'
                            && $codeWithoutPref != 'WORK_COMPANY') {
                            if(!checkWord($vCode)) {
                                $arError[$codeWithoutPref]['MESS'] = Loc::getMessage('F_ADD_USER_ERROR_INCORECT_'
                                    . $codeWithoutPref);
                            }
                        }
                        $arFields[$codeWithoutPref] = $vCode;
                    }
                }
            }
            if(!empty($arError)) {
                $this->arResult['F_USER_ADD_ERROR'] = $arError;
            } else {
                $user = new CUser;
                $arFields['LOGIN'] = $arFields['EMAIL'];
                $arFields['UF_FULL_NAME'] = $arFields['LAST_NAME']
                    . ' ' . $arFields['NAME']
                    . ' ' . $arFields['SECOND_NAME'];
                $arFields['GROUP_ID'] = $this->getGroupId();
                $ID = $user->Add($arFields);
                if (intval($ID) > 0) {
                    $this->arResult['F_ADD_USER_SUCCESS'] = Loc::getMessage('F_ADD_USER_SUCCESS_1')
                        . $arFields['LOGIN'] . Loc::getMessage('F_ADD_USER_SUCCESS_2')
                        . $arFields['PASSWORD'] . Loc::getMessage('F_ADD_USER_SUCCESS_3');
                    $el = new CIBlockElement;
                    $idCompany = $this->getParamFromSelect($this->arPost['F_ADD_USER_ORGANIZATION'], true);
                    $arLoadUser = array(
                        "IBLOCK_ID" => $this->getIdFromCode($this->arParams['IBLOCK_CODE_CLIENTS']),
                        "NAME" => $arFields['LAST_NAME'] . ' ' . $arFields['NAME'] . ' ' . $arFields['SECOND_NAME'],
                        'ACTIVE' => 'Y',
                        "PROPERTY_VALUES" => array(
                            'PROP_CLIENTS_OF_SUPERUSER' => $this->arParams['USER_ID'],
                            'PROP_CLIENT' => $ID,
                            'PROP_ORGANIZATION' => $idCompany,
                            'PROP_REGISTRY' => $this->getParamFromSelect($this->arPost['F_ADD_USER_REGISTRY'], true)
                        )
                    );
                    $el->Add($arLoadUser);
                }
            }
        }
    }
    //endregion
    //region Возвращает значения заголовка реестра выбранного для пользователя
    /** Возвращает значения заголовка реестра выбранного для пользователя
     * @param $obj
     * @param $second - параметр отвечающий за то, чтобы вернуть вторую часть (т.е. ID строки)
     * @return array|mixed|string
     */
    private function getParamFromSelect($obj, $second = false) {
        if(is_array($obj)) {
            $arResult = array();
            foreach($obj as $code) {
                if($second) {
                    $nStr = substr($code, strpos($code, '-ID-') + strlen('-ID-'));
                } else {
                    $nStr = str_replace('TITLE-', '', $code);
                    $nStr = substr($nStr, 0, strpos($nStr, '-ID-'));
                }
                $arResult[] = urldecode($nStr);
            }
            return $arResult;
        } elseif(is_string($obj)) {
            if($second) {
                $nStr = substr($obj, strpos($obj, '-ID-') + strlen('-ID-'));
            } else {
                $nStr = str_replace('TITLE-', '', $obj);
                $nStr = substr($nStr, 0, strpos($nStr, '-ID-'));
            }
            return urldecode($nStr);
        }
    }
    //endregion
    //region Возвращаем id группы пользователей
    /** Вернем id группы пользователей
     * @return int
     */
    private function getGroupId() {
        $arIdGroupUser = array();
        $rsGroups = CGroup::GetList($by = "c_sort", $order = "asc", array("ACTIVE" => 'Y'));
        while($obGroup = $rsGroups->Fetch()) {
            if($obGroup['STRING_ID'] == U_GROUP_CODE_CLIENT_USER
                || $obGroup['STRING_ID'] == U_GROUP_CODE_ALL) {
                $arIdGroupUser[] = $obGroup['ID'];
            }
        }
        return $arIdGroupUser;
    }
    //endregion
    //region Возвращает id инфоболка по его коду
    /** Возвращает id инфоболка по его коду
     * @param $code - символьный код инфоблока
     * @return mixed
     */
    private function getIdFromCode($code) {
        $arFilter = array('CODE' => $code);
        $arBlock = CIBlock::GetList(array(), array('CODE' => $code))->Fetch();
        return $arBlock['ID'];
    }
    //endregion
    //region Удаление пользователя
    /**
     * Удаление пользователя
     */
    private function deleteUser() {
        foreach($this->arPost['L_CHECK_USER'] as $iUser => $idUser) {
            $idUserInIBlock = $this->arPost['ID_USER_IBLOCK_' . $idUser];
            CIBlockElement::Delete($idUserInIBlock);
            CUser::Delete($idUser);
        }
    }
    //endregion
    //region Блокировка пользователя
    /** Блокировка пользователя
     * @param $active
     */
    private function updateUser($active) {
        $arLoadFields = array('ACTIVE' => $active);
        $arUserId = $this->arPost['L_CHECK_USER'];
        foreach($arUserId as $uId) {
            $idUser = $this->arPost['ID_USER_IBLOCK_' . $uId];

            $user = new CUser;
            $user->Update($uId, $arLoadFields);

            $el = new CIBlockElement;
            $el->Update($idUser, $arLoadFields);
        }
    }
    //endregion
}
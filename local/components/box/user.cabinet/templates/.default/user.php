<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

global $APPLICATION;

$APPLICATION->IncludeComponent(
    'box:user.cabinet.edit',
    '',
    array(
        'GET_STRING_FULL' => $arResult['GET_STRING_FULL'],
        'CURRENT_PATH' => $arResult['CURRENT_PATH'],
        'SEF_FOLDER' => $arParams['SEF_FOLDER'],
        'SUB_USER_ID' => $arResult['VARIABLES']['ID'],
        'IBLOCK_CODE_CLIENTS' => $arParams['IBLOCK_CODE_CLIENTS'],
        'IBLOCK_CODE_CONTRACTS' => $arParams['IBLOCK_CODE_CONTRACTS'],
        'COUNT_SYMBOL_PASSWORD' => $arParams['COUNT_SYMBOL_PASSWORD'],
        'FIELD_SUB_USER' => array(
            'LAST_NAME',
            'NAME',
            'ACTIVE',
            'SECOND_NAME',
            'EMAIL',
            'PERSONAL_PHONE',
            'LIST_COMPANY',
            'PROP_CONTRACTS',
            'PROP_REGISTRY',
            'WORK_POSITION',
            'PASSWORD',
        ),
    )
);

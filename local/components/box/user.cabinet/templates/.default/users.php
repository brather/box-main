<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

global $APPLICATION;?>

<?$APPLICATION->IncludeComponent(
    'box:user.cabinet.users',
    '',
    array(
        'FOLDER' => $arResult['FOLDER'],
        'URL_SUB_SECTION' => 'users/',
        'IBLOCK_CODE_CLIENTS' => $arParams['IBLOCK_CODE_CLIENTS'],
        'IBLOCK_CODE_COMPANY' => $arParams['IBLOCK_CODE_COMPANY'],
        'IBLOCK_CODE_CONTRACTS' => $arParams['IBLOCK_CODE_CONTRACTS'],
        'IBLOCK_TYPE_REGISTRY' => $arParams['IBLOCK_TYPE_REGISTRY'],
        'USER_DATA' => $arResult['USER_DATA'],
        'USER_GROUP' => $arResult['USER_GROUPS'],
        'USER_ID' => $arResult['USER_ID'],
        'COUNT_SYMBOL_PASSWORD' => $arParams['COUNT_SYMBOL_PASSWORD'],
        'FIELD_SUB_USER' => array(
            'LAST_NAME',
            'NAME',
            'SECOND_NAME',
            'EMAIL',
            'PERSONAL_PHONE',
            //'WORK_COMPANY',
            'WORK_POSITION',
            'PASSWORD',
        ),
        'FIELDS_LIST_USERS' => array(
            //'ID',
            'UF_FULL_NAME',
            'WORK_COMPANY',
            'LOGIN',
            'ACTIVE',
            'EMAIL',
            'PERSONAL_PHONE',
            'REGISTRY',
            'LAST_LOGIN_2'
        )
    ))?>
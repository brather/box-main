<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

global $APPLICATION;

$APPLICATION->IncludeComponent(
    'box:user.cabinet.settings',
    '',
    array(
        'IBLOCK_CODE_CLIENTS' => $arParams['IBLOCK_CODE_CLIENTS'],
        'FOLDER' => $arResult['FOLDER'],
        'URL_SUB_SECTION' => 'settings/',
        'USER_DATA' => $arResult['USER_DATA'],
        'USER_GROUP' => $arResult['USER_GROUPS'],
        'USER_FULL_NAME' => $arResult['USER_FULL_NAME'],
        'USER_ID' => $arResult['USER_ID'],
        'MIN_COUNT_SYMBOL_PASS' => 6,
        'EDIT_DATA' => array(
            //'NAME',
            //'LAST_NAME',
            //'SECOND_NAME',
            //'WORK_POSITION',
            //'EMAIL',
            'PERSONAL_PHONE',
            'PERSONAL_MOBILE'
        )
    )
);?>

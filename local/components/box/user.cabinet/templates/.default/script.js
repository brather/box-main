/**
 * Created by AChechel on 13.06.2017.
 */
(function($){
    /**
     * Generate new key and insert into input value
     */
    $( '#keygen' ).on('click',function()
    {
        $.confirm({
            theme: 'light',
            backgroundDismiss : true,
            animation           : 'top',
            closeAnimation      : 'bottom',
            title : "Генерация API Key",
            content : 'Вы действительно хотите создать новый API-ключ?',
            buttons : {
                confirm: {
                    keys : ['enter', 'space'],
                    text : 'Да',
                    action : function () {
                        $( '#apikey' ).val( function () {
                            var d = new Date().getTime();

                            if( window.performance && typeof window.performance.now === "function" )
                                d += performance.now();

                            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                                var r = (d + Math.random() * 16) % 16 | 0;
                                d = Math.floor(d / 16);
                                return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
                            });
                        } );

                        $.ajax({
                            url: '/client/cabinet/',
                            dataType: 'JSON',
                            method: 'POST',
                            data: {
                                API_KEY: document.getElementById('apikey').value
                            },
                            success: function(res) {
                                console.log(res);
                            },
                            error: function(xhr, st, ms) {
                                console.log(xhr, st, ms);
                            }
                        });
                    }
                },
                cancel: {
                    keys: ['esc'],
                    text: 'Нет'
                }
            }
        });

    });
})(jQuery);
<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

use Bitrix\Main\Localization\Loc;
global $APPLICATION;
?>

<div class="order__col">
    <?if($arResult['USER_GROUPS']['STRING_ID'] == U_GROUP_CODE_CLIENT_S_USER) {?>
        <h2><?= Loc::getMessage('CABINET_T_PERSONAL_DATA') ?></h2>
        <ul class="order__list">
            <?if($arParams['USER_FULL_NAME'] == 'Y'){?>
                <li>
                    <div><?= Loc::getMessage('CABINET_T_PERSONAL_FIO') ?></div>
                    <div><?= $arResult['USER_FULL_NAME'] ?></div>
                </li>
            <?}?>
            <?if($arParams['USER_GROUP_ROLE'] == 'Y') {?>
                <li>
                    <div><?= Loc::getMessage('CABINET_T_PERSONAL_ROLE') ?></div>
                    <div><?= $arResult['USER_GROUPS']['USER_ROLE'] ?></div>
                </li>
            <?}?>
            <?foreach($arResult['USER_DATA'] as $cData => $vData){?>
                <?if($cData != 'PASSWORD') {?>
                    <?if($arParams['USER_FULL_NAME'] == 'Y') {
                        if($cData == 'LAST_NAME' || $cData == 'NAME' || $cData == 'SECOND_NAME') {
                            continue;
                        }
                    }?>
                    <li>
                        <div><?= Loc::getMessage('CABINET_T_PERSONAL_' . $cData) ?></div>
                        <div>
                            <?if($cData == 'ACTIVE'){?>
                                <?= Loc::getMessage('CABINET_T_PERSONAL_ACTIVE_' . $vData) ?>
                            <?} else {?>
                                <?= $vData ?>
                            <?}?>
                        </div>
                    </li>
                <?}?>
            <?}?>
        </ul>
        <?if(!empty($arParams['EDIT_DATA_URL'])){?>
            <p>
                <a href="<?= $arParams['EDIT_DATA_URL'] ?>" class="btn">
                    <?= Loc::getMessage('CABINET_T_PERSONAL_EDIT_FULL') ?>
                </a>
            </p>
        <?}?>
        <div class="header">
            <div class="row">
                <div class="col-lg-3 col-lg-offset-4">
                    <button class="form-control btn" id="keygen"><?= Loc::getMessage("GENERATE_API_KEY") ?></button>
                </div>
                <div class="col-lg-4">
                    <input readonly class="form-control form__field" id="apikey" type="text" value="<?= $arResult['API_KEY']?>" placeholder="Click on the button to generate a new API key ..."  />
                </div>
            </div>
        </div>
    <?} elseif($arResult['USER_GROUPS']['STRING_ID'] == U_GROUP_CODE_CLIENT_USER) {?>
        <?$APPLICATION->IncludeComponent(
            'box:user.cabinet.settings',
            'simple',
            array(
                'IBLOCK_CODE_CLIENTS' => $arParams['IBLOCK_CODE_CLIENTS'],
                'FOLDER' => $arResult['FOLDER'],
                'URL_SUB_SECTION' => 'settings/',
                'USER_DATA' => $arResult['USER_DATA'],
                'USER_GROUP' => $arResult['USER_GROUPS'],
                'USER_FULL_NAME' => $arResult['USER_FULL_NAME'],
                'USER_ID' => $arResult['USER_ID'],
                'MIN_COUNT_SYMBOL_PASS' => 6,
                'EDIT_DATA' => array(
                    'NAME',
                    'LAST_NAME',
                    'SECOND_NAME',
                    'WORK_POSITION',
                    'EMAIL',
                    'PERSONAL_PHONE',
                    'PERSONAL_MOBILE'
                )
            )
        );?>
    <?}?>
</div>
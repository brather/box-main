<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();?>

<?$APPLICATION->IncludeComponent(
    'box:user.cabinet.contracts',
    '',
    array(
        'FOLDER' => $arResult['FOLDER'],
        'URL_SUB_SECTION' => 'orders/',
        'USER_ID' => $arResult['USER_ID'],
        'IBLOCK_CODE_CONTRACTS' => $arParams['IBLOCK_CODE_CONTRACTS'],
        'COUNT_CONTRACTS_ON_PAGE' => $arParams['COUNT_CONTRACTS_ON_PAGE'],
        'NAV_TEMPLATE' => $arParams['NAV_TEMPLATE'],
    ));?>

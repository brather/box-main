<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();?>

<?use Bitrix\Main\Localization\Loc;?>

<?$APPLICATION->IncludeComponent(
    'box:user.cabinet.company',
    '',
    array(
        'FOLDER' => $arResult['FOLDER'],
        'URL_SUB_SECTION' => 'company/',
        'IBLOCK_CODE_CONTRACTS' => $arParams['IBLOCK_CODE_CONTRACTS'],
        'IBLOCK_CODE_COMPANY' => $arParams['IBLOCK_CODE_COMPANY'],
        'IBLOCK_TYPE_LISTS' => $arParams['IBLOCK_TYPE_LISTS'],
        'USER_DATA' => $arResult['USER_DATA'],
        'USER_ID' => $arResult['USER_ID'],
        'PHONE_FORMAT' => $arParams['PHONE_FORMAT'],
        'COMPANY_FIELDS' => $arParams['COMPANY_FIELDS'],
        'FIELDS_ADRESS' => array(
            'ID' => 'hidden',
            'UF_TYPE_ADRESS' => 'hidden',
            'UF_COMPANY' => 'hidden',
            'UF_NAME' => 'hidden',
            'UF_COUNTRY' => 'text',
            'UF_REGION' => 'text',
            'UF_TOWN' => 'text',
            'UF_AREA' => 'text',
            'UF_INDEX' => 'text',
            'UF_STREET' => 'text',
            'UF_HOUSE' => 'text',
            'UF_XML_ID' => 'hidden',
            'UF_SORT' => 'hidden',
            'UF_HOUSING' => 'text',
            'UF_STRUCT' => 'text',
            'UF_OFFICE' => 'text',
            'UF_CONTACT_PERSON' => 'text',
            'UF_CONTACT_PERSON_PH' => 'text',
            'UF_DESCRIPTION' => 'textarea',
        ),
    ));?>

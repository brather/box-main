<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application;

$request = Application::getInstance()->getContext()->getRequest();
$arRequest = $request->toArray();
$arPost = $request->getPostList()->toArray();
$arGet = $request->getQueryList()->toArray();
$urlUsers = $arParams['SEF_FOLDER'] . 'users/';
?>
<div class="order__col__frame">
    <div class="form">
        <div class="form__row">
            <p class="form__row__title"><?= Loc::getMessage('T_USER_EDIT_TITLE') ?><b>&laquo;<?= $arResult['USER_FULL_NAME'] ?>&raquo;</b></p>
        </div>
        <div class="form__row">
            <a href="<?= $urlUsers ?>" class="btn">
                <i class="fa fa-reply"></i><?= Loc::getMessage('T_USER_BUTTON_BACK') ?>
            </a>
        </div>
    </div>

    <form action="<?= $arResult['GET_STRING_FULL'] ?>"
          method="POST"
          class="form">
        <input type="hidden" name="<?= $arResult['PREFIX'] ?>EDIT" value="Y">
        <input type="hidden" name="<?= $arResult['PREFIX'] ?>ID_IN_IBLOCK" value="<?= $arResult['USER_DATA']['ID_IN_IBLOCK'] ?>">
        <input type="hidden" name="<?= $arResult['PREFIX'] ?>IBLOCK_ID" value="<?= $arResult['USER_DATA']['IBLOCK_ID'] ?>">
        <input type="hidden" name="<?= $arResult['PREFIX'] ?>ID" value="<?= $arParams['SUB_USER_ID'] ?>">
        <div class="form__row">
            <div class="form__row__col _size5">
                <ul class="order__list edit-user">
                    <?foreach($arParams['FIELD_SUB_USER'] as $iCode => $code) {?>
                        <li>
                            <div><?= Loc::getMessage('T_USER_EDIT_' . $code) ?></div>
                            <div>
                                <?$clsMasketInput = '';
                                $dataMaskedInput = '';
                                if(stristr($code, 'PHONE')) {
                                    $clsMasketInput = ' js-maskedinput';
                                    $dataMaskedInput = ' data-maskedinput="+9 999 999 99 99"';
                                }
                                $value = $arResult['USER_DATA'][$code];
                                if($arPost[$code]) {
                                    $value = $arPost[$code];
                                }

                                if($code == 'LIST_COMPANY') {
                                    $multiple = '';
                                    $size = '';
                                    if(count($arResult['COMPANY_LIST']) > 1) {
                                        $multiple = ' multiple';
                                        $size = getSizeMultiSelect(count($arResult['COMPANY_LIST']));?>
                                        <select name="<?= $arResult['PREFIX'] ?>COMPANY[]"
                                                class="js-select-sub"
                                                data-sub-select="#user-contracts"
                                            <?= $multiple ?>
                                            <?= $size ?>
                                                id="user-company">
                                            <?foreach($arResult['COMPANY_LIST'] as $idCompany => $nameCompany) {
                                                $selected = '';
                                                if(in_array($idCompany, $arResult['USER_DATA']['LIST_COMPANY'])
                                                    || in_array($idCompany, $arPost[$arResult['PREFIX'] . 'LIST_COMPANY'])) {
                                                    $selected = ' selected';
                                                }?>
                                                <option value="<?= $idCompany ?>"<?= $selected ?>><?= $nameCompany ?></option>
                                            <?}?>
                                        </select>
                                    <?} else {
                                        $keysCompany = array_keys($arResult['COMPANY_LIST']);?>
                                        <input type="hidden" name="<?= $arResult['PREFIX'] ?>COMPANY[]" value="<?= $keysCompany[0] ?>">
                                        <p><b><?= $arResult['COMPANY_LIST'][$keysCompany[0]] ?></b></p>
                                    <?}?>
                                <?} elseif($code == 'PROP_CONTRACTS') {
                                    $size = getSizeMultiSelect(count($arResult['S_USER_DATA']['CONTRACTS']));
                                    if($size == 1) {
                                        $multiple = '';
                                        $size = '';
                                    } else {
                                        $multiple = ' multiple';
                                        $size = ' size="' . $size . '"';
                                    }
                                    $disabled = '';
                                    if(count($arResult['COMPANY_LIST']) > 1) {
                                        $disabled = ' disabled';
                                    }?>
                                    <?if(count($arResult['S_USER_DATA']['CONTRACTS']) > 1){?>
                                        <select name="<?= $arResult['PREFIX'] ?><?= $code ?>[]"
                                                class="js-select-sub"
                                                data-sub-select="#user-registry"
                                            <?= $size ?>
                                            <?= $multiple ?>
                                                id="user-contracts">
                                            <?foreach($arResult['S_USER_DATA']['CONTRACTS'] as $idContract => $nameContract) {
                                                $selected = '';
                                                if(in_array($idContract, $arResult['USER_DATA']['CONTRACTS'])
                                                    || in_array($idContract, $arPost[$arResult['PREFIX'] . $code])) {
                                                    $selected = ' selected';
                                                }?>
                                                <option data-sub="<?= $arResult['S_USER_DATA']['LIST_COMPANY'][$idContract] ?>"
                                                        value="<?= $idContract ?>"<?= $selected ?>
                                                    <?= $disabled ?>>
                                                    <?= $nameContract ?>
                                                </option>
                                            <?}?>
                                        </select>
                                    <?} else {
                                        $keysContracts = array_keys($arResult['S_USER_DATA']['CONTRACTS']);?>
                                        <input type="hidden" name="<?= $arResult['PREFIX'] ?><?= $code ?>[]" value="<?= $keysContracts[0] ?>">
                                        <p><b><?= $arResult['S_USER_DATA']['CONTRACTS'][$keysContracts[0]] ?></b></p>
                                    <?}?>
                                <?} elseif($code == 'PROP_REGISTRY') {
                                    $size = getSizeMultiSelect(count($arResult['S_USER_DATA']['REGISTRY']));
                                    $disabled = '';
                                    if(count($arResult['S_USER_DATA']['CONTRACTS']) > 1) {
                                        $disabled = ' disabled';
                                    }
                                    if($size == 1) {
                                        $multiple = '';
                                        $size = '';
                                    } else {
                                        $multiple = ' multiple';
                                        $size = ' size="' . $size . '"';
                                    }?>
                                    <select name="<?= $arResult['PREFIX'] ?><?= $code ?>[]"
                                        <?= $size ?>
                                        <?= $multiple ?>
                                            id="user-registry">
                                        <?foreach($arResult['S_USER_DATA']['REGISTRY'] as $idRegistry => $nameRegistry) {
                                            $selected = '';
                                            if(in_array($idRegistry, $arResult['USER_DATA']['PROPERTY_' . $code])
                                                || in_array($idRegistry, $arPost[$arResult['PREFIX'] . $code])) {
                                                $selected = ' selected';
                                            }?>
                                            <option data-sub="<?= $arResult['S_USER_DATA']['LIST_REGISTRY'][$idRegistry] ?>"
                                                    value="<?= $idRegistry ?>"
                                                    <?= $disabled ?>
                                                    <?= $selected ?>>
                                                <?= $nameRegistry ?>
                                            </option>
                                        <?}?>
                                    </select>
                                <?} elseif($code == 'PASSWORD') {?>
                                    <div class="row__password">
                                        <input type="text" <?= $dataMaskedInput ?>
                                               name="<?= $arResult['PREFIX'] ?>PASSWORD"
                                               value="<?= $value ?>"
                                               class="form__field<?= $clsMasketInput ?>">
                                        <a href="#"
                                           class="js-password-generate"
                                           data-max-symbol="<?= $arParams['COUNT_SYMBOL_PASSWORD'] ?>"
                                           data-ajax="<?= $this->GetFolder() ?>/ajax/password.php">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    </div>
                                <?} elseif($code == 'ACTIVE') {?>
                                    <select name="<?= $arResult['PREFIX'] ?>ACTIVE" id="select-active">
                                        <option value="Y"<?if($arResult['USER_DATA']['ACTIVE'] == 'Y'){?> selected<?}?>><?= Loc::getMessage('USER_ACTIVE_Y') ?></option>
                                        <option value="N"<?if($arResult['USER_DATA']['ACTIVE'] == 'N'){?> selected<?}?>><?= Loc::getMessage('USER_ACTIVE_N') ?></option>
                                    </select>
                                <?} else {?>
                                    <input type="text" <?= $dataMaskedInput ?>
                                           name="<?= $arResult['PREFIX'] ?><?= $code ?>"
                                           value="<?= $value ?>"
                                           class="form__field<?= $clsMasketInput ?>">
                                <?}?>
                            </div>
                        </li>
                    <?}?>
                </ul>
            </div>
        </div>
        <div class="form__row">
            <button class="btn" type="submit"><?= Loc::getMessage('T_USER_EDIT_BUTTON') ?></button>
            <a href="#"
               data-prefix="<?= $arResult['PREFIX'] ?>"
               data-lang-t="<?= Loc::getMessage('T_USER_DELETE_T') ?>"
               data-lang-q="<?= Loc::getMessage('T_USER_DELETE_Q') ?>"
               data-lang-y="<?= Loc::getMessage('T_USER_DELETE_Y') ?>"
               data-lang-n="<?= Loc::getMessage('T_USER_DELETE_N') ?>"
               data-ajax="<?= $this->GetFolder() ?>/ajax/delete.php"
               data-url="<?= $urlUsers ?>"
               class="btn _white js-delete-user">
                <?= Loc::getMessage('T_USER_DELETE') ?>
            </a>
        </div>
    </form>
</div>
<?php
$MESS['T_USER_EDIT_TITLE'] = 'Пользователь ';
$MESS['T_USER_EDIT_NAME'] = 'Имя: ';
$MESS['T_USER_EDIT_LAST_NAME'] = 'Фамилия: ';
$MESS['T_USER_EDIT_SECOND_NAME'] = 'Отчество: ';
$MESS['T_USER_EDIT_EMAIL'] = 'E-mail: ';
$MESS['T_USER_EDIT_PERSONAL_PHONE'] = 'Телефон: ';
$MESS['T_USER_EDIT_WORK_POSITION'] = 'Должность: ';
$MESS['T_USER_EDIT_LIST_COMPANY'] = 'Организация: ';
$MESS['T_USER_EDIT_PROP_REGISTRY'] = 'Реестр: ';
$MESS['T_USER_EDIT_PROP_CONTRACTS'] = 'Договоры: ';
$MESS['T_USER_EDIT_PASSWORD'] = 'Пароль: ';
$MESS['T_USER_EDIT_LOGIN'] = 'Логин: ';
$MESS['T_USER_EDIT_ACTIVE'] = 'Активность: ';
$MESS['T_USER_EDIT_BUTTON'] = 'Изменить';
$MESS['USER_ACTIVE_Y'] = 'Активен';
$MESS['USER_ACTIVE_N'] = 'Неактивен';
$MESS['T_USER_BUTTON_BACK'] = ' Назад';

$MESS['T_USER_DELETE'] = 'Удалить';
$MESS['T_USER_DELETE_Q'] = 'Вы уверены, что хотите удалить пользователя?';
$MESS['T_USER_DELETE_T'] = 'Удалить пользователя';
$MESS['T_USER_DELETE_Y'] = 'Удалить';
$MESS['T_USER_DELETE_N'] = 'Отмена';
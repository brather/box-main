<?php
$MESS['T_USER_EDIT_TITLE'] = 'User ';
$MESS['T_USER_EDIT_NAME'] = 'Name: ';
$MESS['T_USER_EDIT_LAST_NAME'] = 'Last name: ';
$MESS['T_USER_EDIT_SECOND_NAME'] = 'Second name: ';
$MESS['T_USER_EDIT_EMAIL'] = 'E-mail: ';
$MESS['T_USER_EDIT_PERSONAL_PHONE'] = 'Phone: ';
$MESS['T_USER_EDIT_WORK_POSITION'] = 'Work position: ';
$MESS['T_USER_EDIT_LIST_COMPANY'] = 'Company: ';
$MESS['T_USER_EDIT_PROP_REGISTRY'] = 'Registry: ';
$MESS['T_USER_EDIT_PROP_CONTRACTS'] = 'Contracts: ';
$MESS['T_USER_EDIT_PASSWORD'] = 'Password: ';
$MESS['T_USER_EDIT_LOGIN'] = 'Login: ';
$MESS['T_USER_EDIT_ACTIVE'] = 'Active: ';
$MESS['T_USER_EDIT_BUTTON'] = 'Apply';
$MESS['USER_ACTIVE_Y'] = 'Active';
$MESS['USER_ACTIVE_N'] = 'No active';
$MESS['T_USER_BUTTON_BACK'] = ' Back';

$MESS['T_USER_DELETE'] = 'Delete';
$MESS['T_USER_DELETE_Q'] = 'Are you sure you want to delete the user?';
$MESS['T_USER_DELETE_T'] = 'Delete user';
$MESS['T_USER_DELETE_Y'] = 'Delete';
$MESS['T_USER_DELETE_N'] = 'Cancel';
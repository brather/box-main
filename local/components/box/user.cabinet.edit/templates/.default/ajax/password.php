<?php
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use Bitrix\Main\Application;

$arPost = Application::getInstance()->getContext()->getRequest()->getPostList()->toArray();

if($arPost['GENERATE_PASS'] == 'Y') {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $max = 10;
    if(!empty($arPost['MAX_COUNT_SYMBOL'])) {
        $max = $arPost['MAX_COUNT_SYMBOL'];
    }
    $size = strlen($chars)-1;
    $password = null;
    while($max--) {
        $password .= $chars[rand(0,$size)];
    }
    echo $password;
}
<?php
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use Bitrix\Main\Application,
    Bitrix\Main\Loader;

Loader::includeModule('iblock');
$arPost = Application::getInstance()->getContext()->getRequest()->getPostList()->toArray();

if($arPost['U_DELETE'] == 'Y') {
    CIBlockElement::Delete($arPost['ID']);
    CUser::Delete($arPost['USER_ID']);
}


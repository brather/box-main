$(document).ready(function(){
    var ids = {
            contracts   : '#user-contracts',
            registrys   : '#user-registry'
        },
        c = {
            selsub  : '.js-select-sub',
            pass    : '.js-password-generate',
            spin    : 'fa-spin',
            delete  : '.js-delete-user',
            form    : '.form'
        };
    //region Генерация пароля
    $(document).on('click', c.pass, function(e){
        e.preventDefault();
        var t = $(this),
            d = { GENERATE_PASS : 'Y', MAX_COUNT_SYMBOL : t.data('max-symbol') };
        t.toggleClass(c.spin);
        $.ajax({
            url     : t.data('ajax'),
            type    : 'post',
            data    : d,
            success : function(data) {
                t.closest('div').find('input').val(data);
                t.toggleClass(c.spin);
            }
        });
    });
    //endregion
    //region Выбор договора и реестра
    $(c.selsub).on('change', function(e) {
        var t = $(this),
            subSelect = $(t.data('sub-select'));
        subSelect.find('option').attr({ 'selected' : false, 'disabled' : "disabled" });
        t.find('option:selected').each(function(i,y) {
            var contract = $(y).val();
            subSelect.find('option[data-sub="' + contract + '"]').attr({ 'disabled' : false });
        });
        subSelect.trigger('refresh');
    });
    //endregion
    $(document).on('click', c.delete, function(e) {
        e.preventDefault();
        var t = $(this);
        $.confirm({
            theme               : 'light',
            backgroundDismiss   : true,
            animation           : 'top',
            closeAnimation      : 'bottom',
            title               : t.data('lang-t'),
            content             : t.data('lang-q'),
            buttons             : {
                confirm: {
                    keys : ['enter'],
                    text : t.data('lang-y'),
                    action : function () {
                        var form = t.closest(c.form),
                            prefix = t.data('prefix'),
                            dataDel = {
                                U_DELETE    : 'Y',
                                USER_ID     : form.find('[name="' + prefix + 'ID"]').val(),
                                ID   : form.find('[name="' + prefix + 'ID_IN_IBLOCK"]').val()
                            };
                        $.ajax({
                            url     : t.data('ajax'),
                            type    : 'post',
                            data    : dataDel,
                            success : function() {
                                window.location.href = t.data('url');
                            }
                        });
                    }
                },
                cancel: {
                    keys : ['esc'],
                    text : t.data('lang-n')
                }
            }
        });
    });
});
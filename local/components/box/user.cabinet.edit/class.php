<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Application,
    Bitrix\Main\Loader;

class UserCabinetEdit extends CBitrixComponent {
    //region Задание свойств класса
    private $page = 'template';
    private $arPost = array();
    private $arGet = array();
    private $prefix = 'F_USER_';
    //endregion
    //region Подготовка входных параметров компонента
    /** Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        return parent::onPrepareComponentParams($arParams);
    }
    //endregion
    //region Выполнение компонента
    /**
     * Выполнение компонента
     */
    public function executeComponent() {
        $this->getRequest();                                // Возвращение массива request
        $this->requireModule();                             // Подключение модулей
        $this->getContractData();                           // Возвращает список договоров доступных суперпользователю
        $this->getListRegistry();                           // Возвращает список реестров доступных суперпользователю
        $this->editUser();                                  // Обновление данных пользователя
        $this->getDataUser();                               // Возвращение данных пользователя
        $this->includeComponentTemplate($this->page);       // Подключение шаблона
    }
    //endregion
    //region Возвращение массива request
    /**
     * Возвращение массива request
     */
    private function getRequest() {
        $request = Application::getInstance()->getContext()->getRequest();
        $this->arPost = $request->getPostList()->toArray();
        $this->arGet = $request->getQueryList()->toArray();
    }
    //endregion
    //region Подключение модулей
    /**
     * Подключение модулей
     */
    private function requireModule() {
        Loader::includeModule('iblock');
    }
    //endregion
    //region Возвращает список организаций и список договоров доступные суперпользователю
    /**
     * Возвращает список организаций и список договоров доступные суперпользователю
     */
    private function getContractData() {
        if(empty($this->arParams['USER_ID'])) {
            global $USER;
            $this->arParams['USER_ID'] = $USER->GetID();
        }
        $userID = $this->arParams['USER_ID'];
        $arOrderContracts = array('ID' => 'ASC');
        $arFilterContracts = array(
            'IBLOCK_CODE' => $this->arParams['IBLOCK_CODE_CONTRACTS'],
            'PROPERTY_PROP_SUPER_USER' => $userID
        );
        $arSelectContracts = array('ID', 'NAME', 'IBLOCK_ID', 'PROPERTY_PROP_COMPANY', 'PROPERTY_PROP_ID_REGISTRY');
        $rsContracts = CIBlockElement::GetList($arOrderContracts, $arFilterContracts, false, false, $arSelectContracts);
        $arCompanyID = array();
        while($obContracts = $rsContracts->Fetch()) {
            $this->arResult['S_USER_DATA']['LIST_COMPANY'][$obContracts['ID']] = $obContracts['PROPERTY_PROP_COMPANY_VALUE'];
            $this->arResult['S_USER_DATA']['CONTRACTS'][$obContracts['ID']] = $obContracts['NAME'];
            $this->arResult['S_USER_DATA']['LIST_CONTRACTS'][$obContracts['ID']] = $obContracts['PROPERTY_PROP_ID_REGISTRY_VALUE'];

            $arCompanyID[] = $obContracts['PROPERTY_PROP_COMPANY_VALUE'];
            foreach($obContracts['PROPERTY_PROP_ID_REGISTRY_VALUE'] as $idRegistry) {
                $this->arResult['S_USER_DATA']['LIST_REGISTRY'][$idRegistry] = $obContracts['ID'];
            }
        }
        $this->getListCompany($arCompanyID);
    }
    //endregion
    //region Возвращает список организаций доступных суперпользователю
    /** Возвращает список организаций доступных суперпользователю
     * @param $arCompanyID - массив с id организаций доступных пользователю
     */
    private function getListCompany($arCompanyID) {
        $arOrderCompany = array('NAME' => 'ID');
        $arFilterCompany = array(
            'IBLOCK_CODE' => $this->arParams['IBLOCK_CODE_COMPANY'],
            'ID' => $arCompanyID
        );
        $arSelectCompany = array('ID', 'NAME', 'IBLOCK_ID');
        $rsCompany = CIBlockElement::GetList($arOrderCompany, $arFilterCompany, false, false, $arSelectCompany);
        while($obCompany = $rsCompany->Fetch()) {
            $this->arResult['COMPANY_LIST'][$obCompany['ID']] = $obCompany['NAME'];
        }
    }
    //endregion
    //region Возвращает список реестров доступные суперпользователю
    /**
     * Возвращает список реестров доступные суперпользователю
     */
    private function getListRegistry() {
        $rsRegistry = \Bitrix\Iblock\IblockTable::getList(array(
            'filter' => array('ID' => array_keys($this->arResult['S_USER_DATA']['LIST_REGISTRY'])),
            'select' => array('ID', 'NAME')
        ));
        while($obRegistry = $rsRegistry->fetch()) {
            $this->arResult['S_USER_DATA']['REGISTRY'][$obRegistry['ID']] = $obRegistry['NAME'];
        }
    }
    //endregion
    //region Обновление данных пользователя
    /**
     * Обновление данных пользователя
     */
    private function editUser() {
        if($this->arPost['F_USER_EDIT'] == 'Y') {
            $this->getFullName();
            $arLoadIBlockClient = array(
                'NAME' => $this->arResult['USER_FULL_NAME'],
                'ACTIVE' => 'Y'
            );
            $arLoadIBlockClientProperty = array('PROP_REGISTRY' => array());
            $arLoadBitrixUser = array(
                'NAME' => '',
                'ACTIVE' => 'Y',
                'LAST_NAME' => '',
                'SECOND_NAME' => '',
                'WORK_POSITION' => '',
                'LOGIN' => '',
                'EMAIL' => '',
                'PASSWORD' => '',
            );
            foreach ($this->arPost as $cPost => $vPost) {
                $code = str_replace($this->prefix, '', $cPost);
                if($code == 'PASSWORD') {
                    if($this->arPost[$cPost] == '') {
                        unset($arLoadBitrixUser['PASSWORD']);
                    } else {
                        $arLoadBitrixUser['PASSWORD'] = $this->arPost[$cPost];
                    }
                } elseif($code == 'WORK_COMPANY') {
                    $arLoadBitrixUser['WORK_COMPANY'] = $this->arResult['COMPANY_LIST'][$this->arPost[$cPost]];
                } elseif($code == 'NAME') {
                    $arLoadBitrixUser['NAME'] = $this->arPost[$cPost];
                } elseif($code == 'PROP_REGISTRY') {
                    $arLoadIBlockClientProperty[$code] = $this->arPost[$cPost];
                } elseif($code == 'ACTIVE') {
                    $arLoadIBlockClient[$code] = $this->arPost[$cPost];
                    $arLoadBitrixUser[$code] = $this->arPost[$cPost];
                } else {
                    if(array_key_exists($code, $arLoadIBlockClient)) {
                        $arLoadIBlockClient[$code] = $this->arPost[$cPost];
                    } elseif(array_key_exists($code, $arLoadBitrixUser)) {
                        $arLoadBitrixUser[$code] = $this->arPost[$cPost];
                    }
                }
            }
            $user = new CUser;
            $arLoadBitrixUser['LOGIN'] = $this->arPost[$this->prefix . 'EMAIL'];
            $user->Update($this->arPost[$this->prefix . 'ID'], $arLoadBitrixUser);
            $el = new CIBlockElement;
            $clientID = $this->arPost[$this->prefix . 'ID_IN_IBLOCK'];
            $el->Update($clientID, $arLoadIBlockClient);
            CIBlockElement::SetPropertyValuesEx(
                $clientID,
                $this->arPost[$this->prefix . 'IBLOCK_ID'],
                $arLoadIBlockClientProperty);
        }
    }
    //endregion
    //region Возвращение данных пользователя
    /**
     * Возвращение данных пользователя
     */
    private function getDataUser() {
        $this->arResult['PREFIX'] = $this->prefix;
        $arOrderUser = array('ID' => 'ASC');
        $arFilterUser = array(
            'IBLOCK_CODE'           => $this->arParams['IBLOCK_CODE_CLIENTS'],
            'PROPERTY_PROP_CLIENT'  => $this->arParams['SUB_USER_ID']
        );
        $arSelectUser = array(
            'ID',
            'IBLOCK_ID',
            'NAME',
            'ACTIVE',
            'PROPERTY_PROP_CLIENT',
            'PROPERTY_PROP_CLIENTS_OF_SUPERUSER',
            'PROPERTY_PROP_REGISTRY',
        );
        $rsUser = CIBlockElement::GetList($arOrderUser, $arFilterUser, false, false, $arSelectUser);
        if($obUser = $rsUser->Fetch()) {
            $this->arResult['USER_DATA'] = array(
                'ID_IN_IBLOCK'                          => $obUser['ID'],
                'IBLOCK_ID'                             => $obUser['IBLOCK_ID'],
                'NAME_IN_IBLOCK'                        => $obUser['ACTIVE'],
                'PROPERTY_PROP_CLIENT'                  => $obUser['PROPERTY_PROP_CLIENT_VALUE'],
                'PROPERTY_PROP_CLIENTS_OF_SUPERUSER'    => $obUser['PROPERTY_PROP_CLIENTS_OF_SUPERUSER_VALUE'],
                'PROPERTY_PROP_REGISTRY'                => $obUser['PROPERTY_PROP_REGISTRY_VALUE']
            );
            if(!empty($obUser['PROPERTY_PROP_REGISTRY_VALUE'])) {
                foreach($obUser['PROPERTY_PROP_REGISTRY_VALUE'] as $idRegistry) {
                    if(!in_array($this->arResult['S_USER_DATA']['LIST_REGISTRY'][$idRegistry], $this->arResult['USER_DATA']['CONTRACTS'])) {
                        $this->arResult['USER_DATA']['CONTRACTS'][] = $this->arResult['S_USER_DATA']['LIST_REGISTRY'][$idRegistry];
                    }
                }
            }
        }
        if(!empty($this->arResult['USER_DATA']['CONTRACTS'])) {
            foreach($this->arResult['USER_DATA']['CONTRACTS'] as $idContracts) {
                if(!in_array($this->arResult['S_USER_DATA']['LIST_COMPANY'][$idContracts], $this->arResult['USER_DATA']['LIST_COMPANY'])) {
                    $this->arResult['USER_DATA']['LIST_COMPANY'][] = $this->arResult['S_USER_DATA']['LIST_COMPANY'][$idContracts];
                }
            }
        }
        $arFilter = array('ID' => $this->arResult['USER_DATA']['PROPERTY_PROP_CLIENT']);
        $arParameters['FIELDS'] = array(
            'ID',
            'ACTIVE',
            'NAME',
            'LAST_NAME',
            'SECOND_NAME',
            'EMAIL',
            'PERSONAL_PHONE',
            'PERSONAL_MOBILE',
            'WORK_POSITION',
            'WORK_COMPANY'
        );
        $arUser = CUser::GetList($by = 'name', $order = 'desc', $arFilter, $arParameters)->Fetch();
        foreach($arUser as $cUser => $vUser) {
            $this->arResult['USER_DATA'][$cUser] = $vUser;
        }
        if($this->arResult['USER_FULL_NAME'] == '') {
            $this->getFullName();
        }
    }
    //endregion
    //region Метод возвращает полное имя пользователя (ФИО)
    /**
     * Функция возвращает полное имя пользователя (ФИО)
     */
    private function getFullName() {
        if(!empty($this->arResult['USER_DATA']) || $this->arPost['F_USER_EDIT'] == 'Y') {
            $arName = array(
                'LAST_NAME' => $this->arResult['USER_DATA']['LAST_NAME'],
                'NAME' => $this->arResult['USER_DATA']['NAME'],
                'SECOND_NAME' => $this->arResult['USER_DATA']['SECOND_NAME'],
            );
            if($this->arPost['F_USER_EDIT'] == 'Y') {
                $arName = array(
                    'LAST_NAME' => $this->arPost[$this->prefix . 'LAST_NAME'],
                    'NAME' => $this->arPost[$this->prefix . 'NAME'],
                    'SECOND_NAME' => $this->arPost[$this->prefix . 'SECOND_NAME'],
                );
            }
            $fullName = '';
            foreach($arName as $cName => $vName) {
                if(!empty($fullName)) {
                    $fullName .= ' ';
                }
                $fullName .= $vName;
            }
            $this->arResult['USER_FULL_NAME'] = $fullName;
            if($this->arResult['USER_FULL_NAME'] != $this->arResult['USER_DATA']['NAME_IN_IBLOCK']) {
                $el = new CIBlockElement;
                $arLoadUser['NAME'] = $this->arResult['USER_FULL_NAME'];
                $el->Update($this->arResult['USER_DATA']['ID_IN_IBLOCK'], $arLoadUser);
            }
        }
    }
    //endregion
}
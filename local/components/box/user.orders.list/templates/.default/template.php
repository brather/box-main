<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

use \Bitrix\Main\Application,
    \Bitrix\Main\Localization\Loc;
$request = Application::getInstance()->getContext()->getRequest();
$arGet = $request->getQueryList()->toArray();
$htmlHideUrl = '
    <a href="#" 
        class="portlet-content-toggle url_close"
        data-confirm-title="' . Loc::getMessage('SETTINGS_FILTER_SAVE_TITLE') . '" 
        data-confirm-question="' . Loc::getMessage('SETTINGS_FILTER_SAVE_QUESTION') . '" 
        data-confirm-answer-y="' . Loc::getMessage('SETTINGS_FILTER_SAVE_Y') . '" 
        data-confirm-answer-n="' . Loc::getMessage('SETTINGS_FILTER_SAVE_N') . '"> 
    </a>';
?>

<div class="content__item _top top_panel">
    <?if(!empty($arParams['HTML_PAGE_TITLE'])) {?>
        <?= urldecode($arParams['HTML_PAGE_TITLE']) ?>
    <?}?>
    <?if(!empty($arResult['LIST_ORDERS'])) {?>
        <div class="scrollbar-outer scrollbar-center">
            <form action="<?= $arResult['FILTER_ACTION'] ?>" id="form-filter" class="form form__fl-left" method="get">
                <input type="hidden" name="F_ORDER_FILTER" value="Y">
                <div class="form__row form__row__sortable" id="sortable">
                    <a href="#" class="btn-settings save-filter hidden" title="<?= Loc::getMessage('SETTIONGS_FILTER_TITLE') ?>">
                        <i class="fa fa-cog"></i>
                    </a>
                    <ul class="list-filter-settings" style="display: none;">
                        <li>
                            <a href="#" class="lfs-save">
                                <i class="fa fa-save"></i><?= Loc::getMessage('SETTIONGS_FILTER_SAVE') ?>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="lfs-delete">
                                <i class="fa fa-close"></i><?= Loc::getMessage('SETTIONGS_FILTER_DELETE') ?>
                            </a>
                        </li>
                    </ul>
                    <div class="column form__row__col _size4 ui-sortable">
                        <?foreach($arResult['ROW_SORT'] as $cSort => $arSort) {?>
                            <?if($arSort['FLAG']){?>
                                <div class="portlet" data-portlet="<?= $arSort['INDEX'] ?>">
                                    <div class="portlet-header">
                                        <?= Loc::getMessage('C_ORDER_LIST_' . $arSort['CODE'])?><?= $htmlHideUrl ?>
                                    </div>
                                    <div class="portlet-content">
                                        <?if($arSort['CODE'] == 'PROPERTY_STATUS') {?>
                                            <select name="<?= $arResult['FILTER_PREFIX'] ?>PROPERTY_STATUS[]"
                                                    multiple
                                                    size="2"
                                                    id="status-select" >
                                                <option value=""><?= Loc::getMessage('FILTER_EMPTY_SELECT') ?></option>
                                                <?foreach($arResult['WMS_STATUS'] as $cStatus => $arStatus) {
                                                    $select = '';
                                                    if($arGet[$arResult['FILTER_PREFIX'] . 'PROPERTY_STATUS'] == $arStatus['UF_XML_ID']) {
                                                        $select = ' selected';
                                                    }?>
                                                    <option value="<?= $arStatus['UF_XML_ID'] ?>"<?= $select ?>>
                                                        <?= $arStatus['UF_NAME'] ?>
                                                    </option>
                                                <?}?>
                                            </select>
                                        <?} elseif(stristr($arSort['CODE'], 'DATE')) {?>
                                            <input type="text"
                                                   class="form__field js-datepicker"
                                                   name="<?= $arResult['FILTER_PREFIX'] ?><?= $arSort['CODE'] ?>"
                                                   value="<?= $arGet[$arResult['FILTER_PREFIX'] . $arSort['CODE']] ?>"
                                                   placeholder="date...">
                                            <a href="#" class="js-reset-input"><i class="fa fa-close"></i></a>
                                        <?} else {?>
                                            <input type="text"
                                                   class="form__field"
                                                   name="<?= $arResult['FILTER_PREFIX'] ?><?= $arSort['CODE'] ?>"
                                                   value="<?= $arGet[$arResult['FILTER_PREFIX'] . $arSort['CODE']] ?>"
                                                   placeholder="text...">
                                            <a href="#" class="js-reset-input"><i class="fa fa-close"></i></a>
                                        <?}?>
                                    </div>
                                </div>
                            <?}?>
                        <?}?>
                    </div>
                </div>
                <div class="form__row">
                    <button type="submit" class="btn"><?= Loc::getMessage('USER_REG_T_BUTTON_APPLY') ?></button>
                    <a href="<?= $arParams['SEF_FOLDER'] ?>" class="btn _white">
                        <?= Loc::getMessage('USER_REG_T_BUTTON_RESET') ?>
                    </a>
                </div>
            </form>
        </div>
    <?}
    else {?>
        <div class="empty-order-list" id="empty-list">
            <p class="title-empty">
                <?
                if($arParams['REQUEST'] == 'Y') {?>
                    <?if(in_array(U_GROUP_CODE_CLIENT_S_USER, $arParams['USER_GROUPS'])) {?>
                        <?= Loc::getMessage('T_S_USER_REQUEST_LIST_EMPTY_TEXT_1') ?>
                        <a href="<?= $arParams['FOLDER'] ?>create/"><?= Loc::getMessage('T_S_USER_REQUEST_LIST_EMPTY_URL_CREATE') ?></a>
                        <?= Loc::getMessage('T_S_USER_REQUEST_LIST_EMPTY_TEXT_2') ?>
                        <a href="<?= $arParams['FOLDER'] ?>"><?= Loc::getMessage('T_S_USER_REQUEST_LIST_EMPTY_URL_REQUEST') ?></a>
                        <?= Loc::getMessage('T_S_USER_REQUEST_LIST_EMPTY_TEXT_3') ?>
                    <?} elseif(in_array(U_GROUP_CODE_CLIENT_USER, $arParams['USER_GROUPS'])) {?>
                        <?= Loc::getMessage('T_USER_REQUEST_LIST_EMPTY_TEXT_1') ?>
                        <a href="<?= $arParams['FOLDER'] ?>create/"><?= Loc::getMessage('T_USER_REQUEST_LIST_EMPTY_URL_CREATE') ?></a>
                        <?= Loc::getMessage('T_S_USER_REQUEST_LIST_EMPTY_TEXT_3') ?>
                    <?}?>
                <?} else {?>
                    <? if (in_array('completed', $arParams['PAGE_TYPES_FROM_STATUS']) || in_array('formation', $arParams['PAGE_TYPES_FROM_STATUS'])) :?>
                        <?= Loc::getMessage('T_ORDER_LIST_EMPTY_TEXT_COMPLETED') ?>
                    <? elseif (in_array('processing', $arParams['PAGE_TYPES_FROM_STATUS'])) :?>
                        <?= Loc::getMessage('T_ORDER_LIST_EMPTY_TEXT_1') ?>
                        <a href="<?= $arParams['FOLDER'] ?>create/"><?= Loc::getMessage('T_ORDER_LIST_EMPTY_URL_CREATE') ?></a>
                        <?= Loc::getMessage('T_S_USER_REQUEST_LIST_EMPTY_TEXT_3') ?>
                    <? elseif (in_array('canceled', $arParams['PAGE_TYPES_FROM_STATUS'])) :?>
                        <?= Loc::getMessage('T_ORDER_LIST_EMPTY_TEXT_DELETED')?>
                    <? endif;?>
                <?}?>
            </p>
        </div>
    <?}?>
</div>
<div class="content__item _bottom">
    <?if(!empty($arResult['LIST_ORDERS'])) {?>
        <div class="scrollbar-outer">
            <div class="control">
                <div class="control__result">
                    <?= Loc::getMessage('C_ORDER_LIST_COUNT_UNITS_FINDED')?><?= $arResult['COUNT_ORDERS'] ?>
                </div>
            </div>
            <table class="table js-table" id="orders-list">
                <thead>
                <tr>
                    <? if (in_array('formation', $arParams['PAGE_TYPES_FROM_STATUS'])) :?>
                    <th>#</th>
                    <? endif; ?>
                    <?foreach($arResult['ROW_SORT'] as $cField => $arField) {?>
                        <th>
                            <a class="filtr" href="<?= $arField['URL'] ?>">
                                <?= Loc::getMessage('C_ORDER_LIST_' . $arField['CODE']) ?>
                            </a>
                        </th>
                    <?}?>
                </tr>
                </thead>
                <tbody>
                <?foreach($arResult['LIST_ORDERS'] as $iOrder => $arOrder) {
                    $clsRow = '';
                    if($arOrder['PROPERTY_STATUS_XML_ID'] == 'canceled') {
                        $clsRow = 'no-active';
                    }
                    $title = '';
                    if($arOrder['PROPERTY_FORMING'] == 'Y') {
                        if($clsRow != '') {
                            $clsRow .= ' ';
                        }
                        $title = Loc::getMessage('C_ORDER_LIST_PROPERTY_FORMING');
                        $clsRow .= 'js-tooltip bgd-forming';
                    }
                    if($arOrder['ERROR']) {
                        $title = $arOrder['ERROR'];
                    }?>
                    <tr class="<?= $clsRow ?><?= $arOrder['ERROR_CLASS'] ?>" title="<?= $title ?>">
                        <? if (in_array('formation', $arParams['PAGE_TYPES_FROM_STATUS'])) :?>
                        <td>
                            <i class="fa fa-trash" data-id="<?=$arOrder['ID']?>"></i>
                        </td>
                        <? endif; ?>
                        <?foreach($arParams['SET_SORT_FILTER'] as $cField) {?>
                            <td>
                                <?if($cField == 'NAME') {
                                    $detailUri = $arParams['SEF_FOLDER'] . $arOrder['ID'] . '/';
                                    if(!empty($arOrder['DETAIL_URL']) && $arOrder['PROPERTY_FORMING'] == 'Y') {
                                        $detailUri = $arOrder['DETAIL_URL'];
                                    }?>
                                    <a href="<?= $detailUri ?>" class="order-detail">
                                        <?= $arOrder[$cField] ?>
                                        <!--<span><i class="fa fa-eye"></i></span>-->
                                    </a>
                                <?} else if ($cField === 'PROPERTY_STATUS') {?>
                                    <span data-order-id="<?= $arOrder['ID']?>" id="orderStatus_<?=$arOrder['ID']?>" class="fa fa-icon fa-spin fa-spinner orderLoaderStatus"></span>
                                <?} else {?>
                                    <?= $arOrder[$cField] ?>
                                <?}?>
                            </td>
                        <?}?>
                    </tr>
                <?}?>
                </tbody>
            </table>
            <div class="navigation">
                <?= $arResult['NAV_PRINT'] ?>
                <div class="pager">
                    <div class="pager__current">
                        <span><?= Loc::getMessage('USER_REG_T_COUNT_ITEMS') ?></span>
                        <form action="<?= $arResult['FORM_PATH_DEFAULT'] ?>" method="GET" class="nav-form">
                            <?= getHTMLInputExcludeParam($arGet, 'COUNT_ON_PAGE') ?>
                            <input type="text" name="COUNT_ON_PAGE" value="<?= $arResult['COUNT_ON_PAGE'] ?>">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <?}
    else {?>

    <?}?>
</div>
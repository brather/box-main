<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Application;

$request = Application::getInstance()->getContext()->getRequest();
$arGet = $request->getQueryList()->toArray();
$arPost = $request->getPostList()->toArray();

$arResult['FILTER_ACTION'] = $arServer['REQUEST_URI'];
if($arServer['REDIRECT_URL']) {
    $arResult['FILTER_ACTION'] = $arServer['REDIRECT_URL'];
}
if($arParams['CURRENT_PATH']) {
    $arResult['FILTER_ACTION'] = $arParams['CURRENT_PATH'];
}


$arExceptionFromRow = array(
    'IBLOCK_NAME',
    'NAME'
);
$iSort = 0;
foreach($arResult['ROW_SORT'] as $cSort => $arSort) {
    if(in_array($cSort, $arExceptionFromRow)) {
        $arResult['ROW_SORT'][$cSort]['FLAG'] = false;
        $arResult['ROW_SORT'][$cSort]['INDEX'] = $iSort;
    } else {
        $arResult['ROW_SORT'][$cSort]['FLAG'] = true;
        $arResult['ROW_SORT'][$cSort]['INDEX'] = $iSort;
    }
    $iSort++;
}

$arResult['FILTER_PREFIX'] = 'F_ORDER_';

$arResult['COUNT_ON_PAGE'] = $arParams['COUNT_ITEM_ON_PAGE'];
if(!empty($arGet['COUNT_ON_PAGE']) && is_numeric($arGet['COUNT_ON_PAGE'])) {
    $arResult['COUNT_ON_PAGE'] = $arGet['COUNT_ON_PAGE'];
}
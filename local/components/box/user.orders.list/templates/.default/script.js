$(document).ready(function(){
    //region Установка для фильтра функции перетаскивания
    var sortable    = {
            contain : '#vertical',
            form    : '#sortable',
            column  : '.column',
            columns : 'column',
            item    : '.portlet',
            iheader : '.portlet-header',
            icancel : '.portlet-toggle',
            icontent: '.portlet-content',
            iplaceh : 'portlet-placeholder ui-corner-all',
            iadd    : 'ui-widget ui-widget-content ui-helper-clearfix ui-corner-all',
            ihadd   : 'ui-widget-header ui-corner-all',
            span    : 'ui-icon ui-icon-minusthick portlet-toggle',
            toggle  : '.portlet-content-toggle',
            hidden  : 'hidden',
            split   : {
                top         : '.top_panel',
                bottom      : '.bottom_panel',
                left        : '.left_panel',
                right       : '.right_panel'
            }
        },
        filter      = {
            list        : '.list-filter-settings',
            listshow    : 'lfs-show',
            toggle      : '.btn-settings',
            fa          : '.fa',
            spin        : 'fa-spin',
            link        : {
                save    : '.lfs-save',
                delete  : '.lfs-delete',
                revert  : '.lfs-revert'
            }
        },
        stData      = {
            default         : 'Y',
            columnObj       : {},
            hiddenObj       : {},
            posSplitHor     : 30
        },
        objLS       = {
            default : 'Y'
        },
        split       = {
            contain : sortable.contain,
            split   : '.hsplitter',
            empty   : '#empty-list',
            top     : {
                f   : '_top',
                s   : 'top_panel'
            },
            bottom  : {
                f   : '_bottom',
                s   : 'bottom_panel'
            }
        },
        forming     = '.bgd-forming',
        tooltipcls  = '.js-tooltip',
        togglespeed = 400;
    //endregion
    $(tooltipcls).tooltip({
        position    : { my  : 'left bottom+100%' },
        tooltipClass: 'row__tooltip'
    });
    //region Если список заказов пуст то убираем разбивку рабочей области
    if($(split.empty).length > 0) {
        setTimeout(function(){
            var cont = $(split.contain),
                top = cont.find('.' + split.top.f),
                bottom = cont.find('.' + split.bottom.f);
            $(split.contain).find(split.split).remove();
            top.removeClass(split.top.f).removeClass(split.top.s);
            bottom.remove();
        }, 100);
    }
    //endregion
    //region Функция запуска sortable для блоков в колонках
    function startSortable() {
        $(sortable.column).sortable({
            connectWith: sortable.column,
            handle: sortable.iheader,
            cancel: sortable.icancel,
            placeholder: sortable.iplaceh
        });
        $(sortable.item).addClass(sortable.iadd)
            .find(sortable.iheader).addClass(sortable.ihadd);
    }
    //endregion
    //region Скрытие блока с input
    $(document).on('click', sortable.toggle, function(e) {
        e.preventDefault();
        var $this = $(this),
            blkToHide = $this.closest(sortable.item),
            confirm = {
                title       : $this.data('confirm-title'),
                text        : $this.data('confirm-question'),
                answery     : $this.data('confirm-answer-y'),
                answern     : $this.data('confirm-answer-n')
            };
        $.confirm({
            theme: 'light',
            backgroundDismiss : true,
            animation           : 'top',
            closeAnimation      : 'bottom',
            title               : confirm.title,
            content             : confirm.text,
            buttons             : {
                confirm: {
                    keys : ['enter'],
                    text : confirm.answery,
                    action : function () {
                        blkToHide.addClass(sortable.hidden);
                    }
                },
                cancel: {
                    keys : ['esc'],
                    text : confirm.answern,
                    action : function () {}
                }
            }
        });
    });
    //endregion
    //region Если настройки фильтра уже имеются
    if(localStorage.stdataorder) {
        objLS = $.parseJSON(localStorage.stdataorder);
    }
    if(objLS.default == 'N') {
        stData = {
            default         : objLS.default,
            columnObj       : objLS.columnObj,
            hiddenObj       : objLS.hiddenObj,
            posSplitHor     : objLS.posSplitHor
        };
        //region Показ кнопки настроек фильтра
        $(sortable.form).find(filter.toggle).removeClass(sortable.hidden);
        //endregion
        if(countOfOject(stData.hiddenObj) > 0) {
            $.each(stData.hiddenObj, function(ihide, yhide){
                $(sortable.form).find(sortable.item + '[data-portlet="' + yhide + '"]').addClass(sortable.hidden);
            });
        }
    }
    //endregion
    //region Разбивка рабочей области со списком ЕУ реестра и фильтра
    $(sortable.contain).split({
        orientation : 'horizontal',
        limit       : 100,
        position    : stData.posSplitHor + '%',
        onDragEnd   : function() {
            if($(sortable.form).find(filter.toggle).hasClass(sortable.hidden)) {
                $(sortable.form).find(filter.toggle).removeClass(sortable.hidden);
            }
        }
    });
    setColumn(false);
    //endregion
    //region Функция разбивки контейнера по столбцам с учетом количества блоков внутренних
    /**
     * Функция разбивки контейнера по столбцам с учетом количества блоков внутренних
     * @param changeWidth - boolean, если true то функция запущена при изменении размера экрана, если false то при загрузке страницы
     */
    function setColumn(changeWidth) {
        var form = $(sortable.form),
            wForm = form.width(),
            wColumn = form.find(sortable.column).outerWidth(true),
            cColumn = Math.floor(wForm / wColumn),
            tempCls = 'temp',
            nColumn = 1;

        if(stData.default == 'N' && !changeWidth) {
            cColumn = countOfOject(stData.columnObj);
        }

        if(form.find(sortable.column).length != cColumn) {
            form.append('<div class="' + tempCls + '"></div>');
            var tempBlk = form.find('.' + tempCls),
                iColumn = 1;
            tempBlk.append(form.find(sortable.item));
            form.find(sortable.column).remove();

            while(iColumn <= cColumn) {
                form.append('<div class="' + sortable.columns + ' form__row__col _size4 column-' + iColumn + '"></div>');
                if(stData.default == 'N' && !changeWidth) {
                    $.each(stData.columnObj['column-' + iColumn], function(i, y) {
                        form.find('.column-' + iColumn).append(form.find('[data-portlet="' + y + '"]'));
                    });
                }
                iColumn = iColumn + 1;
            }
            if(stData.default != 'N' || changeWidth) {
                form.find(sortable.item).each(function(i, y){
                    var $this = $(y),
                        blkTo = form.find('.column-' + nColumn);
                    if(nColumn < cColumn) {
                        nColumn = nColumn + 1;
                    } else {
                        nColumn = 1;
                    }
                    blkTo.append($this);
                });
            }
            form.find('.' + tempCls).remove();
        } else if(stData.default == 'N' && !changeWidth) {
            form.find(sortable.column).each(function(icol, ycol) {
                iColumn = icol + 1;
                $.each(stData.columnObj['column-' + iColumn], function(i, y) {
                    form.find('.column-' + iColumn).append(form.find('[data-portlet="' + y + '"]'));
                });
            });
        }
        startSortable();
    }
    //endregion
    //region Сохранение настроек фильтра
    $(document).on('click', filter.link.save, function(e){
        e.preventDefault();
        var $this = $(this),
            list = $this.closest(filter.list),
            icon = $(sortable.form).find(filter.toggle).find(filter.fa);
        saveDataToLocalStorage();
        icon.addClass(filter.spin);
        list.toggle(togglespeed, function() {
            icon.removeClass(filter.spin);
        });
    });
    //endregion
    //region Сброс настроект фильтра
    $(document).on('click', filter.link.delete, function(e){
        e.preventDefault();
        localStorage.removeItem('stdataorder');
        location.reload();
    });
    //endregion
    //region Показ и скрытие списка опций настроек фильтра
    $(document).on('click', filter.toggle, function(e) {
        e.preventDefault();
        var $this = $(this),
            icon = $this.find(filter.fa),
            list = $this.closest(sortable.form).find(filter.list);
        icon.addClass(filter.spin);
        list.toggle(400, function() {
            icon.removeClass(filter.spin);
        });
    });
    $(document).click(function(e){
        var toggleUri = $(sortable.form).find(filter.toggle),
            icon = toggleUri.find(filter.fa),
            settingsList = $(sortable.form).find(filter.list);
        if ($(e.target).closest(toggleUri).length
            || $(e.target).closest(settingsList).length) {
            return;
        }
        if(settingsList.is(':visible')) {
            icon.addClass(filter.spin);
            settingsList.toggle(400, function() {
                icon.removeClass(filter.spin);
            });
        }
    });
    //endregion
    //region Отправка формы без пустых параметров
    checkSendForm('#form-filter');
    //endregion
    //region Показ кнопки для очистки содержимого в input
    hoverInput('.portlet-content');
    //endregion
    //region Функция сохранения данных в локальное хранилище
    function saveDataToLocalStorage() {
        stData.default = 'N';
        stData.posSplitHor = Math.floor($(sortable.contain).children(sortable.split.top).height() / $(sortable.contain).height() * 100);

        $(sortable.form).find(sortable.column).each(function(icol, ycol) {
            var iColumn = icol + 1,
                clsColumn = 'column-' + iColumn;
            stData.columnObj[clsColumn] = {};
            $(ycol).find(sortable.item).each(function(iport, yport) {
                stData.columnObj[clsColumn][iport] = $(yport).data('portlet');
            });
        });
        $(sortable.form).find(sortable.item + '.' + sortable.hidden).each(function(ihide, yhide) {
            stData.hiddenObj[ihide] = $(yhide).data('portlet');
        });
        localStorage.setItem("stdataorder", JSON.stringify(stData));
    }
    //endregion

    //region Событие удаления Черновика
    $(document).on('click', 'i.fa.fa-trash', function () {
        var orderId = $(this).attr('data-id'),
            $this = $(this);

        $.confirm({
            animation           : 'top',
            closeAnimation      : 'bottom',
            theme               : 'light',
            backgroundDismiss   : true,
            title               : "Сохранено как Черновик",
            content: "Черновик <b>ID - " + orderId + "</b> будет безвозвратно удален.<br>Вы уверены в своем выборе?",
            buttons             : {
                ok : {
                    keys    : ['enter', 'space'],
                    text    : 'Удалить',
                    action  : function () {
                        $.ajax({
                            url: '/local/components/box/user.orders/ajax.php',
                            method: 'POST',
                            dataType: 'json',
                            data: {ORDER_ID: orderId},
                            success: function (json) {
                                if (json.REMOVED === '1') {
                                    $this.parents('tr').fadeOut(700, function(){
                                        $(this).remove();
                                    });
                                } else {
                                    console.error(json.ERRORS);
                                }
                            },
                            error: function (xhr, st, ms) {
                                console.error(xhr, st, ms);
                            }
                        });
                    }
                },
                cancel: {
                    keys    :['esc'],
                    text    : 'Отмена'
                }
            }
        });
    });
    //endregion

    $('.orderLoaderStatus').each(function(i, v) {
        var self = v;
        $.ajax({
            url: '/local/components/box/user.orders/orderstatus.php',
            method: 'POST',
            dataType: 'JSON',
            data: {
                ORDER_ID: self.dataset.orderId
                //ORDER_ID: "1212"
            },
            success: function (res) {
                if (typeof res === 'object') {
                    if ("status" in res) {
                        $('#orderStatus_' + self.dataset.orderId).removeClass('fa fa-icon fa-spin fa-spinner').text(res.status);
                    }
                }
            },
            error: function (xhr, st, msg) {
                $('#orderStatus_' + self.dataset.orderId).removeClass('fa fa-icon fa-spin fa-spinner').html("<span class='frn-danger'>" + xhr.responseJSON.error + "</span>");
            }
        });
    });
});
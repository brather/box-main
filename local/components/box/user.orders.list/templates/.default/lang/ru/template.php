<?php
$MESS['C_ORDER_LIST_PROPERTY_DATE'] = 'Срок исполнения ';
$MESS['C_ORDER_LIST_DATE_CREATE'] = 'Дата оформления ';
$MESS['C_ORDER_LIST_ID'] = 'Номер заказа ';
$MESS['C_ORDER_LIST_NAME'] = 'Название ';
$MESS['C_ORDER_LIST_PROPERTY_TYPE_ORDER'] = 'Тип заказа ';
$MESS['C_ORDER_LIST_CREATED_BY'] = 'Оформил ';
$MESS['C_ORDER_LIST_PROPERTY_STATUS'] = 'Статус заказа ';

$MESS['C_ORDER_LIST_COUNT_UNITS'] = 'Количество ЕУ';
$MESS['C_ORDER_LIST_COUNT_UNITS_FINDED'] = 'Найдено: ';

$MESS['SETTIONGS_FILTER_TITLE'] = 'Настройки фильтра';
$MESS['SETTIONGS_FILTER_SAVE'] = ' Сохранить';
$MESS['SETTIONGS_FILTER_DELETE'] = ' Вид по умолчанию';
$MESS['SETTIONGS_FILTER_REVERT'] = ' Вернуть';

$MESS['SETTINGS_FILTER_SAVE_TITLE'] = 'Скрытие поля';
$MESS['SETTINGS_FILTER_SAVE_QUESTION'] = 'Вы действительно хотите скрыть поле? Если не будут сохранены настройки внешнего вида то после обновления страницы скрытые поля появятся снова.';
$MESS['SETTINGS_FILTER_SAVE_Y'] = 'Скрыть';
$MESS['SETTINGS_FILTER_SAVE_N'] = 'Отмена';

$MESS['USER_REG_T_BUTTON_APPLY'] = 'Применить';
$MESS['USER_REG_T_BUTTON_RESET'] = 'Сбросить';

$MESS['FILTER_EMPTY_SELECT'] = '-';
$MESS['USER_REG_T_COUNT_ITEMS'] = 'Количество ЕУ на странице: ';

$MESS['T_ORDER_LIST_EMPTY_TEXT_1'] = 'Список заказов пуст. ';
$MESS['T_ORDER_LIST_EMPTY_TEXT_COMPLETED'] = 'Еще ни один заказ не выполнен. ';
$MESS['T_ORDER_LIST_EMPTY_TEXT_DELETED'] = 'Еще ни один заказ не удален.';
$MESS['T_ORDER_LIST_EMPTY_URL_CREATE'] = 'Создать заказ';
$MESS['T_ORDER_LIST_EMPTY_TEXT_2'] = ' либо сформировать его из ';
$MESS['T_ORDER_LIST_EMPTY_URL_REQUEST'] = 'запросов пользователей';
$MESS['T_ORDER_LIST_EMPTY_TEXT_3'] = '.';

$MESS['T_S_USER_REQUEST_LIST_EMPTY_TEXT_1'] = 'На данный момент список запросов пуст. Вы можете ';
$MESS['T_S_USER_REQUEST_LIST_EMPTY_URL_CREATE'] = 'создать заказ';
$MESS['T_S_USER_REQUEST_LIST_EMPTY_TEXT_2'] = ' либо вернуться на  ';
$MESS['T_S_USER_REQUEST_LIST_EMPTY_URL_REQUEST'] = 'страницу списка заказов';
$MESS['T_S_USER_REQUEST_LIST_EMPTY_TEXT_3'] = '.';

$MESS['T_USER_REQUEST_LIST_EMPTY_TEXT_1'] = 'На данный момент список запросов пуст. Вы можете ';
$MESS['T_USER_REQUEST_LIST_EMPTY_URL_CREATE'] = 'создать новый запрос';

$MESS['C_ORDER_LIST_PROPERTY_FORMING'] = 'Неоформленный заказ';
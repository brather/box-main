<?php
$MESS['C_ORDER_LIST_PROPERTY_DATE'] = 'Date of performance ';
$MESS['C_ORDER_LIST_DATE_CREATE'] = 'Date create ';
$MESS['C_ORDER_LIST_ID'] = 'Number ';
$MESS['C_ORDER_LIST_NAME'] = 'Title ';
$MESS['C_ORDER_LIST_PROPERTY_TYPE_ORDER'] = 'Type order ';
$MESS['C_ORDER_LIST_CREATED_BY'] = 'Created by ';
$MESS['C_ORDER_LIST_PROPERTY_STATUS'] = 'Status of order ';

$MESS['C_ORDER_LIST_COUNT_UNITS'] = 'Count of units';
$MESS['C_ORDER_LIST_COUNT_UNITS_FINDED'] = 'Finded: ';

$MESS['SETTIONGS_FILTER_TITLE'] = 'Settings of filter';
$MESS['SETTIONGS_FILTER_SAVE'] = ' Save';
$MESS['SETTIONGS_FILTER_DELETE'] = ' Default';
$MESS['SETTIONGS_FILTER_REVERT'] = ' Revert';

$MESS['SETTINGS_FILTER_SAVE_TITLE'] = 'Hiding fields';
$MESS['SETTINGS_FILTER_SAVE_QUESTION'] = 'Are you sure you want to hide the box? If the settings are not saved after the appearance of the page updates the hidden fields will appear again.';
$MESS['SETTINGS_FILTER_SAVE_Y'] = 'Hide';
$MESS['SETTINGS_FILTER_SAVE_N'] = 'Cancel';

$MESS['USER_REG_T_BUTTON_APPLY'] = 'Apply';
$MESS['USER_REG_T_BUTTON_RESET'] = 'Reset';

$MESS['FILTER_EMPTY_SELECT'] = '-';
$MESS['USER_REG_T_COUNT_ITEMS'] = 'Count units on page: ';

$MESS['T_ORDER_LIST_EMPTY_TEXT_1'] = 'List orders is empty.';
$MESS['T_ORDER_LIST_EMPTY_URL_CREATE'] = 'Create order';
$MESS['T_ORDER_LIST_EMPTY_TEXT_2'] = ' or from ';
$MESS['T_ORDER_LIST_EMPTY_URL_REQUEST'] = 'requests other users';
$MESS['T_ORDER_LIST_EMPTY_TEXT_3'] = ' create order.';
$MESS['T_ORDER_LIST_EMPTY_TEXT_COMPLETED'] = 'No order has been fulfilled yet.';
$MESS['T_ORDER_LIST_EMPTY_TEXT_DELETED'] = 'No orders have been deleted yet.';

$MESS['T_REQUEST_LIST_EMPTY_TEXT_1'] = 'At the moment, the list of requests is empty. You can ';
$MESS['T_REQUEST_LIST_EMPTY_URL_CREATE'] = 'create order';
$MESS['T_REQUEST_LIST_EMPTY_TEXT_2'] = ' or return on  ';
$MESS['T_REQUEST_LIST_EMPTY_URL_REQUEST'] = 'page of list of orders';
$MESS['T_REQUEST_LIST_EMPTY_TEXT_3'] = '.';

$MESS['T_USER_REQUEST_LIST_EMPTY_TEXT_1'] = 'At the moment, the list of requests is empty. You can ';
$MESS['T_USER_REQUEST_LIST_EMPTY_URL_CREATE'] = 'create new request';

$MESS['C_ORDER_LIST_PROPERTY_FORMING'] = 'Forming order';
<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Application,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Loader,
    \Bitrix\Highloadblock as HL;

class UserOrdersListComponent extends CBitrixComponent {
    //region Задание свойств класса
    private $page = 'template';
    private $arGet = array();
    private $arPost = array();
    private $dClassStatusOrder = '';
    private $prefix = 'F_ORDER_';
    //endregion
    //region Подготовка входных параметров компонента
    /** Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        return parent::onPrepareComponentParams($arParams);
    }
    //endregion
    //region Подключение файлов перевода
    /**
     * Подключение файлов перевода
     */
    public function onIncludeComponentLang() {
        Loc::loadMessages(__FILE__);
    }
    //endregion
    //region Выполнение компонента
    /**
     * Выполнение компонента
     */
    public function executeComponent() {
        $this->includeModule();                                 // Подключение модулей
        $this->getRequest();                                    // Определяет массив request данных
        $this->setSort();                                       // Возвращает массив для сортировки полей
        $this->getListWMSStatus();                              // Возвращает список статусов WMS
        $this->getListOrders();                                 // Возвращает список заказов
        //$this->checkSoapData();                                 // Инициализирует класс для синхронизации статусов заказов с базой WMS
        $this->includeComponentTemplate($this->page);           // Подключение шаблона
    }
    //endregion
    //region Подключаем список модулей
    /**
     * Подключаем список модулей
     */
    private function includeModule() {
        Loader::includeModule('iblock');
        Loader::includeModule("highloadblock");
    }
    //endregion
    //region Получение данных request
    /**
     * Получение данных request
     */
    private function getRequest() {
        $request = Application::getInstance()->getContext()->getRequest();
        $this->arGet = $request->getQueryList()->toArray();
        $this->arPost = $request->getPostList()->toArray();
    }
    //endregion
    //region Возвращает массив свойств инфоблока
    /** Возвращает массив свойств инфоблока
     * @param $iblockID - ID инфоблока с реестром
     * @return array
     */
    private function getProperty($iblockID) {
        $rsProperty = CIBlock::GetProperties($iblockID);
        $arReturn = array();
        while($obProperty = $rsProperty->Fetch()) {
            $arReturn[$obProperty['CODE']] = array(
                'ID' => $obProperty['ID'],
                'NAME' => $obProperty['NAME'],
                'PROPERTY_TYPE' => $obProperty['PROPERTY_TYPE'],
                'ACTIVE' => $obProperty['ACTIVE'],
                'SORT' => $obProperty['SORT'],
                'CODE' => $obProperty['CODE'],
            );
        }
        return $arReturn;
    }
    //endregion
    //region Возвращает список заказов
    /**
     * Возвращает список заказов
     */
    private function getListOrders() {
        $arListContract = $this->getListContractUser();
        if(!empty($this->arGet['SORT']) && !empty($this->arGet['ORDER'])) {
            $arOrderOrders = array($this->arGet['SORT'] => $this->arGet['ORDER'], 'ID' => 'ASC');
        } else {
            $arOrderOrders = array('PROPERTY_FORMING' => 'ASC', 'ID' => 'ASC');
        }

        $arFilterOrders = array(
            'IBLOCK_CODE' => $this->arParams['IBLOCK_CODE_ORDERS'],
            'PROPERTY_CONTRACT' => $arListContract
        );
        $arPropertyRequest = $this->getPropertyEnum($this->arParams['IBLOCK_CODE_CONTRACTS'], 'REQUEST');
        $arPropertyForming = $this->getPropertyEnum($this->arParams['IBLOCK_CODE_CONTRACTS'], 'FORMING');
        $arPropertyFormingType = array();
        foreach($arPropertyForming as $cForming => $idValueForming) {
            $arPropertyFormingType[$idValueForming] = $cForming;
        }

        if(!empty($this->arParams['REQUEST'])) {
            $arFilterOrders['PROPERTY_REQUEST'] = $arPropertyRequest[$this->arParams['REQUEST']];
        } else {
            $arFilterOrders['!PROPERTY_REQUEST'] = $arPropertyRequest['Y'];
        }
        if(!empty($this->arParams['FORMING'])) {
            //$arFilterOrders['PROPERTY_FORMING'] = $arPropertyForming[$this->arParams['FORMING']];
        } else {
            //$arFilterOrders['!PROPERTY_FORMING'] = $arPropertyForming['Y'];
        }
        $prefix = $this->prefix;
        if($this->arGet[$prefix . 'FILTER'] == 'Y') {
            foreach($this->arResult['ROW_SORT'] as $cSort => $arSort) {
                if(!empty($this->arGet[$prefix . $cSort])) {
                    if(stristr($cSort, 'DATE')) {
                        $dateFrom = ConvertTimeStamp(strtotime($this->arGet[$prefix . $cSort]), 'FULL', 'ru');
                        $dateTo = ConvertTimeStamp(strtotime($this->arGet[$prefix . $cSort]) + 86400, 'FULL', 'ru');
                        if(stristr($cSort, 'PROPERTY')) {
                            $dateFrom = date('Y-m-d H:i:s', strtotime($this->arGet[$prefix . $cSort]));
                            $dateTo = date('Y-m-d H:i:s', strtotime($this->arGet[$prefix . $cSort]) + 86400);
                        }
                        if ($cSort == "PROPERTY_DATE") {
                            $arFilterOrders['>=' . $cSort] = date('Y-m-d', strtotime($this->arGet[$prefix . $cSort]));
                            $arFilterOrders['<=' . $cSort] = date('Y-m-d', strtotime($this->arGet[$prefix . $cSort]));
                        } else {
                            $arFilterOrders['>=' . $cSort] = $dateFrom;
                            $arFilterOrders['<' . $cSort] = $dateTo;
                        }
                    } else {
                        $arFilterOrders[$cSort] = $this->arGet[$prefix . $cSort];
                    }
                }
            }
        }
        $arNavParams = array('nPageSize' => $this->arParams['COUNT_ITEM_ON_PAGE']);
        if(!empty($this->arGet['COUNT_ON_PAGE']) && is_numeric($this->arGet['COUNT_ON_PAGE'])) {
            $arNavParams['nPageSize'] = $this->arGet['COUNT_ON_PAGE'];
        }
        $arSelectOrders = array(
            'ID',
            'IBLOCK_ID',
            'NAME',
            'CREATED_BY',
            'DATE_CREATE',
            'PROPERTY_TYPE_ORDER',
            'PROPERTY_DATE',
            'PROPERTY_STATUS',
            'PROPERTY_FORMING'
        );

        $ufXmlId = array("LOGIC" => "OR");

        foreach ($this->arParams['PAGE_TYPES_FROM_STATUS'] as $xml_id)
            $ufXmlId[] = array("PROPERTY_STATUS" => $xml_id);

        $arFilterOrders[] = $ufXmlId;

        $rsOrders = CIBlockElement::GetList($arOrderOrders, $arFilterOrders, false, $arNavParams, $arSelectOrders);
        $this->arResult['COUNT_ORDERS'] = $rsOrders->SelectedRowsCount();
        $this->arResult['NAV_PRINT'] = $rsOrders->GetPageNavStringEx($navComponentObject, Loc::getMessage('USER_ORDERS_C_PAGES'), $this->arParams['NAV_TEMPLATE']);
        $iOrder = 0;
        while($obOrders = $rsOrders->Fetch()) {
            $this->arResult['LIST_ORDERS'][$iOrder] = array(
                'ID' => $obOrders['ID'],
                'NAME' => $obOrders['NAME'],
                'DATE_CREATE' => $obOrders['DATE_CREATE'],
                'CREATED_BY' => $this->getUserName($obOrders['CREATED_BY']),
                'PROPERTY_DATE' => $obOrders['PROPERTY_DATE_VALUE'],
                'PROPERTY_TYPE_ORDER' => $obOrders['PROPERTY_TYPE_ORDER_VALUE'],
                'PROPERTY_FORMING' => $arPropertyFormingType[$obOrders['PROPERTY_FORMING_ENUM_ID']],
                'PROPERTY_STATUS' => $this->arResult['WMS_STATUS'][$obOrders['PROPERTY_STATUS_VALUE']]['UF_NAME'],
                'PROPERTY_STATUS_XML_ID' => $this->arResult['WMS_STATUS'][$obOrders['PROPERTY_STATUS_VALUE']]['UF_XML_ID'],
            );
            if($arPropertyFormingType[$obOrders['PROPERTY_FORMING_ENUM_ID']] == 'Y') {
                $this->arResult['LIST_ORDERS'][$iOrder]['DETAIL_URL'] = $this->arParams['SEF_FOLDER'] . 'create/?ORDER_ID=' . $obOrders['ID'];
            }
            $iOrder++;
        }
    }
    //endregion
    private function checkSoapData() {
        if(count($this->arResult['LIST_ORDERS']) > 0) {
            $arStatusOfOrder = array_keys($this->arResult['WMS_STATUS']);
            $arRelationTypes = array(
                'digitizing'            => 'digitization',              // Оцифровка
                //''                    => 'first_placement',           // Первичное размещение
                'issueMu'               => 'delivery',                  // Временный вывоз со склада
                'incomeMu'              => 'reception_of_units',        // Возврат на склад
                'applicationDestruction'=> 'destruction',               // Заявка на уничтожение
                'departureExpert'       => 'checkout_out_expert',       // Выезд эксперта
                'shippingSupplies'      => 'supplies',                  // Доставка расходных материалов
                //''                    => 'arbitrarily'                // Произвольно
            );
            $wmsClass = new \EME\WMS(true);
            foreach($this->arResult['LIST_ORDERS'] as $iOrder => $arOrder) {
                if($arOrder['PROPERTY_FORMING'] != 'Y') {
                    $dateXml = $wmsClass->getXMLDateTime();
                    $ResponseStatusOrder = $wmsClass->GetStatusOrder($dateXml, $arOrder['ID']);
                    $listStatusOfOrder = $ResponseStatusOrder->listStatusOrder[0];
                    $numberOfStatus = $listStatusOfOrder->currentStatus;
                    if($ResponseStatusOrder->state) {
                        $this->arResult['LIST_ORDERS'][$iOrder]['ERROR'] = Loc::getMessage('ERROR_WMS_CONNECT_' . $ResponseStatusOrder->descriptionError);
                        $this->arResult['LIST_ORDERS'][$iOrder]['ERROR_CLASS'] = ' js-tooltip bgd-warning';
                    } else {
                        if($arStatusOfOrder[$numberOfStatus] != $arOrder['PROPERTY_STATUS_XML_ID']) {
                            $newStatus = $arStatusOfOrder[$numberOfStatus];
                            $this->arResult['LIST_ORDERS'][$iOrder]['PROPERTY_STATUS_XML_ID'] = $newStatus;
                            //CIBlockElement::SetPropertyValuesEx($arOrder['ID'], $arOrder['IBLOCK_ID'], array('STATUS' => $newStatus));
                        }
                    }
                }
            }
        }
    }
    //region Возвращает имя пользователя
    /** Возвращает имя пользователя
     * @param $userID
     * @return string
     */
    private function getUserName($userID) {
        $arUser = CUser::GetByID($userID)->Fetch();
        $fullName = '';
        if(!empty($arUser['LAST_NAME'])) {
            $fullName .= $arUser['LAST_NAME'] . '&nbsp;';
        }
        if(!empty($arUser['NAME'])) {
            $fullName .= $arUser['NAME'] . '&nbsp;';
        }
        if(!empty($arUser['SECOND_NAME'])) {
            $fullName .= $arUser['SECOND_NAME'] . '&nbsp;';
        }
        return $fullName;
    }
    //endregion
    //region Возвращает список свойств WMS статусов
    /**
     * Возвращает список свойств WMS статусов
     */
    private function getListWMSStatus() {
        if(empty($this->dClassStatusOrder)) {
            $idHLBlock = $this->getIdHLBlock($this->arParams['HL_CODE_STATUS']);
            $this->dClassStatusOrder = $this->getDataClass($idHLBlock);
        }
        $dataClass = $this->dClassStatusOrder;

        $ufXmlId = array("LOGIC" => "OR");

        foreach ($this->arParams['PAGE_TYPES_FROM_STATUS'] as $xml_id)
            $ufXmlId[] = array("=UF_XML_ID" => $xml_id);

        $rsStatus = $dataClass::getList(array(
            'filter' => $ufXmlId,
            'select' => array('*'),
            'order' => array('UF_SORT' => 'ASC'),
            'limit' => ''
        ));
        while($obStatus = $rsStatus->fetch()) {
            $this->arResult['WMS_STATUS'][$obStatus['UF_XML_ID']] = array(
                'ID' => $obStatus['ID'],
                'UF_NAME' => $obStatus['UF_NAME'],
                'UF_XML_ID' => $obStatus['UF_XML_ID']
            );
        }
    }
    //endregion
    //region Возвращаем id HL блока по его коду
    /**
     * Возвращаем id HL блока по его коду
     */
    private function getIdHLBlock($codeHL) {
        $connection = Application::getConnection();
        $sql = "SELECT ID,NAME FROM b_hlblock_entity WHERE NAME='" . $codeHL . "';";
        $recordset = $connection->query($sql)->fetch();
        return $recordset['ID'];
    }
    //endregion
    //region Возвращаем класс для работы с HL блоком
    /** Возвращаем класс для работы с HL блоком
     * @throws \Bitrix\Main\SystemException
     */
    private function getDataClass($idHL) {
        $hlblock = HL\HighloadBlockTable::getById($idHL)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        return $entity->getDataClass();
    }
    //endregion
    //region Возвращает список договоров пользователя
    /** Возвращает список договоров пользователя
     * @return array
     */
    private function getListContractUser() {
        $arListContracts = array();
        if(empty($this->arResult['USER_ID'])) {
            if(empty($this->arParams['USER_ID'])) {
                global $USER;
                $this->arResult['USER_ID'] = $USER->GetID();
            } else {
                $this->arResult['USER_ID'] = $this->arParams['USER_ID'];
            }
        }
        if(empty($this->arParams['USER_GROUPS'])) {
            $this->arParams['USER_GROUPS'] = $this->getUserGroup();
        }

        if(!in_array(U_GROUP_CODE_CLIENT_S_USER, $this->arParams['USER_GROUPS'])) {
            $arFilterClients = array(
                'IBLOCK_CODE'               => $this->arParams['IBLOCK_CODE_CLIENTS'],
                'PROPERTY_PROP_CLIENT'      => $this->arParams['USER_ID'],
                'PROPERTY_PROP_STATUS'      => $this->arParams['PAGE_TYPES_FROM_STATUS'],
            );
            $arSelectClients = array('ID', 'IBLOCK_ID', 'PROPERTY_PROP_REGISTRY');
            $rsClients = CIBlockElement::GetList(array('ID' => 'ASC'), $arFilterClients, false, false, $arSelectClients);
            $arRegistryList = array();
            while($obClients = $rsClients->Fetch()) {
                foreach($obClients['PROPERTY_PROP_REGISTRY_VALUE'] as $idRegistry) {
                    if(!in_array($idRegistry, $arRegistryList)) {
                        $arRegistryList[] = $idRegistry;
                    }
                }
            }
            $this->arResult['LIST_REGISTRY'] = $arRegistryList;
            $arFilterContracts = array(
                '=IBLOCK_CODE' => $this->arParams['IBLOCK_CODE_CONTRACTS'],
                '=PROPERTY_PROP_ID_REGISTRY' => $arRegistryList
            );
        } else {
            $arFilterContracts = array(
                '=IBLOCK_CODE' => $this->arParams['IBLOCK_CODE_CONTRACTS'],
                '=PROPERTY_PROP_SUPER_USER' => array($this->arResult['USER_ID'])
            );
        }

        $arOrderContracts = array('NAME' => 'ASC');
        $arSelectContracts = array('ID', 'IBLOCK_ID', 'NAME');
        $rsContracts = CIBlockElement::GetList($arOrderContracts, $arFilterContracts, false, false, $arSelectContracts);
        while($obContracts = $rsContracts->Fetch()) {
            $arListContracts[] = $obContracts['ID'];
        }
        return $arListContracts;
    }
    //endregion
    //region Возвращает код группы которой принадлежит пользователь
    /**
     * Возвращает код группы которой принадлежит пользователь
     */
    private function getUserGroup() {
        $res = CUser::GetUserGroupList($this->arResult['USER_ID']);
        $arUserGroupID = array();
        while ($arGroup = $res->Fetch()){
            $arUserGroupID[] = $arGroup['GROUP_ID'];
        }
        $arFilterGroups = array('ACTIVE' => 'Y', 'ID' => implode('|', $arUserGroupID));
        $rsGroups = CGroup::GetList($by = 'SORT', $order = 'ASC', $arFilterGroups);
        $arUserGroupCode = array();
        while($obGroup = $rsGroups->Fetch()) {
            $arUserGroupCode[] = $obGroup['STRING_ID'];
        }
        return $arUserGroupCode;
    }
    //endregion
    //region Возвращает массив с полями для сортировки
    /** Возвращает массив с полями для сортировки
     */
    private function setSort() {
        $arRowSort = array();
        $preffixGet = '';
        if(!empty($this->arGet)) {
            $iGet = 0;
            foreach($this->arGet as $cGet => $vGet) {
                if(!empty($this->arParams['SET_SORT_FILTER'])) {
                    if(!empty($this->arParams['SET_SORT_FILTER'][$cGet])) {
                        continue;
                    }
                }
                if($cGet != 'SORT' && $cGet != 'ORDER') {
                    if($iGet == 0) {
                        $preffixGet .= '?';
                    } else {
                        $preffixGet .= '&';
                    }
                    if(is_array($vGet)) {
                        foreach($vGet as $iVal => $val) {
                            $preffixGet .= $cGet . urlencode('[]') . '=' . urlencode($val);
                            if($iVal + 1 != count($vGet)) {
                                $preffixGet .= '&';
                            }
                        }
                    } else {
                        $preffixGet .= $cGet . '=' . $vGet;
                    }

                    $iGet++;
                }
            }
        }
        foreach($this->arParams['SET_SORT_FILTER'] as $cSort) {
            $order = 'ASC';
            if(!empty($this->arGet['SORT']) && $cSort == $this->arGet['SORT'] && $this->arGet['ORDER'] == $order) {
                $order = 'DESC';
            }
            $arRowSort[$cSort]['CODE'] = $cSort;
            if($preffixGet == '') {
                $arRowSort[$cSort]['URL'] = '?';
            } else {
                $arRowSort[$cSort]['URL'] = $preffixGet . '&';
            }
            $arRowSort[$cSort]['URL'] .= 'SORT=' . $cSort . '&ORDER=' . $order;
        }
        $this->arResult['ROW_SORT'] = $arRowSort;
    }
    //endregion
    //region Возвращает массив со списком значений свойства типа список
    /** Возвращает массив со списком значений свойства типа список
     * @param $iblock - ID инфоблока / или код инфоблока
     * @param $propertyCode - код свойства
     * @return array - массив в котором ключом является XML_ID, а значением ID
     */
    private function getPropertyEnum($iblock, $propertyCode) {
        $arReturn = array();
        $arOrderProperty = array('sort' => 'asc');
        if(is_numeric($iblock)) {
            $arFilterProperty = array('IBLOCK_ID' => $iblock);
        } else {
            $arFilterProperty = array('IBLOCK_CODE' => $iblock);
        }
        $rsProperty = CIBlockProperty::GetPropertyEnum($propertyCode, $arOrderProperty, $arFilterProperty);
        while($obProperty = $rsProperty->Fetch()) {
            $arReturn[$obProperty['XML_ID']] = $obProperty['ID'];
        }
        return $arReturn;
    }
    //endregion
}
<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 06.04.2017
 * Time: 12:22
 */
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

use \Bitrix\Main\Application,
    \Bitrix\Main\Localization\Loc;
$request = Application::getInstance()->getContext()->getRequest();
$arGet = $request->getQueryList()->toArray();

?>

<div class="content__item _top top_panel">
    <div class="page__title">
        <h1><a href="<?= $_SERVER['SCRIPT_URL'];?>"><?= Loc::getMessage("HEADER_TITLE_1") ?></a></h1>
    </div>

        <div class="scrollbar-outer scrollbar-center">
            <form id="form-filter" class="form form__fl-left" method="get">
                <input type="hidden" name="F_USER_FILTER" value="Y">
                <div class="form__row form__row__sortable" id="sortable">
                    <?foreach($arResult['ROW_FILTER'] as $key => $arFilter) { ?>
                        <div class="column form__row__col _size4 ui-sortable">
                            <div class="portlet" data-portlet="<?= $key ?>">
                                <div class="portlet-header">
                                    <?= Loc::getMessage('C_USER_LIST_' . $arFilter)?>
                                </div>
                                <div class="portlet-content">
                                    <?if($arFilter == 'ACTIVE') {?>
                                        <? $s = 'selected'; ?>
                                        <select name="F_<?= $arFilter ?>" id="active-select" >
                                            <option value=""><?= Loc::getMessage('FILTER_EMPTY_SELECT') ?></option>
                                            <option value="Y" <?= $arGet['F_' . $arFilter] == "Y"?$s:"";?>><?= Loc::getMessage($arFilter . '_Y')?></option>
                                            <option value="N" <?= $arGet['F_' . $arFilter] == "N"?$s:"";?>><?= Loc::getMessage($arFilter . '_N')?></option>
                                        </select>
                                    <?} elseif(stristr($arFilter, 'DATE')) {?>
                                        <input type="text"
                                               class="form__field js-datepicker"
                                               name="F_<?= $arFilter ?>"
                                               value="<?= $arGet["F_" . $arFilter]; ?>"
                                               placeholder="...">
                                        <a href="#" class="js-reset-input"><i class="fa fa-close"></i></a>
                                    <?} else {?>
                                        <input type="text"
                                               class="form__field <?= stristr($arFilter, 'MOBILE') ? 'js-maskedinput':'';?>"
                                               name="F_<?= $arFilter ?>"
                                                <?= stristr($arFilter, 'MOBILE') ? 'data-maskedinput="+9 999 999 9999"':'';?>
                                               value="<?= $arGet["F_" . $arFilter]; ?>"
                                               placeholder="...">
                                        <a href="#" class="js-reset-input"><i class="fa fa-close"></i></a>
                                    <?}?>
                                </div>
                            </div>
                        </div>
                    <?}?>
                </div>
                <div class="form__row">
                    <button type="submit" class="btn"><?= Loc::getMessage('USER_REG_T_BUTTON_APPLY') ?></button>
                    <a href="<?= $_SERVER['SCRIPT_URL'] ?>" class="btn _white">
                        <?= Loc::getMessage('USER_REG_T_BUTTON_RESET') ?>
                    </a>
                </div>
            </form>
        </div>
</div>
<div class="content__item _bottom">
    <?if(!empty($arResult['LIST_USERS'])) {?>
        <div class="scrollbar-outer">
            <!--PLUS-->
            <div class="control">
                <div class="control__result">
                    <?= Loc::getMessage('C_USER_LIST_COUNT_UNITS_FINDED')?><?= $arResult['COUNT_ORDERS'] ?>
                </div>
            </div>
            <table class="table js-table" id="orders-list">
                <thead>
                <tr>
                    <?foreach($arResult['ROW'] as $cField => $arField) {?>
                        <th><?= Loc::getMessage('C_USER_LIST_' . $arField) ?></th>
                    <?}?>
                </tr>
                </thead>
                <tbody>
                    <?foreach($arResult['LIST_USERS'] as $iOrder => $arOrder) {
                    $title = '';

                    if($arOrder['ERROR']) {
                        $title = $arOrder['ERROR'];
                    }?>
                    <tr class="<?= $arOrder['ERROR_CLASS'] ?>" title="<?= $title ?>">
                         <?foreach($arResult['ROW'] as $cField) {?>
                            <td class="<?= $cField == 'COMPANY'? 'row-company':'';?>">
                                <?if($cField == 'NAME') {
                                    $detailUri = $arParams['SEF_FOLDER'] . $arOrder['USER_ID'] . '/';
                                    if(!empty($arOrder['DETAIL_URL']) && $arOrder['PROPERTY_FORMING'] == 'Y') {
                                        $detailUri = $arOrder['DETAIL_URL'];
                                    }?>
                                    <a href="<?= $detailUri ?>" class="order-detail">
                                        <?= $arOrder[$cField] ?>
                                    </a>
                                <?} else {
                                    if ($cField === 'ACTIVE')
                                        echo Loc::getMessage($cField . '_' . $arOrder[$cField]);
                                    else
                                        echo $arOrder[$cField];
                                }?>
                            </td>
                        <?}?>
                    </tr>
                <?}?>
                </tbody>
            </table>
            <div class="navigation">
                <?= $arResult['NAV_PRINT'] ?>
                <div class="pager">
                    <div class="pager__current">
                        <span><?= Loc::getMessage('USER_REG_T_COUNT_ITEMS') ?></span>
                        <form action="<?= $arResult['FORM_PATH_DEFAULT'] ?>" method="GET" class="nav-form">
                            <?= getHTMLInputExcludeParam($arGet, 'USERS_ON_PAGE') ?>
                            <input type="text" name="USERS_ON_PAGE" value="<?= $arGet['USERS_ON_PAGE'] ? : $arParams['USERS_ON_PAGE'] ?>">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <?}
    else {?>
        <h2><?= Loc::getMessage("EMPTY_SEARCH_RESULT") ?></h2>
    <?}?>
</div>
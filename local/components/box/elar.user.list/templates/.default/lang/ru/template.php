<?php
$MESS['C_USER_LIST_ID'] = "ID";
$MESS['C_USER_LIST_NAME'] = "ФИО";
$MESS['C_USER_LIST_ACTIVE'] = "Активен";
$MESS['C_USER_LIST_DATE_CREATE'] = "Дата регистрации";
$MESS['C_USER_LIST_EMAIL'] = "Email";
$MESS['C_USER_LIST_PERSONAL_MOBILE'] = "Мобильный телефон";
$MESS['C_USER_LIST_LOGIN'] = "Логин";
$MESS['C_USER_LIST_COMPANY'] = "Организация";
$MESS['C_USER_LIST_PHONE'] = "Мобильный телефон";
$MESS['C_USER_LIST_COUNT_UNITS_FINDED'] = "Всего пользователей в системе: ";
$MESS['USER_REG_T_COUNT_ITEMS'] = "Количество пользователей на странице: ";
$MESS['USER_REG_T_BUTTON_RESET'] = "Отменить";
$MESS['USER_REG_T_BUTTON_APPLY'] = "Найти";
$MESS['FILTER_EMPTY_SELECT'] = "-";
$MESS['ACTIVE_Y'] = "Да";
$MESS['ACTIVE_N'] = "Нет";
$MESS["EMPTY_SEARCH_RESULT"] = "Пользователей на найдено, попробуйте изменить критерии поиска";
$MESS["HEADER_TITLE_1"] = "Пользователи";

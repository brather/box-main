<?php
use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 06.04.2017
 * Time: 12:21
 */

class ElarUsertList
    extends CBitrixComponent {

    private $prefix = 'F_USER_';
    private $arGet = array();
    private $arPost = array();

    public function executeComponent(  )
    {
        global $USER;

        Loc::loadMessages(__FILE__);
        Loader::includeModule('iblock');
        Loader::includeModule("highloadblock");
        $this->arResult['USER_ID'] = $USER->GetID();
        $this->_getCodeUserGroup();
        $request = Application::getInstance()->getContext()->getRequest();
        $this->arGet = $request->getQueryList()->toArray();
        $this->arPost = $request->getPostList()->toArray();
        $this->_getListUsers();

        $this->includeComponentTemplate();
    }

    private function _getCodeUserGroup() {
        $res = CUser::GetUserGroupList($this->arResult['USER_ID']);
        $arUserGroupID = array();
        while ($arGroup = $res->Fetch()){
            $arUserGroupID[] = $arGroup['GROUP_ID'];
        }
        $arFilterGroups = array('ACTIVE' => 'Y', 'ID' => implode('|', $arUserGroupID));
        $rsGroups = CGroup::GetList($by = 'SORT', $order = 'ASC', $arFilterGroups);
        $arUserGroupCode = array();
        while($obGroup = $rsGroups->Fetch()) {
            $arUserGroupCode[] = $obGroup['STRING_ID'];
        }
        $this->arResult['USER_GROUPS'] = $arUserGroupCode;
    }

    private function _getListUsers() {
        if(!empty($this->arGet['SORT']) && !empty($this->arGet['ORDER'])) {
            $arOrderBy = array($this->arGet['SORT'] => $this->arGet['ORDER']);
        } else {
            $arOrderBy = array('ID' => 'DESC');
        }

        $arFilter = array(
            'IBLOCK_ID' => $this->arParams['IBLOCK_ID']
        );

        if($this->arGet[$this->prefix . 'FILTER'] == 'Y') {

            foreach ($this->arGet as $idx => $value) {
                if (stristr($idx, 'F_') === false || $idx === 'F_USER_FILTER') continue; // не значения для фильтра
                if (empty($value) ) continue; // продолжить, значение пустое

                $idx  = ltrim($idx, 'F_'); // удалить префикс

                /*  по полям LOGIN и EMAIL производить дополнительную выборку в фильтре */
                if (stristr($idx, 'LOGIN') || stristr($idx, 'EMAIL') || stristr($idx, 'PERSONAL_MOBILE')) {
                    $db = CUser::GetList(($by = 'ID'), ($order = 'DESC'), array($idx => $value));
                    $userIn = array();
                    while ($res = $db->Fetch()) {
                        $userIn[] = $res['ID'];
                    }

                    if (isset($arFilter['PROPERTY_PROP_CLIENT']) && is_array($arFilter['PROPERTY_PROP_CLIENT']) && count($arFilter['PROPERTY_PROP_CLIENT']) > 0)
                        $arFilter['PROPERTY_PROP_CLIENT'] = array_merge($arFilter['PROPERTY_PROP_CLIENT'], $userIn);
                    else
                        $arFilter['PROPERTY_PROP_CLIENT'] = $userIn;

                    continue;
                }

                if(stristr($idx, 'DATE')) { // условия для Даты
                    $arFilter['>=' . $idx] = date('Y-m-d H:i:s', strtotime($value));
                    $arFilter['<'. $idx] = date('Y-m-d H:i:s', strtotime($value) + 86400);
                } else {
                    $arFilter["%" . $idx] = $value;
                }
            }
        }

        $arNavParams = array('nPageSize' => $this->arParams['USERS_ON_PAGE']);
        if(!empty($this->arGet['USERS_ON_PAGE']) && is_numeric($this->arGet['USERS_ON_PAGE'])) {
            $arNavParams['nPageSize'] = $this->arGet['USERS_ON_PAGE'];
        }

        $arSelect = array(
            'ID',
            'ACTIVE',
            'NAME',
            'DATE_CREATE',
            'PROPERTY_PROP_CLIENT'
        );

        $this->arResult['ROW'] = array(
            0 => 'ID',
            1 => 'ACTIVE',
            2 => 'NAME',
            3 => 'LOGIN',
            4 => 'DATE_CREATE',
            5 => 'PERSONAL_MOBILE',
            6 => 'EMAIL',
            7 => 'COMPANY',
        );

        $this->arResult['ROW_FILTER'] = array(
            0 => 'ID',
            2 => 'NAME',
            3 => 'LOGIN',
            4 => 'DATE_CREATE',
            5 => 'EMAIL',
            6 => 'PERSONAL_MOBILE',
            7 => 'ACTIVE',
        );

        $db = CIBlockElement::GetList($arOrderBy, $arFilter, false, $arNavParams, $arSelect);
        $this->arResult['COUNT_ORDERS'] = $db->SelectedRowsCount();
        $this->arResult['NAV_PRINT'] = $db->GetPageNavStringEx($navComponentObject, Loc::getMessage('USER_ORDERS_C_PAGES'), $this->arParams['NAV_TEMPLATE']);

        while($obOrders = $db->Fetch()) {

            $user = CUser::GetByID($obOrders['PROPERTY_PROP_CLIENT_VALUE'])->Fetch();

            $document = CIBlockElement::GetList(
                array('SORT' => 'ASC'),
                array('=PROPERTY_PROP_SUPER_USER' => $user['ID'], '=IBLOCK_ID' => IBLOCK_CODE_CONTRACTS_ID),
                false, false,
                array('PROPERTY_PROP_COMPANY')
            )->Fetch();

            $company = CIBlockElement::GetList(
                array('SORT' => 'ASC'),
                array('=IBLOCK_ID' => IBLOCK_CODE_COMPANY_ID, '=ID' => $document['PROPERTY_PROP_COMPANY_VALUE']),
                false, false,
                array('ID', 'NAME', 'PERSONAL_MOBILE')
            )->Fetch();

            $this->arResult['LIST_USERS'][$obOrders['ID']] = array(
                'ID' => $obOrders['ID'],
                'NAME' => $obOrders['NAME'],
                'DATE_CREATE' => $obOrders['DATE_CREATE'],
                'EMAIL' => $user['EMAIL'],
                'LOGIN' => $user['LOGIN'],
                'ACTIVE' => $obOrders['ACTIVE'],
                'USER_ID' => $user['ID'],
                'PERSONAL_MOBILE' => $user['PERSONAL_MOBILE'],
                'COMPANY' => '<a href="/elar/company/'.$company['ID'].'/">'.$company['NAME'].'</a>'
            );
        }
    }
}
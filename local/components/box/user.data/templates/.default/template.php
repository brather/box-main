<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();?>

<?use Bitrix\Main\Localization\Loc;?>

<?if(!empty($arResult['USER_DATA'])){?>
    <dl class="data-user-default">
        <?foreach($arResult['USER_DATA'] as $cProperty => $vProperty){?>
            <?if($cProperty != 'PASSWORD' && $cProperty != 'CHECKWORD') {?>
                <dt><?= Loc::getMessage('U_DATA_' . $cProperty) ?></dt>
                <dd><?= $vProperty ?></dd>
            <?}?>
        <?}?>
    </dl>
<?}?>

<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();?>

<?use Bitrix\Main\Localization\Loc;?>
<div class="order__col">
    <h2><?= Loc::getMessage('CABINET_T_PERSONAL_DATA') ?></h2>
    <ul class="order__list">
        <?if($arParams['USER_FULL_NAME'] == 'Y'){?>
            <li>
                <div><?= Loc::getMessage('CABINET_T_PERSONAL_FIO') ?></div>
                <div><?= $arResult['USER_FULL_NAME'] ?></div>
            </li>
        <?}?>
        <?if($arParams['USER_GROUP_ROLE'] == 'Y') {?>
            <li>
                <div><?= Loc::getMessage('CABINET_T_PERSONAL_ROLE') ?></div>
                <div><?= $arResult['USER_GROUPS']['USER_ROLE'] ?></div>
            </li>
        <?}?>
        <?foreach($arResult['USER_DATA'] as $cData => $vData){?>
            <?if($arParams['USER_FULL_NAME'] == 'Y') {
                if($cData == 'LAST_NAME' || $cData == 'NAME' || $cData == 'SECOND_NAME') {
                    continue;
                }
            }?>
            <li>
                <div><?= Loc::getMessage('CABINET_T_PERSONAL_' . $cData) ?></div>
                <div>
                    <?if($cData == 'ACTIVE'){?>
                        <?= Loc::getMessage('CABINET_T_PERSONAL_ACTIVE_' . $vData) ?>
                    <?} else {?>
                        <?= $vData ?>
                    <?}?>
                </div>
            </li>
        <?}?>
    </ul>
    <?if(!empty($arParams['EDIT_DATA_URL'])){?>
        <p>
            <a href="<?= $arParams['EDIT_DATA_URL'] ?>" class="btn">
                <?= Loc::getMessage('CABINET_T_PERSONAL_EDIT_FULL') ?>
            </a>
        </p>
    <?}?>
</div>
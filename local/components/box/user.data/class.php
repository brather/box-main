<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc;

class UserGetDataComponent extends CBitrixComponent {
    //region Задание свойств класса
    private $page = 'template';
    private $userId = 0;
    //endregion
    //region Подготовка входных параметров компонента
    /** Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        return parent::onPrepareComponentParams($arParams);
    }
    //endregion
    //region Подключение файлов перевода
    /**
     * Подключение файлов перевода
     */
    public function onIncludeComponentLang() {
        Loc::loadMessages(__FILE__);
    }
    //endregion
    //region Выполнение компонента
    /**
     * Выполнение компонента
     */
    public function executeComponent() {
        $this->getUserId();
        $this->getUserData();
        $this->getUserGroup();
        $this->getFullName();
        $this->includeComponentTemplate($this->page);
    }
    //endregion
    //region Возвращаем id пользователя
    /**
     * Возвращаем id пользователя
     */
    private function getUserId() {
        global $USER;
        if($USER->isAuthorized()) {
            $this->userId = $USER->GetID();
        }
    }
    //endregion
    //region Метод возвращает данные пользователя указанные в параметрах компонента (если параметры отсутсвуют то возвращает все данные по пользователю)
    /**
     * Метод возвращает данные пользователя указанные в параметрах компонента (если параметры отсутсвуют то возвращает все данные по пользователю)
     */
    private function getUserData() {
        if($this->userId) {
            $arFilter = array(
                'ID' => $this->userId
            );
            if(!empty($this->arParams['DATA_PROPERTY_SELECT'])) {
                $arParameters['SELECT'] = $this->arParams['DATA_PROPERTY_SELECT'];
            }
            if(!empty($this->arParams['DATA_PROPERTY_FIELDS'])) {
                $arParameters['FIELDS'] = $this->arParams['DATA_PROPERTY_FIELDS'];
            } else {
                $arParameters['FIELDS'] = array(
                    'ID',
                    'ACTIVE',
                    'NAME',
                    'LAST_NAME',
                    'SECOND_NAME',
                    'EMAIL',
                    'LOGIN',
                    'PERSONAL_GENDER',
                    'WORK_COMPANY',
                    'WORK_DEPARTMENT',
                    'WORK_POSITION',
                    'WORK_PHONE',
                    'WORK_FAX',
                    'WORK_STREET',
                    'WORK_CITY',
                    'WORK_STATE',
                    'WORK_ZIP',
                    'PERSONAL_BIRTHDAY'
                );
            }
            $arUser = CUser::GetList($by = 'name', $order = 'desc', $arFilter, $arParameters)->Fetch();
            $this->arResult['USER_DATA'] = $arUser;
        }
    }
    //endregion
    //region Метод возвращает данные по группе пользователя
    /**
     * Метод возвращает данные по группе пользователя
     */
    private function getUserGroup() {
        if($this->arParams['USER_GROUP_ROLE'] == 'Y') {
            $stringGroupsId = '';
            $res = CUser::GetUserGroupList($this->userId);
            $i = 0;
            while ($arGroup = $res->Fetch()){
                if($i != 0) {
                    $stringGroupsId .= ' | ';
                }
                $stringGroupsId .= $arGroup['GROUP_ID'];
                $i++;
            }
            $arFilterGroups = array('ACTIVE' => 'Y', 'ID' => $stringGroupsId);
            $rsGroups = CGroup::GetList($by = 'SORT', $order = 'ASC', $arFilterGroups);
            $arGroups = array();
            while($obGroup = $rsGroups->Fetch()) {
                if($arGroups['C_SORT'] < $obGroup['C_SORT']) {
                    $arGroups = array(
                        'ID' => $obGroup['ID'],
                        'ACTIVE' => $obGroup['ACTIVE'],
                        'STRING_ID' => $obGroup['STRING_ID'],
                        'C_SORT' => $obGroup['C_SORT'],
                        'USER_ROLE' => self::getGroupRole($obGroup['STRING_ID'])
                    );
                }
            }
            $this->arResult['USER_GROUPS'] = $arGroups;
        }
    }
    //endregion
    //region Функция возвращает роль пользователя в системе по коду группы
    /** Функция возвращает роль пользователя в системе по коду группы
     * @param $codeGroup - код группы которой принадлежит пользователь
     * @return mixed
     */
    public function getGroupRole($codeGroup) {
        $arGroupCode = array(
            U_GROUP_CODE_ELAR_ADMIN => Loc::getMessage('C_GROUP_ROLE_ELAR_ADMIN'),
            U_GROUP_CODE_ELAR_SUPERVISOR => Loc::getMessage('C_GROUP_ROLE_ELAR_SUPERVISOR'),
            U_GROUP_CODE_ELAR_OPERATOR => Loc::getMessage('C_GROUP_ROLE_ELAR_OPERATOR'),
            U_GROUP_CODE_CLIENT_S_USER => Loc::getMessage('C_GROUP_ROLE_CLIENT_S_USER'),
            U_GROUP_CODE_CLIENT_USER => Loc::getMessage('C_GROUP_ROLE_CLIENT_USER'),
        );
        return $arGroupCode[$codeGroup];
    }
    //endregion
    //region Метод возвращает полное имя пользователя (ФИО)
    /**
     * Функция возвращает полное имя пользователя (ФИО)
     */
    private function getFullName() {
        if(!empty($this->arResult['USER_DATA'])) {
            $arName = array(
                'LAST_NAME' => $this->arResult['USER_DATA']['LAST_NAME'],
                'NAME' => $this->arResult['USER_DATA']['NAME'],
                'SECOND_NAME' => $this->arResult['USER_DATA']['SECOND_NAME'],
            );
            $fullName = '';
            foreach($arName as $cName => $vName) {
                if(!empty($fullName)) {
                    $fullName .= ' ';
                }
                $fullName .= $vName;
            }
            $this->arResult['USER_FULL_NAME'] = $fullName;
        }
    }
    //endregion
}
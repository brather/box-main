<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */
global $APPLICATION;

$APPLICATION->IncludeComponent(
    'box:user.registry.detail',
    'design.list',
    array(
        'USER_ID' => $arResult['USER_ID'],
        'SEF_FOLDER' => $arParams['SEF_FOLDER'],
        'IBLOCK_CODE_CLIENTS' => $arParams['IBLOCK_CODE_CLIENTS'],
        'IBLOCK_CODE_CONTRACTS' => $arParams['IBLOCK_CODE_CONTRACTS'],
        'VARIABLES' => $arResult['VARIABLES'],
        'USE_ORDER_TYPE' => $arResult['USE_ORDER_TYPE'],
        'IBLOCK_CODE_ORDERS' => $arParams['IBLOCK_CODE_ORDERS'],
        'CURRENT_PATH' => $arResult['CURRENT_PATH'],
        'PATH_TO_FILES' => $arParams['UNIT_FILES'],
        'COUNT_IN_ROW_PARAMS' => $arParams['COUNT_IN_ROW_PARAMS'],
        'COUNT_ITEM_ON_PAGE' => $arParams['COUNT_ITEM_ON_PAGE'],
        'MAX_COUNT_ITEM_ON_PAGE' => $arParams['MAX_COUNT_ITEM_ON_PAGE'],
        'NAV_TEMPLATE' => $arParams['NAV_TEMPLATE'],
        'OPTIONS_TO_UNIT' => $arParams['OPTIONS_TO_UNIT'],
        'USE_VIEWER' => $arParams['USE_VIEWER']
    )
);?>
<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */
global $APPLICATION;

$APPLICATION->IncludeComponent(
    'box:user.registry.list',
    'menu.left',
    array(
        'USER_ID' => $arResult['USER_ID'],                                                                                  // ID текущего пользователя (для уменьшения числа запросов передается из комплексного компонента)
        'SEF_FOLDER' => $arParams['SEF_FOLDER'],                                                                            // Корневая директория (необходим для вывода списка реестров, необязателен)
        'CURRENT_PATH' => $arResult['CURRENT_PATH'],                                                                        // Путь к текущей директории если включен режим ЧПУ
        'IBLOCK_CODE_CLIENTS' => $arParams['IBLOCK_CODE_CLIENTS'],                                                          // Код инфоблока с пользователями
        'IBLOCK_TYPE_LISTS' => $arParams['IBLOCK_TYPE_LISTS'],                                                              // Тип инфоблока списков
        'REGISTRY_ID' => $arResult['LIST_CONTRACTS'][$arResult['VARIABLES']['CODE']]['PROPERTY_PROP_ID_REGISTRY_VALUE'],    // Список ID реестров (необязателен, если не задан, будет проводится дополнительная выборка для суперпользователя)
        'COUNT_ON_PAGE_REGISTRY' => $arParams['COUNT_ON_PAGE_REGISTRY'],                                                    // Количество реестров на странице (необязателен)
        'VARIABLES' => $arResult['VARIABLES'],                                                                              // Вывод и формирование ссылок для отдельных реестров в рамках шаблона комплексного компонента
        'NAV_PRINT' => 'Y',                                                                                                 // Вывод навигационной цепочки (необязателен)
        'SET_SORT' => 'Y'                                                                                                   // Использовать дополнительную сортировку при выводе (пр. если блок меню то параметр не нужен)
    )
);?>
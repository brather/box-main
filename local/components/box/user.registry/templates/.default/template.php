<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */
global $APPLICATION;

$APPLICATION->IncludeComponent(
    'box:user.registry.detail',
    'design.list',
    array(
        'USER_ID' => $arResult['USER_ID'],
        'SEF_FOLDER' => $arParams['SEF_FOLDER'],
        'VARIABLES' => $arResult['VARIABLES'],
        'CURRENT_PATH' => $arResult['CURRENT_PATH'],
        'SHOW_ALL' => 'Y',
        'PATH_TO_FILES' => $arParams['UNIT_FILES'],
        'USE_ORDER_TYPE' => $arParams['USE_ORDER_TYPE'],
        'IBLOCK_CODE_ORDERS' => $arParams['IBLOCK_CODE_ORDERS'],
        'IBLOCK_CODE_CONTRACTS' => $arParams['IBLOCK_CODE_CONTRACTS'],
        'IBLOCK_CODE_CLIENTS' => $arParams['IBLOCK_CODE_CLIENTS'],
        'COUNT_IN_ROW_PARAMS' => $arParams['COUNT_IN_ROW_PARAMS'],
        'COUNT_ITEM_ON_PAGE' => $arParams['COUNT_ITEM_ON_PAGE'],
        'MAX_COUNT_ITEM_ON_PAGE' => $arParams['MAX_COUNT_ITEM_ON_PAGE'],
        'NAV_TEMPLATE' => $arParams['NAV_TEMPLATE'],
        'OPTIONS_TO_UNIT' => $arParams['OPTIONS_TO_UNIT'],
        'TITLE_PAGE' => $arParams['TITLE_PAGE'],
        'USE_VIEWER' => $arParams['USE_VIEWER']
    )
);?>
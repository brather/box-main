<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Loader;

class UserRegistryComponent extends CBitrixComponent {
    //region Задание свойств класса
    private $page = 'template';
    //endregion
    //region Подготовка входных параметров компонента
    /** Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        $arParams = $this->getSef($arParams);
        return parent::onPrepareComponentParams($arParams);
    }
    //endregion
    //region Выполнение компонента
    /**
     * Выполнение компонента
     */
    public function executeComponent() {
        $this->includeModule();                                 // Подключение модулей
        $this->getUserID();                                     // Возвращает в результирующий массив USER_ID
        $this->getListContracts();                              // Возвращаем список договоров
        $this->includeComponentTemplate($this->page);           // Подключение шаблона
    }
    //endregion
    //region Подключаем список модулей
    /**
     * Подключаем список модулей
     */
    private function includeModule() {
        Loader::includeModule('iblock');
    }
    //endregion
    //region Возвращает в результирующий массив USER_ID
    /**
     * Возвращает в результирующий массив USER_ID
     */
    private function getUserID() {
        global $USER;
        $this->arResult['USER_ID'] = $USER->GetID();
    }
    //endregion
    //region Возвращает список договоров
    /**
     * Возвращает список договоров
     */
    private function getListContracts() {
        if(empty($this->arResult['VARIABLES']['ID'])) {
            $userId = $this->arResult['USER_ID'];
            $arOrderContracts = array('NAME' => 'ASC');
            $arFilterContracts = array('IBLOCK_CODE' => $this->arParams['IBLOCK_CODE_CONTRACTS'], 'PROPERTY_PROP_SUPER_USER' => $userId);
            $arSelectContracts = array(
                'ID',
                'NAME',
                'CODE',
                'IBLOCK_ID',
                'PROPERTY_PROP_ID_REGISTRY'
            );
            $rsContracts = CIBlockElement::GetList($arOrderContracts, $arFilterContracts, false, false, $arSelectContracts);
            while($obContracts = $rsContracts->Fetch()) {
                $this->arResult['LIST_CONTRACTS'][$obContracts['CODE']] = $obContracts;
            }
        }
    }
    //endregion
    //region Обработка ссылок на договора ЧПУ
    /** Обработка ссылок на договора ЧПУ
     * @param $arParams
     * @return mixed
     */
    private function getSef($arParams) {
        $this->page = 'template';
        $arComponentVariables = array(
            'list',
            'section',
            'detail',
        );
        $arDefaultUrlTemplates404 = array(
            'list' => '/',
            'detail' => '#ID#/',
        );
        $arDefaultVariableAliases404 = array();
        $arDefaultVariableAliases = array();
        $SEF_FOLDER = "";
        $arUrlTemplates = array();
        $path = '';
        if($arParams['SEF_MODE'] == 'Y') {
            $arVariables = array();

            $arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams['SEF_URL_TEMPLATES']);
            $arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases404, $arParams['VARIABLE_ALIASES']);
            $componentPage = CComponentEngine::ParseComponentPath($arParams["SEF_FOLDER"], $arUrlTemplates, $arVariables);
            if (StrLen($componentPage) <= 0) {
                $componentPage = 'template';
            }
            CComponentEngine::initComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);
            $SEF_FOLDER = $arParams['SEF_FOLDER'];

            $arPath = explode('/', $arUrlTemplates[$componentPage]);
            $path = $SEF_FOLDER;
            foreach($arPath as $tPath) {
                if(stristr($tPath, '#')) {
                    $path .= $arVariables[str_replace('#', '', $tPath)] . '/';
                }
            }
        } else {
            $arVariables = array();
            $arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases, $arParams['VARIABLE_ALIASES']);
            CComponentEngine::InitComponentVariables(false, $arComponentVariables, $arVariableAliases, $arVariables);
            $componentPage = 'template';
        }
        $this->arResult['FOLDER'] = $SEF_FOLDER;
        $this->arResult['URL_TEMPLATES'] = $arUrlTemplates;
        $this->arResult['VARIABLES'] = $arVariables;
        $this->arResult['ALIASES'] = $arVariableAliases;
        $this->arResult['CURRENT_PATH'] = $path;

        $this->page = $componentPage;
        return $arParams;
    }
    //endregion
}
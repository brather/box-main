<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Application;

class DataHelpSettingsClass {
    //region Определяем свойства класса
    private $arPost = array();
    private $result = array();
    private $params = array();
    //endregion
    //region Конструктор
    /** Конструктор
     * DataHelp constructor.
     * @param array $result
     */
    public function __construct(array $result, array $params) {
        $this->result = $result;
        $this->params = $params;
    }
    //endregion
    //region Возвращает данные из формы
    /**
     * Возвращает данные из формы
     */
    public function getRequest() {
        $request = Application::getInstance()->getContext()->getRequest();
        $this->arPost = $request->getPostList()->toArray();
        return $this->arPost;
    }
    //endregion
    //region Возвращает html ошибки для формы изменения данных пользователя
    /** Возвращает html ошибки для формы изменения данных пользователя
     * @param $code - код изменяемого параметра на котором проверяется наличие ошибки
     * @return string
     */
    public function getError($code) {
        $html = '';
        if(!empty($this->result['ERROR_F_EDIT_DATA']['F_EDIT_' . $code])) {
            $html = '
            <div class="form__row__col _size2 frn-danger">
                <div class="form__row__name">' . Loc::getMessage('CABINET_T_MESS_ERROR') . '</div>
                <div class="form__row__col__item _size2">
                    ' . $this->result['ERROR_F_EDIT_DATA']['F_EDIT_' . $code] . '
                </div>
            </div>';
        }
        return $html;
    }
    //endregion
    //region Возвращает значения для поля input
    /** Возвращает значения для поля input
     * @param $code - USER_DATA / USER_GROUP
     * @param $sub - код свойства
     * @return mixed
     */
    public function getValueForInput($code, $sub) {
        $result = $this->params[$code][$sub];
        if($this->arPost['F_EDIT_' . $sub]) {
            $result = $this->arPost['F_EDIT_' . $sub];
        }
        return $result;
    }
    //endregion
    //region Проверяет активность поля input (если $btn == true то возвращается значение для кнопки отправки формы)
    /** Проверяет активность поля input (если $btn == true то возвращается значение для кнопки отправки формы)
     * @param bool $btn - параметр отвечающий за проверку кнопки отправки формы
     * @param bool $form - параметр отвечающий за проверку формы отправки формы
     * @return string
     */
    public function checkDisabled($prefForm, $btn = false, $form = false) {
        if(empty($this->result[$prefForm])) {
            if($btn) {
                return ' btn-disabled';
            } elseif($form) {
                return ' no-edit';
            } else {
                return ' disabled';
            }
        }
    }
    //endregion
}
<?php
$MESS['CABINET_T_FORM_LAST_NAME'] = 'Last name: ';
$MESS['CABINET_T_FORM_FIRST_NAME'] = 'Name: ';
$MESS['CABINET_T_FORM_SECOND_NAME'] = 'Second name: ';
$MESS['CABINET_T_FORM_WORK_POSITION'] = 'Work position: ';
$MESS['CABINET_T_FORM_LOGIN'] = 'Login: ';
$MESS['CABINET_T_FORM_WORK_COMPANY'] = 'Company: ';
$MESS['CABINET_T_FORM_ROLE'] = 'Role: ';
$MESS['CABINET_T_FORM_PHONE'] = 'Phone: ';
$MESS['CABINET_T_FORM_MOBILE'] = 'Mobile: ';
$MESS['CABINET_T_FORM_EMAIL'] = 'E-mail: ';
$MESS['CABINET_T_FORM_SAVE'] = 'Save';
$MESS['CABINET_T_FORM_TITLE_EDIT_DATA'] = 'User settings ';

$MESS['CABINET_T_FORM_PLACEHOLDER_NAME'] = 'enter name...';
$MESS['CABINET_T_FORM_PLACEHOLDER_LAST_NAME'] = 'enter last name...';
$MESS['CABINET_T_FORM_PLACEHOLDER_SECOND_NAME'] = 'enter second name...';
$MESS['CABINET_T_FORM_PLACEHOLDER_WORK_POSITION'] = 'enter work position...';
$MESS['CABINET_T_FORM_PLACEHOLDER_EMAIL'] = 'enter email...';
$MESS['CABINET_T_FORM_PLACEHOLDER_PHONE'] = 'enter phone...';
$MESS['CABINET_T_FORM_PLACEHOLDER_MOBILE'] = 'enter mobile...';
$MESS['CABINET_T_FORM_PLACEHOLDER_PASS_CUR'] = 'enter curent password...';
$MESS['CABINET_T_FORM_PLACEHOLDER_PASS_NEW'] = 'enter new password...';
$MESS['CABINET_T_FORM_PLACEHOLDER_PASS_CONFIRM'] = 'repeat new password...';

$MESS['CABINET_T_FORM_TITLE_CHANGE_PASS'] = 'Change password';
$MESS['CABINET_T_FORM_BTN_CHANGE_PASS'] = 'Change';
$MESS['CABINET_T_FORM_PASS_CURENT'] = 'Curent password: ';
$MESS['CABINET_T_FORM_PASS_NEW'] = 'New password: ';
$MESS['CABINET_T_FORM_PASS_NEW_CONFIRM'] = 'Confirm: ';

$MESS['CABINET_T_MESS_EMPTY_CHANGE'] = 'You have not made any changes in the form';
$MESS['CABINET_T_MESS_CANCEL'] = 'Cancel';
$MESS['CABINET_T_MESS_APPLY'] = 'Ok';
$MESS['CABINET_T_MESS_ERROR'] = 'Error!';
$MESS['CABINET_T_MESS_SUCCESS'] = 'Success!';
$MESS['CABINET_T_MESS_EDIT'] = 'Edit ';
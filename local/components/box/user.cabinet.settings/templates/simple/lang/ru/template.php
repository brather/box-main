<?php
$MESS['CABINET_T_FORM_LAST_NAME'] = 'Фамилия: ';
$MESS['CABINET_T_FORM_FIRST_NAME'] = 'Имя: ';
$MESS['CABINET_T_FORM_SECOND_NAME'] = 'Отчество: ';
$MESS['CABINET_T_FORM_WORK_POSITION'] = 'Должность: ';
$MESS['CABINET_T_FORM_LOGIN'] = 'Логин: ';
$MESS['CABINET_T_FORM_WORK_COMPANY'] = 'Организация: ';
$MESS['CABINET_T_FORM_ROLE'] = 'Роль: ';
$MESS['CABINET_T_FORM_PHONE'] = 'Телефон: ';
$MESS['CABINET_T_FORM_MOBILE'] = 'Моб. телефон: ';
$MESS['CABINET_T_FORM_EMAIL'] = 'E-mail: ';
$MESS['CABINET_T_FORM_SAVE'] = 'Сохранить';
$MESS['CABINET_T_FORM_TITLE_EDIT_DATA'] = 'Настройки пользователя ';

$MESS['CABINET_T_FORM_PLACEHOLDER_NAME'] = 'введите имя...';
$MESS['CABINET_T_FORM_PLACEHOLDER_LAST_NAME'] = 'введите фамилию...';
$MESS['CABINET_T_FORM_PLACEHOLDER_SECOND_NAME'] = 'введите отчество...';
$MESS['CABINET_T_FORM_PLACEHOLDER_WORK_POSITION'] = 'введите должность...';
$MESS['CABINET_T_FORM_PLACEHOLDER_EMAIL'] = 'введите email...';
$MESS['CABINET_T_FORM_PLACEHOLDER_PHONE'] = 'введите телефон...';
$MESS['CABINET_T_FORM_PLACEHOLDER_MOBILE'] = 'введите номер мобильного телефона...';
$MESS['CABINET_T_FORM_PLACEHOLDER_PASS_CUR'] = 'введите текущий пароль...';
$MESS['CABINET_T_FORM_PLACEHOLDER_PASS_NEW'] = 'введите новый пароль...';
$MESS['CABINET_T_FORM_PLACEHOLDER_PASS_CONFIRM'] = 'повторите ввод нового пароля...';

$MESS['CABINET_T_FORM_TITLE_CHANGE_PASS'] = 'Смена пароля';
$MESS['CABINET_T_FORM_BTN_CHANGE_PASS'] = 'Изменить';
$MESS['CABINET_T_FORM_PASS_CURENT'] = 'Текущий пароль: ';
$MESS['CABINET_T_FORM_PASS_NEW'] = 'Новый пароль: ';
$MESS['CABINET_T_FORM_PASS_NEW_CONFIRM'] = 'Подтвердить: ';

$MESS['CABINET_T_MESS_EMPTY_CHANGE'] = 'Вы не произвели никаких изменений в форме';
$MESS['CABINET_T_MESS_CANCEL'] = 'Отмена';
$MESS['CABINET_T_MESS_APPLY'] = 'Ok';
$MESS['CABINET_T_MESS_ERROR'] = 'Ошибка!';
$MESS['CABINET_T_MESS_SUCCESS'] = 'Успешно!';
$MESS['CABINET_T_MESS_EDIT'] = 'Редактировать ';
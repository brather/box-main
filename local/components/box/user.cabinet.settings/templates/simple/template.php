<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

use Bitrix\Main\Localization\Loc;

$dataClass = new DataHelpSettingsClass($arResult, $arParams);
$arPost = $dataClass->getRequest();?>

<form action="<?= $arParams['FOLDER'] ?>" method="POST" class="form<?= $dataClass->checkDisabled('ERROR_F_EDIT_DATA', false, true) ?>" id="user-form">
    <div class="form__row">
        <div class="form__row__name"></div>
        <div class="form__row__col__item _size4">
            <h3><?= Loc::getMessage('CABINET_T_FORM_TITLE_EDIT_DATA') ?></h3>
        </div>
    </div>
    <input type="hidden" name="F_CHANGE_DATA" value="Y">
    <input type="hidden"
           name="F_CHANGE_DATA_USER_ID"
           maxlength="50"
           value="<?= $arParams["USER_ID"] ?>"/>
    <?if(!empty($arResult['F_CHANGE_DATA_SUCCESS'])) {?>
        <div class="frn-success hidden"
             data-mess="<?= $arResult['F_CHANGE_DATA_SUCCESS'] ?>",
             data-uri="<?= $arParams['FOLDER'] ?>">
        </div>
    <?}?>
    <div class="form__row">
        <?= $dataClass->getError('LAST_NAME') ?>
        <?= $dataClass->getError('NAME') ?>
        <?= $dataClass->getError('SECOND_NAME') ?>
    </div>
    <div class="form__row">
        <div class="form__row__col">
            <div class="form__row__name"><?= Loc::getMessage('CABINET_T_FORM_LAST_NAME') ?></div>
            <div class="form__row__col__item _size6">
                <input type="text"
                       name="F_EDIT_LAST_NAME"
                       class="form__field"
                       <?= $dataClass->checkDisabled('ERROR_F_EDIT_DATA') ?>
                       placeholder="<?= Loc::getMessage('CABINET_T_FORM_PLACEHOLDER_LAST_NAME') ?>"
                       value="<?= $dataClass->getValueForInput('USER_DATA', 'LAST_NAME') ?>">
            </div>
        </div>
        <div class="form__row__col">
            <div class="form__row__name"><?= Loc::getMessage('CABINET_T_FORM_FIRST_NAME') ?></div>
            <div class="form__row__col__item _size6">
                <input type="text"
                       name="F_EDIT_NAME"
                       class="form__field"
                        <?= $dataClass->checkDisabled('ERROR_F_EDIT_DATA') ?>
                       placeholder="<?= Loc::getMessage('CABINET_T_FORM_PLACEHOLDER_NAME') ?>"
                       value="<?= $dataClass->getValueForInput('USER_DATA', 'NAME') ?>">
            </div>
        </div>
        <div class="form__row__col">
            <div class="form__row__name"><?= Loc::getMessage('CABINET_T_FORM_SECOND_NAME') ?></div>
            <div class="form__row__col__item _size6">
                <input type="text"
                       name="F_EDIT_SECOND_NAME"
                       class="form__field"
                        <?= $dataClass->checkDisabled('ERROR_F_EDIT_DATA') ?>
                       placeholder="<?= Loc::getMessage('CABINET_T_FORM_PLACEHOLDER_SECOND_NAME') ?>"
                       value="<?= $dataClass->getValueForInput('USER_DATA', 'SECOND_NAME') ?>">
            </div>
        </div>
    </div>
    <div class="form__row">
        <?= $dataClass->getError('WORK_POSITION') ?>
    </div>
    <div class="form__row">
        <div class="form__row__col">
            <div class="form__row__name"><?= Loc::getMessage('CABINET_T_FORM_WORK_POSITION') ?></div>
            <div class="form__row__col__item _size6">
                <input type="text"
                       name="F_EDIT_WORK_POSITION"
                       class="form__field"
                        <?= $dataClass->checkDisabled('ERROR_F_EDIT_DATA') ?>
                       placeholder="<?= Loc::getMessage('CABINET_T_FORM_PLACEHOLDER_WORK_POSITION') ?>"
                       value="<?= $dataClass->getValueForInput('USER_DATA', 'WORK_POSITION') ?>">
            </div>
        </div>
        <div class="form__row__col _size5">
            <div class="form__row__name"><?= Loc::getMessage('CABINET_T_FORM_ROLE') ?></div>
            <div class="form__row__col__item _size6">
                <p><?= $arParams['USER_GROUP']['USER_ROLE'] ?></p>
            </div>
        </div>
    </div>
    <div class="form__row">
        <?= $dataClass->getError('EMAIL') ?>
        <?= $dataClass->getError('PERSONAL_PHONE') ?>
        <?= $dataClass->getError('PERSONAL_MOBILE') ?>
    </div>
    <div class="form__row">
        <div class="form__row__col">
            <div class="form__row__name"><?= Loc::getMessage('CABINET_T_FORM_LOGIN') ?></div>
            <div class="form__row__col__item">
                <p><?= $arParams['USER_DATA']['EMAIL'] ?></p>
            </div>
        </div>
        <div class="form__row__col">
            <div class="form__row__name"><?= Loc::getMessage('CABINET_T_FORM_EMAIL') ?></div>
            <div class="form__row__col__item _size6">
                <input type="text"
                       name="F_EDIT_EMAIL"
                       class="form__field"
                        <?= $dataClass->checkDisabled('ERROR_F_EDIT_DATA') ?>
                       placeholder="<?= Loc::getMessage('CABINET_T_FORM_PLACEHOLDER_EMAIL') ?>"
                       value="<?= $dataClass->getValueForInput('USER_DATA', 'EMAIL') ?>">
            </div>
        </div>
        <div class="form__row__col">
            <div class="form__row__name"><?= Loc::getMessage('CABINET_T_FORM_PHONE') ?></div>
            <div class="form__row__col__item _size6">
                <input type="text"
                       name="F_EDIT_PERSONAL_PHONE"
                       data-maskedinput="+9 999 999 9999"
                       class="form__field js-maskedinput"
                        <?= $dataClass->checkDisabled('ERROR_F_EDIT_DATA') ?>
                       placeholder="<?= Loc::getMessage('CABINET_T_FORM_PLACEHOLDER_PHONE') ?>"
                       value="<?= $dataClass->getValueForInput('USER_DATA', 'PERSONAL_PHONE') ?>">
            </div>
        </div>
        <div class="form__row__col">
            <div class="form__row__name"><?= Loc::getMessage('CABINET_T_FORM_MOBILE') ?></div>
            <div class="form__row__col__item _size6">
                <input type="text"
                       name="F_EDIT_PERSONAL_MOBILE"
                       data-maskedinput="+9 999 999 9999"
                       class="form__field js-maskedinput"
                        <?= $dataClass->checkDisabled('ERROR_F_EDIT_DATA') ?>
                       placeholder="<?= Loc::getMessage('CABINET_T_FORM_PLACEHOLDER_MOBILE') ?>"
                       value="<?= $dataClass->getValueForInput('USER_DATA', 'PERSONAL_MOBILE') ?>">
            </div>
        </div>
    </div>
    <div class="form__row">
        <button type="submit" class="btn<?= $dataClass->checkDisabled('ERROR_F_EDIT_DATA', true, false) ?>"><?= Loc::getMessage('CABINET_T_FORM_SAVE') ?></button>
    </div>
    <a href="#" class="js-edit-form" title="<?= Loc::getMessage('CABINET_T_MESS_EDIT') ?>"><?= Loc::getMessage('CABINET_T_MESS_EDIT') ?><i class="fa fa-edit"></i></a>
</form>
<hr>
<form action="<?= $arParams['FOLDER'] ?>" class="form<?= $dataClass->checkDisabled('F_CHANGE_PASS_ERROR', false, true) ?>" method="POST" id="user-form-pass">
    <input type="hidden"
           name="F_CHANGE_PASS_USER_ID"
           maxlength="50"
           value="<?= $arParams["USER_ID"] ?>"/>
    <input type="hidden"
           name="F_CHANGE_PASS_CURRENT_PASS"
           value="<?= $arParams['USER_DATA']['PASSWORD'] ?>">
    <div class="form__row">
        <div class="form__row__name"></div>
        <div class="form__row__col__item _size4">
            <h3><?= Loc::getMessage('CABINET_T_FORM_TITLE_CHANGE_PASS') ?></h3>
        </div>
    </div>
    <?if($arResult['F_CHANGE_PASS_SUCCESS']) {?>
        <div class="frn-success hidden"
             data-mess="<?= $arResult['F_CHANGE_PASS_SUCCESS'] ?>"
             data-uri="<?= $arParams['FOLDER'] ?>">
        </div>
    <?}?>
    <div class="form__row">
        <?$clsCurentPass = '';
        $password = '';
        if(!empty($arResult['F_CHANGE_PASS_ERROR']['NEW_PASS_ERROR'])) {
            $password = $arPost['F_CHANGE_PASS_OLD_PASS'];
        }
        if(!empty($arResult['F_CHANGE_PASS_ERROR']['CURRENT_PASS_EMPTY'])
            || !empty($arResult['F_CHANGE_PASS_ERROR']['CURRENT_PASS_INCORRECT'])){
            $clsCurentPass = ' placeholder-danger';?>
            <div class="form__row__col _size2 frn-danger">
                <div class="form__row__name"><?= Loc::getMessage('CABINET_T_MESS_ERROR') ?></div>
                <div class="form__row__col__item _size2">
                    <?if(!empty($arResult['F_CHANGE_PASS_ERROR']['CURRENT_PASS_EMPTY'])){?>
                        <?= $arResult['F_CHANGE_PASS_ERROR']['CURRENT_PASS_EMPTY'] ?>
                    <?} else {?>
                        <?= $arResult['F_CHANGE_PASS_ERROR']['CURRENT_PASS_INCORRECT'] ?>
                    <?}?>
                </div>
            </div>
        <?}?>
        <div class="form__row__col _size6">
            <div class="form__row__name"><?= Loc::getMessage('CABINET_T_FORM_PASS_CURENT') ?></div>
            <div class="form__row__col__item _size4 js-password">
                <input type="password"
                       name="F_CHANGE_PASS_OLD_PASS"
                       class="form__field<?= $clsCurentPass ?>"
                       autocomplete="off"
                        <?= $dataClass->checkDisabled('F_CHANGE_PASS_ERROR') ?>
                       placeholder="<?= Loc::getMessage('CABINET_T_FORM_PLACEHOLDER_PASS_CUR') ?>"
                       value="<?= $password ?>">
                <a href="#" class="js-show-pass">
                    <i class="fa fa-eye"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="form__row">
        <?$clsNewPass = '';
        if(!empty($arResult['F_CHANGE_PASS_ERROR']['NEW_PASS_ERROR'])){?>
            <?$clsNewPass = ' placeholder-danger';?>
            <div class="form__row__col _size2  frn-danger">
                <div class="form__row__name"><?= Loc::getMessage('CABINET_T_MESS_ERROR') ?></div>
                <div class="form__row__col__item _size2">
                    <?= $arResult['F_CHANGE_PASS_ERROR']['NEW_PASS_ERROR'] ?>
                </div>
            </div>
        <?}?>
        <div class="form__row__col _size6">
            <div class="form__row__name"><?= Loc::getMessage('CABINET_T_FORM_PASS_NEW')?></div>
            <div class="form__row__col__item _size4 js-password">
                <?$newPassword = '';
                if($arPost['USER_NEW_PASSWORD'] && !empty($arPost['ERROR'])) {
                    $newPassword = $arPost['USER_NEW_PASSWORD'];
                }?>
                <input type="password"
                       name="F_CHANGE_PASS_NEW_PASSW"
                       class="form__field<?= $clsNewPass ?>"
                       autocomplete="off"
                        <?= $dataClass->checkDisabled('F_CHANGE_PASS_ERROR') ?>
                       placeholder="<?= Loc::getMessage('CABINET_T_FORM_PLACEHOLDER_PASS_NEW') ?>"
                       value="<?= $newPassword ?>">
                <a href="#" class="js-show-pass">
                    <i class="fa fa-eye"></i>
                </a>
            </div>
        </div>
        <div class="form__row__col _size6">
            <div class="form__row__name"><?= Loc::getMessage('CABINET_T_FORM_PASS_NEW_CONFIRM')?></div>
            <div class="form__row__col__item _size4 js-password">
                <?$confPassword = '';
                if($arPost['USER_CONFIRM_PASSWORD'] && !empty($arPost['ERROR'])) {
                    $confPassword = $arPost['USER_CONFIRM_PASSWORD'];
                }?>
                <input type="password"
                       name="F_CHANGE_PASS_CONFIRM_PASSW"
                       class="form__field<?= $clsNewPass ?>"
                       autocomplete="off"
                        <?= $dataClass->checkDisabled('F_CHANGE_PASS_ERROR') ?>
                       placeholder="<?= Loc::getMessage('CABINET_T_FORM_PLACEHOLDER_PASS_CONFIRM') ?>"
                       value="<?= $confPassword ?>">
                <a href="#" class="js-show-pass">
                    <i class="fa fa-eye"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="form__row">
        <button type="submit" class="btn<?= $dataClass->checkDisabled('F_CHANGE_PASS_ERROR', true) ?>"><?= Loc::getMessage('CABINET_T_FORM_BTN_CHANGE_PASS') ?></button>
    </div>
    <a href="#" class="js-edit-form" title="<?= Loc::getMessage('CABINET_T_MESS_EDIT') ?>"><?= Loc::getMessage('CABINET_T_MESS_EDIT') ?><i class="fa fa-edit"></i></a>
</form>

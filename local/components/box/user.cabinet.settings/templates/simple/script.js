$(document).ready(function(){
    var editf = {
        form    : '.form',
        notedit : 'no-edit',                    // класс-метка по которому определяется возможность редактирования формы
        btnedit : '.js-edit-form',              // класс кнопки редактирования формы
        btnsave : '.btn',                       // кнопка отправки данных формы на сервер
        btndis  : 'btn-disabled'                // класс неактивной кнопки
    };
    $(document).on('click', editf.btnedit, function(e) {
        e.preventDefault();
        var $this = $(this),
            form = $this.closest(editf.form);
        if(form.hasClass(editf.notedit)) {
            form.find('input').attr('disabled', false);
        } else {
            form.find('input').attr('disabled', true);
        }
        form.find(editf.btnsave).toggleClass(editf.btndis);
        form.toggleClass(editf.notedit);
    });

    var blkSuccess = $('.frn-success');

    if(blkSuccess.length > 0) {
        $.confirm({
            theme: 'light',
            backgroundDismiss : true,
            animation           : 'top',
            closeAnimation      : 'bottom',
            title : blkSuccess.data('mess'),
            content : '',
            buttons : {
                confirm: {
                    keys : ['enter', 'esc'],
                    text : 'ok',
                    action : function () {
                        window.location.href = blkSuccess.data('uri');
                    }
                }
            }
        });
    }
});
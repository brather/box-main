<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();?>

<?use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Application;
$arRequest = Application::getInstance()->getContext()->getRequest()->toArray();?>

<div class="order__col">
    <div class="order__col__frame">
        <h2><?= Loc::getMessage('EDIT_DATA') ?></h2>
        <form action="<?= $arParams['FOLDER'] ?><?= $arParams['URL_SUB_SECTION'] ?>"
              method="POST"
              id="f-edit-data"
              class="form">
            <input type="hidden" name="F_CHANGE_DATA" value="Y">
            <input type="hidden"
                   name="F_CHANGE_DATA_USER_ID"
                   maxlength="50"
                   value="<?= $arParams["USER_ID"] ?>"/>
            <ul class="order__list">
                <?foreach($arParams['USER_DATA'] as $cData => $vData){?>
                    <?if($cData != 'PASSWORD') {?>
                        <?if(!empty($arResult['ERROR_F_EDIT_DATA']['F_EDIT_' . $cData])) {?>
                            <li class="text-danger">
                                <div><?= Loc::getMessage('CABINET_T_EDIT_ERROR') ?></div>
                                <div><p><?= $arResult['ERROR_F_EDIT_DATA']['F_EDIT_' . $cData] ?></p></div>
                            </li>
                        <?}?>
                        <li>
                            <div><?= Loc::getMessage('CABINET_T_EDIT_' . $cData) ?></div>
                            <div>
                                <?if(in_array($cData, $arParams['EDIT_DATA'])) {?>
                                    <?$clsDanger = '';
                                    $valInput = '';
                                    $placeholder = Loc::getMessage('CABINET_T_EDIT_PLACEHOLDER');
                                    if(!empty($arRequest['F_EDIT_' . $cData])
                                        || !empty($arResult['ERROR_F_EDIT_DATA']['F_EDIT_' . $cData])) {
                                        if(!empty($arResult['ERROR_F_EDIT_DATA']['F_EDIT_' . $cData])) {
                                            $clsDanger = ' placeholder-danger';
                                            $valInput = '';
                                            $placeholder = $arRequest['F_EDIT_' . $cData];
                                        } else {
                                            $valInput = $arRequest['F_EDIT_' . $cData];
                                        }
                                    } elseif(!empty($vData)) {
                                        $valInput = $vData;
                                    }?>
                                    <?if(stristr($cData, 'PHONE') || stristr($cData, 'MOBILE')) {?>
                                        <input type="text"
                                               placeholder="<?//= $placeholder ?>"
                                               name="F_EDIT_<?= $cData ?>"
                                               class="form__field js-maskedinput<?= $clsDanger ?>"
                                               value="<?= $valInput ?>"
                                               data-maskedinput="+9 999 999 9999">
                                    <?} else {?>
                                        <input type="text"
                                               placeholder="<?//= $placeholder ?>"
                                               name="F_EDIT_<?= $cData ?>"
                                               class="form__field<?= $clsDanger ?>"
                                               value="<?= $valInput ?>">
                                    <?}?>
                                <?} else {?>
                                    <?if($cData == 'ACTIVE'){?>
                                        <?= Loc::getMessage('CABINET_T_EDIT_ACTIVE_' . $vData) ?>
                                    <?} else {?>
                                        <?= $vData ?>
                                    <?}?>
                                <?}?>
                            </div>
                        </li>
                    <?}?>
                <?}?>
            </ul>
            <?if(!empty($arResult['F_CHANGE_DATA_ERROR'])) {?>
                <hr>
                <p class="text-danger"><?= $arResult['F_CHANGE_DATA_ERROR'] ?></p>
            <?} elseif(!empty($arResult['F_CHANGE_DATA_SUCCESS'])) {?>
                <hr>
                <p class="text-success"><?= $arResult['F_CHANGE_DATA_SUCCESS'] ?></p>
            <?}?>
            <hr>
            <button class="btn" type="submit"><?= Loc::getMessage('SAVE') ?></button>
        </form>
    </div>
</div>
<div class="order__col">
    <h2><?= Loc::getMessage('CABINET_T_EDIT_PASSWORD_TITLE') ?></h2>
    <form action="<?= $arParams['FOLDER'] ?><?= $arParams['URL_SUB_SECTION'] ?>"
          id="f-change-pass"
          method="POST"
          name="F_CHANGE_PASS"
          class="form">
        <input type="hidden"
               name="F_CHANGE_PASS_USER_ID"
               maxlength="50"
               value="<?= $arParams["USER_ID"] ?>"/>
        <input type="hidden"
               name="F_CHANGE_PASS_CURRENT_PASS"
               value="<?= $arParams['USER_DATA']['PASSWORD'] ?>">
        <ul class="order__list">
            <?$clsCurentPass = '';
            if(!empty($arResult['F_CHANGE_PASS_ERROR']['CURRENT_PASS_EMPTY'])
                || !empty($arResult['F_CHANGE_PASS_ERROR']['CURRENT_PASS_INCORRECT'])){
                $clsCurentPass = ' placeholder-danger';?>
                <li class="text-danger">
                    <div><?= Loc::getMessage('CABINET_T_EDIT_ERROR') ?></div>
                    <div>
                        <p>
                            <?if(!empty($arResult['F_CHANGE_PASS_ERROR']['CURRENT_PASS_EMPTY'])){?>
                                <?= $arResult['F_CHANGE_PASS_ERROR']['CURRENT_PASS_EMPTY'] ?>
                            <?} else {?>
                                <?= $arResult['F_CHANGE_PASS_ERROR']['CURRENT_PASS_INCORRECT'] ?>
                            <?}?>
                        </p>
                    </div>
                </li>
            <?}?>
            <li>
                <div><?= Loc::getMessage('CABINET_T_EDIT_OLD_PASSWORD') ?></div>
                <div>
                    <input name="F_CHANGE_PASS_OLD_PASS"
                           type="password"
                           placeholder="<?= Loc::getMessage('CABINET_T_EDIT_PLACEHOLDER') ?>"
                           autocomplete="off"
                           value="<?= $arRequest['F_CHANGE_PASS_OLD_PASS'] ?>"
                           class="form__field<?= $clsCurentPass ?>">
                </div>
            </li>
            <?$clsNewPass = '';
            if(!empty($arResult['F_CHANGE_PASS_ERROR']['NEW_PASS_ERROR'])){?>
                <?$clsNewPass = ' placeholder-danger';?>
                <li class="text-danger">
                    <div><?= Loc::getMessage('CABINET_T_EDIT_ERROR') ?></div>
                    <div>
                        <p>
                            <?= $arResult['F_CHANGE_PASS_ERROR']['NEW_PASS_ERROR'] ?>
                        </p>
                    </div>
                </li>
            <?}?>
            <li>
                <div><?= Loc::getMessage('CABINET_T_EDIT_NEW_PASSWORD') ?></div>
                <div>
                    <input name="F_CHANGE_PASS_NEW_PASSW"
                           type="password"
                           placeholder="<?= Loc::getMessage('CABINET_T_EDIT_PLACEHOLDER') ?>"
                           value="<?= $arRequest['F_CHANGE_PASS_NEW_PASSW'] ?>"
                           autocomplete="off"
                           class="form__field<?= $clsNewPass ?>">
                </div>
            </li>
            <li>
                <div><?= Loc::getMessage('CABINET_T_EDIT_CONFIRM_PASSWORD') ?></div>
                <div>
                    <input name="F_CHANGE_PASS_CONFIRM_PASSW"
                           type="password"
                           placeholder="<?= Loc::getMessage('CABINET_T_EDIT_PLACEHOLDER') ?>"
                           value="<?= $arRequest['F_CHANGE_PASS_CONFIRM_PASSW'] ?>"
                           autocomplete="off"
                           class="form__field<?= $clsNewPass ?>">
                </div>
            </li>
        </ul>
        <?if(!empty($arResult['F_CHANGE_PASS_SUCCESS'])) {?>
            <hr>
            <p class="text-success"><?= $arResult['F_CHANGE_PASS_SUCCESS'] ?></p>
        <?}?>
        <hr>
        <button class="btn" type="submit" name="F_CHANGE_PASS_SUBMIT">
            <?= Loc::getMessage('CABINET_T_EDIT_PASSWORD_BUTTON') ?>
        </button>
    </form>
</div>


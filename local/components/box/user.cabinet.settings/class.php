<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application,
    \Bitrix\Iblock\IblockTable;

class UserCabinetSettingsComponent extends CBitrixComponent {
    //region Задание свойств класса
    private $page = 'template';
    private $arRequest = array();
    private $arClientIblock = array();
    //endregion
    //region Подготовка входных параметров компонента
    /** Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        return parent::onPrepareComponentParams($arParams);
    }
    //endregion
    //region Подключение файлов перевода
    /**
     * Подключение файлов перевода
     */
    public function onIncludeComponentLang() {
        Loc::loadMessages(__FILE__);
    }
    //endregion
    //region Выполнение компонента
    /**
     * Выполнение компонента
     */
    public function executeComponent() {
        $this->getClientID();                                   // Вернем данные по клиенту
        $this->getRequest();                                    // Вернем данные из формы
        $this->chacngeData();                                   // Изменение данных клиента
        $this->changePassword();                                // Изменение пароля
        $this->includeComponentTemplate($this->page);
    }
    //endregion
    //region Получение данных request
    /**
     * Получение данных request
     */
    private function getRequest() {
        $this->arRequest = Application::getInstance()->getContext()->getRequest()->toArray();
    }
    //endregion
    //region Изменение данных пользователя
    /**
     * Изменение данных пользователя
     */
    private function chacngeData() {
        if(!empty($this->arRequest['F_CHANGE_DATA'])) {
            $arError = array();
            $arField = array();
            $fullName = $this->getFullName();
            foreach($this->arRequest as $cReq => $vReq) {
                if($cReq != 'F_CHANGE_DATA') {
                    $code = str_replace('F_EDIT_', '', $cReq);
                    $arField[$code] = $vReq;
                    if(empty($vReq)) {
                        $arError[$cReq] = Loc::getMessage('F_CHANGE_DATA_ERROR_EMPTY_' . $code);
                    } else {
                        if($cReq == 'F_EDIT_PERSONAL_PHONE' || $cReq == 'F_EDIT_PERSONAL_MOBILE') {
                            if(!checkPhone($vReq)) {
                                $arError[$cReq] = Loc::getMessage('F_CHANGE_DATA_ERROR_INCORECT_' . $code);
                            }
                        } elseif($cReq == 'F_EDIT_NAME' || $cReq == 'F_EDIT_SECOND_NAME' || $cReq == 'F_EDIT_LAST_NAME') {
                            if(!checkWord($vReq)) {
                                $arError[$cReq] = Loc::getMessage('F_CHANGE_DATA_ERROR_INCORECT_' . $code);
                            }
                        }
                    }
                }
            }
            if(!empty($arError)) {
                $this->arResult['ERROR_F_EDIT_DATA'] = $arError;
            } else {
                if($fullName != $this->arClientIblock['NAME']) {
                    $this->updateClientName($fullName);
                }
                $user = new CUser();
                if($user->Update($this->arRequest['F_CHANGE_DATA_USER_ID'], $arField)) {
                    $this->arResult['F_CHANGE_DATA_SUCCESS'] = Loc::getMessage('F_CHANGE_DATA_SUCCESS');
                } else {
                    $this->arResult['F_CHANGE_DATA_ERROR'] = $user->LAST_ERROR;
                }
            }
        }
    }
    //endregion
    //region Возвращает ФИО из формы
    /** Возвращает ФИО из формы
     * @return string
     */
    private function getFullName() {
        $result = '';
        if(!empty($this->arRequest['F_EDIT_LAST_NAME'])) {
            $result .= $this->arRequest['F_EDIT_LAST_NAME'];
        }
        if(!empty($this->arRequest['F_EDIT_NAME'])) {
            if($result != '') {
                $result .= ' ';
            }
            $result .= $this->arRequest['F_EDIT_NAME'];
        }
        if(!empty($this->arRequest['F_EDIT_SECOND_NAME'])) {
            if($result != '') {
                $result .= ' ';
            }
            $result .= $this->arRequest['F_EDIT_SECOND_NAME'];
        }
        return $result;
    }
    //endregion
    //region Изменение пароля
    /**
     * Изменение пароля
     */
    private function changePassword() {
        if(!empty($this->arRequest['F_CHANGE_PASS_USER_ID'])) {
            $this->checkOldPassword();
        }
    }
    //endregion
    //region Обновление ФИО в инфоблоке клиентов
    /**
     * Обновление ФИО в инфоблоке клиентов
     */
    private function updateClientName($name) {
        $arClient = $this->arClientIblock;
        $arLoadClient = array('NAME' => $name);
        $client = new CIBlockElement;
        $client->Update($arClient['ID'], $arLoadClient);
    }
    //endregion
    //region Возвращает массив с данными клиента в инфоблоке клиентов
    /** Возвращает массив с данными клиента в инфоблоке клиентов
     * @return mixed
     */
    private function getClientID() {
        $userID = $this->arParams['USER_ID'];
        if(empty($userID)) {
            global $USER;
            $userID = $USER->GetID();
            $this->arParams['USER_ID'] = $userID;
        }
        $arOrderClient = array('ID' => 'ASC');
        $arFilterClient = array(
            'IBLOCK_CODE' => $this->arParams['IBLOCK_CODE_CLIENTS'],
            'PROPERTY_PROP_CLIENT' => $userID
        );
        $arSelectClient = array('IBLOCK_ID', 'ID', 'NAME');
        $this->arClientIblock = CIBlockElement::GetList($arOrderClient, $arFilterClient, false, false, $arSelectClient)->Fetch();
    }
    //endregion
    //region Возвращает ID инфоблока
    /** Возвращает ID инфоблока
     * @param $iblockCode - код инфоблока
     * @return mixed
     */
    private function getIBlockID($iblockCode) {
        $arIBlock = IblockTable::getList(array(
            'filter' => array('CODE' => $iblockCode),
            'select' => array('ID')
        ))->fetch();
        return $arIBlock['ID'];
    }
    //endregion
    //region Проверка пароля
    /**
     * Проверка пароля
     */
    private function checkOldPassword() {
        if(!empty($this->arRequest['F_CHANGE_PASS_OLD_PASS'])) {
            $salt = substr($this->arRequest['F_CHANGE_PASS_CURRENT_PASS'], 0, (strlen($this->arRequest['F_CHANGE_PASS_CURRENT_PASS']) - 32));
            $realPassword = substr($this->arRequest['F_CHANGE_PASS_CURRENT_PASS'], -32);
            $rCurentPass = md5($salt . $this->arRequest['F_CHANGE_PASS_OLD_PASS']);
            if($realPassword == $rCurentPass) {
                if(!empty($this->arRequest['F_CHANGE_PASS_NEW_PASSW'])
                    || !empty($this->arRequest['F_CHANGE_PASS_CONFIRM_PASSW'])) {
                    if(strlen($this->arRequest['F_CHANGE_PASS_NEW_PASSW']) < $this->arParams['MIN_COUNT_SYMBOL_PASS']) {
                        $this->arResult['F_CHANGE_PASS_ERROR']['NEW_PASS_ERROR'] = Loc::getMessage('F_CHANGE_PASS_ERROR_NEW_PASS_SHORT_1')
                            . $this->arParams['MIN_COUNT_SYMBOL_PASS']
                            . Loc::getMessage('F_CHANGE_PASS_ERROR_NEW_PASS_SHORT_2');
                    } else {
                        if($this->arRequest['F_CHANGE_PASS_NEW_PASSW'] != $this->arRequest['F_CHANGE_PASS_CONFIRM_PASSW']) {
                            $this->arResult['F_CHANGE_PASS_ERROR']['NEW_PASS_ERROR'] = Loc::getMessage('F_CHANGE_PASS_ERROR_NEW_PASS_NOT_MATCH');
                        } else {
                            $user = new CUser;
                            $fields = array(
                                "PASSWORD" => $this->arRequest['F_CHANGE_PASS_NEW_PASSW']
                            );
                            $user->Update($this->arRequest['F_CHANGE_PASS_USER_ID'], $fields);
                            if(!empty($user->LAST_ERROR)) {
                                $this->arResult['F_CHANGE_PASS_ERROR']['NEW_PASS_ERROR'] = $user->LAST_ERROR;
                            } else {
                                $this->arResult['F_CHANGE_PASS_SUCCESS'] = Loc::getMessage('F_CHANGE_PASS_SUCCESS');
                            }
                        }
                    }
                } else {
                    if(empty($this->arRequest['F_CHANGE_PASS_NEW_PASSW']) && empty($this->arRequest['F_CHANGE_PASS_CONFIRM_PASSW'])) {
                        $this->arResult['F_CHANGE_PASS_ERROR']['NEW_PASS_ERROR'] = Loc::getMessage('F_CHANGE_PASS_ERROR_NEW_PASS_EMPTY');
                    } elseif(empty($this->arRequest['F_CHANGE_PASS_NEW_PASSW'])) {
                        $this->arResult['F_CHANGE_PASS_ERROR']['NEW_PASS_ERROR'] = Loc::getMessage('F_CHANGE_PASS_ERROR_NEW_PASS_EMPTY');
                    } elseif(empty($this->arRequest['F_CHANGE_PASS_CONFIRM_PASSW'])) {
                        $this->arResult['F_CHANGE_PASS_ERROR']['NEW_PASS_ERROR'] = Loc::getMessage('F_CHANGE_PASS_ERROR_CONFIRM_PASS_EMPTY');
                    }
                }
            } else {
                $this->arResult['F_CHANGE_PASS_ERROR']['CURRENT_PASS_INCORRECT'] = Loc::getMessage('F_CHANGE_PASS_ERROR_CURRENT_PASS_INCORECT');
            }
        } else {
            $this->arResult['F_CHANGE_PASS_ERROR']['CURRENT_PASS_EMPTY'] = Loc::getMessage('F_CHANGE_PASS_ERROR_CURRENT_PASS_EMPTY');
        }
    }
    //endregion
}
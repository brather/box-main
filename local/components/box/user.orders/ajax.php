<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 18.04.2017
 * Time: 14:51
 */
require $_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php";

use Bitrix\Main\Application;
use Bitrix\Main\Loader;

global $USER;

$rq = Application::getInstance()->getContext()->getRequest();

/* if not ajax method, not post request, in post data not {ORDER_ID} value and this user is not authorised return die */
if (
    $rq->isAjaxRequest() === false
    && $rq->isPost() === false
    && !$rq->getPost('ORDER_ID')
    && !$USER->IsAuthorized()
) die();

Loader::includeModule('iblock');


$error = false;
$result = array();

$db = CIBlockElement::GetByID((int)$rq->getPost('ORDER_ID'));
$u_id = $USER->GetID();

if ($res = $db->Fetch()) {
    /* Действительно авторизированный пользователь является создателем этого черновика */
    if ( $res['CREATED_BY'] == $USER->GetID()) {
        /* Точно ли прошло удаление */
        if (!CIBlockElement::Delete($res['ID'])) {
            $error[] = "Something wrong in procedure remove order, make later.";
        } else {
            $result["REMOVED"] = "1";
        }
    } else {
        $error[] = "This user not permitted to remove this order.";
    }
} else {

    $error[] = "No order with this ID [" . $rq->getPost('ORDER_ID') .  "]";
}
/* если были какие то ошибки вернем их в результате */
if ($error !== false) {
    $result['REMOVED'] = "0";
    $result['ERRORS'] = $error;
}

die(json_encode($result));
<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Loader;

class UserCabinetOrdersComponent extends CBitrixComponent {
    //region Задание свойств класса
    private $page = 'template';
    //endregion
    //region Подготовка входных параметров компонента
    /** Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        $arParams = $this->getSef($arParams);
        return parent::onPrepareComponentParams($arParams);
    }
    //endregion
    //region Подключение файлов перевода
    /**
     * Подключение файлов перевода
     */
    public function onIncludeComponentLang() {
        Loc::loadMessages(__FILE__);
    }
    //endregion
    //region Выполнение компонента
    /**
     * Выполнение компонента
     */
    public function executeComponent() {

        $this->includeModule();                                 // Подключение модулей
        $this->getUserID();                                     // Возвращает в результирующий массив USER_ID
        $this->getCodeUserGroup();                              // Определяет массив с кодами групп которым принадлежит пользователь
        $this->getHtmlPageTitle();                              // Возвращает html блока с заголовком страницы
        $this->includeComponentTemplate($this->page);           // Подключение шаблона
    }
    //endregion
    //region Подключаем список модулей
    /**
     * Подключаем список модулей
     */
    private function includeModule() {
        Loader::includeModule('iblock');
    }
    //endregion
    //region Возвращает в результирующий массив USER_ID
    /**
     * Возвращает в результирующий массив USER_ID
     */
    private function getUserID() {
        global $USER;
        $this->arResult['USER_ID'] = $USER->GetID();
    }
    //endregion
    //region Возвращает код группы которой принадлежит пользователь
    /**
     * Возвращает код группы которой принадлежит пользователь
     */
    private function getCodeUserGroup() {
        $res = CUser::GetUserGroupList($this->arResult['USER_ID']);
        $arUserGroupID = array();
        while ($arGroup = $res->Fetch()){
            $arUserGroupID[] = $arGroup['GROUP_ID'];
        }
        $arFilterGroups = array('ACTIVE' => 'Y', 'ID' => implode('|', $arUserGroupID));
        $rsGroups = CGroup::GetList($by = 'SORT', $order = 'ASC', $arFilterGroups);
        $arUserGroupCode = array();
        while($obGroup = $rsGroups->Fetch()) {
            $arUserGroupCode[] = $obGroup['STRING_ID'];
        }
        $this->arResult['USER_GROUPS'] = $arUserGroupCode;
    }
    //endregion
    //region Возвращает html блока с заголовком страницы
    /**
     * Возвращает html блока с заголовком страницы
     */
    private function getHtmlPageTitle() {
        $html = '<div class="page__title';
        $arTemplateRequests = array('requests', 'drequest');            // массив с шаблонами запросов
        $arTemplateWithoutSplit = array('create', 'detail', 'drequest');
        $clsDisable = ' class="disable"';
        $clsFirstTitle = '';
        $clsSecondTitle = '';
        if(in_array($this->page, $arTemplateRequests)) {
            $clsFirstTitle = $clsDisable;
        } else {
            $clsSecondTitle = $clsDisable;
        }
        if(in_array($this->page, $arTemplateWithoutSplit)) {
            $html .= ' no-split';
        }
        $html .= '">
                <h1' . $clsFirstTitle . '>
                    <a href="' . $this->arParams['SEF_FOLDER'] . '">
                        ' . Loc::getMessage('U_CABINET_ORDERS_C_TITLE_ORDERS') . '
                    </a>
                </h1>';
        if($this->arParams['USE_REQUESTS_MENU'] == 'Y') {
            $html .= '
                <h1' . $clsSecondTitle . '>
                    <a href="' . $this->arParams['SEF_FOLDER'] . 'requests/">
                        ' . Loc::getMessage('U_CABINET_ORDERS_C_TITLE_REQUESTS') . '
                    </a>
                </h1>';
        }
        $html .= '</div>';
        $this->arResult['PAGE_TITLE'] = $html;
    }
    //endregion
    //region Обработка ссылок на договора ЧПУ
    /** Обработка ссылок на договора ЧПУ
     * @param $arParams
     * @return mixed
     */
    private function getSef($arParams) {
        $this->page = 'template';
        $arComponentVariables = array(
            'list',
            'detail',
            'drafts',
            'completed',
            'deleted',
            'create',
            'requests',
            'drequest',
        );
        $arDefaultUrlTemplates404 = array(
            'list' => '/',
            'drafts' => 'drafts/',
            'completed' => 'completed/',
            'deleted' => 'deleted/',
            'detail' => '#ID#/',
            'create' => 'create/',
            'requests' => 'requests/',
            'drequest' => 'requests/#ID#/',
        );
        $arDefaultVariableAliases404 = array();
        $arDefaultVariableAliases = array();
        $SEF_FOLDER = "";
        $arUrlTemplates = array();
        $path = '';
        if($arParams['SEF_MODE'] == 'Y') {
            $arVariables = array();

            $arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams['SEF_URL_TEMPLATES']);
            $arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases404, $arParams['VARIABLE_ALIASES']);
            $componentPage = CComponentEngine::ParseComponentPath($arParams["SEF_FOLDER"], $arUrlTemplates, $arVariables);
            if (StrLen($componentPage) <= 0) {
                $componentPage = 'template';
            }
            CComponentEngine::initComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);
            $SEF_FOLDER = $arParams['SEF_FOLDER'];

            $arPath = explode('/', $arUrlTemplates[$componentPage]);
            $path = $SEF_FOLDER;
            foreach($arPath as $tPath) {
                if(stristr($tPath, '#')) {
                    $path .= $arVariables[str_replace('#', '', $tPath)] . '/';
                }
            }
        } else {
            $arVariables = array();
            $arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases, $arParams['VARIABLE_ALIASES']);
            CComponentEngine::InitComponentVariables(false, $arComponentVariables, $arVariableAliases, $arVariables);
            $componentPage = 'template';
        }
        $this->arResult['FOLDER'] = $SEF_FOLDER;
        $this->arResult['URL_TEMPLATES'] = $arUrlTemplates;
        $this->arResult['VARIABLES'] = $arVariables;
        $this->arResult['ALIASES'] = $arVariableAliases;
        $this->arResult['CURRENT_PATH'] = $path;

        $this->page = $componentPage;
        return $arParams;
    }
    //endregion
}
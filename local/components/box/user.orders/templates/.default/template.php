<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */
global $APPLICATION;

$APPLICATION->IncludeComponent(
    'box:user.orders.list',
    '',
    array(
        'USER_ID' => $arResult['USER_ID'],
        'SEF_FOLDER' => $arParams['SEF_FOLDER'],
        'USER_GROUPS' => $arResult['USER_GROUPS'],
        'IBLOCK_CODE_ORDERS' => $arParams['IBLOCK_CODE_ORDERS'],
        'COUNT_ITEM_ON_PAGE' => $arParams['COUNT_ITEM_ON_PAGE'],
        'NAV_TEMPLATE' => $arParams['NAV_TEMPLATE'],
        'HTML_PAGE_TITLE' => urlencode($arResult['PAGE_TITLE']),
        'SET_SORT_FILTER' => $arParams['LIST_ORDERS']['SORT_FIELDS'],
        'HL_CODE_STATUS' => $arParams['LIST_ORDERS']['HL_CODE_STATUS'],
        'IBLOCK_CODE_CONTRACTS' => $arParams['IBLOCK_CODE_CONTRACTS'],
        /* Показывать заказы на этой странице только с определенным статусом */
        'PAGE_TYPES_FROM_STATUS' => array(
            "processing", "execution", "prepared"
        ),
    )
);?>
<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */
global $APPLICATION;?>

<?$APPLICATION->IncludeComponent(
    'box:user.orders.detail',
    'request',
    array(
        'USER_ID' => $arResult['USER_ID'],
        'USER_GROUPS' => $arResult['USER_GROUPS'],                                  // Массив с кодами групп которым принадлежит текущий пользователь
        'ORDER_ID' => $arResult['VARIABLES']['ID'],
        'CURENT_PATH' => $arResult['CURENT_PATH'],
        'LIST_URL' => $arParams['SEF_FOLDER'] . 'requests/',
        'IBLOCK_CODE_ORDERS' => $arParams['IBLOCK_CODE_ORDERS'],
        'IBLOCK_CODE_CONTRACTS' => $arParams['IBLOCK_CODE_CONTRACTS'],
        'COUNT_ITEM_ON_PAGE' => $arParams['COUNT_ITEM_ON_PAGE'],
        'SET_SORT_FILTER' => $arParams['SET_SORT_FILTER'],
        'NAV_PRINT' => 'Y',
        'NAV_TEMPLATE' => $arParams['NAV_TEMPLATE'],
        'HTML_PAGE_TITLE' => urlencode($arResult['PAGE_TITLE']),
        'HL_CODE_STATUS' => $arParams['LIST_ORDERS']['HL_CODE_STATUS'],
    )
);?>
<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */
global $APPLICATION;?>

<?$APPLICATION->IncludeComponent(
    'box:user.orders.detail',
    '',
    array(
        'USER_ID' => $arResult['USER_ID'],
        'USER_GROUPS' => $arResult['USER_GROUPS'],
        'ORDER_ID' => $arResult['VARIABLES']['ID'],
        'CURENT_PATH' => $arResult['CURENT_PATH'],
        'LIST_URL' => $arParams['SEF_FOLDER'],
        'IBLOCK_CODE_ORDERS' => $arParams['IBLOCK_CODE_ORDERS'],
        'IBLOCK_CODE_CONTRACTS' => $arParams['IBLOCK_CODE_CONTRACTS'],
        'IBLOCK_CODE_CLIENTS' => $arParams['IBLOCK_CODE_CLIENTS'],
        'COUNT_ITEM_ON_PAGE' => $arParams['COUNT_ITEM_ON_PAGE'],
        'SET_SORT_FILTER' => $arParams['SET_SORT_FILTER'],
        'NAV_PRINT' => 'Y',
        'NAV_TEMPLATE' => $arParams['NAV_TEMPLATE'],
        'HTML_PAGE_TITLE' => urlencode($arResult['PAGE_TITLE']),
        'HL_CODE_STATUS' => $arParams['LIST_ORDERS']['HL_CODE_STATUS'],
        'HL_CODE_ADRESS_CLIENT' => $arParams['LIST_ORDERS']['HL_CODE_ADRESS_CLIENT'],
        'HL_CODE_STATUS_OF_WMS' => $arParams['LIST_ORDERS']['HL_CODE_STATUS_OF_WMS'], // код hl блока со статусами wms
    )
);?>
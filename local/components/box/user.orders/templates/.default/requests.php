<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */
global $APPLICATION;

$APPLICATION->IncludeComponent(
    'box:user.orders.list',
    '',
    array(
        'USER_ID' => $arResult['USER_ID'],
        'REQUEST' => 'Y',                                                           // Свойства "Запросы" должно быть со значением "Y"
        'USER_GROUPS' => $arResult['USER_GROUPS'],                                  // Массив с кодами групп которым принадлежит текущий пользователь
        'SEF_FOLDER' => $arParams['SEF_FOLDER'] . 'requests/',
        'FOLDER' => $arParams['SEF_FOLDER'],
        'IBLOCK_CODE_ORDERS' => $arParams['IBLOCK_CODE_ORDERS'],
        'IBLOCK_CODE_CONTRACTS' => $arParams['IBLOCK_CODE_CONTRACTS'],
        'COUNT_ITEM_ON_PAGE' => $arParams['COUNT_ITEM_ON_PAGE'],
        'NAV_TEMPLATE' => $arParams['NAV_TEMPLATE'],
        'HTML_PAGE_TITLE' => urlencode($arResult['PAGE_TITLE']),
        'SET_SORT_FILTER' => $arParams['LIST_ORDERS']['SORT_FIELDS'],
        'HL_CODE_STATUS' => $arParams['LIST_ORDERS']['HL_CODE_STATUS'],
    )
);?>
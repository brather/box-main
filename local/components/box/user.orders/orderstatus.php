<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 15.06.2017
 * Time: 12:06
 */
require $_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php";

use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Application;
use Bitrix\Main\Loader;

global $USER;

$arError = [
    'ONF' => 'В БД WMS отсутствует заказ',
    'EST' => 'Превышено время ожидания ответа от WMS'
];

$rq = Application::getInstance()->getContext()->getRequest();

/* if not ajax method, not post request, in post data not {ORDER_ID} value and this user is not authorised return die */
if (
    $rq->isAjaxRequest() === false
    || $rq->isPost() === false
    || !$rq->getPost('ORDER_ID')
    || !$USER->IsAuthorized()
) _jR('Bad request', true);

Loader::includeModule('iblock');
Loader::includeModule('highloadblock');

/*
 * Обработка
 * Подготовлен
 * */

//region/* Cache Order Status */
$cache = new CPHPCache();
$st = [];
$cTime = 86400;
$cDir = "status_order";
if ($cache->InitCache($cTime, md5(HL_BLOCK_CODE_STATUS_ORDER), $cDir)) {
    $c = $cache->GetVars();
    $st = $c['STATUS_ORDER'];
    unset($c);
}
if ($cache->StartDataCache($cTime, md5(HL_BLOCK_CODE_STATUS_ORDER), $cDir)) {
    $hlblock = HighloadBlockTable::getById(7)->fetch();
    $entity = HighloadBlockTable::compileEntity($hlblock);
    $obClass = $entity->getDataClass();

    $rsStatus = $obClass::getList(array(
        'select' => array('ID', 'UF_NAME', 'UF_XML_ID'),
        'order' => array('UF_SORT' => 'ASC'),
        'limit' => ''
    ));

    while($obStatus = $rsStatus->fetch()) {
        $st[$obStatus['UF_XML_ID']] = $obStatus['UF_NAME'];
    }

    $cache->EndDataCache(array('STATUS_ORDER' => $st));
}
//endregion/* End cache Order Status */

$client = new EME\WMS(SOAP_TEST_MODE);

$res = $client->GetStatusOrder($rq->getPost('ORDER_ID'));
$prop = CIBlockElement::GetProperty(IBLOCK_CODE_ORDERS_ID, $rq->getPost('ORDER_ID'), "sort", "asc", ['CODE' => 'STATUS'])->Fetch();

/* Ошибка в запросе */
if ($res->GetStatusOrderResult->state > 0 && !isset($res->GetStatusOrderResult->listStatusOrder) && strpos("formation canceled", $prop['VALUE']) === false) {
    _jR($arError[$res->GetStatusOrderResult->descriptionError], true);
}
/* Заказ есть в EME и можно посмотреть его статус */
else if ($res->GetStatusOrderResult->state == 0 && isset($res->GetStatusOrderResult->listStatusOrder)) {
    CIBlockElement::SetPropertyValuesEx($rq->getPost('ORDER_ID'), IBLOCK_CODE_ORDERS_ID,
        ['STATUS' => array_search($res->GetStatusOrderResult->listStatusOrder->StatusOrders->currentStatus, $st)]);
}
/** Если WMS на "Формирующийся" или "Анулированный" заказ вернет НИЧЕГО,
 *  оставим статус "Формирование" или "Анулирован", обновлять ничего не нужно */
else if (strpos("formation canceled", $prop['VALUE']) !== false) {
    _jR(['status' => $st[$prop['VALUE']]]);
}

_jR(['status' => $res->GetStatusOrderResult->listStatusOrder->StatusOrders->currentStatus]);
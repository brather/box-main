<?php
use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 22.05.2017
 * Time: 10:43
 */

class ElarCompanyList extends CBitrixComponent {

    private $arGet  = array();
    private $arPost = array();
    private $prefix = 'F_COMPANY_';

    public function executeComponent()
    {
        Loader::includeModule('iblock');
        Loader::includeModule("highloadblock");

        $request        = Application::getInstance()->getContext()->getRequest();
        $this->arGet    = $request->getQueryList()->toArray();
        $this->arPost   = $request->getPostList()->toArray();

        $this->_rowFilter();
        $this->_getCompanyList();

        $this->includeComponentTemplate();
    }

    private function _getCompanyList() {

        if(!empty($this->arGet['SORT']) && !empty($this->arGet['ORDER'])) {
            $arOrderBy = array($this->arGet['SORT'] => $this->arGet['ORDER']);
        } else {
            $arOrderBy = array('ID' => 'DESC');
        }

        $arFilter = array( 'IBLOCK_ID' => IBLOCK_CODE_COMPANY_ID );

        /* Filter By */
        if($this->arGet[$this->prefix . 'FILTER'] == 'Y') {
            foreach ($this->arGet as $idx => $value) {
                if (stristr($idx, 'F_') === false || $idx === 'F_COMPANY_FILTER') continue; // не значения для фильтра
                if (empty($value) ) continue; // продолжить, значение пустое

                $idx  = ltrim($idx, 'F_'); // удалить префикс

                if ("ID" === $idx)
                    $arFilter[$idx] = $value;
                else if ("CONTACT_FACE" === $idx) {
                    $clientIn = array();

                    /* Выберем клиентов из */
                    $db = CIBlockElement::GetList(
                        array("ID" => "DESC"),
                        array(
                            "IBLOCK_ID" => IBLOCK_CODE_CLIENTS_ID,
                            "%NAME" => $value
                        ),
                        false, false,
                        array("PROPERTY_PROP_CLIENT")
                    );
                    while ($res = $db->Fetch()) {
                        $clientIn[] = $res['PROPERTY_PROP_CLIENT_VALUE'];
                    }

                    $db = CIBlockElement::GetList(
                        array("ID" => "DESC"),
                        array(
                            "IBLOCK_ID" => IBLOCK_CODE_CONTRACTS_ID,
                            "=PROPERTY_PROP_SUPER_USER" => $clientIn
                        ),
                        false, false,
                        array("PROPERTY_PROP_COMPANY")
                    );

                    while ($res = $db->Fetch())
                        $arFilter['ID'][] = $res['PROPERTY_PROP_COMPANY_VALUE'];
                } else if ("MOBILE" === $idx) {
                    $arFilter['PROPERTY_PROP_PHONE'] = $value;
                } else if ("EMAIL" === $idx) {
                    $arFilter['PROPERTY_EMAIL'] = $value;
                } else if ("INN" === $idx) {
                    $arFilter['PROPERTY_PROP_INN'] = $value;
                }
            }
        }

        $arNavParams = array('nPageSize' => $this->arParams['COMPANY_ON_PAGE']);
        if(!empty($this->arGet['COMPANY_ON_PAGE']) && is_numeric($this->arGet['COMPANY_ON_PAGE'])) {
            $arNavParams['nPageSize'] = $this->arGet['COMPANY_ON_PAGE'];
        }

        $arSelect = array(
            'ID',
            'NAME',
            'PROPERTY_PROP_INN',
            'PROPERTY_PROP_PHONE',
            'PROPERTY_EMAIL',
            'PROPERTY_PROP_ADDITIONAL_INFORM'
        );

        $this->arResult['ROW'] = array(
            0 => 'ID',
            1 => 'NAME',
            2 => 'PROP_INN',
            3 => 'CONTACT_FACE',
            4 => 'EMAIL',
            5 => 'MOBILE',
            6 => 'COMMENT'
        );

        $db = CIBlockElement::GetList(
            $arOrderBy,
            $arFilter,
            false,
            $arNavParams,
            $arSelect
        );

        $this->arResult['CNT_COMPANIES'] = $db->SelectedRowsCount();
        $this->arResult['NAV_PRINT'] = $db->GetPageNavStringEx($navComponentObject, Loc::getMessage('USER_ORDERS_C_PAGES'), $this->arParams['NAV_TEMPLATE']);

        while ($ob = $db->Fetch()) {

            $this->arResult['COMPANY_LIST'][$ob['ID']] = array(
                'ID'        => $ob['ID'],
                'NAME'      => $ob['NAME'],
                'PROP_INN'  => $ob['PROPERTY_PROP_INN_VALUE'],
                'MOBILE'    => $ob['PROPERTY_PROP_PHONE_VALUE'],
                'EMAIL'     => $ob['PROPERTY_EMAIL_VALUE'],
                'COMMENT'   => $ob['PROPERTY_PROP_ADDITIONAL_INFORM_VALUE'],
            );

            $client = $this->_getClientByCompany($ob['ID']);

            if ($client !== null) {
                $this->arResult['COMPANY_LIST'][$ob['ID']]['CONTACT_FACE'] = $client['CONTACT_FACE']['LAST_NAME'] . " " . $client['CONTACT_FACE']['NAME'] . " " . $client['CONTACT_FACE']['SECOND_NAME'];
                $this->arResult['COMPANY_LIST'][$ob['ID']]['CONTACT_FACE_ID'] = $client['ID'];
            } else {
                $this->arResult['COMPANY_LIST'][$ob['ID']]['CONTACT_FACE'] = $this->arResult['COMPANY_LIST'][$ob['ID']]['CONTACT_FACE_ID'] = "";
            }

        }
    }

    private function _rowFilter() {

        $this->arResult['ROW_FILTER'] = array(
            0 => 'ID',
            1 => 'INN',
            2 => 'CONTACT_FACE',
            3 => 'EMAIL',
            4 => 'MOBILE',
        );
    }

    private function _getClientByCompany($companyID) {

        if (is_string($companyID) || is_numeric($companyID))
            $arFilter = array("IBLOCK_ID" => IBLOCK_CODE_CONTRACTS_ID, "PROPERTY_PROP_COMPANY" => $companyID);
        else if (is_array($companyID) && count($companyID))
            $arFilter = array_merge(array("IBLOCK_ID" => IBLOCK_CODE_CONTRACTS_ID), $companyID);
        else
            return null;

        $db = CIBlockElement::GetList(
            array("ID" => "DESC"),
            $arFilter,
            false,
            false,
            array("ID", "PROPERTY_PROP_SUPER_USER")
        );

        if ($ob = $db->Fetch()) {
            return array(
                'ID' => $ob['PROPERTY_PROP_SUPER_USER_VALUE'][0],
                'CONTACT_FACE' => CUser::GetByID($ob['PROPERTY_PROP_SUPER_USER_VALUE'][0])->Fetch()
            );
        } else {
            unset($db);
            return null;
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 22.05.2017
 * Time: 10:43
 *
 * @var $arResult
 * @var $arParams
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;

$request = Application::getInstance()->getContext()->getRequest();
$arGet = $request->getQueryList()->toArray();
?>
<div class="content__item _top top_panel">
    <div class="page__title">
        <h1><a href="<?= $_SERVER['SCRIPT_URL'];?>"><?= Loc::getMessage("HEADER_1_TITLE") ?></a></h1>
    </div>
    <!-- FILTER -->
    <div class="scrollbar-outer scrollbar-center">
        <form id="form-filter" class="form form__fl-left" method="get">
            <input type="hidden" name="F_COMPANY_FILTER" value="Y">
            <div class="form__row">
                <?foreach($arResult['ROW_FILTER'] as $key => $arFilter) { ?>
                    <div class="column form__row__col _size4">
                        <div class="portlet" data-portlet="<?= $key ?>">
                            <div class="portlet-header">
                                <?= Loc::getMessage('C_COMPANY_LIST_' . $arFilter)?>
                            </div>
                            <div class="portlet-content">
                                    <input type="text"
                                           class="form__field <?= stristr($arFilter, 'MOBILE') ? 'js-maskedinput':'';?>"
                                           name="F_<?= $arFilter ?>"
                                        <?= stristr($arFilter, 'MOBILE') ? 'data-maskedinput="+9 999 999 9999"':'';?>
                                           value="<?= $arGet["F_" . $arFilter]; ?>"
                                           placeholder="...">
                                    <a href="#" class="js-reset-input"><i class="fa fa-close"></i></a>
                            </div>
                        </div>
                    </div>
                <?}?>
            </div>
            <div class="form__row">
                <button type="submit" class="btn"><?= Loc::getMessage('C_BUTTON_APPLY') ?></button>
                <a href="<?= $_SERVER['SCRIPT_URL'] ?>" class="btn _white">
                    <?= Loc::getMessage('C_BUTTON_RESET') ?>
                </a>
            </div>
        </form>
    </div>
    <!-- END FILTER -->
</div>
<div class="content__item _bottom">
    <? if (!empty($arResult['COMPANY_LIST'])) { ?>
        <div class="scrollbar-outer">
            <!-- COMPANY LIST -->
            <div class="control">
                <div class="control__result">
                    <?= Loc::getMessage('CNT_COMPANIES')?><?= $arResult['CNT_COMPANIES'] ?>
                </div>
            </div>
            <table class="table js-table" id="orders-list">
                <thead>
                <tr>
                    <?foreach($arResult['ROW'] as $cField => $arField) {?>
                        <th><?= Loc::getMessage('ROW_TITLE_' . $arField) ?></th>
                    <?}?>
                </tr>
                </thead>
                <tbody>
                <?foreach($arResult['COMPANY_LIST'] as $arCompany) {
                    $title = '';

                    if($arCompany['ERROR']) {
                        $title = $arCompany['ERROR'];
                    }?>
                    <tr class="<?= $arCompany['ERROR_CLASS'] ?>" title="<?= $title ?>">
                        <?foreach($arResult['ROW'] as $cField) {?>
                            <td class="<?= $cField == 'COMPANY'? 'row-company':'';?>">
                                <? if($cField == 'NAME') {
                                    $detailUri = $arParams['SEF_FOLDER'] . $arCompany['ID'] . '/';
                                    if(!empty($arCompany['DETAIL_URL']) && $arCompany['PROPERTY_FORMING'] == 'Y') {
                                        $detailUri = $arCompany['DETAIL_URL'];
                                    }?>
                                    <a href="<?= $detailUri ?>" class="order-detail">
                                        <?= $arCompany[$cField] ?>
                                    </a>
                                <? } else if ("CONTACT_FACE" === $cField) { ?>
                                    <a href="/elar/users/<?=$arCompany["CONTACT_FACE_ID"]?>/" class="order-detail">
                                        <?= $arCompany[$cField] ?>
                                    </a>
                                <? } else {
                                    if ($cField === 'ACTIVE')
                                        echo Loc::getMessage($cField . '_' . $arCompany[$cField]);
                                    else
                                        echo $arCompany[$cField];
                                } ?>
                            </td>
                        <?}?>
                    </tr>
                <?}?>
                </tbody>
            </table>
            <div class="navigation">
                <?= $arResult['NAV_PRINT'] ?>
                <div class="pager">
                    <div class="pager__current">
                        <span><?= Loc::getMessage('T_COUNT_ITEMS') ?></span>
                        <form action="<?= $arResult['FORM_PATH_DEFAULT'] ?>" method="GET" class="nav-form">
                            <?= getHTMLInputExcludeParam($arGet, 'COMPANY_ON_PAGE') ?>
                            <input type="text" name="COMPANY_ON_PAGE" value="<?= $arGet['COMPANY_ON_PAGE'] ? : $arParams['COMPANY_ON_PAGE'] ?>">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- END COMPANY LIST -->
    <?} else {?>
        <h2><?= Loc::getMessage("EMPTY_SEARCH_RESULT") ?></h2>
    <?}?>
</div>

<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Application;
$arGet = Application::getInstance()->getContext()->getRequest()->getQueryList()->toArray();
global $APPLICATION;
//$arShowBtnCancel = array('formation', 'processing');
$arShowBtnCancel = array();
?>
<div class="page__frame">
    <div class="page__title">
        <h1><?= Loc::getMessage('U_ORDER_DETAIL_T_TITLE_1') ?><?= $arResult['ORDER']['NAME'] ?><?= Loc::getMessage('U_ORDER_DETAIL_T_TITLE_2') ?></h1>
    </div>
    <div class="order">
        <div class="order__col">
            <div class="scrollbar-outer">
                <div class="form">
                    <div class="_size3">
                        <div class="form__row">
                           <!-- <a href="<?/*= $arParams['LIST_URL'] */?>" class="btn">
                                <i class="fa fa-mail-reply"></i><?/*= Loc::getMessage('U_ORDER_DETAIL_T_BACK') */?>
                            </a>-->
                            <?if(in_array($arResult['PROPERTIES']['STATUS']['XML_ID'], $arShowBtnCancel)) {?>
                                <a href="#"
                                   class="js-cancel-order btn _white"
                                   data-redirect="<?= $arParams['LIST_URL'] ?>"
                                   data-iblock="<?= $arResult['ORDER']['IBLOCK_ID'] ?>"
                                   data-url="<?= $this->GetFolder() ?>/ajax/order.delete.php"
                                   data-order="<?= $arResult['ORDER']['ID'] ?>"
                                   data-lang-title="<?= Loc::getMessage('U_CANCEL_LANG_TITLE') ?>"
                                   data-lang-y="<?= Loc::getMessage('U_CANCEL_LANG_Y') ?>"
                                   data-lang-n="<?= Loc::getMessage('U_CANCEL_LANG_N') ?>"
                                   data-lang-question="<?= Loc::getMessage('U_CANCEL_LANG_QUESTION') ?>">
                                    <?= Loc::getMessage('U_ORDER_DETAIL_T_CANCEL') ?>
                                </a>
                            <?}?>
                        </div>
                        <ul class="order__list">
                            <li>
                                <div><?= Loc::getMessage('U_ORDER_DETAIL_T_ORDER_NUMB') ?></div>
                                <div id="orderId" data-order-id="<?= $arResult['ORDER']['ID'] ?>"><?= $arResult['ORDER']['ID'] ?></div>
                            </li>
                            <?foreach($arResult['PROPERTIES'] as $cProperty => $arProperty) {
                                if($cProperty == 'UNITS' || $cProperty == 'FORMING' || $cProperty == 'REQUEST') continue; ?>
                                <li>
                                    <div><?= $arProperty['NAME'] ?></div>
                                    <div <?= $cProperty === 'STATUS'?'id="orderStatus"':''?>>
                                        <?
                                        if($arProperty['PROPERTY_TYPE'] == 'L')
                                            $value = $arProperty['VALUE_ENUM'];
                                        else
                                            $value = $arProperty['VALUE'];

                                        if ($cProperty == 'ADRESS_CLIENT'){
                                            $value  = $arProperty['VALUE']['INDEX'] . ", ";
                                            $value .= $arProperty['VALUE']['TOWN'] . ", ";
                                            $value .= $arProperty['VALUE']['STREET'] . ", ";
                                            $value .= $arProperty['VALUE']['HOUSE'];
                                            $value .= (!empty($arProperty['VALUE']['OFFICE'])) ? ", офис " . $arProperty['VALUE']['OFFICE']: "";
                                        }

                                        if ($cProperty == 'STATUS') {
                                            $value = '<i class="fa fa-spin fa-icon fa-spinner"></i>';
                                        }
                                        ?>
                                        <?= $value ?>
                                    </div>
                                </li>
                            <?}?>
                            <li class="row-hide">
                                <div><?= Loc::getMessage("HIDE_FIELD_COUNT_UNITS")?></div>
                                <div><?= $arResult['COUNT_UNITS'] ?></div>
                            </li>
                        </ul>
                    </div>
                    <div class="warning"><p><i class="fa fa-exclamation" aria-hidden="true"></i> Вы можете аннулировать Ваш заказ, в случае если он еще не запущен в работу. Для аннулирования заказа обратитесь к Вашему менеджеру.</p></div>
                </div>
                <div class="form">
                    <table class="table" data-registry-iblock-id="<?=IBLOCK_ID_REGISTRIES?>">
                        <thead>
                        <tr>
                            <th><?= Loc::getMessage('U_ORDER_DETAIL_T_TABLE_NUMB') ?></th>
                            <?foreach($arResult['ROW_SORT'] as $code => $url) {?>
                                <th>
                                    <a href="<?= $url ?>" class="filtr">
                                        <?= Loc::getMessage('U_ORDER_DETAIL_T_TABLE_' . $code) ?>
                                    </a>
                                </th>
                            <?}?>
                        </tr>
                        </thead>
                        <tbody>
                        <?$startIndex = $arResult['START_INDEX_UNIT'];

                        /* Покажем таблицу данных если в $arResult['LIST_UNITS'] массив данных */
                        if ($arResult['LIST_UNITS'] !== false)
                        {
                            foreach (
                                $arResult['LIST_UNITS'] as $iUnit => $arUnit
                            )
                            { ?>
                                <tr>
                                    <td><?= $startIndex ?></td>
                                    <? foreach (
                                        $arParams['SET_SORT_FILTER'] as $code
                                    )
                                    { ?>
                                        <td<?=($code == 'PROPERTY_PROP_WMS_STATUS')? ' data-ui-id="' . $arUnit['PROPERTY_PROP_SSCC'] . '" class="fa fa-icon fa-spin fa-spinner"' : ""?>>
                                            <?= $arUnit[ $code ] ?>
                                        </td>
                                    <?
                                    } ?>
                                    <? $startIndex++; ?>
                                </tr>
                            <?
                            }
                        } else { ?>
                            <tr>
                                <td align="center" colspan="<?= count ($arResult['ROW_SORT']) + 1?>">ЕУ для этого заказа отсуствуют или не были добавлены.</td>
                            </tr>
                        <?}?>
                        </tbody>
                    </table>
                </div>
                <? /* Только если ЕУ для заказа > 0 , есть смысл отображать пагинацию */?>
                <? if ($arResult['LIST_UNITS'] !== false):?>
                <div class="navigator">
                    <?= $arResult['NAV_PRINT'] ?>
                    <div class="pager">
                        <div class="pager__current">
                            <span><?= Loc::getMessage('USER_REG_T_COUNT_ITEMS') ?></span>
                            <form action="<?= $arResult['FORM_PATH_DEFAULT'] ?>" method="GET" class="nav-form">
                                <?= getHTMLInputExcludeParam($arGet, 'COUNT_ON_PAGE') ?>
                                <input type="text" name="COUNT_ON_PAGE" value="<?= $arResult['COUNT_ON_PAGE'] ?>">
                            </form>
                        </div>
                    </div>
                </div>
                <? endif; ?>
                <div class="clearfix">&nbsp;</div>
            </div>
        </div>
    </div>
</div>
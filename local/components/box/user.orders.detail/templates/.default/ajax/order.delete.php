<?php
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use \Bitrix\Main\Application,
    \Bitrix\Main\Loader;
Loader::includeModule('iblock');
$context = Application::getInstance()->getContext();
$arServer = $context->getServer()->toArray();
$arPost = $context->getRequest()->getPostList()->toArray();

if($arPost['AJAX_DELETE'] == 'Y' && !empty($arPost['ORDER_ID']) && !empty($arPost['IBLOCK_ID'])) {
    $el = new CIBlockElement;
    $el->Update($arPost['ORDER_ID'], array('ACTIVE' => 'N'));
    CIBlockElement::SetPropertyValuesEx($arPost['ORDER_ID'], $arPost['IBLOCK_ID'], array('STATUS' => 'canceled'));
}
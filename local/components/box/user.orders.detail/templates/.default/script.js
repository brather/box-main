function getWmsStatus() {

    var that = $('.table'),
        unitOnPage = $(that).find('td[data-ui-id]'),
        obCodes = {},
        IBLOCK_ID = $(that).data('registry-iblock-id');

    for (var i = 0; i < unitOnPage.length; i++) {
        obCodes[i] =unitOnPage[i].dataset.uiId;
    }

    $.ajax({
        url: '/local/components/box/user.registry.detail/templates/design.list/ajax/wmsstatus.php',
        data: {
            UI_IDS: obCodes,
            IBLOCK_ID: IBLOCK_ID
        },
        method: 'POST',
        dataType: 'json',
        success: function(myJson) {
            unitOnPage.map(function(k, v) {
                $(v).removeClass('fa fa-icon fa-spin fa-spinner');
                if (myJson[v.dataset.uiId].SOAP_ERROR) {
                    $(v).addClass('frn-danger').text(myJson[v.dataset.uiId].SOAP_ERROR);
                } else {
                    $(v).text(myJson[v.dataset.uiId].SOAP_RESPONSE);
                }
            });
        },
        error: function(xhs, st, msg){
            console.log(st, msg);
            unitOnPage.map(function(i,v){
                $(v).removeClass('fa fa-icon fa-spin fa-spinner').addClass('frn-danger').text('Ошибка ! Попробуйте позже.');
            });
        }
    });
};

$(document).ready(function(){
    var i = {
            fpage   : '#vertical'
        },
        c = {
            title   : '.page__title',
            border  : '.order',
            scroll  : '.order__col',
            rorder  : '.js-cancel-order'
        },
        heightTitle = $(i.fpage).find(c.title).outerHeight(),
        marginOrder = getNumberFromString($(i.fpage).find(c.border).css('margin-top')),
        bxPanelHeight = ((window.document.getElementById('bx-panel')) ? window.document.getElementById('bx-panel').clientHeight : 0),
        height = getPageSize().windowHeight - heightTitle - marginOrder - bxPanelHeight;
    //region Определение высота блока с содержимым для запуска scroll
    $(c.scroll).children('div').css('max-height', height);
    $(window).on('resize', function(){
        height = getPageSize().windowHeight - heightTitle - marginOrder - bxPanelHeight;
        $(c.scroll).children('div').css('max-height', height);
    });
    //endregion
    $(document).on('click', c.rorder, function(e){
        e.preventDefault();
        var t = $(this);
        $.confirm({
            animation           : 'top',
            closeAnimation      : 'bottom',
            theme               : 'light',
            backgroundDismiss   : true,
            title               : t.data('lang-title'),
            content             : t.data('lang-question'),
            buttons             : {
                confirm : {
                    keys    : ['enter'],
                    text    : t.data('lang-y'),
                    action  : function() {
                        startLoader(i.fpage);
                        var dataDelete = {
                            'AJAX_DELETE' : 'Y',
                            'ORDER_ID' : t.data('order'),
                            'IBLOCK_ID' : t.data('iblock')
                        };
                        $.ajax({
                            method  : 'post',
                            data    : dataDelete,
                            url     : t.data('url'),
                            success : function() {
                                stopLoader();
                                location.href = t.data('redirect');
                            }
                        });
                    }
                },
                cancel  : {
                    keys : ['esc'],
                    text : t.data('lang-n')
                }
            }
        });
    });

    $.ajax({
        url: '/local/components/box/user.orders/orderstatus.php',
        method: 'POST',
        dataType: 'JSON',
        data: {
            ORDER_ID: document.getElementById('orderId').dataset.orderId
            //ORDER_ID: "1212"
        },
        success: function (res) {
            if (typeof res === 'object') {
                if ("status" in res) {
                    $('#orderStatus').text(res.status);
                }
            }
        },
        error: function (xhr, st, msg) {
            var error = (xhr.responseJSON != undefined)? xhr.responseJSON.error : 'Ошибка ! Попробуйте позже.';
            $('#orderStatus').html("<span class='frn-danger'>" + error + "</span>");
        }
    });

    getWmsStatus();
});
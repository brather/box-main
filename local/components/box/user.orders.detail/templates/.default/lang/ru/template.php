<?php
$MESS['U_ORDER_DETAIL_T_TITLE_1'] = 'Заказ &laquo;';
$MESS['U_ORDER_DETAIL_T_TITLE_2'] = '&raquo;';
$MESS['U_ORDER_DETAIL_T_STATUS'] = 'Статус ';
$MESS['U_ORDER_DETAIL_T_ORDER_NUMB'] = 'Номер заказа ';
$MESS['U_ORDER_DETAIL_T_UNITS'] = 'Список ЕУ заказа ';
$MESS['U_ORDER_DETAIL_T_UNITS_NAV'] = 'ЕУ ';
$MESS['U_ORDER_DETAIL_T_UNITS_FOUND'] = 'Найдено: ';
$MESS['U_ORDER_DETAIL_T_BACK'] = ' Назад';
$MESS['U_ORDER_DETAIL_T_CANCEL'] = 'Отменить';

$MESS['U_ORDER_DETAIL_T_TABLE_NUMB'] = '№ ';
$MESS['U_ORDER_DETAIL_T_TABLE_ID'] = 'ID ';
$MESS['U_ORDER_DETAIL_T_TABLE_IBLOCK_NAME'] = 'Реестр ';
$MESS['U_ORDER_DETAIL_T_TABLE_NAME'] = 'Название ЕУ ';
$MESS['U_ORDER_DETAIL_T_TABLE_PROPERTY_PROP_SSCC'] = 'SSCC';
$MESS['U_ORDER_DETAIL_T_TABLE_PROPERTY_PROP_WMS_STATUS'] = 'Статус WMS';

$MESS['U_CANCEL_LANG_TITLE'] = 'Удалить заказ?';
$MESS['U_CANCEL_LANG_QUESTION'] = 'Вы действительно ходите отменить заказ?';
$MESS['U_CANCEL_LANG_Y'] = 'Да';
$MESS['U_CANCEL_LANG_N'] = 'Нет';

$MESS['HIDE_FIELD_COUNT_UNITS'] = 'Всего ЕУ';
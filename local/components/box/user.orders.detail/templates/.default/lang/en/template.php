<?php
$MESS['U_ORDER_DETAIL_T_TITLE_1'] = 'Order &laquo;';
$MESS['U_ORDER_DETAIL_T_TITLE_2'] = '&raquo;';
$MESS['U_ORDER_DETAIL_T_STATUS'] = 'Status ';
$MESS['U_ORDER_DETAIL_T_ORDER_NUMB'] = 'Number of order ';
$MESS['U_ORDER_DETAIL_T_UNITS'] = 'List units of order ';
$MESS['U_ORDER_DETAIL_T_UNITS_NAV'] = 'Units ';
$MESS['U_ORDER_DETAIL_T_UNITS_FOUND'] = 'Found: ';
$MESS['U_ORDER_DETAIL_T_BACK'] = ' Back';
$MESS['U_ORDER_DETAIL_T_CANCEL'] = 'Cancel';

$MESS['U_ORDER_DETAIL_T_TABLE_NUMB'] = '№ ';
$MESS['U_ORDER_DETAIL_T_TABLE_ID'] = 'ID ';
$MESS['U_ORDER_DETAIL_T_TABLE_IBLOCK_NAME'] = 'Registry ';
$MESS['U_ORDER_DETAIL_T_TABLE_NAME'] = 'Unit title ';

$MESS['U_CANCEL_LANG_TITLE'] = 'Remove order?';
$MESS['U_CANCEL_LANG_QUESTION'] = 'Are you sure you want to canceled an order?';
$MESS['U_CANCEL_LANG_Y'] = 'Yes';
$MESS['U_CANCEL_LANG_N'] = 'No';

$MESS['HIDE_FIELD_COUNT_UNITS'] = 'Count units';
<?php
$MESS['U_ORDER_DETAIL_T_TITLE_1'] = 'Order &laquo;';
$MESS['U_ORDER_DETAIL_T_TITLE_2'] = '&raquo;';
$MESS['U_ORDER_DETAIL_T_STATUS'] = 'Status ';
$MESS['U_ORDER_DETAIL_T_ORDER_NUMB'] = 'Number of order ';
$MESS['U_ORDER_DETAIL_T_UNITS'] = 'List units of order ';
$MESS['U_ORDER_DETAIL_T_UNITS_NAV'] = 'Units ';
$MESS['U_ORDER_DETAIL_T_UNITS_FOUND'] = 'Found: ';
$MESS['U_ORDER_DETAIL_T_BACK'] = ' Back';
$MESS['U_ORDER_DETAIL_T_CANCEL'] = 'Cancel';
$MESS['U_ORDER_DETAIL_T_EDIT'] = 'Edit';
$MESS['U_ORDER_DETAIL_T_SAVE'] = 'Save';
$MESS['U_ORDER_DETAIL_T_SEND_TO_ORDER'] = 'Approve request';
$MESS['U_ORDER_DETAIL_T_SUCCESS_TITLE'] = 'Add success!';
$MESS['U_ORDER_DETAIL_T_SUCCESS_TEXT'] = 'The request has been successfully added to your order, you can now view it on the orders list page.';

$MESS['U_ORDER_DETAIL_T_TABLE_NUMB'] = '№ ';
$MESS['U_ORDER_DETAIL_T_TABLE_ID'] = 'ID ';
$MESS['U_ORDER_DETAIL_T_TABLE_IBLOCK_NAME'] = 'Registry ';
$MESS['U_ORDER_DETAIL_T_TABLE_NAME'] = 'Unit title ';
$MESS['U_ORDER_DETAIL_T_DELETE_ROW'] = 'Delete ';

$MESS['U_CANCEL_LANG_TITLE'] = 'Remove order?';
$MESS['U_CANCEL_LANG_QUESTION'] = 'Are you sure you want to canceled an order?';
$MESS['U_CANCEL_LANG_TITLE_UNIT'] = 'Remove unit?';
$MESS['U_CANCEL_LANG_QUESTION_UNIT'] = 'Are you sure you want to remove unit from request?';
$MESS['U_CANCEL_LANG_Y'] = 'Yes';
$MESS['U_SUCCESS_LANG_Y'] = 'Ok';
$MESS['U_CANCEL_LANG_N'] = 'No';
$MESS['U_CANCEL_LANG_REESTABLISH'] = 'Reestablish';

$MESS['HIDE_FIELD_COMMENT'] = 'Comment ';
$MESS['HIDE_FIELD_DATE'] = 'Date of complition	';

$MESS['HIDE_FIELD_PLACEHOLDER_COMMENT'] = 'enter your message';
$MESS['HIDE_FIELD_PLACEHOLDER_DATE'] = 'select date';

$MESS['HIDE_FIELD_COUNT_UNITS'] = 'Count units';
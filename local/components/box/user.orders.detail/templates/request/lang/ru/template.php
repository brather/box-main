<?php
$MESS['U_ORDER_DETAIL_T_TITLE_1'] = 'Заказ &laquo;';
$MESS['U_ORDER_DETAIL_T_TITLE_2'] = '&raquo;';
$MESS['U_ORDER_DETAIL_T_STATUS'] = 'Статус ';
$MESS['U_ORDER_DETAIL_T_ORDER_NUMB'] = 'Номер заказа ';
$MESS['U_ORDER_DETAIL_T_UNITS'] = 'Список ЕУ заказа ';
$MESS['U_ORDER_DETAIL_T_UNITS_NAV'] = 'ЕУ ';
$MESS['U_ORDER_DETAIL_T_UNITS_FOUND'] = 'Найдено: ';
$MESS['U_ORDER_DETAIL_T_BACK'] = ' Назад';
$MESS['U_ORDER_DETAIL_T_CANCEL'] = 'Отменить';
$MESS['U_ORDER_DETAIL_T_EDIT'] = 'Редактировать';
$MESS['U_ORDER_DETAIL_T_SAVE'] = 'Сохранить';
$MESS['U_ORDER_DETAIL_T_SEND_TO_ORDER'] = 'Согласовать запрос';
$MESS['U_ORDER_DETAIL_T_SUCCESS_TITLE'] = 'Добавлено!';
$MESS['U_ORDER_DETAIL_T_SUCCESS_TEXT'] = 'Запрос был успешно добавлен в заказы, теперь Вы можете его просмотреть на странице списка заказов.';

$MESS['U_ORDER_DETAIL_T_TABLE_NUMB'] = '№ ';
$MESS['U_ORDER_DETAIL_T_TABLE_ID'] = 'ID ';
$MESS['U_ORDER_DETAIL_T_TABLE_IBLOCK_NAME'] = 'Реестр ';
$MESS['U_ORDER_DETAIL_T_TABLE_NAME'] = 'Название ЕУ ';
$MESS['U_ORDER_DETAIL_T_DELETE_ROW'] = 'Удалить ';

$MESS['U_CANCEL_LANG_TITLE'] = 'Удалить заказ?';
$MESS['U_CANCEL_LANG_QUESTION'] = 'Вы действительно ходите отменить заказ?';
$MESS['U_CANCEL_LANG_TITLE_UNIT'] = 'Удалить ЕУ?';
$MESS['U_CANCEL_LANG_QUESTION_UNIT'] = 'Вы действительно ходите удалить ЕУ из запроса?';
$MESS['U_CANCEL_LANG_Y'] = 'Да';
$MESS['U_SUCCESS_LANG_Y'] = 'Принять';
$MESS['U_CANCEL_LANG_N'] = 'Нет';
$MESS['U_CANCEL_LANG_REESTABLISH'] = 'Восстановить';

$MESS['HIDE_FIELD_COMMENT'] = 'Комментарий ';
$MESS['HIDE_FIELD_DATE'] = 'Дата выполнения	';

$MESS['HIDE_FIELD_PLACEHOLDER_COMMENT'] = 'введите сообщение';
$MESS['HIDE_FIELD_PLACEHOLDER_DATE'] = 'выберите дату';

$MESS['HIDE_FIELD_COUNT_UNITS'] = 'Всего ЕУ';
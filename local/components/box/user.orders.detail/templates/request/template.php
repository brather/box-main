<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Application;
$arGet = Application::getInstance()->getContext()->getRequest()->getQueryList()->toArray();
global $APPLICATION;
$arShowBtnCancel = array('formation', 'processing');
$flag = false;
if(in_array($arResult['PROPERTIES']['STATUS']['XML_ID'], $arShowBtnCancel)) {
    $flag = true;
}
?>
<div class="page__frame">
    <div class="page__title">
        <h1><?= Loc::getMessage('U_ORDER_DETAIL_T_TITLE_1') ?><?= $arResult['ORDER']['NAME'] ?><?= Loc::getMessage('U_ORDER_DETAIL_T_TITLE_2') ?></h1>
    </div>
    <div class="order">
        <div class="order__col">
            <div class="scrollbar-outer">
                <div class="form">
                    <div class="_size3">
                        <div class="form__row">
                            <a href="<?= $arParams['LIST_URL'] ?>" class="btn">
                                <i class="fa fa-mail-reply"></i><?= Loc::getMessage('U_ORDER_DETAIL_T_BACK') ?>
                            </a>
                            <?if($flag) {?>
                                <a href="#"
                                   class="js-cancel-order btn _white"
                                   data-redirect="<?= $arParams['LIST_URL'] ?>"
                                   data-iblock="<?= $arResult['ORDER']['IBLOCK_ID'] ?>"
                                   data-url="<?= $this->GetFolder() ?>/ajax/order.delete.php"
                                   data-order="<?= $arResult['ORDER']['ID'] ?>"
                                   data-lang-title="<?= Loc::getMessage('U_CANCEL_LANG_TITLE') ?>"
                                   data-lang-y="<?= Loc::getMessage('U_CANCEL_LANG_Y') ?>"
                                   data-lang-n="<?= Loc::getMessage('U_CANCEL_LANG_N') ?>"
                                   data-lang-question="<?= Loc::getMessage('U_CANCEL_LANG_QUESTION') ?>">
                                    <?= Loc::getMessage('U_ORDER_DETAIL_T_CANCEL') ?>
                                </a>
                                <a href="#" class="js-edit-order btn">
                                    <?= Loc::getMessage('U_ORDER_DETAIL_T_EDIT') ?>
                                </a>
                            <?}?>
                        </div>
                        <form action="<?= $arParams['LIST_URL'] ?><?= $arResult['ORDER']['ID'] ?>/" method="post" class="form disable-form" id="form-order">
                            <input type="hidden" name="F_UNIT_REQUEST" value="Y">
                            <input type="hidden" name="F_UNIT_ID" value="<?= $arResult['ORDER']['ID'] ?>">
                            <input type="hidden" name="F_UNIT_IBLOCK" value="<?= $arResult['ORDER']['IBLOCK_ID'] ?>">
                            <ul class="order__list">
                                <li>
                                    <div><?= Loc::getMessage('U_ORDER_DETAIL_T_ORDER_NUMB') ?></div>
                                    <div><?= $arResult['ORDER']['ID'] ?></div>
                                </li>
                                <?$arEditFields = array('DATE', 'COMMENT');
                                foreach($arResult['PROPERTIES'] as $cProperty => $arProperty) {
                                    if($cProperty == 'UNITS' || $cProperty == 'REQUEST') {
                                        continue;
                                    }?>
                                    <li>
                                        <div><?= $arProperty['NAME'] ?></div>
                                        <div>
                                            <?if($arProperty['PROPERTY_TYPE'] == 'L') {
                                                $value = $arProperty['VALUE_ENUM'];
                                            } else {
                                                $value = $arProperty['VALUE'];
                                            }?>
                                            <?if(in_array($cProperty, $arEditFields)) {?>
                                                <span class="js-value"><?= $value ?></span>
                                                <span class="js-input">
                                                    <?if($cProperty == 'DATE') {?>
                                                        <input type="text"
                                                               value="<?= $value ?>"
                                                               class="js-datepicker form__field"
                                                               name="F_UNIT_DATE"
                                                               placeholder="<?= Loc::getMessage('HIDE_FIELD_PLACEHOLDER_DATE') ?>">
                                                    <?} elseif($cProperty == 'COMMENT') {?>
                                                        <textarea class="form__field"
                                                                  name="F_UNIT_COMMENT"
                                                                  cols="30"
                                                                  rows="10"
                                                                  placeholder="<?= Loc::getMessage('HIDE_FIELD_PLACEHOLDER_COMMENT') ?>"><?= $value ?></textarea>
                                                    <?}?>
                                                </span>
                                            <?} else {?>
                                                <?= $value ?>
                                            <?}?>
                                        </div>
                                    </li>
                                <?}?>
                                <?foreach($arEditFields as $cField) {
                                    if(!empty($arResult['PROPERTIES'][$cField])) {
                                        continue;
                                    }?>
                                    <li class="row-hide">
                                        <?if($cField == 'DATE') {?>
                                            <div><?= Loc::getMessage('HIDE_FIELD_DATE') ?></div>
                                            <div>
                                                <input type="text"
                                                       class="js-datepicker form__field"
                                                       name="F_UNIT_DATE"
                                                       value=""
                                                       placeholder="<?= Loc::getMessage('HIDE_FIELD_PLACEHOLDER_DATE') ?>">
                                            </div>
                                        <?} elseif($cField == 'COMMENT') {?>
                                            <div><?= Loc::getMessage('HIDE_FIELD_COMMENT') ?></div>
                                            <div>
                                                <textarea class="form__field"
                                                          name="F_UNIT_COMMENT"
                                                          cols="30"
                                                          rows="10"
                                                          placeholder="<?= Loc::getMessage('HIDE_FIELD_PLACEHOLDER_COMMENT') ?>"></textarea>
                                            </div>
                                        <?}?>
                                    </li>
                                    <?
                                }?>
                                <li class="row-hide">
                                    <div><?= Loc::getMessage("HIDE_FIELD_COUNT_UNITS")?></div>
                                    <div><?= $arResult['COUNT_UNITS'] ?></div>
                                </li>
                            </ul>
                            <button class="btn js-btn-save" type="submit">
                                <?= Loc::getMessage('U_ORDER_DETAIL_T_SAVE') ?>
                            </button>
                        </form>
                    </div>
                </div>
                <div class="form" id="blk-order">
                    <table class="table js-table" id="list-unit">
                        <thead>
                        <tr>
                            <?if($flag){?>
                                <th class="js-checkbox-td">
                                    <label class="checkbox">
                                        <input type="checkbox" name="F_UNITS_CHECK_ALL" class="js-table-checkbox-all">
                                        <span></span>
                                    </label>
                                </th>
                            <?}?>
                            <?foreach($arResult['ROW_SORT'] as $code => $url) {?>
                                <th>
                                    <a href="<?= $url ?>" class="filtr">
                                        <?= Loc::getMessage('U_ORDER_DETAIL_T_TABLE_' . $code) ?>
                                    </a>
                                </th>
                            <?}?>
                        </tr>
                        </thead>
                        <tbody>
                        <?$startIndex = $arResult['START_INDEX_UNIT'];
                        foreach($arResult['LIST_UNITS'] as $iUnit => $arUnit) {?>
                            <tr data-row="<?= $iUnit ?>">
                                <?if($flag){?>
                                    <td class="js-checkbox-td">
                                        <label class="checkbox">
                                            <input type="checkbox"
                                                   name="F_CHECK_UNIT[]"
                                                   value="<?= $arUnit['ID'] ?>"
                                                   class="js-table-checkbox">
                                            <span></span>
                                        </label>
                                    </td>
                                <?}?>
                                <?foreach($arParams['SET_SORT_FILTER'] as $code) {
                                    $clsName = '';
                                    if($code == 'NAME') {
                                        $clsName = ' class="js-name"';
                                    }?>
                                    <td<?= $clsName ?>><?= $arUnit[$code] ?></td>
                                <?}?>
                                <?$startIndex++;?>
                            </tr>
                        <?}?>
                        </tbody>
                    </table>
                </div>
                <div class="navigator">
                    <?= $arResult['NAV_PRINT'] ?>
                    <div class="pager">
                        <div class="pager__current">
                            <span><?= Loc::getMessage('USER_REG_T_COUNT_ITEMS') ?></span>
                            <form action="<?= $arResult['FORM_PATH_DEFAULT'] ?>" method="GET" class="nav-form">
                                <?= getHTMLInputExcludeParam($arGet, 'COUNT_ON_PAGE') ?>
                                <input type="text" name="COUNT_ON_PAGE" value="<?= $arResult['COUNT_ON_PAGE'] ?>">
                            </form>
                        </div>
                    </div>
                </div>
                <div class="form__row">
                    <?if($flag) {?>
                        <a href="#"
                           data-order="<?= $arResult['ORDER']['ID'] ?>"
                           data-iblock="<?= $arResult['ORDER']['IBLOCK_ID'] ?>"
                           data-url="<?= $this->GetFolder() ?>/ajax/unit.delete.php"
                           data-lang-t="<?= Loc::getMessage('U_CANCEL_LANG_TITLE_UNIT') ?>"
                           data-lang-q="<?= Loc::getMessage('U_CANCEL_LANG_QUESTION_UNIT') ?>"
                           data-lang-y="<?= Loc::getMessage('U_CANCEL_LANG_Y') ?>"
                           data-lang-n="<?= Loc::getMessage('U_CANCEL_LANG_N') ?>"
                           data-lang-r="<?= Loc::getMessage('U_CANCEL_LANG_REESTABLISH') ?>"
                           class="btn btn-disabled"
                           data-table="#list-unit"
                           id="btn-delete-unit">
                            <?= Loc::getMessage('U_ORDER_DETAIL_T_DELETE_ROW') ?>
                        </a>
                    <?}?>
                    <?if(in_array(U_GROUP_CODE_CLIENT_S_USER, $arParams['USER_GROUPS'])) {?>
                        <a href="#"
                           data-lang-stitle="<?= Loc::getMessage('U_ORDER_DETAIL_T_SUCCESS_TITLE') ?>"
                           data-lang-stext="<?= Loc::getMessage('U_ORDER_DETAIL_T_SUCCESS_TEXT') ?>"
                           data-lang-y="<?= Loc::getMessage('U_SUCCESS_LANG_Y') ?>"
                           data-url="<?= $this->GetFolder() ?>/ajax/request.add.php"
                           data-reload="<?= $arParams['LIST_URL'] ?>"
                           data-order="<?= $arResult['ORDER']['ID'] ?>"
                           data-iblock="<?= $arResult['ORDER']['IBLOCK_ID'] ?>"
                           class="btn js-send-order _white">
                            <?= Loc::getMessage('U_ORDER_DETAIL_T_SEND_TO_ORDER') ?>
                        </a>
                    <?}?>
                </div>
            </div>
        </div>
    </div>
</div>
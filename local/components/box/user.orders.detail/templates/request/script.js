$(document).ready(function(){
    var i = {
            fPage   : '#vertical',
            fOrder  : '#form-order',
            bDUnit  : '#btn-delete-unit',
            lUnit   : '#list-unit',
            bOrder  : '#blk-order'
        },
        c = {
            title   : '.page__title',
            bOrder  : '.order',
            scroll  : '.order__col',
            rOrder  : '.js-cancel-order',
            eOrder  : '.js-edit-order',
            disable : 'btn-disabled',
            fHide   : 'disable-form',
            chBox   : '.js-table-checkbox',
            chAll   : '.js-table-checkbox-all',
            rActive : '_active',
            recover : 'js-recovery',
            tdName  : '.js-name',
            tOrder  : '.js-send-order'
        },
        flag = false,
        heightTitle = $(i.fPage).find(c.title).outerHeight(),
        marginOrder = getNumberFromString($(i.fPage).find(c.bOrder).css('margin-top')),
        height = getPageSize().pageHeight - heightTitle - marginOrder;
    //region Определение высота блока с содержимым для запуска scroll
    $(c.scroll).children('div').css('max-height', height);
    $(window).on('resize', function(){
        height = getPageSize().pageHeight - heightTitle - marginOrder;
        $(c.scroll).children('div').css('max-height', height);
    });
    //endregion
    //region Отмена заказа (запроса)
    $(document).on('click', c.rOrder, function(e){
        e.preventDefault();
        var t = $(this);
        $.confirm({
            animation           : 'top',
            closeAnimation      : 'bottom',
            theme               : 'light',
            backgroundDismiss   : true,
            title               : t.data('lang-title'),
            content             : t.data('lang-question'),
            buttons             : {
                confirm : {
                    keys    : ['enter'],
                    text    : t.data('lang-y'),
                    action  : function() {
                        startLoader(i.fPage);
                        var dataDelete = {
                            'AJAX_DELETE' : 'Y',
                            'ORDER_ID' : t.data('order'),
                            'IBLOCK_ID' : t.data('iblock')
                        };
                        $.ajax({
                            method  : 'post',
                            data    : dataDelete,
                            url     : t.data('url'),
                            success : function() {
                                stopLoader();
                                location.href = t.data('redirect');
                            }
                        });
                    }
                },
                cancel  : {
                    keys : ['esc'],
                    text : t.data('lang-n')
                }
            }
        });
    });
    //endregion
    //region Редактирование заказа
    $(document).on('click', c.eOrder, function(e) {
        e.preventDefault();
        var t = $(this),
            fedit = $(i.fOrder);
        startLoader(fedit);
        if(!t.hasClass(c.disable)) {
            t.toggleClass(c.disable);
            fedit.removeClass(c.fHide);
            stopLoader();
        }
    });
    //endregion
    //region Блокировка / разблокировка кнопки "Удалить" для списка ЕУ
    $(c.chBox).on('change', function(e) {
        flag = checkFlag(flag);
    });
    $(c.chAll).on('change', function(e) {
        flag = checkFlag(flag);
    });
    function checkFlag(flag) {
        var sflag = false;
        $(i.lUnit).find('tr').each(function(i, y) {
            if($(y).hasClass(c.rActive)) {
                sflag = true;
                return false;
            }
        });
        if(flag != sflag) {
            flag = sflag;
            $(i.bDUnit).toggleClass(c.disable);
        }
        return flag;
    }
    //endregion
    //region Удаление ЕУ из списка
    $(document).on('click', i.bDUnit, function(e) {
        e.preventDefault();
        var t = $(this);
        $.confirm({
            animation           : 'top',
            closeAnimation      : 'bottom',
            theme               : 'light',
            backgroundDismiss   : true,
            title               : t.data('lang-t'),
            content             : t.data('lang-q'),
            buttons             : {
                confirm : {
                    keys    : ['enter'],
                    text    : t.data('lang-y'),
                    action  : function() {
                        startLoader(i.bOrder);
                        var dataDelete = {
                                ORDER_ID    : t.data('order'),
                                IBLOCK_ID   : t.data('iblock'),
                                LIST_UNITS  : {}
                            },
                            rtext = t.data('lang-r'),
                            count = $(i.lUnit).find('tr').find('td').length;
                        $(i.lUnit).find('.' + c.rActive).each(function (i, y) {
                            dataDelete.LIST_UNITS[i] = $(y).find(c.chBox).val();
                            var html = '<tr class="reestablish temp-row" data-rr="' + i + '" style="display: none;">' +
                                '<td colspan="' + count + '">' +
                                '<a href="#" class="' + c.recover + '" data-r="' + i + '">' + rtext + ' ' +
                                '&laquo;' + $(y).find(c.tdName).html() + '&raquo;</a>' +
                                '</td></tr>';
                            $(y).addClass('temp-row');
                            $(y).after(html);
                        });
                        $.ajax({
                            method  : 'post',
                            data    : dataDelete,
                            url     : t.data('url'),
                            success : function() {
                                //$(i.lUnit).find('.temp-row').toggle(50).removeClass('temp-row');
                                $(i.lUnit).find('.' + c.rActive).each(function(i,y) {
                                    var input = $(y).find(c.chBox),
                                        nextRow = $(y).next();
                                    input.click();
                                    $(y).toggle(function() {
                                        nextRow.toggle();
                                    });
                                    $(y).removeClass(c.rActive);
                                });
                                t.addClass(c.disable);
                                stopLoader();
                            }
                        });
                    }
                },
                cancel  : {
                    keys : ['esc'],
                    text : t.data('lang-n')
                }
            }
        });
    });
    //endregion
    //region Восстановление ЕУ
    $(document).on('click', '.' + c.recover, function(e){
        e.preventDefault();
        var rowCur = $(this).closest('tr'),
            rowPrev = rowCur.prev(),
            input = rowPrev.find(c.chBox),
            btnDel = $(i.bDUnit),
            dataReestablish = {
                REESTABLISH : 'Y',
                ORDER_ID    : btnDel.data('order'),
                IBLOCK_ID   : btnDel.data('iblock'),
                UNIT_ID     : input.val()
            };
        startLoader(i.bOrder);
        $.ajax({
            url     : btnDel.data('url'),
            method  : 'post',
            data    : dataReestablish,
            success : function() {
                rowCur.toggle(function(){
                    rowCur.remove();
                    rowPrev.toggle();
                });
                stopLoader();
            }
        });
    });
    //endregion
    //region Добавление Запроса в заказ
    $(document).on('click', c.tOrder, function(e){
        e.preventDefault();
        var t = $(this),
            dataSend = {
                ORDER_ID    : t.data('order'),
                IBLOCK_ID   : t.data('iblock')
            };
        startLoader(i.bOrder);
        $.ajax({
            method  : 'post',
            url     : t.data('url'),
            data    : dataSend,
            success : function() {
                stopLoader();
                $.confirm({
                    animation           : 'top',
                    closeAnimation      : 'bottom',
                    theme               : 'light',
                    backgroundDismiss   : true,
                    title               : t.data('lang-stitle'),
                    content             : t.data('lang-stext'),
                    buttons             : {
                        confirm  : {
                            keys : ['enter', 'esc'],
                            text : t.data('lang-y'),
                            action  : function() {
                                window.location.href = t.data('reload');
                            }
                        }
                    }
                });
            }
        })
    });
    //endregion
});
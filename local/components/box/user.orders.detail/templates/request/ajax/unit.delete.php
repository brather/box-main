<?php
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use \Bitrix\Main\Application,
    \Bitrix\Main\Loader;

Loader::includeModule('iblock');
$context = Application::getInstance()->getContext();
$arServer = $context->getServer()->toArray();
$arPost = $context->getRequest()->getPostList()->toArray();

if(!empty($arPost)) {
    // Возвращаем список ЕУ в заказе
    $arUnits = array();
    $arOrder = array('NAME' => 'ASC');
    $arFilter = array('IBLOCK_ID' => $arPost['IBLOCK_ID'], 'ID' => $arPost['ORDER_ID']);
    $arSelect = array('ID', 'NAME', 'PROPERTY_UNITS');
    $rsUnit = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
    if($obUnit = $rsUnit->Fetch()) {
        $arUnits = $obUnit['PROPERTY_UNITS_VALUE'];
    }
    $arResUnits = array();
    if($arPost['REESTABLISH'] == 'Y') {
        $arResUnits = $arUnits;
        $arResUnits[] = $arPost['UNIT_ID'];
    }
    elseif(!empty($arPost['LIST_UNITS'])) {
        // Возвращаем массив с исключенными ЕУ
        foreach ($arUnits as $iUnit => $vUnit) {
            if(!in_array($vUnit, $arPost['LIST_UNITS'])) {
                $arResUnits[] = $vUnit;
            }
        }
    }
    if(empty($arResUnits)) {
        $arResUnits[] = '';
    }
    // Обновим свойство у заказа
    CIBlockElement::SetPropertyValuesEx($arPost['ORDER_ID'], $arPost['IBLOCK_ID'], array('UNITS' => $arResUnits));
}
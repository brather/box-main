<?php
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use \Bitrix\Main\Application,
    \Bitrix\Main\Loader;

Loader::includeModule('iblock');
$context = Application::getInstance()->getContext();
$arServer = $context->getServer()->toArray();
$arPost = $context->getRequest()->getPostList()->toArray();

if(!empty($arPost)) {
    // Возвращаем id значения N у свойства "Запрос" типа "Список" для заказа
    $rsFieldRequest = CIBlockPropertyEnum::GetList(
        array('sort' => 'asc'),
        array('IBLOCK_ID' => $arPost['IBLOCK_ID'], 'CODE' => 'REQUEST', 'XML_ID' => 'N')
    );
    $idFieldRequest = 0;
    if($obField = $rsFieldRequest->Fetch()) {
        $idFieldRequest = $obField['ID'];
    }
    // Устанавливаем значение свойства у заказа
    if($idFieldRequest) {
        CIBlockElement::SetPropertyValuesEx(
            $arPost['ORDER_ID'],
            $arPost['IBLOCK_ID'],
            array('REQUEST' => $idFieldRequest)
        );
    }
}
<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Application,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Loader,
    \Bitrix\Iblock\ElementTable,
    \Bitrix\Highloadblock as HL;

class UserOrdersDetailComponent extends CBitrixComponent {
    //region Задание свойств класса
    private $page               = 'template';
    private $arAdressList       = array();
    private $arGet              = array();
    private $arPost             = array();
    private $dClassStatusOrder  = '';
    //endregion
    //region Подготовка входных параметров компонента
    /** Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        return parent::onPrepareComponentParams($arParams);
    }
    //endregion
    //region Подключение файлов перевода
    /**
     * Подключение файлов перевода
     */
    public function onIncludeComponentLang() {
        Loc::loadMessages(__FILE__);
    }
    //endregion
    //region Выполнение компонента
    /**
     * Выполнение компонента
     */
    public function executeComponent() {
        $this->includeModule();                                 // Подключение модулей
        $this->getRequest();                                    // Определяет массив request данных
        if($this->arPost['F_UNIT_REQUEST'] == 'Y') {
            $this->updateOrder();                               // Обновление данных заказа
        }
        $this->getStatusList();                                 // Возвращаем список статусов из highload блока со статусами
        $this->getOrder();                                      // Возвращает заказ
        $this->setSort();                                       // Возвращает массив сортировки столбцов в таблице с ЕУ заказа
        $this->includeComponentTemplate($this->page);           // Подключение шаблона
    }
    //endregion
    //region Подключаем список модулей
    /**
     * Подключаем список модулей
     */
    private function includeModule() {
        Loader::includeModule('iblock');
        Loader::includeModule('highloadblock');
    }
    //endregion
    //region Получение данных request
    /**
     * Получение данных request
     */
    private function getRequest() {
        $request = Application::getInstance()->getContext()->getRequest();
        $this->arGet = $request->getQueryList()->toArray();
        $this->arPost = $request->getPostList()->toArray();
    }
    //endregion
    //region Обновление свойств заказа
    /**
     * Обновление свойств заказа
     */
    private function updateOrder() {
        $arPost = $this->arPost;
        CIBlockElement::SetPropertyValuesEx(
            $arPost['F_UNIT_ID'],
            $arPost['F_UNIT_IBLOCK'],
            array(
                'DATE' => $arPost['F_UNIT_DATE'],
                'COMMENT' => $arPost['F_UNIT_COMMENT']
            ));
        header('Location: ' . $this->arParams['LIST_URL'] . $arPost['F_UNIT_ID'] . '/');
    }
    //endregion
    //region Возвращает заказ
    /**
     * Возвращает заказ
     */
    private function getOrder() {
        $arFilterOrder = array('ID' => $this->arParams['ORDER_ID']);
        $arSelectOrder = array('ID', 'NAME', 'IBLOCK_ID');
        $this->arResult['ORDER'] = ElementTable::getList(array(
            'filter' => $arFilterOrder,
            'select' => $arSelectOrder
        ))->fetch();
        $arOrderPropertyOrder = array('sort' => 'asc');
        $arFilterPropertyOrder = array(
            'ACTIVE' => 'Y',
            'EMPTY' => 'N'
        );
        $rsPropertyOrder = CIBlockElement::GetProperty(
            $this->arResult['ORDER']['IBLOCK_ID'],
            $this->arParams['ORDER_ID'],
            $arOrderPropertyOrder,
            $arFilterPropertyOrder);
        $iMultiple = 0;
        $previousCode = '';
        while($obPropertyOrder = $rsPropertyOrder->Fetch()) {
            if($previousCode != $obPropertyOrder['CODE']) {
                $this->arResult['PROPERTIES'][$obPropertyOrder['CODE']] = array(
                    'ID' => $obPropertyOrder['ID'],
                    'NAME' => $obPropertyOrder['NAME'],
                    'SORT' => $obPropertyOrder['SORT'],
                    'PROPERTY_TYPE' => $obPropertyOrder['PROPERTY_TYPE'],
                    'XML_ID' => $obPropertyOrder['XML_ID'],
                    'MULTIPLE' => $obPropertyOrder['MULTIPLE'],
                );
                if($obPropertyOrder['PROPERTY_TYPE'] == 'L') {
                    $this->arResult['PROPERTIES'][$obPropertyOrder['CODE']]['VALUE_ENUM'] = $obPropertyOrder['VALUE_ENUM'];
                    $this->arResult['PROPERTIES'][$obPropertyOrder['CODE']]['VALUE_XML_ID'] = $obPropertyOrder['VALUE_XML_ID'];
                    $this->arResult['PROPERTIES'][$obPropertyOrder['CODE']]['VALUE_SORT'] = $obPropertyOrder['VALUE_SORT'];
                }
                $previousCode = $obPropertyOrder['CODE'];
                $iMultiple = 0;
            } else {
                $iMultiple++;
            }

            if($obPropertyOrder['MULTIPLE'] == 'Y') {
                $this->arResult['PROPERTIES'][$obPropertyOrder['CODE']]['VALUE'][$iMultiple] = $obPropertyOrder['VALUE'];
            } else {
                if($obPropertyOrder['CODE'] == 'CONTRACT') {
                    $arContract = self::getContract($obPropertyOrder['VALUE']);
                    $this->arResult['PROPERTIES'][$obPropertyOrder['CODE']]['VALUE'] = $arContract['NAME'];
                    $this->arResult['PROPERTIES'][$obPropertyOrder['CODE']]['VALUE_ID'] = $arContract['ID'];
                } elseif($obPropertyOrder['CODE'] == 'STATUS') {
                    $this->arResult['PROPERTIES'][$obPropertyOrder['CODE']]['VALUE'] = $this->arResult['STATUS_LIST'][$obPropertyOrder['VALUE']]['UF_NAME'];
                    $this->arResult['PROPERTIES'][$obPropertyOrder['CODE']]['XML_ID'] = $obPropertyOrder['VALUE'];
                }
                else if ( $obPropertyOrder['CODE'] == 'ADRESS_CLIENT' )
                {
                    $this->_getAdressClient(array(
                            "=UF_XML_ID" => $obPropertyOrder['VALUE']
                        )
                    );
                    $this->arResult['PROPERTIES'][$obPropertyOrder['CODE']]['VALUE'] = (count($this->arAdressList) == 1) ? current($this->arAdressList) : $this->arAdressList;
                }
                else {
                    $this->arResult['PROPERTIES'][$obPropertyOrder['CODE']]['VALUE'] = $obPropertyOrder['VALUE'];
                }
            }
        }

        if(!$this->checkContractUser($this->arResult['PROPERTIES']['CONTRACT']['VALUE_ID']) || !$this->arResult['ORDER']) {
            $arServer = Application::getInstance()->getContext()->getServer()->toArray();
            header('Location: ' . $arServer['HTTP_ORIGIN'] . $this->arParams['LIST_URL']);
        }
        $this->arResult['LIST_UNITS'] = $this->getListUnits($this->arResult['PROPERTIES']['UNITS']['VALUE']);
    }
    //endregion
    //region Проверяет договор, к которому относится заказ, на возможность обработки пользователем
    /** Проверяет договор, к которому относится заказ, на возможность обработки пользователем
     * @param $contractId - id договора
     * @return bool
     */
    private function checkContractUser($contractId) {
        $sUserId = $this->arParams['USER_ID'];
        // Если пользователь не входит в группу суперпользователей
        if(!in_array(U_GROUP_CODE_CLIENT_S_USER, $this->arParams['USER_GROUPS'])) {
            $arOrderClient = array('ID' => 'ASC');
            $arFilterClient = array('IBLOCK_CODE' => $this->arParams['IBLOCK_CODE']);
            $arSelectClient = array('ID', 'NAME', 'IBLOCK_ID', 'PROPERTY_PROP_CLIENTS_OF_SUPERUSER');
            $rsClient = CIBlockElement::GetList($arOrderClient, $arFilterClient, false, false, $arSelectClient);
            if($obClient = $rsClient->Fetch()) {
                $sUserId = $obClient['PROPERTY_PROP_CLIENTS_OF_SUPERUSER_VALUE'];
            }
        }
        $arOrderContract = array('ID' => 'ASC');
        $arFilterContract = array('ID' => $contractId, 'PROPERTY_PROP_SUPER_USER' => $sUserId);
        $arSelectContract = array('ID', 'NAME', 'IBLOCK_ID');
        $rsContract = CIBlockElement::GetList($arOrderContract, $arFilterContract, false, false, $arSelectContract);
        if($obContract = $rsContract->Fetch()) {
            return true;
        } else {
            return false;
        }
    }
    //endregion
    //region Возвращает договор
    /** Возвращает договор
     * @param $idContract - ID договора
     * @return array|false
     */
    private function getContract($idContract) {
        $arContract = ElementTable::getList(array(
            'filter' => array('ID' => $idContract),
            'select' => array('IBLOCK_ID', 'NAME', 'ID')
        ))->fetch();
        return $arContract;
    }
    //endregion
    //region Возвращает статус заказа
    /** Возвращает статус заказа */
    private function getStatusList() {
        if(empty($this->dClassStatusOrder)) {
            $idHLBlock = $this->getIdHLBlock($this->arParams['HL_CODE_STATUS']);
            $this->dClassStatusOrder = $this->getDataClass($idHLBlock);
        }
        $dataClass = $this->dClassStatusOrder;
        $rsStatus = $dataClass::getList(array(
            'filter' => array(),
            'select' => array('*'),
            'order' => array('UF_SORT' => 'ASC'),
            'limit' => ''
        ));
        while($obStatus = $rsStatus->fetch()) {
            $this->arResult['STATUS_LIST'][$obStatus['UF_XML_ID']] = array(
                'ID' => $obStatus['ID'],
                'UF_NAME' => $obStatus['UF_NAME']
            );
        }
    }
    //endregion

    /** Сформируем массив данных адреса клиента
     *
     * @param array $filter
     */
    private function _getAdressClient(array $filter = array())
    {
        $arFilter = (count($filter) > 0) ? $filter : array();

        $idHLBlock = $this->getIdHLBlock( $this->arParams['HL_CODE_ADRESS_CLIENT']);
        $dClassAdressClient = $this->getDataClass($idHLBlock);

        $rsStatus = $dClassAdressClient::getList(array(
            'filter' => $arFilter,
            'order' => array('UF_SORT' => 'ASC'),
            'limit' => ''
        ));
        while($obStatus = $rsStatus->fetch()) {
            $this->arAdressList[ $obStatus['ID'] ] = array(
                'ID'            => $obStatus['ID'],
                'INDEX'         => $obStatus['UF_INDEX'],
                'REGION'        => $obStatus['UF_REGION'],
                'TOWN'          => $obStatus['UF_TOWN'],
                'STREET'        => $obStatus['UF_STREET'],
                'HOUSE'         => $obStatus['UF_HOUSE'],
                'OFFICE'        => $obStatus['UF_OFFICE'],
            );
        }

        unset($rsStatus, $dClassAdressClient, $idHLBlock);
    }


    //region Возвращаем id HL блока по его коду
    /**
     * Возвращаем id HL блока по его коду
     */
    private function getIdHLBlock($codeHL) {
        $connection = Application::getConnection();
        $sql = "SELECT ID,NAME FROM b_hlblock_entity WHERE NAME='" . $codeHL . "';";
        $recordset = $connection->query($sql)->fetch();
        return $recordset['ID'];
    }
    //endregion
    //region Возвращаем класс для работы с HL блоком
    /** Возвращаем класс для работы с HL блоком
     * @throws \Bitrix\Main\SystemException
     */
    private function getDataClass($idHL) {
        $hlblock = HL\HighloadBlockTable::getById($idHL)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        return $entity->getDataClass();
    }
    //endregion
    //region Возвращает список ЕУ по массиву ID
    /** Возвращает список ЕУ по массиву ID
     * @param $arID - массив ID ЕУ
     * @return array|bool
     */
    private function getListUnits($arID) {

        if (!is_array($arID) || !$arID ) return false;

        $arUnits = array();
        $arOrderUnits = array('NAME' => 'ASC');
        if(!empty($this->arGet['SORT'])) {
            $arOrderUnits = array($this->arGet['SORT'] => $this->arGet['ORDER']);
        }
        $this->arResult['COUNT_ON_PAGE'] = $this->arParams['COUNT_ITEM_ON_PAGE'];
        if(!empty($this->arGet['COUNT_ON_PAGE']) && is_numeric($this->arGet['COUNT_ON_PAGE'])) {
            $this->arResult['COUNT_ON_PAGE'] = $this->arGet['COUNT_ON_PAGE'];
        }
        $this->arResult['START_INDEX_UNIT'] = 1;
        if(!empty($this->arGet['PAGEN_1'])) {
            $this->arResult['START_INDEX_UNIT'] = ($this->arGet['PAGEN_1'] - 1) * $this->arResult['COUNT_ON_PAGE'] + 1;
        }
        $numPage = 1;
        if(!empty($this->arGet['PAGEN_1'])) {
            $numPage = $this->arGet['PAGEN_1'];
        }
        $arNavParamsUnits = array(
            'nPageSize' => $this->arResult['COUNT_ON_PAGE'],
            'iNumPage' => $numPage
        );

        $arFilterUnits = array('ID' => $arID);
        $arSelectUnits = array('ID', 'NAME', 'IBLOCK_NAME', 'IBLOCK_ID', 'PROPERTY_PROP_SSCC', 'PROPERTY_PROP_WMS_STATUS');
        $rsUnits = CIBlockElement::GetList($arOrderUnits, $arFilterUnits, false, $arNavParamsUnits, $arSelectUnits);
        $this->arResult['COUNT_UNITS'] = $rsUnits->SelectedRowsCount();
        if($this->arResult['START_INDEX_UNIT'] > $this->arResult['COUNT_UNITS']) {
            $this->arResult['START_INDEX_UNIT'] = 1;
        }
        $this->arResult['NAV_PRINT'] = $rsUnits->GetPageNavStringEx($navComponentObject, Loc::getMessage('USER_REG_C_PAGES'), $this->arParams['NAV_TEMPLATE']);
        while($obUnits = $rsUnits->Fetch()) {
            //echo "<pre>"; print_r($obUnits); echo "</pre>";
            $arUnits[] = array(
                'ID' => $obUnits['ID'],
                'NAME' => $obUnits['NAME'],
                'IBLOCK_NAME' => $obUnits['IBLOCK_NAME'],
                'IBLOCK_ID' => $obUnits['IBLOCK_ID'],
                'PROPERTY_PROP_SSCC' => $obUnits['PROPERTY_PROP_SSCC_VALUE'],
                'PROPERTY_PROP_WMS_STATUS' => $this->getWmsStatus($obUnits['PROPERTY_PROP_WMS_STATUS_VALUE'])
            );
        }
        return $arUnits;
    }
    //endregion

    /**
     * возвращает статус WMS по UF_XML_ID
     *
     * @param string $xmlId
     * @return string
     */
    private function getWmsStatus($xmlId)
    {
        if ($xmlId) {
            $idHLBlock = $this->getIdHLBlock( $this->arParams['HL_CODE_STATUS_OF_WMS']);
            $dClassAdressClient = $this->getDataClass($idHLBlock);

            $rsStatus = $dClassAdressClient::getRow([
                'filter' => [
                    'UF_XML_ID' => $xmlId
                ]
            ]);
            if ($rsStatus)
                return $rsStatus['UF_NAME'];
        }

        return "";
    }

    //region Возвращает массив с полями для сортировки
    /**
     * Возвращает массив с полями для сортировки
     */
    private function setSort() {
        $preffixGet = '';
        if(!empty($this->arGet)) {
            $iGet = 0;
            foreach($this->arGet as $cGet => $vGet) {
                if(!empty($this->arParams['SET_SORT_FILTER'])) {
                    if(!empty($this->arParams['SET_SORT_FILTER'][$cGet])) {
                        continue;
                    }
                }
                if($cGet != 'SORT' && $cGet != 'ORDER') {
                    if($iGet == 0) {
                        $preffixGet .= '?';
                    } else {
                        $preffixGet .= '&';
                    }
                    if(is_array($vGet)) {
                        foreach($vGet as $iVal => $val) {
                            $preffixGet .= $cGet . urlencode('[]') . '=' . urlencode($val);
                            if($iVal + 1 != count($vGet)) {
                                $preffixGet .= '&';
                            }
                        }
                    } else {
                        $preffixGet .= $cGet . '=' . $vGet;
                    }

                    $iGet++;
                }
            }
        }
        if(!empty($this->arParams['SET_SORT_FILTER'])) {
            $iFilter = 0;
            foreach($this->arParams['SET_SORT_FILTER'] as $cCode) {
                $vCode = 'ASC';
                if(!empty($this->arGet['SORT']) && $this->arGet['SORT'] == $cCode && $this->arGet['ORDER'] == 'ASC') {
                    $vCode = 'DESC';
                }
                $prefix = $preffixGet;
                if($iFilter == 0 && $prefix == '') {
                    $prefix .= '?';
                } else {
                    $prefix .= '&';
                }
                $this->arResult['ROW_SORT'][$cCode] = $this->arParams['CURENT_PATH'] . $prefix . 'SORT=' . $cCode . '&ORDER=' . $vCode;
                $iFilter++;
            }
        }
    }
    //endregion
}

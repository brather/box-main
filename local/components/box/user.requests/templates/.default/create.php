<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */
global $APPLICATION;?>

<?$APPLICATION->IncludeComponent(
    'box:user.orders.create',
    '',
    array(
        'SEF_FOLDER' => $arParams['SEF_FOLDER'],
        'IBLOCK_CODE_ORDERS' => $arParams['IBLOCK_CODE']['ORDERS'],
        'IBLOCK_CODE_CONTRACTS' => $arParams['IBLOCK_CODE']['CONTRACTS'],
        'IBLOCK_CODE_CLIENTS' => $arParams['IBLOCK_CODE']['CLIENTS'],
        'HTML_PAGE_TITLE' => urlencode($arResult['PAGE_TITLE']),
        'LIST_ORDER_TYPE' => $arResult['LIST_ORDER_TYPE'],
        'COUNT_INPUT_IN_ROW' => $arParams['COUNT_INPUT_IN_ROW'],
        'COUNT_ITEM_ON_PAGE' => $arParams['COUNT_ITEM_ON_PAGE'],
        'CURRENT_PATH' => $arResult['CURRENT_PATH'],
        'USER_GROUPS' => $arResult['USER_GROUPS'],                                  // Массив с кодами групп которым принадлежит текущий пользователь
        'VARIABLES' => $arResult['VARIABLES'],
        'NAV_TEMPLATE' => $arParams['NAV_TEMPLATE'],
        'USER_ID' => $arResult['USER_ID'],
        'EXCEPTION_TYPE_ORDER_CREATE' => $arParams['EXCEPTION_TYPE_ORDER_CREATE'],
    )
);?>
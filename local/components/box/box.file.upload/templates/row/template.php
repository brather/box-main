<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

use Bitrix\Main\Localization\Loc;
$this->GetFolder()
?>
<div class="order order-upload">
    <div class="order__col">
        <span class="url-upload fileinput-button">
            <span><i class="fa fa-upload"></i><?= Loc::getMessage('T_FILEUPLOAD_BUTTON_SELECT') ?></span>
            <input id="<?= $arParams['INPUT_ID'] ?>"
                   data-files="<?= urlencode($arParams['PATH_TO_FILES']) ?>"
                   type="file"
                   name="files[]"
                   multiple>
        </span>
        <div id="progress-<?= $arParams['INPUT_ID'] ?>"
             class="progress progress-striped active"
             style="display: none;">
            <div class="progress-bar progress-bar-success"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        var inputID = "<?= $arParams['INPUT_ID'] ?>",
            animateSpeed = 200,
            cls = {
                count : '.count',
                pbar    : '.progress-bar'
            },
            obj = {
                files : '#files-' + inputID,
                progress : '#progress-' + inputID
            },
            pathToSave = $('#' + inputID).data('files'),
            countFiles = $(obj.files).find(cls.count).html() * 1,
            url = "<?= $arResult['UPLOAD_SCRIPTS'] ?>?path=" + pathToSave + "&count=" + countFiles;

        function hideProgress() {
            $(obj.progress).hide(animateSpeed);
        }

        $('#' + inputID).fileupload({
            url: url,
            dataType: 'json',
            done: function (e, data) {
                setTimeout(hideProgress, 500);
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                if(!$(obj.progress).is(':visible')) {
                    $(obj.progress).show(animateSpeed);
                }
                $(obj.progress).find(cls.pbar).css('width', progress + '%');
            }
        });
    })
</script>

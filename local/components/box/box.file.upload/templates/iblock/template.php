<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Application;
$arServer = Application::getInstance()->getContext()->getServer()->toArray();
$this->GetFolder();?>

<div class="order order-upload">
    <div class="order__col">
        <span class="btn btn-success fileinput-button">
            <span><i class="fa fa-upload"></i><?= Loc::getMessage('T_FILEUPLOAD_BUTTON_SELECT') ?></span>
            <input id="<?= $arParams['INPUT_ID'] ?>"
                   data-files="<?= urlencode($arParams['PATH_TO_FILES']) ?>"
                   type="file"
                   name="files[]"
                   multiple>
            <input type="hidden" name="new_name" value="<?= $arParams['NEW_NAME'] ?>" />
            <?$exist = 'N';
            if(file_exists($arServer['DOCUMENT_ROOT'] . $arParams['PATH_TO_FILES'] . $arParams['NEW_NAME'])) {
                $exist = 'Y';
            }?>
            <input type="hidden" name="exist_file" value="<?= $exist ?>" />
            <input type="hidden" name="change_file" value="Y">
        </span>
        <div id="progress-<?= $arParams['INPUT_ID'] ?>"
             class="progress progress-striped active"
             style="display: none;">
            <div class="progress-bar progress-bar-success"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        var inputID = "<?= $arParams['INPUT_ID'] ?>",
            animateSpeed = 200,
            cls = {
                count : '.count',
                pbar    : '.progress-bar'
            },
            obj = {
                progress : '#progress-' + inputID
            },
            iFile = $('#' + inputID),
            iExist = iFile.closest('span').find('[name="exist_file"]'),
            pathToSave = $('#' + inputID).data('files'),
            url = "<?= $arResult['UPLOAD_SCRIPTS'] ?>?path=" + pathToSave;
        function hideProgress() {
            $(obj.progress).hide(animateSpeed);
        }

        $('#' + inputID).fileupload({
            url: url,
            dataType: 'json',
            done: function (e, data) {
                iExist.val('Y');
                setTimeout(hideProgress, 2000);
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                if(!$(obj.progress).is(':visible')) {
                    $(obj.progress).show(animateSpeed);
                }
                $(obj.progress).find(cls.pbar).css('width', progress + '%');
            }
        });
    })
</script>

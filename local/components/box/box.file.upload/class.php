<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application,
    \Bitrix\Main\Page\Asset;

class BoxFileUploadComponent extends CBitrixComponent {
    //region Задание свойств для класса
    private $page = 'template';                     // Шаблон компонента
    //endregion
    //region Подготовка входных параметров компонента
    /**
     * Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        return parent::onPrepareComponentParams($arParams);
    }
    //endregion
    //region Подключение файлов перевода
    /**
     * Подключение файлов перевода
     */
    public function onIncludeComponentLang() {
        $this->includeComponentLang(basename(__FILE__));
    }
    //endregion
    //region Выполнение компонента
    /**
     * Выполнение компонента
     */
    public function executeComponent() {
        $this->checkDirectory();
        $this->addHelpers();
        $this->getCountFiles();
        $this->includeComponentTemplate($this->page);   // Подключаем шаблон
    }
    //endregion
    //region Подключение файлов стилей и скриптов плагина
    /**
     * Подключение файлов стилей и скриптов плагина
     */
    private function addHelpers() {
        $componentPath = $this->getPath();
        $this->arResult['UPLOAD_SCRIPTS'] = $componentPath . '/help/uploadedFiles.php';
        $this->checkAddFiles($componentPath . '/help/css/jquery.fileupload.css', 'style');
        $this->checkAddFiles($componentPath . '/help/css/jquery.fileupload-ui.css', 'style');
        $this->checkAddFiles($componentPath . '/help/css/jquery.fileupload-ui-noscript.css', 'style');
        $this->checkAddFiles($componentPath . '/help/js/vendor/jquery.ui.widget.js', 'script');
        $this->checkAddFiles($componentPath . '/help/js/jquery.fileupload.js', 'script');
    }
    //endregion
    //region Проверка файлов на существование и их подключение
    /** Проверка файлов на существование и их подключение
     * @param $file - путь к файлу
     * @param $type - тип файла
     */
    private function checkAddFiles($file, $type) {
        if(file_exists(Application::getDocumentRoot() . $file)) {
            if($type == 'style') {
                Asset::getInstance()->addCss($file);
            } elseif($type == 'script') {
                Asset::getInstance()->addJs($file);
            }
        }
    }
    //endregion
    //region Возвращает путь к папке для файлов
    /**
     * Возвращает путь к папке для файлов
     */
    private function checkDirectory() {
        $pathToFile = $this->arParams['PATH_TO_FILES'];
        $firstSymbol = substr($pathToFile, 0, 1);
        $lastSymbol = substr($pathToFile, strlen($pathToFile) - 1, 1);
        if($firstSymbol != '/') {
            $pathToFile = '/' . $pathToFile;
        }
        if($lastSymbol != '/') {
            $pathToFile = $pathToFile . '/';
        }
        $documentRoot = Application::getDocumentRoot();
        $fullPath = $pathToFile;
        if(!stristr($fullPath, $documentRoot)) {
            $fullPath = $documentRoot . $fullPath;
        }
        //if(!file_exists($fullPath)) {
            //mkdir($fullPath, 0777);
        //}
        $this->arResult['FULL_PATH_FILES'] = $fullPath;
    }
    //endregion
    //region Количество файлов для ЕУ
    /**
     * Количество файлов для ЕУ
     */
    private function getCountFiles() {
        $files = scandir($this->arResult['FULL_PATH_FILES']);
        $this->arResult['COUNT_FILES'] = 0;
        foreach($files as $iFile => $nFile) {
            if($nFile != '.' && $nFile != '..' && !stristr($nFile, 'thumbnail')) {
                $this->arResult['COUNT_FILES']++;
            }
        }
    }
    //endregion
}
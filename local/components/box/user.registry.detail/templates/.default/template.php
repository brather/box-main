<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application;

$context = Application::getInstance()->getContext();
$request = $context->getRequest();
$arGet = $request->getQueryList();
$arPost = $request->getPostList();
$arServer = $context->getServer()->toArray();
$arParamsNotEdit = array(
    'ID',
    'IBLOCK_ID',
    'PROPERTY_PROP_WMS_STATUS',
    'PROPERTY_PROP_SSCC'
);
global $APPLICATION;?>
<div class="content__item _top">
    <div class="page__title">
        <h1><?= Loc::getMessage('USER_REG_T_TITLE') ?></h1>
    </div>
    <div class="scrollbar-outer">
        <form action="<?= $arParams['SEF_FOLDER'] ?>" class="form" method="get">
            <input type="hidden" name="F_UNIT_FILTER" value="Y">
            <?
            $countInLine = $arParams['COUNT_IN_ROW_PARAMS'];
            $countRowSort = count($arResult['ROW_SORT']);
            $iSort = 0;?>
            <?foreach($arResult['ROW_SORT'] as $cSort => $vSort){?>
                <?$flag = true;
                if(!empty($arParams['VARIABLES']['ID']) || stristr($cSort, 'PROP_FILE')) {
                    if($cSort == 'IBLOCK_ID' || $cSort == 'PROPERTY_PROP_FILE') {
                        $flag = false;
                        $countRowSort = $countRowSort - 1;
                        if($iSort == $countRowSort) {?>
                            </div>
                        <?}
                    }
                }?>
                <?if($flag) {?>
                    <?if($iSort == 0) {?>
                        <div class="form__row">
                    <?}?>
                    <?if(stristr($cSort, 'PROPERTY_')) {
                        $arField = $arResult['PROPERTY_LIST'][str_replace('PROPERTY_', '', $cSort)];
                        $rowName = $arField['NAME'];
                        $rowPlaceholder = Loc::getMessage('UNIT_F_PLACEHOLDER_' . $arField['PROPERTY_TYPE']);
                    } else {
                        $rowName = Loc::getMessage('UNIT_F_TITLE_' . $vSort['CODE']);
                        $rowPlaceholder = Loc::getMessage('UNIT_F_PLACEHOLDER_' . $vSort['CODE']);
                    }?>
                    <div class="form__row__col   _size6">
                        <div class="form__row__name"><?= $rowName ?></div>
                        <?if($cSort == 'IBLOCK_ID') {?>
                            <select name="F_UNIT_FILTER_IBLOCK_ID[]"
                                    multiple
                                    size="2"
                                    id="unit-filter-iblock">
                                <option value="">-</option>
                                <?foreach($arResult['LIST_REGISTRY'] as $idRegistry => $nRegistry){?>
                                    <option value="<?= $idRegistry ?>"<?if(in_array($idRegistry, $arGet['F_UNIT_FILTER_IBLOCK_ID'])){?> selected<?}?>>
                                        <?= $nRegistry ?>
                                    </option>
                                <?}?>
                            </select>
                        <?} else {?>
                            <input type="text"
                                   value="<?= $arGet['F_UNIT_FILTER_' . $vSort['CODE']] ?>"
                                   class="form__field"
                                   name="F_UNIT_FILTER_<?= $vSort['CODE'] ?>"
                                   placeholder="<?= $rowPlaceholder ?>">
                        <?}?>
                    </div>
                    <?$iSort++;?>
                    <?if(($iSort % $countInLine) == 0 && $iSort != $countRowSort) {?>
                        </div>
                        <div class="form__row">
                    <?} elseif($iSort == $countRowSort) {?>
                        </div>
                    <?}?>
                <?}?>
            <?}?>
            <div class="form__row">
                <button type="submit" class="btn"><?= Loc::getMessage('USER_REG_T_BUTTON_APPLY') ?></button>
                <a href="<?= $arParams['SEF_FOLDER'] ?>" class="btn _white">
                    <?= Loc::getMessage('USER_REG_T_BUTTON_RESET') ?>
                </a>
            </div>
        </form>
    </div>
</div>
<div class="content__item _bottom">
    <div class="scrollbar-outer">
        <form action="<?= $arServer['REQUEST_URI'] ?>"
              method="post"
              enctype="multipart/form-data"
              class="form js-edit-unit">
            <input type="hidden" name="F_UNIT_EDIT" value="Y">
            <table class="table js-table">
                <thead>
                <tr>
                    <th class="js-checkbox-td">
                        <label class="checkbox">
                            <input type="checkbox" name="L_UNITS_CHECK_ALL" class="js-table-checkbox-all">
                            <span></span>
                        </label>
                    </th>
                    <?foreach($arResult['ROW_SORT'] as $cSort => $vSort) {?>
                        <?if($cSort != 'IBLOCK_ID') {?>
                            <th>
                                <a class="filtr" href="<?= $vSort['URL'] ?>">
                                    <?if(stristr($vSort['CODE'], 'PROPERTY_')) {?>
                                        <?= $arResult['PROPERTY_LIST'][str_replace('PROPERTY_', '', $vSort['CODE'])]['NAME'] ?>
                                    <?} else {?>
                                        <?= Loc::getMessage('USER_REG_T_TABLE_' . $vSort['CODE']) ?>
                                    <?}?>
                                </a>
                            </th>
                        <?}?>
                    <?}?>
                </tr>
                </thead>
                <?foreach($arResult['LIST_REG_ITEMS'] as $iItem => $arItem){?>
                    <tr>
                        <td class="js-checkbox-td">
                            <label class="checkbox">
                                <input type="checkbox"
                                       name="L_CHECK_UNIT[]"
                                       value="UNIT_ID-<?= $arItem['ID'] ?>-IBLOCK_ID-<?= $arItem['IBLOCK_ID'] ?>"
                                       class="js-table-checkbox">
                                <span></span>
                            </label>
                        </td>
                        <?foreach($arResult['ROW_SORT'] as $cSort => $vSort) {?>
                            <?if($cSort != 'IBLOCK_ID'){?>
                                <td>
                                    <?if($vSort['CODE'] == 'ACTIVE') {?>
                                        <select name="UNIT_ACTIVE_<?= $arItem['ID'] ?>" id="unit-active-<?= $arItem['ID'] ?>">
                                            <option value="Y"<?if($arItem['ACTIVE'] == 'Y'){?> selected<?}?>>
                                                <?= Loc::getMessage('ORDER_VALUE_ACTIVE_Y') ?>
                                            </option>
                                            <option value="N"<?if($arItem['ACTIVE'] == 'N'){?> selected<?}?>>
                                                <?= Loc::getMessage('ORDER_VALUE_ACTIVE_N') ?>
                                            </option>
                                        </select>
                                    <?} elseif(stristr($vSort['CODE'], 'PROPERTY_') && $vSort['CODE'] == 'PROPERTY_PROP_IF_MANAGER') {?>
                                        <select name="SUB_USER_<?= $arItem['ID'] ?>" id="sub-user">
                                            <option value="">-</option>
                                            <?foreach($arResult['LIST_SUB_USERS'] as $subUser) {?>
                                                <option value="<?= $subUser['PROPERTY_PROP_CLIENT_VALUE'] ?>">
                                                    <?= $subUser['NAME'] ?>
                                                </option>
                                            <?}?>
                                        </select>
                                    <?} elseif(stristr($vSort['CODE'], 'PROPERTY_') && $vSort['CODE'] == 'PROPERTY_PROP_FILE') {?>
                                        <?if(!empty($arItem['PROPERTY_PROP_SSCC'])) {?>
                                            <div class="contain-upload disabled-upload" data-ajax-file="<?= $this->GetFolder() ?>/ajax.files.php">
                                                <?$APPLICATION->IncludeComponent(
                                                    'box:box.file.upload',
                                                    'row',
                                                    array(
                                                        'INPUT_ID' => 'UNIT-' . $arItem['PROPERTY_PROP_SSCC'],
                                                        'PATH_TO_FILES' => $arParams['PATH_TO_FILES'] . $arItem['PROPERTY_PROP_SSCC'] . '/',
                                                    )
                                                );?>
                                            </div>
                                        <?} else {?>
                                            <?= Loc::getMessage('USER_REG_T_EMPTY_SSCC') ?>
                                        <?}?>
                                    <?} elseif(!in_array($vSort['CODE'], $arParamsNotEdit)) {?>
                                        <?$flag = true;?>
                                        <?if(stristr($vSort['CODE'], 'PROPERTY_')
                                            && !array_key_exists(str_replace('PROPERTY_', '', $vSort['CODE']), $arResult['PROPERTY_LIST_IBLOCK'][$arItem['IBLOCK_ID']])) {
                                            $flag = false;
                                        }?>
                                        <?if($flag) {?>
                                            <input type="text"
                                                   disabled
                                                   class="form__field js-input-disabled"
                                                   name="UNIT_<?= $arItem['ID'] ?>_<?= $vSort['CODE'] ?>"
                                                   value="<?= $arItem[$vSort['CODE']] ?>"
                                                   placeholder="<?= Loc::getMessage('PLACEHOLDER_UNIT_' . $vSort['CODE']) ?>">
                                        <?}?>
                                    <?} else {?>
                                        <?= $arItem[$vSort['CODE']] ?>
                                    <?}?>
                                </td>
                            <?}?>
                        <?}?>
                    </tr>
                <?}?>
            </table>
            <hr>
            <br>
            <button class="btn btn-disabled js-btn-submit" type="submit"><?= Loc::getMessage('USER_REG_T_BUTTON_EDIT') ?></button>
        </form>
        <br>
        <hr>
        <?= $arResult['NAV_PRINT'] ?>
    </div>
</div>
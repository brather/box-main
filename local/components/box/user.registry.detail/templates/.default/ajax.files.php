<?php
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use \Bitrix\Main\Application,
    \Bitrix\Main\Localization\Loc;

Loc::loadLanguageFile(__FILE__);

$context = Application::getInstance()->getContext();
$docRoot = $context->getServer()->getDocumentRoot();
$arPost = $context->getRequest()->getPostList()->toArray();


$arData = array();
if($arPost['SFILES'] == 'Y') {
    $sFolder = urldecode($arPost['FOLDER']);
    $folder = $docRoot . $sFolder;
    $files = scandir($folder);
    $tmpHtml = '<div><ol>';
    foreach($files as $iFile => $nFile) {
        if($nFile != '..' & $nFile != '.' && !stristr($nFile, 'thumbnail')) {
            $extFile = pathinfo($nFile);
            $fileName = iconv('cp1251', 'utf-8', transliterateOut($extFile['filename']));
            $arData['FILES'][$iFile] = $extFile;
            $tmpHtml .= '<li><a class="js-file" href="' . $sFolder . urldecode($extFile['basename']) . '">' . $fileName . '</a></li>';
        }
    }
    $tmpHtml .= '</ol></div>';
    $arData['HTML_LIST'] = $tmpHtml;
    $arData['FOLDER'] = $folder;
    echo json_encode($arData);
}
$(document).ready(function () {
    //region Задание переменных
    var ids = {
            v   : '#vertical',
            hl  : '#horizontal-left',
            hr  : '#horizontal-right'
        },
        cls = {
            form        : '.js-edit-unit',
            table       : '.table',
            checkbox    : '.js-table-checkbox',
            checkall    : '.js-table-checkbox-all',
            btn         : '.btn',
            upload      : '.contain-upload',
            uploaddis   : 'disabled-upload',
            btnsubmit   : '.js-btn-submit',
            input       : '.js-input-disabled',
            disabled    : 'btn-disabled',
            filelist    : '.js-file-list',
            blkfiles    : '.files',
            topsplit    : '.top_panel',
            bottomsplit : '.bottom_panel',
            leftsplit   : '.left_panel',
            rightsplit  : '.right_panel',
            content     : '.content',
            subcontent  : '.scrollbar-outer',
            scrcontent  : '.scroll-content',
            file        : '.js-file'
        };
    //endregion
    //region Разбивка рабочей области со списком ЕУ реестра и фильтра
    setSplit(ids.hl, 'horizontal', 100, '30%');
    //endregion
    //region Разбивка рабочей области на область просмотра документа (5%) и область со списком ЕУ
    setSplit(ids.v, 'vertical', 100, '95%');
    //endregion
    //region Клик по чекбоксу отдельной ЕУ
    $(cls.checkbox).on('click', function(e){
        var $this = $(this),
            row = $this.closest('tr'),
            table = row.closest(cls.table),
            blcUpload = row.find(cls.upload),
            buttonSubmit = table.closest(cls.form).find(cls.btnsubmit),
            countCheck = 0,
            flag = true;

        //region Проверка количества включенных чекбоксов
        table.find(cls.checkbox).each(function(i,y){
            if($(y).prop('checked')) {
                countCheck = countCheck + 1;
            }
        });
        //endregion
        //region Проверка наличия блокировки на загрузку файлов
        blcUpload.removeClass(cls.uploaddis).addClass(cls.uploaddis);
        //endregion
        //region Если чекбокс активен то разблокируем возможность загрузки файлов
        if($this.prop('checked')) {
            blcUpload.removeClass(cls.uploaddis);
            flag = false;
        }
        //endregion
        //region Включение / отключение возможности редактирования инпутов
        row.find(cls.input).prop('disabled', flag);
        row.find('select').prop('disabled', flag).trigger('refresh');
        //endregion
        //region Блокировка и разблокировка кнопки "Изменить" для формы
        if(countCheck == 0) {
            buttonSubmit.addClass(cls.disabled);
        } else if(countCheck == 1) {
            buttonSubmit.removeClass(cls.disabled);
        }
        //endregion
    });
    //endregion
    //region Клик по чекбоксу "выделить все"
    $(document).on('click', cls.checkall, function(e){
        var $this = $(this),
            table = $this.closest(cls.table),
            blcUpload = table.find(cls.upload),
            buttonSubmit = table.closest(cls.form).find(cls.btnsubmit),
            flag = true;

        if($this.prop('checked')) {
            flag = false;
            buttonSubmit.removeClass(cls.disabled);
            blcUpload.removeClass(cls.uploaddis);
        } else {
            blcUpload.addClass(cls.uploaddis);
            buttonSubmit.addClass(cls.disabled);
        }
        table.find(cls.input).prop('disabled', flag);
        table.find('select').prop('disabled', flag).trigger('refresh');
    });
    //endregion
    //region Отправка формы при ключеннной кнопкуе изменить
    $(cls.btnsubmit).on('click', function(e){
        e.preventDefault();
        var $this = $(this),
            form = $this.closest(cls.form);
        if(!$this.hasClass(cls.disabled)) {
            form.submit();
        }
    });
    //endregion
    //region Возвращает список документов ЕУ
    $(document).on('click', cls.filelist, function(e){
        e.preventDefault();
        var $this = $(this),
            url = $this.closest(cls.upload).data('ajax-file'),
            data = {
                SFILES : 'Y',
                FOLDER : $this.data('files')
            },
            bFiles = $this.closest(cls.upload);
        startLoader($(ids.v).find(cls.rightsplit));
        refreshSplit(ids.v, 'vertical', 70);
        setSplit(ids.hr, 'horizontal', 100, '80%');

        $.ajax({
            url : url,
            data : data,
            type : 'post',
            success : function(data) {
                var rdata = $.parseJSON(data);
                if($(ids.hr).find(cls.bottomsplit).find(cls.subcontent).find(cls.scrcontent).length > 0) {
                    $(ids.hr).find(cls.bottomsplit).find(cls.subcontent).find(cls.scrcontent).empty();
                    $(ids.hr).find(cls.bottomsplit).find(cls.subcontent).find(cls.scrcontent).append(rdata.HTML_LIST);
                } else {
                    $(ids.hr).find(cls.bottomsplit).find(cls.subcontent).append(rdata.HTML_LIST);
                }

                stopLoader();
            }
        });
        stopLoader();
    });
    //endregion
    //region Показывает отдельный документ для просмотра
    $(document).on('click', cls.file, function (e){
        e.preventDefault();
        var $this = $(this),
            blkIframe = '<iframe ' +
            'src="' + $this.attr('href') + '" ' +
            'style="width: 100%; height: 100%;" ' +
            'frameborder="0"></iframe>';
        $(ids.hr).find(cls.topsplit).find(cls.subcontent).empty();
        $(ids.hr).find(cls.topsplit).find(cls.subcontent).append(blkIframe);
    });
    //endregion
});
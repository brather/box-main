<?php
$MESS['USER_REG_T_BACK_TO_LIST_CONTRACTS'] = 'Вернуться к списку договоров';
$MESS['USER_REG_T_BACK_TO_LIST_REGISTRY'] = 'Вернуться к списку реестров';
$MESS['USER_REG_T_TITLE'] = 'Реестр. Список Единиц учета';
$MESS['USER_REG_T_TABLE_ID'] = 'ID';
$MESS['USER_REG_T_TABLE_NAME'] = 'Название ЕУ';
$MESS['USER_REG_T_TABLE_ACTIVE'] = 'Заявка на хранение';
$MESS['USER_REG_T_TABLE_IBLOCK_ID'] = 'ID реестра';
$MESS['USER_REG_T_TABLE_IBLOCK_NAME'] = 'Название реестра';
$MESS['USER_REG_T_TABLE_XML_ID'] = 'xml_id';
$MESS['ORDER_VALUE_ACTIVE_Y'] = 'Активна';
$MESS['ORDER_VALUE_ACTIVE_N'] = 'Неактивна';
$MESS['USER_REG_T_FILES'] = 'Загруженных файлов: ';
$MESS['USER_REG_T_BUTTON_APPLY'] = 'Применить';
$MESS['USER_REG_T_BUTTON_RESET'] = 'Сбросить';
$MESS['USER_REG_T_BUTTON_EDIT'] = 'Изменить';
$MESS['USER_REG_T_EMPTY_SSCC'] = 'Не задан код SSCC';
$MESS['USER_REG_T_FOUND'] = 'Найдено: ';
$MESS['USER_REG_T_TITLE_COUNT_FILES'] = ' файла(ов)';

$MESS['UNIT_F_TITLE_ID'] = 'ID ЕУ: ';
$MESS['UNIT_F_TITLE_IBLOCK_ID'] = 'ID реестра: ';
$MESS['UNIT_F_TITLE_IBLOCK_NAME'] = 'Название реестра: ';
$MESS['UNIT_F_TITLE_NAME'] = 'Наименование ЕУ: ';

$MESS['UNIT_F_PLACEHOLDER_ID'] = 'число...';
$MESS['UNIT_F_PLACEHOLDER_NAME'] = 'текст...';
$MESS['UNIT_F_PLACEHOLDER_IBLOCK_ID'] = 'число...';
$MESS['UNIT_F_PLACEHOLDER_IBLOCK_NAME'] = 'текст...';
$MESS['UNIT_F_PLACEHOLDER_S'] = 'текст...';
$MESS['UNIT_F_PLACEHOLDER_N'] = 'число...';

$MESS['USER_REG_T_OPTION_DOWNLOAD'] = 'Скачать';
$MESS['USER_REG_T_OPTION_VIEW'] = 'Просмотр';
$MESS['USER_REG_T_OPTION_EDIT'] = 'Редактировать';
$MESS['USER_REG_T_OPTION_PRINT'] = 'Печать';
$MESS['USER_REG_T_OPTION_DELETE'] = 'Удалить';

$MESS['USER_ORDER_T_SELECT_REGISTRY'] = 'Выберите ЕУ из реестра ';
$MESS['USER_ORDER_T_SHOW_FILTER'] = 'Раскрыть фильтр ';
$MESS['USER_ORDER_T_HIDE_FILTER'] = 'Скрыть фильтр ';
$MESS['SPLIT'] = '  |  ';
$MESS['SHOW_ONLY_CHECK'] = 'Показать выделенные';
$MESS['SHOW_ONLY_SELECTED'] = 'Показать только добавленные ЕУ';
$MESS['SHOW_ALL'] = 'Показать все';
$MESS['SHOW_ALL_UNITS'] = 'Показать все ЕУ';
$MESS['USER_REG_T_COUNT_ITEMS'] = 'Количество ЕУ на странице: ';
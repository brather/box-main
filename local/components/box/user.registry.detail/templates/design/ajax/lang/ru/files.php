<?php
$MESS['FILE_DELETE_T'] = 'Удаление файла';
$MESS['FILE_DELETE_Q'] = 'Вы действительно хотите удалить выбранный файл?';
$MESS['FILE_DELETE_Y'] = 'Удалить';
$MESS['FILE_DELETE_N'] = 'Отмена';
<?php
$MESS['USER_REG_T_BACK_TO_LIST_CONTRACTS'] = 'Вернуться к списку договоров';
$MESS['USER_REG_T_BACK_TO_LIST_REGISTRY'] = 'Вернуться к списку реестров';
$MESS['USER_REG_T_TITLE'] = 'Реестр. Список Единиц учета';
$MESS['USER_REG_T_TABLE_ID'] = 'ID';
$MESS['USER_REG_T_TABLE_NAME'] = 'Название ЕУ';
$MESS['USER_REG_T_TABLE_ACTIVE'] = 'Заявка на хранение';
$MESS['USER_REG_T_TABLE_IBLOCK_ID'] = 'ID реестра';
$MESS['USER_REG_T_TABLE_IBLOCK_NAME'] = 'Название реестра';
$MESS['USER_REG_T_TABLE_XML_ID'] = 'xml_id';
$MESS['ORDER_VALUE_ACTIVE_Y'] = 'Активна';
$MESS['ORDER_VALUE_ACTIVE_N'] = 'Неактивна';
$MESS['USER_REG_T_FILES'] = 'Загруженных файлов: ';
$MESS['USER_REG_T_BUTTON_APPLY'] = 'Применить';
$MESS['USER_REG_T_BUTTON_RESET'] = 'Сбросить';
$MESS['USER_REG_T_BUTTON_EDIT'] = 'Изменить';
$MESS['USER_REG_T_BUTTON_SAVE'] = 'Сохранить';
$MESS['USER_REG_T_COUNT_ITEMS'] = 'Количество ЕУ на странице: ';
$MESS['USER_REG_T_EMPTY_SSCC'] = 'Не задан код SSCC';
$MESS['USER_REG_T_FOUND'] = 'Найдено: ';
$MESS['USER_REG_T_TITLE_COUNT_FILES'] = ' файла(ов)';
$MESS['USER_REG_T_ADD_NEW_ORDER'] = 'Добавить к новому заказу: ';
$MESS['USER_REG_T_ADD_EXIST_ORDER'] = 'Добавить к оформляемому заказу: ';
$MESS['USER_REG_T_SELECT_ORDER'] = 'Выбрать заказ';
$MESS['USER_REG_T_EMPTY_VALUE'] = '-';
$MESS['USER_REG_T_EMPTY_LIST'] = 'ЕУ отсутствуют';

$MESS['UNIT_F_TITLE_ID'] = 'ID ЕУ: ';
$MESS['UNIT_F_TITLE_IBLOCK_ID'] = 'ID реестра: ';
$MESS['UNIT_F_TITLE_IBLOCK_NAME'] = 'Название реестра: ';
$MESS['UNIT_F_TITLE_NAME'] = 'Наименование ЕУ: ';

$MESS['UNIT_F_PLACEHOLDER_ID'] = 'число...';
$MESS['UNIT_F_PLACEHOLDER_NAME'] = 'текст...';
$MESS['UNIT_F_PLACEHOLDER_IBLOCK_ID'] = 'число...';
$MESS['UNIT_F_PLACEHOLDER_IBLOCK_NAME'] = 'текст...';
$MESS['UNIT_F_PLACEHOLDER_S'] = 'текст...';
$MESS['UNIT_F_PLACEHOLDER_N'] = 'число...';

$MESS['USER_REG_T_OPTION_DOWNLOAD'] = 'Скачать';
$MESS['USER_REG_T_OPTION_VIEW'] = 'Просмотр';
$MESS['USER_REG_T_OPTION_EDIT'] = 'Редактировать';
$MESS['USER_REG_T_OPTION_PRINT'] = 'Печать';
$MESS['USER_REG_T_OPTION_DELETE'] = 'Удалить';

$MESS['SETTINGS_FILTER_TITLE'] = ' Настройки фильтра ';
$MESS['SETTINGS_FILTER_SAVE'] = ' Сохранить';
$MESS['SETTINGS_FILTER_DELETE'] = ' Вернуться к виду по умолчанию';
$MESS['SETTINGS_FILTER_REVERT'] = ' Вернуть';
$MESS['SETTINGS_TABLE_TITLE'] = ' Настройки списка ';

$MESS['SETTINGS_FILTER_SAVE_TITLE'] = 'Скрытие поля';
$MESS['SETTINGS_FILTER_SAVE_QUESTION'] = 'Вы действительно хотите скрыть поле? Если не будут сохранены настройки внешнего вида то после обновления страницы скрытые поля появятся снова.';
$MESS['SETTINGS_FILTER_SAVE_Y'] = 'Скрыть';
$MESS['SETTINGS_FILTER_SAVE_N'] = 'Отмена';
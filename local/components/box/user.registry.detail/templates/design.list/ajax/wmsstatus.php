<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.04.2017
 * Time: 12:42
 *
 * SOAP_RESPONSE
 * SOAP_ERROR
 */
define('STOP_STATISTICS', true);
//define('NOT_CHECK_PERMISSIONS', true);
set_time_limit(0);

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use \Bitrix\Main\Application;
use \Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;

Loc::loadLanguageFile(__FILE__);

$rq = Application::getInstance()->getContext()->getRequest();

/* Выбросим если это не AJAX  и не POST запрос */
if (!$rq->isAjaxRequest() && !$rq->isPost()) die();

global $USER;

/* Так же выбросим если это не авторизированный пользователь */
if (!$USER->IsAuthorized()) die();

$client = new EME\WMS(SOAP_TEST_MODE);

$un = $rq->getPostList()->toArray();

$res = $client->GetStatusMeasUnit($un['UI_IDS']);

/* Выбросим если нет нужных параметров */
foreach (['UI_IDS', 'IBLOCK_ID'] as $param)
    if (!$rq->getPost($param))
        die();

Loader::includeModule('iblock');

$db = CIBlockElement::GetList([],
    ['PROPERTY_PROP_SSCC' => $un['UI_IDS'], 'IBLOCK_ID' => $un['IBLOCK_ID']],
    false, false,
    ['ID', 'PROPERTY_PROP_SSCC']
);

while ($dbRes = $db->Fetch()) {
    $un['ELM'][$dbRes['ID']] = $dbRes['PROPERTY_PROP_SSCC_VALUE'];
}

$statusMap = [
    1 => 'arrived',
    2 => 'placed_storage',
    3 => 'withdrawn_audit',
    6 => 'expects_operation',
    7 => 'transportation_operation',
    8 => 'delivered',
    4 => 'withdrawn_scan',
    5 => 'withdrawn_archive',
    9 => 'withdrawn_destruction',
    'Уничтожено' => 'destructed',
    10 => 'storage_canceled'
];
$arJson = [];

if ($res->GetStatusMeasUnitResult->state > 0) {

    /* Ошибка типа UNF - "В БД WMS отсутствует Еденица учета" */
    if ($res->GetStatusMeasUnitResult->descriptionError == "UNF")
        $arJson['SOAP_ERROR'] = Loc::getMessage( "SOAP_ERROR_UNF" );

    /* Ошибка типа EST - "Превышено время ожидания ответа от WMS" */
    if ($res->GetStatusMeasUnitResult->descriptionError == "EST")
        $arJson['SOAP_ERROR'] = Loc::getMessage( "SOAP_ERROR_EST" );

    /* Ошибка типа EST - "Превышено время ожидания ответа от WMS" */
    if ($res->GetStatusMeasUnitResult->descriptionError == "DEF")
        $arJson['SOAP_ERROR'] = Loc::getMessage( "SOAP_ERROR_DEF" );

    /* Исключительная ситуация когда не приходит никакой ощшибки. Да, и такое может быть! */
    if (!$res->GetStatusMeasUnitResult->descriptionError)
        $arJson['SOAP_ERROR'] = Loc::getMessage( "SOAP_ERROR_EMPTY" );

} else {

    $_statusMu = $res->GetStatusMeasUnitResult->listStatusMu->StatusMu;

    if (is_array($_statusMu)) {

        foreach ($_statusMu as $statusMu) {

            $SOAP_AVAILABLE = $statusMu->currentStatusId > 0;

            if ($SOAP_AVAILABLE !== false) {

                /* Обновим свойство PROP_WMS_STATUS = placed_storage (Размещено на хранение (складирование))*/
                $propStatus = $statusMap[$statusMu->currentStatusId];

                CIBlockElement::SetPropertyValuesEx(array_search($statusMu->barCode, $un['ELM']), $un['IBLOCK_ID'], array('PROP_WMS_STATUS' => $propStatus));

                $arJson[$statusMu->barCode]['SOAP_RESPONSE'] = $statusMu->currentStatus;

            } else {

                /* Для всех ситуаций когда вроде бы ошибки нет но и статуса тоже нет */
                $arJson[$statusMu->barCode]['SOAP_ERROR'] = Loc::getMessage( "SOAP_ERROR_NOT_SET" );
            }
        }
    } else if (is_object($_statusMu)) {
        $SOAP_AVAILABLE = $_statusMu->currentStatusId > 0;

        if ($SOAP_AVAILABLE !== false) {

            /* Обновим свойство PROP_WMS_STATUS = placed_storage (Размещено на хранение (складирование))*/
            $propStatus = $statusMap[$_statusMu->currentStatusId];

            CIBlockElement::SetPropertyValuesEx(array_search($_statusMu->barCode, $un['ELM']), $un['IBLOCK_ID'], array('PROP_WMS_STATUS' => $propStatus));

            $arJson[$_statusMu->barCode]['SOAP_RESPONSE'] = $_statusMu->currentStatus;

        } else {

            /* Для всех ситуаций когда вроде бы ошибки нет но и статуса тоже нет */
            $arJson[$_statusMu->barCode]['SOAP_ERROR'] = Loc::getMessage( "SOAP_ERROR_NOT_SET" );
        }
    }
}

die(json_encode($arJson));


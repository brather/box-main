<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Page\Asset,
    \Bitrix\Main\Application;

class FileViewerComponent extends CBitrixComponent {
    //region Задание свойств класса
    private $page = 'template';
    private $arPost = array();
    private $arGet = array();
    private $dataHTML = '';
    //endregion
    //region Подготовка входных параметров компонента
    /** Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        return parent::onPrepareComponentParams($arParams);
    }
    //endregion
    //region Подключение файлов перевода
    /**
     * Подключение файлов перевода
     */
    public function onIncludeComponentLang() {
        Loc::loadMessages(__FILE__);
    }
    //endregion
    //region Выполнение компонента
    /**
     * Выполнение компонента
     */
    public function executeComponent() {
        $this->getRequest();                                    // Получение данных request
        if($this->arParams['USE_HTML_COMPONENT'] == 'Y') {
            $this->addHelpers();                                // Подключение файлов стилей и скриптов
            $this->dataHTML .= $this->getHtmlViewer();
            return $this->dataHTML;
        } else {
            $this->includeComponentTemplate($this->page);       // Подключение шаблона
        }
    }
    //endregion
    //region Получение данных request
    /**
     * Получение данных request
     */
    private function getRequest() {
        $request = Application::getInstance()->getContext()->getRequest();
        $this->arPost = $request->getPostList()->toArray();
        $this->arGet = $request->getQueryList()->toArray();
    }
    //endregion
    //region Подключение файлов стилей и скриптов плагина
    /**
     * Подключение файлов стилей и скриптов плагина
     */
    private function addHelpers() {
        $componentPath = $this->getPath();
        $this->checkAddFiles($componentPath . '/help/css/style.css', 'style');
        if($this->arParams['USE_ADD_SCRIPTS']) {
            $this->checkAddFiles($componentPath . '/help/js/script.js', 'script');
        }
    }
    //endregion
    //region Проверка файлов на существование и их подключение
    /** Проверка файлов на существование и их подключение
     * @param $file - путь к файлу
     * @param $type - тип файла
     */
    private function checkAddFiles($file, $type) {
        if(file_exists(Application::getDocumentRoot() . $file)) {
            if($type == 'style') {
                $this->dataHTML .= '<link rel="stylesheet" href="' . $file . '" />';
            } elseif($type == 'script') {
                $this->dataHTML .= '<script type="text/javascript" src="' . $file . '"></script>';
            }
        }
    }
    //endregion
    //region Возвращает html просмотровщика
    /** Возвращает html просмотровщика
     * @return string
     */
    private function getHtmlViewer() {
        $documentRoot = Application::getInstance()->getContext()->getServer()->getDocumentRoot();
        $subFolder = $this->arParams['FILE_FOLDER'];
        $folderFile = $documentRoot . $subFolder;

        $returnHtml = '
            <div class="fl-list">
                <div class="fl-viewer">
                    <div class="fl-manager">';
        if(count($this->arParams['FILE_LIST']) > 1) {
            $returnHtml .= '
                        <div class="fl-m-numfile">
                            <div class="pager">
                                <a href="#" class="pager__arrow js-pa-file _prev js-nav-disabled">' . Loc::getMessage('B_FILE_VIEWER_MANAGE_PREV') . '</a>                                                    
                                <a href="#" class="pager__arrow js-pa-file _next">' . Loc::getMessage('B_FILE_VIEWER_MANAGE_NEXT') . '</a>
                                <div class="pager__current">
                                    <span>' . Loc::getMessage('B_FILE_VIEWER_MANAGE_FILE') . '</span>
                                    <input type="text" class="pager__files" name="FILE_1" value="1">
                                    <span>' . Loc::getMessage('B_FILE_VIEWER_MANAGE_FROM') . count($this->arParams['FILE_LIST']) .'</span>
                                </div>
                            </div>
                        </div>';
        }
        $arFileFirst = pathinfo($this->arParams['FILE_LIST'][0]);
        $clsHidden = ' pager__hidden';
        $countPagesFD = 0;                          // количество страниц в первом документе (если он pdf)
        if($arFileFirst['extension'] == 'pdf') {
            $countPagesFD = $this->getCountPagesPdf($folderFile . $this->arParams['FILE_LIST'][0]);
            if($countPagesFD > 1) {
                $clsHidden = '';
            }
        }
        $returnHtml .= '
                        <div class="fl-m-numpage' . $clsHidden . '" 
                             data-lang-pref="' . Loc::getMessage('B_FILE_VIEWER_MANAGE_FROM') . '" 
                             data-pages="' . $countPagesFD . '" 
                             data-ajax="' . $this->getPath() . '/help/ajax/get.page.php" 
                             data-src="' . $subFolder . $this->arParams['FILE_LIST'][0] . '">
                            <div class="pager">
                                <a href="#" class="pager__arrow js-pa-page _prev js-nav-disabled">' . Loc::getMessage('B_FILE_VIEWER_MANAGE_PREV') . '</a>                                                    
                                <a href="#" class="pager__arrow js-pa-page _next">' . Loc::getMessage('B_FILE_VIEWER_MANAGE_NEXT') . '</a>
                                <div class="pager__current">
                                    <span>' . Loc::getMessage('B_FILE_VIEWER_MANAGE_PAGE') . '</span>
                                    <input type="text" class="pager__pages" name="PAGE_1" value="1">
                                    <span class="js-pages">' . Loc::getMessage('B_FILE_VIEWER_MANAGE_FROM') . $countPagesFD .'</span>
                                </div>
                            </div>
                        </div>';
        $returnHtml .= '
                        <div class="fl-m-zoom" data-zoom="100">
                            <div class="pager">
                                <a href="#f-zoom-minus" class="pager__arrow _minus js-nav-disabled js-minus">' . Loc::getMessage('B_FILE_VIEWER_MANAGE_MINUS') . '</a>                                                    
                                <a href="#f-zoom-plus" class="pager__arrow _plus js-plus">' . Loc::getMessage('B_FILE_VIEWER_MANAGE_PLUS') . '</a>
                                <div class="pager__current pager__number">
                                    <!--<input type="text" name="ZOOM_0" value="100">-->
                                    <input type="number" min="80" max="200" value="100" step="10" />
                                    <div class="pn-nav">
                                        <div class="pn-nav-button pn-nav-up js-plus"></div>
                                        <div class="pn-nav-button pn-nav-down js-minus"></div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                        <div class="fl-m-download">
                            <a class="js-fl-m-download" href="#">
                                <i class="fa fa-upload"></i>
                            </a>
                            <ul class="fl-add-list">                                
                                <li>
                                    <a class="fl-md-current" 
                                       target="_blank" 
                                       download="" 
                                       href="' . $subFolder . $this->arParams['FILE_LIST'][0] . '">
                                        ' . Loc::getMessage('B_FILE_VIEWER_MANAGE_DOWNLOAD_CURRENT') . '
                                    </a>
                                </li>
                                <li>
                                    <a class="fl-md-all" 
                                       href="#" 
                                       data-folder="' . urlencode($subFolder) . '"
                                       data-ajax="' . $this->getPath() . '/help/ajax/get.archive.php">
                                        ' . Loc::getMessage('B_FILE_VIEWER_MANAGE_DOWNLOAD_ALL') . '
                                    </a>
                                </li>
                            </ul>   
                        </div>
                        <div class="fl-m-print">
                            <a class="js-fl-m-print" href="#"><i class="fa fa-print"></i></a>
                            <ul class="fl-add-list">
                                <li>
                                    <a class="fl-mp-current" 
                                       href="#">
                                        ' . Loc::getMessage('B_FILE_VIEWER_MANAGE_PRINT_CURRENT') . '
                                    </a>
                                </li>
                                <li>
                                    <a class="fl-mp-all" 
                                       href="#">
                                        ' . Loc::getMessage('B_FILE_VIEWER_MANAGE_PRINT_ALL') . '    
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="fl-container">
                        <div class="scrollbar-outer">';
        global $USER;
        $userID = $USER->GetID();
        if(!empty($this->arParams['FILE_IMG_EXTENSION'])) {
            $extension = $this->arParams['FILE_IMG_EXTENSION'];
        } else {
            $extension = 'jpg';
        }
        foreach ($this->arParams['FILE_LIST'] as $iFile => $file) {
            $returnHtml .= '<div class="file-' . ($iFile + 1) . ' fl-c-image';
            if($iFile == 0) {
                $returnHtml .= ' fl-c-active';
            }
            $returnHtml .= '"><img src="';
            $arFile = pathinfo($file);
            if($arFile['extension'] == 'pdf') {
                $pathImages = $folderFile . 'thumbnail_pdf/';
                if(!file_exists($pathImages)) {
                    mkdir($pathImages, 0777);
                }
                $pathImages = $pathImages . 'user-' . $userID . '/';
                if(!file_exists($pathImages)) {
                    mkdir($pathImages, 0777);
                }
                $imgName = $arFile['filename'] . '.' . $extension;          // одна картинка для любой страницы (картинка перезаписывается для каждой страницы)
                createImageOutPdf($folderFile . $file, 0, $pathImages, $arFile['filename'], $extension);
                $countPages = $this->getCountPagesPdf($folderFile . $file);
                $returnHtml .= $subFolder . 'thumbnail_pdf/user-' . $userID . '/' . $imgName . '?' . rand() . '"';
                $returnHtml .= ' data-pdf="Y"';                             // параметр отвечающий за то, что изображение получено из файла pdf
                $returnHtml .= ' data-current="1"';                         // текущая страница pdf файла
                $returnHtml .= ' data-pages="' . $countPages . '"';         // количество страницы
                $returnHtml .= ' data-src="' . $subFolder . $file . '"';    // путь к файлу
                $returnHtml .= ' data-ext="' . $extension . '"';            // расширение для формируемого изображения
                $returnHtml .= '/></div>';
            } else {
                $returnHtml .= $subFolder . $file . '" data-pdf="N" /></div>';
            }
        }
        $returnHtml .= '</div>
                    </div>
                </div>
            </div>';
        return $returnHtml;
    }
    //endregion
    //region Возвращает количество страниц в pdf
    /** Возвращает количество страниц в pdf
     * @param $filename
     * @return int|string
     */
    private function getCountPagesPdf($filename) {
        $fp = fopen($filename, 'r');
        if ($fp) {
            $count = 0;
            while(!feof($fp)) {
                $line = fgets($fp, 255);
                if (preg_match('|/Count [0-9]+|', $line, $matches)) {
                    preg_match('|[0-9]+|', $matches[0], $matches2);
                    if ($count < $matches2[0]) {
                        $count = trim($matches2[0]);
                    }
                }
            }
            fclose($fp);
            return $count;
        }
    }
    //endregion
}
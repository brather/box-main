<?php
$MESS['B_FILE_VIEWER_EXTENSION'] = 'Расширение: ';
$MESS['B_FILE_VIEWER_MANAGE_PREV'] = 'Назад';
$MESS['B_FILE_VIEWER_MANAGE_NEXT'] = 'Вперед';
$MESS['B_FILE_VIEWER_MANAGE_FILE'] = ' Файл: ';
$MESS['B_FILE_VIEWER_MANAGE_FROM'] = ' из ';
$MESS['B_FILE_VIEWER_MANAGE_PAGE'] = ' Страница: ';
$MESS['B_FILE_VIEWER_MANAGE_PLUS'] = 'Увеличить';
$MESS['B_FILE_VIEWER_MANAGE_MINUS'] = 'Уменьшить';
$MESS['B_FILE_VIEWER_MANAGE_DOWNLOAD_ALL'] = 'Скачать все документы реестра';
$MESS['B_FILE_VIEWER_MANAGE_DOWNLOAD_CURRENT'] = 'Скачать данный документ';
$MESS['B_FILE_VIEWER_MANAGE_PRINT_ALL'] = 'Распечатать весь документ';
$MESS['B_FILE_VIEWER_MANAGE_PRINT_CURRENT'] = 'Распечатать страницу';
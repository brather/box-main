$(document).ready(function(){
    //region Задание классов
    var ids = {
            container   : '#horizontal-right'               // id блока контейнера
        },
        cviwer = {                                          // объект содержащий общий блок
            vcontain    : '.fl-viewer',                     // класс общего блока
            mcontain    : '.fl-manager',                    // класс блока с кнопками управления
            icontainer  : '.fl-container'                   // класс блока со слайдами скринов документов
        },
        cmanage = {                                         // объект блока управления
            pcontain    : '.pager',                         // класс контейнера навигации
            pcurent     : '.pager__current',                // класс контейнера содержащего в себе инпут со значением для навигации
            btnfile     : '.js-pa-file',                    // класс кнопки для переключения между файлами
            btnarrow    : '.pager__arrow',                  // класс для навигации по страницам и файлам
            btnpage     : '.js-pa-page',                    // класс кнопки для переключения между страницами файла
            btnnext     : '_next',                          // класс кнопки переключающая на следующий элемент
            btnprev     : '_prev',                          // класс кнопки переключающая на предыдущий элемент
            phidden     : 'pager__hidden',                  // класс скрытия блока навигации
            btndisabled : 'js-nav-disabled',                // класс отключенной кнопки
            stpages     : '.js-pages',                      // класс в который заключена строка с количеством страниц
            pfile       : '.fl-m-numfile',                  // класс блока управления файлами
            ifiles      : '.pager__files',                  // класс для input управления файлами
            ipages      : '.pager__pages',                  // класс для input управления страницами
            ppage       : '.fl-m-numpage',                  // класс блока управления страницами
            zoom        : '.fl-m-zoom',                     // класс блока управления размером изображения
            zplus        : 'js-plus',                       // кнопка увеличения изображения
            zminus       : 'js-minus',                      // кнопка уменьшения изображения
            download    : '.fl-m-download',                 // блок списка ссылок на скачивание
            addlist     : '.fl-add-list',                   // список дополнительных опций
            hcdownload  : '.fl-md-current',                 // ссылка на скачивание конкретного документа
            alldownload : '.fl-md-all',                     // ссылка на скачивание всех файлов из директории
            tdownload   : '.js-fl-m-download',              // кнопка показа и скрытия списка со ссылками на скачивание файлов
            print       : '.fl-m-print',                    // блок для со ссылкой на скачивание
            hprint      : '.js-fl-m-print',                 // ссылка на скачивание
            hcprint     : '.fl-mp-current',                 // ссылка на печать одной страницы
            allprint    : '.fl-mp-all',                     // ссылка на печать всего документа
            screen      : '.fl-m-fullscreen'
        },
        ccontainer = {                                      // объект блока с изображениями
            ccontain    : '.fl-container',                  // класс контейнера со всеми изображениями
            blkimage    : '.fl-c-image',                    // класс отдельного блока с изображениями файлов
            active      : 'fl-c-active',                    // класс активности блока
            preffix     : '.file-'                          // преффикс для номера блока у класса

        },
        scroll = {
            scouter     : '.scrollbar-outer',
            scwrapper   : '.scroll-wrapper',
            scelement   : '.scroll-element'
        },
        listfiles = {                                       // обект содержащий в себе список файлов
            active      : 'js-file-active',
            urlfile     : '.js-file'                        // класс для ссылки на файл
        },
        flag = 0;
    //endregion

    //region Скрытие списков опций на скачивание и печать если они открыты
    $(document).on('click', function(e) {
        if(flag == 1) {
            if(!$(e.target).closest(cmanage.tdownload).length && !$(e.target).closest(cmanage.hprint).length) {
                closeAddList();
            }
        }
    });
    //endregion

    //region Переключение файлов
    //region Переключение файла с помощью клика по стрелкам
    $(document).on('click', cmanage.btnfile, function(e){
        e.preventDefault();
        var $this = $(this),
            blkPager = $this.closest(cmanage.pcontain),                             // блок управления файлами
            bManage = blkPager.closest(cviwer.mcontain),                            // блок управления в котором находятся все остальные управляющие контейнеры
            iFile = blkPager.find(cmanage.pcurent).find('input'),                   // input в котором записан номер текущего файла
            containFiles = $this.closest(cviwer.vcontain).find(cviwer.icontainer),  // блок с файлами
            countFiles = containFiles.find(ccontainer.blkimage).size(),             // количество файлов
            curFile = iFile.val() * 1,                                              // номер текущего активного файла
            nextFile = 0;                                                           // номер блока с файлом для показа

        if(!$this.hasClass(cmanage.btndisabled)) {
            startLoader(containFiles);
            //region Определяем номер страницы на которую осуществляется переключение
            if($this.hasClass(cmanage.btnnext)) {
                nextFile = curFile + 1;
            }
            else if($this.hasClass(cmanage.btnprev)) {
                nextFile = curFile - 1;
            }
            //endregion
            //region Обновляем классы у кнопок переключения файлов (активируем / деактивируем)
            checkArrowActive(blkPager, nextFile, countFiles);
            //endregion
            //region Обновляем значение в input
            iFile.val(nextFile);
            //endregion
            //region Скрываем блок управления страницами если не pdf
            var bHide = containFiles.find(ccontainer.preffix + curFile),        // блок для скрытия
                bShow = containFiles.find(ccontainer.preffix + nextFile);       // блок для показа
            //region Активируем необходимый блок
            checkNextFile(bShow, bHide);
            //endregion
            //endregion
            stopLoader();
        }
    });
    //endregion
    //region Переключение файла через input
    $(document).on('change', cmanage.ifiles, function(){
        var t = $(this),
            curVal = t.val() * 1,
            blkPager = t.closest(cmanage.pcontain),
            containFiles = t.closest(cviwer.vcontain).find(cviwer.icontainer),
            bShow = containFiles.find(ccontainer.preffix + curVal),
            actFileBlock = containFiles.find('.' + ccontainer.active),
            countFiles = containFiles.find(ccontainer.blkimage).size();
        if(curVal != 0 && curVal <= countFiles) {
            startLoader(containFiles);
            //region Отключение и включение кнопок перелистывания файлов
            checkArrowActive(blkPager, curVal, countFiles);
            //endregion
            //region Активируем необходимый блок
            checkNextFile(bShow, actFileBlock);
            //endregion
            var activeFile = $(ids.container).find(listfiles.urlfile + '.' + listfiles.active),
                nActive = activeFile.data('f-number') * 1;
            if(curVal != nActive) {
                activeFile.toggleClass(listfiles.active);
                $(ids.container).find(listfiles.urlfile + '[data-f-number="' + curVal + '"]').toggleClass(listfiles.active);
            }
            stopLoader();
        }
    });
    //endregion
    //endregion
    //region Перелистывание страниц в pdf
    //region Перелистывание с помощью кнопок
    $(document).on('click', cmanage.btnpage, function(e) {
        e.preventDefault();
        var $this = $(this),
            bPage = $this.closest(cmanage.ppage),                                   // блок управления страницами
            pagerContain = bPage.find(cmanage.pcontain),                            // блок с элементами навигации
            iPage = pagerContain.find('input'),                                     // input со значением текущей страницы
            curPage = iPage.val() * 1,                                              // номер текущей страницы
            nextPage = 0,                                                           // номер страницы для показа
            containFiles = $this.closest(cviwer.vcontain).find(cviwer.icontainer);  // блок с файлами

        if(!$this.hasClass(cmanage.btndisabled)) {
            if($this.hasClass(cmanage.btnnext)) {
                nextPage = curPage + 1;
            } else if ($this.hasClass(cmanage.btnprev)) {
                nextPage = curPage - 1;
            }
            getImageFromFile(containFiles, nextPage);
        }
    });
    //endregion
    //region Перелистывание с помощью изменения значений в input
    $(document).on('change', cmanage.ipages, function(){
        var t = $(this),
            nPage = t.val() * 1,                                                // номер требуемой страницы
            containFiles = t.closest(cviwer.vcontain).find(cviwer.icontainer);  // блок с файлами
        getImageFromFile(containFiles, nPage);
    });
    //endregion
    //endregion
    //region Скачивание файлов
    //region Кнопка показа / скрытия списка ссылок на скачивание
    $(document).on('click', cmanage.tdownload, function(e) {
        e.preventDefault();
        var $this = $(this),
            listOption = $this.closest(cmanage.download).find(cmanage.addlist);
        if(listOption.hasClass(ccontainer.active)) {
            listOption.removeClass(ccontainer.active);
            flag = 0;
        } else {
            closeAddList();
            listOption.addClass(ccontainer.active);
            flag = 1;
        }
    });
    //endregion
    //region Скачивание всех файлов
    $(document).on('click', cmanage.alldownload, function(e){
        e.preventDefault();
        var $this = $(this),
            url = $this.data('ajax');
        closeAddList();
        window.open(url + '?FOLDER=' + $this.data('folder'), '_blank');
    });
    //endregion
    //region Скачивание одного файла
    $(document).on('click', cmanage.hcdownload, function(e){
        closeAddList();
    });
    //endregion
    //endregion

    //region Печать файлов
    //region Кнопка показа / скрытия списка ссылок на печать документа (ов)
    $(document).on('click', cmanage.hprint, function(e){
        var $this = $(this),
            blkManager = $this.closest(cviwer.mcontain),
            srcPrint = '',
            blkPager = blkManager.find(cmanage.ppage),
            listOption = $this.closest(cmanage.print).find(cmanage.addlist);
        //region Одна страницe у активного pdf либо активен сейчас скрин в виде png
        if(blkPager.hasClass(cmanage.phidden)) {
            srcPrint = $this.closest(cviwer.vcontain).find(cviwer.icontainer).find('.' + ccontainer.active).find('img').attr('src');
            win = window.open(srcPrint, '_print');
            win.print();
        }
        //endregion
        //region Активен pdf у которого больше одной страницы
        else {
            //region Показываем или скрываем список опций для печати
            if(listOption.hasClass(ccontainer.active)) {
                listOption.removeClass(ccontainer.active);
                flag = 0;
            } else {
                closeAddList();
                listOption.addClass(ccontainer.active);
                flag = 1;
            }
            //endregion
        }
        //endregion
    });
    //endregion
    //region Печать одной страницы
    $(document).on('click', cmanage.hcprint, function(e){
        e.preventDefault();
        var $this = $(this),
            srcPrint = $this.closest(cviwer.vcontain).find(cviwer.icontainer).find('.' + ccontainer.active).find('img').attr('src'),
            win = window.open(srcPrint, '_print');
        win.print();
        closeAddList();
    });
    //endregion
    //region Печать всего документа
    $(document).on('click', cmanage.allprint, function(e){
        e.preventDefault();
        var $this = $(this),
            srcPrint = $this.closest(cviwer.vcontain).find(cviwer.icontainer).find('.' + ccontainer.active).find('img').data('src'),
            win = window.open(srcPrint, '_print');
        win.print();
        closeAddList();
    });
    //endregion
    //endregion
    //region Изменение размера изображения
    //region Клик по указателю. Увеличение изображения
    $(document).on('click', '.' + cmanage.zplus, function(e){
        e.preventDefault();
        clickZoom(this, 'plus');
    });
    //endregion
    //region Клик по указателю. Уменьшение изображения
    $(document).on('click', '.' + cmanage.zminus, function(e){
        e.preventDefault();
        clickZoom(this, 'minus');
    });
    //endregion
    //region Изменение значения размера изображения через input
    $(cmanage.zoom).find('input').on('change', function(e){
        var $this = $(this),
            maxZoom = $this.attr('max') * 1,
            minZoom = $this.attr('min') * 1,
            nZoom = $this.val() * 1,
            vSend = nZoom;
        if(nZoom < minZoom) {
            vSend = minZoom;
        } else if(nZoom > maxZoom) {
            vSend = maxZoom;
        }
        setZoom(this, vSend);
    });
    //endregion
    //endregion

    //region Функция расчета значения zoom при клике по стрелкам изменения размера
    /**
     * Функция расчета значения zoom при клике по стрелкам изменения размера
     * @param obj - стрелка по которой произведен клик
     */
    function clickZoom(obj) {
        var $this = $(obj),
            imgContainer = $this.closest(cviwer.vcontain).find(cviwer.icontainer),
            imgContActive = imgContainer.find('.' + ccontainer.active),
            img = imgContActive.find('img'),
            zContain = $this.closest(cmanage.zoom),
            iZoom = zContain.find('input'),
            step = iZoom.attr('step') * 1,
            curZoom = zContain.data('zoom') * 1,
            maxZoom = iZoom.attr('max') * 1,
            minZoom = iZoom.attr('min') *1,
            nZoom = 0;
        if($this.hasClass(cmanage.zplus)) {
            if(curZoom + step <= maxZoom) {
                nZoom = curZoom + step;
            } else {
                nZoom = maxZoom;
            }
        } else {
            if(curZoom - step <= minZoom) {
                nZoom = minZoom;
            } else {
                nZoom = curZoom - step;
            }
        }
        setZoom(obj, nZoom);
    }
    //endregion
    //region Функция установки значения zoom
    /**
     * Функция установки значения zoom
     * @param obj - объект на котором произведено событие вызвавшее функцию
     * @param nzoom - значение zoom на которое необходимо поменять размер
     */
    function setZoom(obj, nzoom) {
        var $this = $(obj),
            imgContainer = $this.closest(cviwer.vcontain).find(cviwer.icontainer),
            imgContActive = imgContainer.find('.' + ccontainer.active),
            img = imgContActive.find('img'),
            zContain = $this.closest(cmanage.zoom),
            iZoom = zContain.find('input'),
            vZoom = iZoom.val() * 1,
            maxZoom = iZoom.attr('max') * 1,
            minZoom = iZoom.attr('min') * 1;
        if(vZoom != nzoom) {
            iZoom.val(nzoom);
        }
        img.css({ width: nzoom + '%' });
        zContain.data('zoom', nzoom);
        zContain.find(cmanage.btnarrow).removeClass(cmanage.btndisabled);
        if(nzoom == minZoom) {
            if(!zContain.find(cmanage.btnarrow + '.' + cmanage.zminus).hasClass(cmanage.btndisabled)) {
                zContain.find(cmanage.btnarrow + '.' + cmanage.zminus).addClass(cmanage.btndisabled);
            }
        } else if (nzoom == maxZoom) {
            if(!zContain.find(cmanage.btnarrow + '.' + cmanage.zplus).hasClass(cmanage.btndisabled)) {
                zContain.find(cmanage.btnarrow + '.' + cmanage.zplus).addClass(cmanage.btndisabled);
            }
        }
        if(nzoom > 100) {
            if(imgContainer.find(scroll.scouter).find(scroll.scelement).length > 0) {
                if(!imgContainer.find(scroll.scouter).hasClass(scroll.scwrapper)) {
                    imgContainer.find(scroll.scouter).addClass(scroll.scwrapper)
                }
            } else {
                imgContainer.find(scroll.scouter).scrollbar();
            }
            var hParentScrollOuter = imgContainer.closest(scroll.scouter).height(),
                hManage = imgContainer.closest(cviwer.vcontain).find(cviwer.mcontain).outerHeight(true),
                nHeight = hParentScrollOuter - hManage;
            imgContainer.css({ 'height' : nHeight + 'px' });
        } else {
            if(imgContainer.find(scroll.scouter).hasClass(scroll.scwrapper)) {
                imgContainer.find(scroll.scouter).removeClass(scroll.scwrapper);
            }
            imgContainer.css({ 'height' : '' });
        }
    }
    //endregion
    //region Функция скрытия блока с дополнительными опциями
    /**
     * Функция скрытия блока с дополнительными опциями
     */
    function closeAddList() {
        $(cmanage.addlist).removeClass(ccontainer.active);
        flag = 0;
    }
    //endregion
    //region Функция для активации и деактивации блоков с изображениями
    /**
     * Функция для активации и деактивации блоков с изображениями
     * @param bShow - блок, который необходимо показать
     * @param bHide - блок, который необходимо скрыть
     */
    function checkNextFile(bShow, bHide) {
        bHide.removeClass(ccontainer.active);
        bShow.addClass(ccontainer.active);
        var bManager = bShow.closest(cviwer.vcontain).find(cviwer.mcontain),    // блок управления страницами и файлами
            uriDownload = '',                                                   // ссылка на скачивание файла
            blkPages = bManager.find(cmanage.ppage),                            // блок управления страницами
            imgShow = bShow.find('img'),                                        // изображение подготовленное к выводу
            iZoom = bManager.find(cmanage.zoom).find('input'),                  //
            vZoom = iZoom.val(),                                                // текущее значение zoom
            wImage = imgShow.width(),                                           // текущая ширина изображения
            wParentImg = imgShow.closest(ccontainer.blkimage).width(),          // ширина блока родителя
            langPdf = blkPages.data('langPref'),                                // строковое значение текста для указания количества страниц
            isPdf = imgShow.data('pdf');                                        // параметр определяющий изображение получено из pdf файла или нет

        if(vZoom == 100 && wImage != wParentImg || vZoom != 100 && wImage == wParentImg) {
            setZoom(iZoom, vZoom);
        }

        if(isPdf == 'Y') {
            //region Определение переменных
            var nPage = imgShow.data('current'),                // активная страница у данного pdf файла
                inputPage = blkPages.find('input'),             // input со значением страницы
                cPage = inputPage.val() * 1,                    // номер страницы указанный в блоке управления страницами
                srcPdf = imgShow.data('src'),                   // путь к файлу
                countPages = imgShow.data('pages') * 1;         // количество страниц в файле pdf
            //endregion
            //region Если номер активной страницы указанный у активного изображения не равен текущему значению указанному в блоке управления страницами
            if(nPage != cPage) {
                inputPage.val(nPage);
            }
            //endregion
            //region Обновляем классы у кнопок переключения страниц (активируем / деактивируем)
            checkArrowActive(blkPages, nPage, countPages);
            //endregion
            //region Обновление значений параметров в блоке управления страницами
            blkPages.data('src', srcPdf);
            blkPages.data('pages', countPages).find(cmanage.stpages).html(langPdf + '' + countPages);
            //endregion
            //region Если количество страниц больше 1 и блок управления был скрыт то показываем его
            if(countPages > 1 && blkPages.hasClass(cmanage.phidden)) {
                blkPages.removeClass(cmanage.phidden);
            }
            //endregion
            //region Формируем ссылку на скачивание файла
            uriDownload = imgShow.data('src');
            //endregion
        }
        else {
            //region Если блок с навигацией по страницам показан то скрыть
            if(!blkPages.hasClass(cmanage.phidden)) {
                blkPages.addClass(cmanage.phidden);
            }
            //endregion
            //region Формируем ссылку на скачивание файла
            uriDownload = imgShow.attr('src');
            //endregion
        }
        //region Обновляем значение на скачивание файла
        bManager.find(cmanage.download).find(cmanage.hcdownload).attr('href', uriDownload);
        //endregion
    }
    //endregion
    //region Функция для обновления изображения у файла pdf (возвращает изображение заданной страницы pdf)
    /**
     * Функция для обновления изображения у файла pdf (возвращает изображение заданной страницы pdf)
     * @param containerFiles - блок включающий в себя контейнеры с изображениями
     * @param numpage - номер страницы необходимой для вывода
     */
    function getImageFromFile(containerFiles, numpage) {
        startLoader(containerFiles);
        var blkManage = containerFiles.closest(cviwer.vcontain).find(cviwer.mcontain).find(cmanage.ppage),  // блок с элементами управления файлами
            url = blkManage.data('ajax'),                                                                   // путь к файлу для запроса с помощью ajax
            imageNew = containerFiles.find('.' + ccontainer.active).find('img'),                            // изображение для вывода пользователю
            countPages = imageNew.data('pages') * 1,                                                        // количество страниц в файле
            countPagesBManage = blkManage.data('pages') * 1,                                                // количетсво страниц указанное в блоке управления
            htmlPages = blkManage.data('lang-pref') + countPages,                                           // текст содержащий количество страниц (пр. " из 21")
            data = {                                                                                        // данные для отправки
                FILE_PATH   : imageNew.data('src'),
                IMG_EXT     : imageNew.data('ext'),
                PAGE        : numpage
            };
        if(numpage > 0 && numpage <= countPages) {
            //region При несовпадении количества страниц требуемого файла с количеством страниц в блоке управления страницами
            if(countPages != countPagesBManage) {
                blkManage.data('pages', countPages);
                blkManage.find(cmanage.stpages).html(htmlPages);
            }
            //endregion
            //region Обновляем классы у кнопок переключения страниц (активируем / деактивируем)
            checkArrowActive(blkManage, numpage, countPages);
            //endregion
            //region Отправка ajax запроса на получение нового изображения
            $.ajax({
                method  : 'post',
                url     : url,
                data    : data,
                success : function(data) {
                    var iCurentPage = blkManage.find('input');
                    if(iCurentPage.val() != numpage) {       // если в input текущей страницы указана не та на которую необходимо переключится то обновим значение
                        iCurentPage.val(numpage);
                    }
                    imageNew.attr('src', data);              // обновим путь к изображению в активном блоке с изображением
                    imageNew.data('current', numpage);       // обновим параметр текущей страницы
                    stopLoader();
                }
            });
            //endregion
        } else {
            stopLoader();
        }
    }
    //endregion
    //region Функция проверки активности кнопок переключения (страниц / файлов)
    /**
     * Функция проверки активности кнопок переключения (страниц / файлов)
     * @param bmanage - блок управления кнопками переключения
     * @param npage - номер страницы / файла на которую необходимо переключаться
     * @param cpages - максимальное значение страниц / файлов
     */
    function checkArrowActive(bmanage, npage, cpages) {
        bmanage.find(cmanage.btnarrow).removeClass(cmanage.btndisabled);
        if(npage == 1) {
            if(!bmanage.find('.' + cmanage.btnprev).hasClass(cmanage.btndisabled)) {
                bmanage.find('.' + cmanage.btnprev).addClass(cmanage.btndisabled);
            }
        }
        else if (npage == cpages) {
            if(!bmanage.find('.' + cmanage.btnnext).hasClass(cmanage.btndisabled)) {
                bmanage.find('.' + cmanage.btnnext).addClass(cmanage.btndisabled);
            }
        }
    }
    //endregion

});
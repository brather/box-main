<?php
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use \Bitrix\Main\Application;

$context = Application::getInstance()->getContext();
$docRoot = $context->getServer()->getDocumentRoot();
$arPost = $context->getRequest()->getPostList()->toArray();

global $USER;

if(!empty($arPost['FILE_PATH'])) {
    $page = $arPost['PAGE'] - 1;
    $arFile = pathinfo($arPost['FILE_PATH']);
    $nExtension = 'png';
    $newName = $arFile['filename'] . '.' . $nExtension;
    $returnSrc = '';
    $file = $docRoot . $arPost['FILE_PATH'];
    $dirToSave = $docRoot . $arFile['dirname'] . '/thumbnail_pdf/user-' . $USER->GetID() . '/';

    createImageOutPdf($file, $page, $dirToSave, $arFile['filename'], $nExtension);

    $returnSrc = $arFile['dirname'] . '/thumbnail_pdf/user-' . $USER->GetID() . '/' . $newName . '?' . rand();
    echo $returnSrc;
}
<?php
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use \Bitrix\Main\Application;

$context = Application::getInstance()->getContext();
$docRoot = $context->getServer()->getDocumentRoot();
$arGet = $context->getRequest()->getQueryList()->toArray();

global $USER;

if(!empty($arGet['FOLDER'])) {
    $folder = $docRoot . urldecode($arGet['FOLDER']);
    if(file_exists($folder)) {
        mkdir('files/', 0777);
        $newName = time() . '.zip';
        $zip = new ZipArchive();
        $zip->open($newName, ZIPARCHIVE::CREATE);
        $arFilesAll = scandir($folder);
        foreach($arFilesAll as $iFile => $file) {
            if($file != '..' & $file != '.' && !stristr($file, 'thumbnail')) {
                copy($folder . $file, 'files/' . $file);
                $zip->addFile('files/' . $file);
            }
        }
        $zip->close();
        header('Content-type: application/zip');
        header('Content-Disposition: attachment; filename="' . $newName . '"');
        readfile($newName);
        unlink($newName);
        $arFiles = scandir('files/');
        foreach($arFiles as $file) {
            unlink('files/' . $file);
        }
    }
}
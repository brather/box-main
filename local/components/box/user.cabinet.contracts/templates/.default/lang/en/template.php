<?php
$MESS['C_USER_CABINET_CONTRACTS_TITLE'] = 'List of contracts';

$MESS['C_USER_CABINET_CONTRACTS_TABLE_NUMBER'] = '№';
$MESS['C_USER_CABINET_CONTRACTS_TABLE_CONTRACT_NAME'] = 'Name of contract';
$MESS['C_USER_CABINET_CONTRACTS_TABLE_FILE_NAME'] = 'File';
$MESS['C_USER_CABINET_CONTRACTS_TABLE_FILE_SIZE'] = 'Size';
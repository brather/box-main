<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc;?>
<?//p($arResult)?>
<h2><?= Loc::getMessage('C_USER_CABINET_CONTRACTS_TITLE') ?></h2>
<table class="table">
    <thead>
        <th><?= Loc::getMessage('C_USER_CABINET_CONTRACTS_TABLE_NUMBER') ?></th>
        <th>
            <a href="<?= $arResult['SORT_URI'] ?>" class="filtr">
                <?= Loc::getMessage('C_USER_CABINET_CONTRACTS_TABLE_CONTRACT_NAME') ?>
            </a>
        </th>
        <th><?= Loc::getMessage('C_USER_CABINET_CONTRACTS_TABLE_FILE_NAME') ?></th>
        <th><?= Loc::getMessage('C_USER_CABINET_CONTRACTS_TABLE_FILE_SIZE') ?></th>
    </thead>
    <tbody>
    <?foreach($arResult['CONTRACT_LIST'] as $iContract => $arContract) {?>
        <tr>
            <td><?= $iContract + 1 ?></td>
            <td><?= $arContract['NAME'] ?></td>
            <td>
                <?foreach($arContract['FILES'] as $iFile => $arFile) {?>
                    <a class="download-file" href="<?= $arFile['PATH'] ?>">
                        <?= $arFile['ORIGINAL_NAME'] ?>
                    </a>
                    <?if($iFile + 1 < count($arContract['FILES'])){?>
                        <br>
                    <?}?>
                <?}?>
            </td>
            <td>
                <?foreach($arContract['FILES'] as $iFile => $arFile) {?>
                    <?= fileSizeToString($arFile['SIZE']) ?>
                    <?if($iFile + 1 < count($arContract['FILES'])){?>
                        <br>
                    <?}?>
                <?}?>
            </td>
        </tr>
    <?}?>
    </tbody>
</table>
<?= $arResult['NAV_PRINT'] ?>
<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Loader,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application;

class UserCabinetContracts extends CBitrixComponent {
    //region Задание свойств класса
    private $page = 'template';
    private $arGet = array();
    private $arPost = array();
    //endregion
    //region Подготовка входных параметров компонента
    /** Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        return parent::onPrepareComponentParams($arParams);
    }
    //endregion
    //region Подключение файлов перевода
    /**
     * Подключение файлов перевода
     */
    public function onIncludeComponentLang() {
        Loc::loadMessages(__FILE__);
    }
    //endregion
    //region Выполнение компонента
    /**
     * Выполнение компонента
     */
    public function executeComponent() {
        $this->getRequest();                                    // Получение данных request
        $this->includeModule();                                 // Подключение модулей
        $this->getListContracts();                              // Возвращает список договоров
        $this->setUrlSort();                                    // Возвращает ссылку для сортировки списка договоров по названию
        $this->includeComponentTemplate($this->page);           // Подключение шаблона
    }
    //endregion
    //region Получение данных request
    /**
     * Получение данных request
     */
    private function getRequest() {
        $request = Application::getInstance()->getContext()->getRequest();
        $this->arGet = $request->getQueryList()->toArray();
        $this->arPost = $request->getPostList()->toArray();
    }
    //endregion
    //region Подключение модулей
    /**
     * Подключение модулей
     */
    private function includeModule() {
        Loader::includeModule('iblock');
    }
    //endregion
    //region Возвращает список договоров
    /**
     * Возвращает список договоров
     */
    private function getListContracts() {
        $arOrderContracts = array('NAME' => 'ASC');
        if(!empty($this->arGet['NAME'])) {
            $arOrderContracts['NAME'] = $this->arGet['NAME'];
        }
        $arFilterContracts = array(
            'IBLOCK_CODE' => $this->arParams['IBLOCK_CODE_CONTRACTS'],
            'PROPERTY_PROP_SUPER_USER' => array($this->arParams['USER_ID'])
        );
        $arNavParamsContracts = array('nPageSize' => $this->arParams['COUNT_CONTRACTS_ON_PAGE']);
        $arSelectContracts = array(
            'ID',
            'NAME',
            'IBLOCK_ID',
            'PROPERTY_PROP_SUPER_USER',
            'PROPERTY_PROP_FILES',
        );
        $cIBlockElement = new CIBlockElement();
        $rsContracts = $cIBlockElement->GetList($arOrderContracts, $arFilterContracts, false, $arNavParamsContracts, $arSelectContracts);
        $this->arResult['COUNT_UNITS'] = $rsContracts->SelectedRowsCount();
        $this->arResult['NAV_PRINT'] = $rsContracts->GetPageNavStringEx($navComponentObject, Loc::getMessage('NAV_LIST_CONTARCTS'), $this->arParams['NAV_TEMPLATE']);
        $iContract = 0;
        while($obContracts = $rsContracts->Fetch()) {
            $this->arResult['CONTRACT_LIST'][$iContract] = array(
                'ID' => $obContracts['ID'],
                'NAME' => $obContracts['NAME'],
                'IBLOCK_ID' => $obContracts['IBLOCK_ID'],
                'SUPER_USER' => $obContracts['PROPERTY_PROP_SUPER_USER_VALUE'],
            );
            if(!empty($obContracts['PROPERTY_PROP_FILES_VALUE'])) {
                foreach($obContracts['PROPERTY_PROP_FILES_VALUE'] as $iFile => $vFile) {
                    $cFile = new CFile();
                    $arFile = $cFile->GetByID($vFile)->Fetch();
                    if($arFile) {

                    }
                    $this->arResult['CONTRACT_LIST'][$iContract]['FILES'][] = array(
                        'ORIGINAL_NAME' => $arFile['ORIGINAL_NAME'],
                        'FILE_NAME' => $arFile['FILE_NAME'],
                        'SIZE' => $arFile['FILE_SIZE'],
                        'ID' => $arFile['ID'],
                        'PATH' => $cFile->GetPath($vFile),
                    );
                }
            }
            $iContract++;
        }
    }
    //endregion
    //region Возвращает ссылку для сортировки списка договоров по названию
    /**
     * Возвращает ссылку для сортировки списка договоров по названию
     */
    private function setUrlSort() {
        $baseUrl = $this->arParams['FOLDER'] . $this->arParams['URL_SUB_SECTION'];
        if(!empty($this->arGet)) {
            $iGet = 0;
            foreach($this->arGet as $cGet => $vGet) {
                if($cGet != 'NAME') {
                    if($iGet == 0) {
                        $baseUrl .= '?';
                    } else {
                        $baseUrl .= '&';
                    }
                    $baseUrl .= $cGet . '=' . $vGet;
                }
            }
        }
        $baseUrl .= '&NAME=';
        $order = 'ASC';
        if(!empty($this->arGet['NAME'])) {
            if($this->arGet['NAME'] == 'ASC') {
                $order = 'DESC';
            } else {
                $order = 'ASC';
            }
        }
        $baseUrl .= $order;
        $this->arResult['SORT_URI'] = $baseUrl;
    }
    //endregion
}
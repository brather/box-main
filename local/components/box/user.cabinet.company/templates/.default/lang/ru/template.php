<?php
$MESS['T_USER_COMPANY_ADRESS_LEGAL_TITLE'] = 'Юридический адрес';
$MESS['T_USER_COMPANY_ADRESS_ACTUAL_TITLE'] = 'Фактический адрес';
$MESS['T_COMPANY_CHANGE'] = 'Выберите организацию: ';
$MESS['T_CONTRACT_CHANGE'] = 'Выберите договор: ';
$MESS['T_COMPANY_CHANGE_DEFAULT'] = '-не выбрано-';
$MESS['T_FORM_BUTTON_EDIT'] = 'Изменить';
$MESS['T_FORM_BUTTON_ADD'] = 'Добавить';
$MESS['T_FORM_EDIT_BLOCK'] = 'Редактировать ';
$MESS['T_FORM_EDIT_LEGAL_ADRESS'] = 'Редактировать юридический адрес';
$MESS['T_FORM_VIEW_LEGAL_ADRESS'] = 'Просмотреть юридический адрес';
$MESS['T_FORM_ADD_LEGAL_ADRESS'] = 'Добавить юридический адрес';
$MESS['T_FORM_EMPTY_LEGAL_ADRESS'] = 'Юридический адрес на данный момент не внесен в систему';
$MESS['T_FORM_ADD_ACTUAL_ADRESS'] = 'Добавить дополнительный адрес';
$MESS['T_FORM_ADD_ACTUAL_ADRESS_SPACE'] = ', ';
$MESS['T_FORM_BREAK'] = '<br/>';

$MESS['T_FORM_EDIT_COMPANY_ADRESS_LEGAL'] = 'Адрес:  ';
$MESS['T_FORM_EDIT_COMPANY_NAME'] = 'Организация:  ';
$MESS['T_FORM_EDIT_COMPANY_PROPERTY_PROP_FORM_VALUE'] = 'Организационно-правовая форма: ';
$MESS['T_FORM_EDIT_COMPANY_PROPERTY_PROP_KPP_VALUE'] = 'КПП: ';
$MESS['T_FORM_EDIT_COMPANY_PROPERTY_PROP_INN_VALUE'] = 'ИНН: ';
$MESS['T_FORM_EDIT_COMPANY_PROPERTY_PROP_OGRN_VALUE'] = 'ОГРН: ';
$MESS['T_FORM_EDIT_COMPANY_PROPERTY_PROP_PHONE_VALUE'] = 'Телефон: ';
$MESS['T_FORM_EDIT_COMPANY_PROPERTY_PROP_FAX_VALUE'] = 'Факс: ';
$MESS['T_FORM_EDIT_COMPANY_PROPERTY_PROP_ADDITIONAL_INFORM_VALUE'] = 'Дополнительная информация: ';

$MESS['T_FORM_EDIT_COMPANY_PLACEHOLDER_NAME'] = 'организация...';
$MESS['T_FORM_EDIT_COMPANY_PLACEHOLDER_PROPERTY_PROP_FORM_VALUE'] = 'пао, зао, ао, оао, ооо...';
$MESS['T_FORM_EDIT_COMPANY_PLACEHOLDER_PROPERTY_PROP_KPP_VALUE'] = '11111111...';
$MESS['T_FORM_EDIT_COMPANY_PLACEHOLDER_PROPERTY_PROP_INN_VALUE'] = '11111111...';
$MESS['T_FORM_EDIT_COMPANY_PLACEHOLDER_PROPERTY_PROP_OGRN_VALUE'] = '11111111...';
$MESS['T_FORM_EDIT_COMPANY_PLACEHOLDER_PROPERTY_PROP_PHONE_VALUE'] = '+7 999 999 99-99...';
$MESS['T_FORM_EDIT_COMPANY_PLACEHOLDER_PROPERTY_PROP_FAX_VALUE'] = '+7 999 999 99-99...';
$MESS['T_FORM_EDIT_COMPANY_PLACEHOLDER_PROPERTY_PROP_ADDITIONAL_INFORM_VALUE'] = 'текст...';

$MESS['T_FORM_ADRESS_TITLE_UF_NAME'] = 'псевдоним адреса ';
$MESS['T_FORM_ADRESS_TITLE_UF_COUNTRY'] = 'страна ';
$MESS['T_FORM_ADRESS_TITLE_UF_REGION'] = 'регион ';
$MESS['T_FORM_ADRESS_TITLE_UF_TOWN'] = 'город ';
$MESS['T_FORM_ADRESS_TITLE_UF_AREA'] = 'район ';
$MESS['T_FORM_ADRESS_TITLE_UF_INDEX'] = 'почтовый индекс ';
$MESS['T_FORM_ADRESS_TITLE_UF_STREET'] = 'улица ';
$MESS['T_FORM_ADRESS_TITLE_UF_HOUSE'] = 'дом ';
$MESS['T_FORM_ADRESS_TITLE_UF_HOUSING'] = 'корпус ';
$MESS['T_FORM_ADRESS_TITLE_UF_STRUCT'] = 'строение ';
$MESS['T_FORM_ADRESS_TITLE_UF_OFFICE'] = 'офис ';
$MESS['T_FORM_ADRESS_TITLE_UF_CONTACT_PERSON'] = 'контактное лицо';
$MESS['T_FORM_ADRESS_TITLE_UF_CONTACT_PERSON_PH'] = 'телефон ';
$MESS['T_FORM_ADRESS_TITLE_UF_DESCRIPTION'] = 'дополнительно ';

$MESS['T_FORM_ADRESS_TITLE_S_UF_NAME'] = 'псевдоним адреса ';
$MESS['T_FORM_ADRESS_TITLE_S_UF_COUNTRY'] = '';
$MESS['T_FORM_ADRESS_TITLE_S_UF_REGION'] = '';
$MESS['T_FORM_ADRESS_TITLE_S_UF_TOWN'] = '';
$MESS['T_FORM_ADRESS_TITLE_S_UF_AREA'] = '';
$MESS['T_FORM_ADRESS_TITLE_S_UF_INDEX'] = '';
$MESS['T_FORM_ADRESS_TITLE_S_UF_STREET'] = '';
$MESS['T_FORM_ADRESS_TITLE_S_UF_HOUSE'] = 'дом ';
$MESS['T_FORM_ADRESS_TITLE_S_UF_HOUSING'] = 'корп. ';
$MESS['T_FORM_ADRESS_TITLE_S_UF_STRUCT'] = 'стр. ';
$MESS['T_FORM_ADRESS_TITLE_S_UF_OFFICE'] = 'офис ';
$MESS['T_FORM_ADRESS_TITLE_S_UF_DESCRIPTION'] = 'дополнительно ';

$MESS['T_FORM_PLACEHOLDER_UF_NAME'] = 'краткий заголовок...';
$MESS['T_FORM_PLACEHOLDER_UF_COUNTRY'] = 'РФ...';
$MESS['T_FORM_PLACEHOLDER_UF_REGION'] = 'Московская область..';
$MESS['T_FORM_PLACEHOLDER_UF_TOWN'] = 'Мытищи...';
$MESS['T_FORM_PLACEHOLDER_UF_AREA'] = 'район...';
$MESS['T_FORM_PLACEHOLDER_UF_INDEX'] = '123456...';
$MESS['T_FORM_PLACEHOLDER_UF_STREET'] = 'улица...';
$MESS['T_FORM_PLACEHOLDER_UF_HOUSE'] = 'дом...';
$MESS['T_FORM_PLACEHOLDER_UF_HOUSING'] = 'корпус...';
$MESS['T_FORM_PLACEHOLDER_UF_STRUCT'] = 'строение...';
$MESS['T_FORM_PLACEHOLDER_UF_OFFICE'] = 'офис...';
$MESS['T_FORM_PLACEHOLDER_UF_CONTACT_PERSON'] = 'ФИО контактного лица...';
$MESS['T_FORM_PLACEHOLDER_UF_CONTACT_PERSON_PH'] = 'Телефон контактного лица...';
$MESS['T_FORM_PLACEHOLDER_UF_DESCRIPTION'] = 'дополнительно...';
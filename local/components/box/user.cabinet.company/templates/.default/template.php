<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application;
$request = Application::getInstance()->getContext()->getRequest();
$arGet = $request->getQueryList()->toArray();
$arPost = $request->getPostList()->toArray();

$arAdressCodes = array(
    'UF_COUNTRY',
    'UF_REGION',
    'UF_TOWN',
    'UF_AREA',
    'UF_STREET',
    'UF_HOUSE',
    'UF_HOUSING',
    'UF_STRUCT',
    'UF_OFFICE',
);
$actionForm = $arParams['FOLDER'] . $arParams['URL_SUB_SECTION'];
$arEditOrganization = array(
    //'NAME',
    //'PROPERTY_PROP_FORM_VALUE',
    //'PROPERTY_PROP_INN_VALUE',
    //'PROPERTY_PROP_KPP_VALUE',
    //'PROPERTY_PROP_OGRN_VALUE',
    'PROPERTY_PROP_ADDITIONAL_INFORM_VALUE',
    'PROPERTY_PROP_PHONE_VALUE',
    'PROPERTY_PROP_FAX_VALUE',
);?>
<?if(!empty($arResult['MESS']['ERROR']) && empty($arPost['ADRESS_ADD'])) {
    getHtmlError($arResult['MESS']['ERROR']);
}?>
<h2><?= Loc::getMessage('T_CONTRACT_CHANGE') ?></h2>
<select name="NAME_OF_COMPANY" id="JS_NAME_OF_COMPANY" data-container="#option-container">
    <!--<option value="<?= $actionForm ?>" data-show=""><?= Loc::getMessage('T_COMPANY_CHANGE_DEFAULT') ?></option>-->
    <?$arEmptyContract = array();
    $iContract = 0;
    foreach($arResult['LIST_CONTRACTS'] as $idContract => $arContract) {?>
        <?$selected = '';
        if(!empty($arGet['contract']) && $arGet['contract'] == $idContract
            || $arPost['CONTRACT_ID'] == $idContract
            || $iContract == 0) {
            $selected = ' selected';
        }
        $companyIdBlock = $arContract['PROPERTY_PROP_COMPANY_VALUE'];
        if(empty($arContract['PROPERTY_PROP_COMPANY_VALUE'])) {
            $arEmptyContract[] = $idContract;
        }?>
        <option value="<?= $actionForm ?>?contract=<?= $idContract ?>"<?= $selected ?>
                data-show="#contract-<?= $idContract ?>">
            <?= $arContract['NAME'] ?>
        </option>
        <?$iContract++;?>
    <?}?>
</select>
<div class="option-container" id="option-container">
    <?$iContract = 0;
    foreach($arResult['LIST_CONTRACTS'] as $idContract => $arContract) {?>
        <?$arCompany = $arResult['LIST_COMPANY'][$arContract['PROPERTY_PROP_COMPANY_VALUE']];
        $idCompany = $arCompany['ID'];
        $clsActive = '';
        if(!empty($arGet['contract']) && $arGet['contract'] == $idContract
            || $arPost['CONTRACT_ID'] == $idContract
            || $iContract == 0 && empty($arGet['contract'])) {
            $clsActive = ' ocb-active';
        }?>
        <div class="oc-block<?= $clsActive ?>" id="contract-<?= $idContract ?>">
            <div class="company-edit">
                <form action="<?= $actionForm ?>?contract=<?= $idContract ?>" method="post" class="form">
                    <?if(!empty($arCompany)) {?>
                        <input type="hidden" name="COMPANY_EDIT" value="Y">
                        <input type="hidden" name="COMPANY_ID" value="<?= $idCompany ?>">
                    <?} else {?>
                        <input type="hidden" name="COMPANY_ADD" value="Y">
                        <input type="hidden" name="CONTRACT_ID" value="<?= $idContract ?>">
                    <?}
                    if(!empty($arCompany)) {
                        $arForm = $arCompany;
                    } else {
                        $arForm = $arEditOrganization;
                    }
                    $arEmptyCode = array();?>
                    <div class="form__row descript__row">
                        <p>
                            <?$iProp = 0; $i = 1;
                            foreach($arForm as $cProp => $vProp){
                                $i++;
                                $code = $cProp;
                                if(empty($arCompany)){
                                    $code = $vProp;
                                }
                                if(in_array($code, $arEditOrganization)
                                    || $code == 'ID'
                                    || $code == 'CONTRACT_ID'
                                    || $vProp == ''){
                                    if($vProp == '') {
                                        $arEmptyCode[] = $code;
                                    }
                                    continue;
                                }

                                if($code == 'ADRESS_LEGAL') {
                                    $iAdress = 0;
                                    foreach($arAdressCodes as $cAdress) {
                                        if(empty($vProp[$cAdress])) {
                                            continue;
                                        }
                                        if($iAdress != 0) {
                                            echo Loc::getMessage('T_FORM_ADD_ACTUAL_ADRESS_SPACE');
                                        }
                                        //echo Loc::getMessage('T_FORM_ADRESS_TITLE_' . $cAdress);
                                        echo $vProp[$cAdress];
                                        $iAdress++;
                                    }
                                } else {
                                    echo Loc::getMessage('T_FORM_EDIT_COMPANY_' . $code) . trim($vProp);
                                    if (count($arForm) > $i) echo Loc::getMessage('T_FORM_ADD_ACTUAL_ADRESS_SPACE');
                                }
                                $iProp++;

                                echo Loc::getMessage('T_FORM_BREAK');
                            }?>
                        </p>
                    </div>
                    <div class="form-row">
                        <?foreach($arForm as $cProp => $vProp) {?>
                            <?$disabled = '';
                            if(!empty($arCompany)) {
                                $code = $cProp;
                                $value = $vProp;
                                if(!$arParams['COMPANY_FIELDS'][str_replace('PROPERTY_', '', $code)] && !empty($value)) {
                                    $disabled = ' disabled';
                                }
                            } else {
                                $code = $vProp;
                                $value = '';
                            }
                            $tInput = 'text';
                            $masketInput = '';
                            $clsMasked = '';
                            $required = '';
                            if($code == 'PROPERTY_PROP_INN_VALUE'
                                || $code == 'PROPERTY_PROP_KPP_VALUE'
                                || $code == 'PROPERTY_PROP_OGRN_VALUE') {
                                $tInput = 'number';
                            } elseif($code == 'PROPERTY_PROP_PHONE_VALUE'
                                || $code == 'PROPERTY_PROP_FAX_VALUE') {
                                $masketInput = ' data-maskedinput="' . $arParams['PHONE_FORMAT'] . '"';
                                $clsMasked = ' js-maskedinput';
                                $required = ' required';
                            }
                            if(in_array($code, $arEditOrganization) || in_array($code, $arEmptyCode)) {?>
                                <div class="form__row__col _size4">
                                    <div class="form__row__name"><?= Loc::getMessage('T_FORM_EDIT_COMPANY_' . $code) ?></div>
                                    <?if($code == 'PROPERTY_PROP_ADDITIONAL_INFORM_VALUE') {?>
                                        <textarea name="<?= $code ?>"
                                                  id="id-<?= $code ?>"
                                                  cols="30"
                                                  class="form__field<?= $clsMasked ?>"
                                                  rows="5"><?= $value ?></textarea>
                                    <?} else {?>
                                        <input type="<?= $tInput ?>"
                                               name="<?= $code ?>"
                                               value="<?= $value ?>"
                                            <?= $masketInput ?>
                                            <?= $disabled ?>
                                            <?= $required ?>
                                               placeholder="<?//= Loc::getMessage('T_FORM_EDIT_COMPANY_PLACEHOLDER_' . $code) ?>"
                                               class="form__field<?= $clsMasked ?>">
                                    <?}?>
                                </div>
                            <?}?>
                        <?}?>
                    </div>
                    <button type="submit" class="btn">
                        <?if(!empty($arCompany)){?>
                            <?= Loc::getMessage('T_FORM_BUTTON_EDIT') ?>
                        <?} else {?>
                            <?= Loc::getMessage('T_FORM_BUTTON_ADD') ?>
                        <?}?>
                    </button>
                </form>
            </div>
            <?if(!empty($arCompany)) {?>
                <div class="company-adress form__row form__row__sheet">
                    <div class="order__col">
                        <div class="order__col__frame">
                            <h2><?= Loc::getMessage('T_USER_COMPANY_ADRESS_LEGAL_TITLE') ?></h2>
                            <?$arTmpCompany = array(
                                'UF_COMPANY' => $arCompany['ID'],
                                'UF_NAME' => $arCompany['NAME']
                                    . ' (' . Loc::getMessage('T_USER_COMPANY_ADRESS_LEGAL_TITLE') . ')',
                                'ACTION' => 'ADD'
                            );
                            $messShow = Loc::getMessage('T_FORM_ADD_LEGAL_ADRESS');
                            if(!empty($arCompany['ADRESS_LEGAL'])){
                                $arTmpCompany = $arCompany['ADRESS_LEGAL'];
                                $messShow = Loc::getMessage('T_FORM_VIEW_LEGAL_ADRESS');?>
                                <div class="b-container" id="container-legal-adress">
                                    <div class="b-container-block">
                                        <div class="bcb-head">
                                            <?= getAdressInString($arCompany['ADRESS_LEGAL'], $arParams['FIELDS_ADRESS']); ?>
                                        </div>
                                    </div>
                                </div>
                            <?} else {?>
                                <h4><?= Loc::getMessage('T_FORM_EMPTY_LEGAL_ADRESS') ?></h4>
                            <?}?>
                        </div>
                    </div>
                    <div class="order__col">
                        <h2><?= Loc::getMessage('T_USER_COMPANY_ADRESS_ACTUAL_TITLE') ?></h2>
                        <?$arActualParams = $arParams['FIELDS_ADRESS'];
                        $arActualParams['UF_NAME'] = 'text';?>
                        <div class="b-container" id="container-form">
                            <?if(!empty($arCompany['ADRESS_ACTUAL'])) {?>
                                <?foreach($arCompany['ADRESS_ACTUAL'] as $iArAdress => $arAdress){?>
                                    <div class="b-container-block">
                                        <div class="bcb-head adress-actual">
                                            <p>
                                                <?if(count($arCompany['ADRESS_ACTUAL']) > 1) {?>
                                                    <?= $iArAdress + 1 ?>
                                                <?}?>
                                                <b><?= $arAdress['UF_NAME'] ?></b><br>
                                                <?= getAdressInString($arAdress, $arParams['FIELDS_ADRESS']); ?>
                                            </p>
                                            <p><a class="js-toggle-uri" href="#"><i class="fa fa-pencil"></i></a></p>
                                        </div>
                                        <div class="bcb-collapse">
                                            <div class="bcb-body">
                                                <?$arAdress['UF_TYPE_ADRESS'] = intval($arResult['LIST_TYPES_OF_ADRESS']['ADRESS_ACTUAL']['ID']);
                                                setHtmlAdressFromArray(
                                                    $arActualParams,
                                                    $arAdress,
                                                    $actionForm . '?contract=' . $idContract);?>
                                            </div>
                                        </div>
                                    </div>
                                <?}?>
                                <hr>
                            <?}?>
                            <?$clsOpen = '';
                            $displayBody = 'none';
                            if($arPost['ADRESS_ADD'] == 'Y' && !empty($arResult['MESS']['ERROR'])){
                                $clsOpen = ' js-open';
                                $displayBody = 'block';
                            }?>
                            <div class="b-container-block<?= $clsOpen ?>">
                                <div class="bcb-head">
                                    <h4>
                                        <a class="js-toggle-uri" href="#">
                                            <?= Loc::getMessage('T_FORM_ADD_ACTUAL_ADRESS') ?>
                                        </a>
                                    </h4>
                                </div>
                                <div class="bcb-collapse" style="display: <?= $displayBody ?>">
                                    <div class="bcb-body">
                                        <?
                                        if($arPost['ADRESS_ADD'] == 'Y' && !empty($arResult['MESS']['ERROR'])) {
                                            getHtmlError($arResult['MESS']['ERROR']);
                                        }
                                        setHtmlAdressFromArray(
                                            $arActualParams,
                                            array(
                                                'UF_COMPANY' => $arCompany['ID'],
                                                'UF_TYPE_ADRESS' => intval($arResult['LIST_TYPES_OF_ADRESS']['ADRESS_ACTUAL']['ID']),
                                                'ACTION' => 'ADD'
                                            ),
                                            $actionForm . '?contract=' . $idContract);?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?}?>
        </div>
        <?$iContract++;?>
    <?}?>
</div>





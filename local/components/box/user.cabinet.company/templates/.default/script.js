$(document).ready(function(){
    var selectCompany = $('#JS_NAME_OF_COMPANY'),
        animationSpeed = 200,
        cls = {
            block : '.oc-block',
            active : 'ocb-active'
        };
    selectCompany.on('change', function(e){
        var $this = $(this),
            addHash = $this.val(),
            containBlock = $($this.data('container')),
            idBlkShow = $this.find('option:selected').data('show'),
            optionSel = $(idBlkShow);
        if(idBlkShow) {
            if(!optionSel.hasClass(cls.active)) {
                containBlock.find('.' + cls.active).hide(animationSpeed, function(){
                    $(this).removeClass(cls.active);
                });
                optionSel.show(animationSpeed, function() {
                    $(this).addClass(cls.active);
                });
            }
        } else {
            containBlock.find('.' + cls.active).hide(animationSpeed, function(){
                $(this).removeClass(cls.active);
            });
        }
        setLocation(addHash);
    });
});
function setLocation(curLoc){
    try {
        history.pushState(null, null, curLoc);
        return;
    } catch(e) {}
    location.hash = curLoc;
}
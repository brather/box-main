<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc,
    Bitrix\Main\Application;

function setHtmlAdressFromArray($arAdress, $arResultAdress, $actionForm, $hideEdit = false) {
    $request = Application::getInstance()->getContext()->getRequest()->toArray();?>
    <form action="<?= $actionForm ?>"
          class="form"
          method="post">
        <?if($arResultAdress['ACTION'] == 'ADD') {
            $action = 'ADRESS_ADD';
            ?><input type="hidden" name="UF_TYPE_ADRESS" value="<?= $arResultAdress['UF_TYPE_ADRESS'] ?>" /><?
        } else {
            $action = 'ADRESS_UPDATE';
        }?>
        <input type="hidden" name="<?= $action ?>" value="Y" />
        <ul class="order__list">
            <?foreach($arAdress as $cProperty => $typeInput) {
                $value = $arResultAdress[$cProperty];
                if(!empty($request[$cProperty])) {
                    $value = $request[$cProperty];
                }
                if($typeInput == 'hidden') {?>
                    <input type="<?= $typeInput ?>"
                           class="form__field"
                           name="<?= $cProperty ?>"
                           value="<?= $value ?>" />
                <?} else {?>
                    <li>
                        <div class="form__row__name"><?= Loc::getMessage('T_FORM_ADRESS_TITLE_' . $cProperty) ?></div>
                        <div>
                            <?if($hideEdit) {?>
                                <p class="view-data"><?= $value ?></p>
                            <?} else {?>
                                <?if($typeInput == 'textarea'){?>
                                    <textarea name="<?= $cProperty ?>"
                                              class="form__field"
                                              id="ID_<?= $cProperty ?>"
                                              placeholder="<?= Loc::getMessage('T_FORM_PLACEHOLDER_' . $cProperty) ?>"
                                              cols="30"
                                              rows="10"><?= $value ?></textarea>
                                <?} else {?>
                                    <input type="<?= $typeInput ?>"
                                           class="form__field"
                                           name="<?= $cProperty ?>"
                                           placeholder="<?= Loc::getMessage('T_FORM_PLACEHOLDER_' . $cProperty) ?>"
                                           value="<?= $value ?>" />
                                <?}?>
                            <?}?>
                        </div>
                    </li>
                <?}?>
            <?}?>
        </ul>
        <?if(!$hideEdit) {?>
            <button class="btn" type="submit">
                <?if($arResultAdress['ACTION'] == 'ADD') {?>
                    <?= Loc::getMessage('T_FORM_BUTTON_ADD') ?>
                <?} else {?>
                    <?= Loc::getMessage('T_FORM_BUTTON_EDIT') ?>
                <?}?>
            </button>
        <?}?>
    </form>
<?}

function getAdressInString($arAdress, $arParams) {
    $iAdress = 0;
    $html = '';
    foreach($arAdress as $kAdress => $vAdress) {
        if($arParams[$kAdress] != 'hidden' && !empty($vAdress)) {
            if($iAdress != 0) {
                $html .= Loc::getMessage('T_FORM_ADD_ACTUAL_ADRESS_SPACE');
            }
            $html .= '<small>' . Loc::getMessage('T_FORM_ADRESS_TITLE_S_' . $kAdress) . '</small>' . $vAdress;
            $iAdress++;
        }
    }
    return $html;
}

function getHtmlError($arMessError) {?>
    <div class="frn-danger">
        <?if(is_array($arMessError)) {
            foreach($arMessError as $iError => $vError) {?>
                <?= $vError ?>
            <?}
        } else {?>
            <?= $arMessError ?>
        <?}?>
    </div>
    <br><?
}
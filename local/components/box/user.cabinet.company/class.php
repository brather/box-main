<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Application,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Iblock\IblockTable,
    \Bitrix\Highloadblock as HL;

class UserCabinetCompanyComponent extends CBitrixComponent {
    //region Задание свойств класса
    private $page = 'template';
    private $arRequest = array();
    private $arGet = array();
    private $arPost = array();
    private $dClassAdress = '';
    private $idHLBlockAdress = 0;
    //endregion
    //region Подготовка входных параметров компонента
    /** Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        return parent::onPrepareComponentParams($arParams);
    }
    //endregion
    //region Подключение файлов перевода
    /**
     * Подключение файлов перевода
     */
    public function onIncludeComponentLang() {
        Loc::loadMessages(__FILE__);
    }
    //endregion
    //region Выполнение компонента
    /**
     * Выполнение компонента
     */
    public function executeComponent() {
        $this->getRequest();                                    // Получение данных request
        $this->includeModule();                                 // Подключение модулей
        if($this->arPost['COMPANY_ADD'] == 'Y') {
            $this->addCompany();                                // Добавление организации
        } elseif($this->arPost['COMPANY_EDIT'] == 'Y') {
            $this->editCompany();                               // Редактирование организации
        } elseif($this->arPost['ADRESS_ADD'] == 'Y') {
            $this->addAdress();                                 // Добавление адреса
        } elseif($this->arPost['ADRESS_UPDATE'] == 'Y') {
            $this->updateAdress();                              // Обновление свойств адреса
        }
        $this->getListPropertyStatus();                         // Получим значения списка свойства типа список для HL блока
        $this->getUserCompany();                                // Возвращаем список организаций по свойству указанному в договоре
        $this->getAdress();                                     // Возвращаем список адресов
        $this->includeComponentTemplate($this->page);           // Подключение шаблона
    }
    //endregion
    //region Получение данных request
    /**
     * Получение данных request
     */
    private function getRequest() {
        $request = Application::getInstance()->getContext()->getRequest();
        $this->arRequest = $request->toArray();
        $this->arGet = $request->getQueryList()->toArray();
        $this->arPost = $request->getPostList()->toArray();
    }
    //endregion
    //region Подключение модулей
    /**
     * Подключение модулей
     */
    private function includeModule() {
        Loader::includeModule('entity');
        Loader::includeModule('highloadblock');
        Loader::includeModule('iblock');
    }
    //endregion
    //region Возвращаем список договоров принадлежащих пользователю
    /**
     * Возвращаем список договоров принадлежащих пользователю
     */
    private function getListContracts() {
        $arFilter = array(
            'IBLOCK_CODE' => $this->arParams['IBLOCK_CODE_CONTRACTS'],
            'PROPERTY_PROP_SUPER_USER' => array($this->arParams['USER_ID'])
        );
        $arSelect = array(
            'ID',
            'IBLOCK_ID',
            'NAME',
            'PROPERTY_PROP_COMPANY'
        );
        $rsContracts = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
        while($obContracts = $rsContracts->Fetch()) {
            $this->arResult['LIST_CONTRACTS'][$obContracts['ID']] = $obContracts;
        }
    }
    //endregion
    //region Возвращаем список организаций по свойству указанному в договоре
    /**
     * Возвращаем список организаций по свойству указанному в договоре
     */
    private function getUserCompany() {
        $this->getListContracts();
        $arCompanyId = array();
        $arContractsId = array();
        foreach($this->arResult['LIST_CONTRACTS'] as $contract) {
            if(!empty($contract['PROPERTY_PROP_COMPANY_VALUE'])) {
                $arCompanyId[] = $contract['PROPERTY_PROP_COMPANY_VALUE'];
                $arContractsId[$contract['PROPERTY_PROP_COMPANY_VALUE']] = $contract['ID'];
            }
        }
        if(!empty($arCompanyId)) {
            $arFilterCompany = array('IBLOCK_CODE' => $this->arParams['IBLOCK_CODE_COMPANY'], 'ID' => $arCompanyId);
            $arSelectCompany = array(
                'ID',
                'IBLOCK_ID',
                'NAME',
                'PROPERTY_PROP_FORM',
                'PROPERTY_PROP_INN',
                'PROPERTY_PROP_KPP',
                'PROPERTY_PROP_OGRN',
                'PROPERTY_PROP_PHONE',
                'PROPERTY_PROP_FAX',
                'PROPERTY_PROP_ADDITIONAL_INFORM',
            );
            $rsCompany = CIBlockElement::GetList(array(), $arFilterCompany, false, false, $arSelectCompany);
            while($obCompany = $rsCompany->Fetch()) {
                $this->arResult['LIST_COMPANY'][$obCompany['ID']] = array(
                    'ID' => $obCompany['ID'],
                    'NAME' => $obCompany['NAME'],
                    'PROPERTY_PROP_FORM_VALUE' => $obCompany['PROPERTY_PROP_FORM_VALUE'],
                    'PROPERTY_PROP_INN_VALUE' => $obCompany['PROPERTY_PROP_INN_VALUE'],
                    'PROPERTY_PROP_KPP_VALUE' => $obCompany['PROPERTY_PROP_KPP_VALUE'],
                    'PROPERTY_PROP_OGRN_VALUE' => $obCompany['PROPERTY_PROP_OGRN_VALUE'],
                    'PROPERTY_PROP_PHONE_VALUE' => $obCompany['PROPERTY_PROP_PHONE_VALUE'],
                    'PROPERTY_PROP_FAX_VALUE' => $obCompany['PROPERTY_PROP_FAX_VALUE'],
                    'PROPERTY_PROP_ADDITIONAL_INFORM_VALUE' => $obCompany['PROPERTY_PROP_ADDITIONAL_INFORM_VALUE'],
                    'CONTRACT_ID' => $arContractsId[$obCompany['ID']]
                );
            }
        }
    }
    //endregion
    //region Возвращаем список адресов
    /**
     * Возвращаем список адресов
     */
    private function getAdress() {
        $idHLBlockAdress = $this->idHLBlockAdress;
        $listTypeAdress = $this->arResult['LIST_TYPES_OF_ADRESS'];
        if(!empty($this->arResult['LIST_COMPANY'])) {
            $this->dClassAdress = self::getDataClass($idHLBlockAdress);
            $dClassContract = $this->dClassAdress;
            foreach($this->arResult['LIST_COMPANY'] as $idCompany => $arCompany) {
                $this->getListAdressOfCompany($dClassContract, $idCompany, $listTypeAdress);
            }
        }
    }
    //endregion
    //region Возвращаем список адресов по id компании
    /**
     * @param $dClass - dataClass highload блока адресов
     * @param $idCompany - id компании
     * @param $listTypeAdress - массив свойств типа алреса (юридический, фактический)
     */
    private function getListAdressOfCompany($dClass, $idCompany, $listTypeAdress) {
        if(empty($this->dClassAdress)) {
            $this->dClassAdress = $this->getDataClass($this->idHLBlockAdress);
        }
        $dClass = $this->dClassAdress;
        $rsData = $dClass::getList(array(
            'select' => array('*'),
            'filter' => array('UF_COMPANY' => $idCompany)
        ));
        while($arData = $rsData->Fetch()) {
            if($listTypeAdress['ADRESS_LEGAL']['ID'] == $arData['UF_TYPE_ADRESS']) {
                $this->arResult['LIST_COMPANY'][$idCompany]['ADRESS_LEGAL'] = $arData;
            } elseif($listTypeAdress['ADRESS_ACTUAL']['ID'] == $arData['UF_TYPE_ADRESS']) {
                $this->arResult['LIST_COMPANY'][$idCompany]['ADRESS_ACTUAL'][] = $arData;
            }
        }
    }
    //endregion
    //region Возвращаем id HL блока по его коду
    /**
     * Возвращаем id HL блока по его коду
     */
    private function getIdHLBlock($codeHL) {
        $connection = Application::getConnection();
        $sql = "SELECT ID,NAME FROM b_hlblock_entity WHERE NAME='" . $codeHL . "';";
        $recordset = $connection->query($sql)->fetch();
        return $recordset['ID'];
    }
    //endregion
    //region Возвращаем класс для работы с HL блоком
    /** Возвращаем класс для работы с HL блоком
     * @throws \Bitrix\Main\SystemException
     */
    private function getDataClass($idHL) {
        $hlblock = HL\HighloadBlockTable::getById($idHL)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        return $entity->getDataClass();
    }
    //endregion
    //region Получим значения списка свойства типа список для HL блока (если не задан код свойства, то по умолчанию ищем статус)
    /** Получим значения списка свойства типа список для HL блока (если не задан код свойства, то по умолчанию ищем статус)
     * @param $idHLBlock - id HL блока
     * @param bool $propertyCode - символьный код свойства
     * @return array
     */
    private function getListPropertyStatus() {
        if($this->idHLBlockAdress == 0) {
            $this->idHLBlockAdress = $this->getIdHLBlock(HL_BLOCK_CODE_ADRESS_ALIAS);
        }
        $idHLBlock = $this->idHLBlockAdress;
        $propertyCode = 'UF_TYPE_ADRESS';
        $resProp = CUserTypeEntity::GetList(
            array(),
            array('FIELD_NAME' => $propertyCode, 'ENTITY_ID' => 'HLBLOCK_' . $idHLBlock)
        )->Fetch();
        $cUserFieldEnum = new CUserFieldEnum();
        $rsUFValues = $cUserFieldEnum->GetList(array(), array('USER_FIELD_ID' => $resProp['ID']));
        $arReturn = array();
        while($obUFValues = $rsUFValues->Fetch()) {
            $this->arResult['LIST_TYPES_OF_ADRESS'][$obUFValues['XML_ID']] = $obUFValues;
        }
    }
    //endregion
    //region Добавление организации
    /**
     * Добавление организации
     */
    private function addCompany() {
        $arPost = $this->arPost;
        $arIBlockCompany = CIBlock::GetList(array(), array('TYPE' => $this->arParams['IBLOCK_TYPE_LISTS'], 'CODE' => $this->arParams['IBLOCK_CODE_COMPANY']))->Fetch();
        $iBlockCompanyId = intval($arIBlockCompany['ID']);

        $arIBlockContract = CIBlock::GetList(array(), array('TYPE' => $this->arParams['IBLOCK_TYPE_LISTS'], 'CODE' => $this->arParams['IBLOCK_CODE_CONTRACTS']))->Fetch();
        $iBlockContractId = intval($arIBlockContract['ID']);

        $el = new CIBlockElement;
        $arPropertyCompany = array(
            'PROP_FORM' => trim($arPost['PROPERTY_PROP_FORM_VALUE']),
            'PROP_INN' => trim($arPost['PROPERTY_PROP_INN_VALUE']),
            'PROP_KPP' => trim($arPost['PROPERTY_PROP_KPP_VALUE']),
            'PROP_OGRN' => trim($arPost['PROPERTY_PROP_OGRN_VALUE']),
            'PROP_PHONE' => trim($arPost['PROPERTY_PROP_PHONE_VALUE']),
            'PROP_FAX' => trim($arPost['PROPERTY_PROP_FAX_VALUE']),
            'PROP_ADDITIONAL_INFORM' => trim($arPost['PROPERTY_PROP_ADDITIONAL_INFORM_VALUE']),
        );
        $arLoadCompany = array(
            'NAME' => $arPost['NAME'],
            'IBLOCK_ID' => $iBlockCompanyId,
            "PROPERTY_VALUES"=> $arPropertyCompany,
        );
        if(!empty($arPost['NAME'])) {
            if($newCompanyId = $el->Add($arLoadCompany)) {
                CIBlockElement::SetPropertyValuesEx($arPost['CONTRACT_ID'], $iBlockContractId, array('PROP_COMPANY' => $newCompanyId));
            } else {
                $this->arResult['MESS']['ERROR'] = Loc::getMessage('C_ADD_COMPANY_ERROR') . $el->LAST_ERROR;
            }
        } else {
            $this->arResult['MESS']['ERROR'] = Loc::getMessage('C_ADD_COMPANY_ERROR_EMPTY_NAME');
        }

    }
    //endregion
    //region Редактирование организации
    /**
     * Редактирование организации
     */
    private function editCompany() {
        $arPost = $this->arPost;
        $arIBlockCompany = IblockTable::getList(array(
            'filter' => array('CODE' => $this->arParams['IBLOCK_CODE_COMPANY']),
            'select' => array('ID')
        ))->fetch();
        $iBlockCompanyId = intval($arIBlockCompany['ID']);
        $arPropertyCompany = array(
            'PROP_FORM' => trim($arPost['PROPERTY_PROP_FORM_VALUE']),
            'PROP_PHONE' => trim($arPost['PROPERTY_PROP_PHONE_VALUE']),
            'PROP_FAX' => trim($arPost['PROPERTY_PROP_FAX_VALUE']),
            'PROP_ADDITIONAL_INFORM' => trim($arPost['PROPERTY_PROP_ADDITIONAL_INFORM_VALUE']),
        );
        CIBlockElement::SetPropertyValuesEx($arPost['COMPANY_ID'], $iBlockCompanyId, $arPropertyCompany);
        if(!empty($this->arPost['NAME'])) {
            $arLoadFields = array('NAME' => $this->arPost['NAME']);
            $elem = new CIBlockElement();
            $elem->Update($this->arPost['COMPANY_ID'], $arLoadFields);
        }
        header('Location: ' . $this->arParams['FOLDER'] . $this->arParams['URL_SUB_SECTION']);
    }
    //endregion
    //region Добавление адреса
    /**
     * Добавление адреса
     */
    private function addAdress() {
        $arPost = $this->arPost;
        $this->idHLBlockAdress = $this->getIdHLBlock(HL_BLOCK_CODE_ADRESS_ALIAS);
        $this->dClassAdress = $this->getDataClass($this->idHLBlockAdress);
        $arData = array();
        foreach($arPost as $cPost => $vPost) {
            if($cPost != 'ID' && array_key_exists($cPost, $this->arParams['FIELDS_ADRESS'])) {
                $arData[$cPost] = $vPost;
            }
        }
        if($arData['UF_NAME'] != '') {
            $dClassContract = $this->dClassAdress;
            $arData['UF_XML_ID'] = 'ADRESS_' . $arData['UF_COMPANY'] . '_DATE_' . str_replace('.', '_', getmicrotime());
            $result = $dClassContract::add($arData);
            $this->arResult['MESS']['ERROR'] = $result->getErrorMessages();
            if(empty($this->arResult['MESS']['ERROR'])) {
               header('Location: ' . $this->arParams['FOLDER'] . $this->arParams['URL_SUB_SECTION']);
            }
        } else {
            $this->arResult['MESS']['ERROR'] = Loc::getMessage('C_ADD_ADDRESS_ERROR_EMPTY_NAME');
        }
    }
    //endregion
    //region Обновление свойств адреса
    /**
     * Обновление свойств адреса
     */
    private function updateAdress() {
        $arPost = $this->arPost;
        if($arPost['UF_NAME'] == '') {
            $this->arResult['MESS']['ERROR'] = Loc::getMessage('C_EDIT_ADDRESS_ERROR_EMPTY_NAME');
        } else {
            $this->idHLBlockAdress = $this->getIdHLBlock(HL_BLOCK_CODE_ADRESS_ALIAS);
            $this->dClassAdress = $this->getDataClass($this->idHLBlockAdress);
            $dClassAdress = $this->dClassAdress;
            foreach($arPost as $cPost => $vPost) {
                if($cPost != 'ID'
                    && $cPost != 'UF_TYPE_ADRESS'
                    && $cPost != 'UF_COMPANY'
                    && array_key_exists($cPost, $this->arParams['FIELDS_ADRESS'])) {
                    $dClassAdress::update($arPost['ID'], array($cPost => $vPost));
                }
            }
        }
    }
    //endregion
}
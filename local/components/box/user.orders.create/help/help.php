<?php require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Application,
    \Bitrix\Highloadblock as HL;

class UserOrdersCreateHelp extends CBitrixComponent {
    //region Подключение файлов перевода
    /**
     * Подключение файлов перевода
     */
    public function onIncludeComponentLang() {
        Loc::loadMessages(__FILE__);
    }
    //endregion
    //region Возвращает тег и тип тега для формы по типу свойства
    /** Возвращает тег и тип тега для формы по типу свойства
     * @param $propertyType - тип базового свойства (S, L, ...)
     * @param bool $userType - дополнительный параметр пользовательского свойства (пр. S:HTML, HTML - пользовательское свойство), необязательное
     * @return mixed - возвращаемый массив
     */
    public function getTagType($propertyType, $userType = false) {
        $arTypesProperty = array(
            'BASE' => array(
                'S' => array(
                    'NAME' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_S'),
                    'EXIST_UF' => true,  //  (может быть пользовательское свойство с данным типом)
                    'TAG' => 'input',
                    'TYPE' => 'text',
                    'PLACEHOLDER' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_PLACEHOLDER_S'),
                ),
                'N' => array(
                    'NAME' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_N'),
                    'EXIST_UF' => true,  //  (может быть пользовательское свойство с данным типом)
                    'TAG' => 'input',
                    'TYPE' => 'number',
                    'PLACEHOLDER' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_PLACEHOLDER_N'),
                ),
                'L' => array(
                    'NAME' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_L'),
                    'EXIST_UF' => false,  //  (пользовательского свойства нет)
                    'TAG' => 'select',
                    'TYPE' => '',
                ),
                'F' => array(
                    'NAME' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_F'),
                    'EXIST_UF' => false,  //  (пользовательского свойства нет)
                    'TAG' => 'input',
                    'TYPE' => 'file',
                ),
                'E' => array(
                    'NAME' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_E'),
                    'EXIST_UF' => true,  //  (может быть пользовательское свойство с данным типом)
                    'TAG' => 'input',
                    'TYPE' => 'hidden',
                ),
                'G' => array(
                    'NAME' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_G'),
                    'EXIST_UF' => true,  //  (может быть пользовательское свойство с данным типом)
                    'TAG' => 'select',
                    'TYPE' => '',
                ),
            ),
            'USER_FIELDS' => array(
                'S:HTML' => array(
                    'NAME' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_S_HTML'),
                    'TAG' => 'input',
                    'TYPE' => 'text',
                    'PLACEHOLDER' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_PLACEHOLDER_S'),
                ),
                'S:video' => array(
                    'NAME' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_S_VIDEO'),
                    'TAG' => 'input',
                    'TYPE' => 'file',
                ),
                'S:Date' => array(
                    'NAME' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_S_DATE'),
                    'TAG' => 'input',
                    'TYPE' => 'date',
                    'PLACEHOLDER' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_PLACEHOLDER_S_DATE')
                ),
                'S:DateTime' => array(
                    'NAME' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_S_DATE_TIME'),
                    'TAG' => 'input',
                    'TYPE' => 'date',
                    'PLACEHOLDER' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_PLACEHOLDER_S_DATE_TIME')
                ),
                'S:SASDPalette' => array(
                    'NAME' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_S_SASDPALETTE'),
                    'TAG' => 'input',
                    'TYPE' => 'text',
                ),
                'S:map_yandex' => array(
                    'NAME' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_S_MAP_YANDEX'),
                    'TAG' => 'input',
                    'TYPE' => 'text',
                ),
                'S:map_google' => array(
                    'NAME' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_S_MAP_GOOGLE'),
                    'TAG' => 'input',
                    'TYPE' => 'text',
                ),
                'S:UserID' => array(
                    'NAME' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_S_USER_ID'),
                    'TAG' => 'input',
                    'TYPE' => 'text',
                ),
                'S:TopicID' => array(
                    'NAME' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_S_TOPIC_ID'),
                    'TAG' => 'input',
                    'TYPE' => 'text',
                ),
                'S:FileMan' => array(
                    'NAME' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_S_FILE_MAN'),
                    'TAG' => 'input',
                    'TYPE' => 'file',
                ),
                'S:ElementXmlID' => array(
                    'NAME' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_S_ELEMENT_XML_ID'),
                    'TAG' => 'input',
                    'TYPE' => 'file',
                ),
                'S:SASDCheckbox' => array(
                    'NAME' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_S_SASDCHECKBOX'),
                    'TAG' => 'label',
                    'TYPE' => '',
                ),
                'S:directory' => array(
                    'NAME' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_S_DIRECTORY'),
                    'TAG' => 'select',
                    'TYPE' => '',
                ),
                'G:SectionAuto' => array(
                    'NAME' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_G_SECTION_AUTO'),
                    'TAG' => 'input',
                    'TYPE' => 'text',
                ),
                'N:SASDSection' => array(
                    'NAME' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_N_SASDSECTION'),
                    'TAG' => 'input',
                    'TYPE' => 'text',
                ),
                'N:SASDCheckboxNum' => array(
                    'NAME' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_N_SASDCHECKBOX_NUM'),
                    'TAG' => 'input',
                    'TYPE' => 'text',
                ),
                'N:Sequence' => array(
                    'NAME' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_N_SEQUENCE'),
                    'TAG' => 'input',
                    'TYPE' => 'text',
                ),
                'E:SKU' => array(
                    'NAME' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_E_SKU'),
                    'TAG' => 'input',
                    'TYPE' => 'text',
                ),
                'E:EList' => array(
                    'NAME' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_E_LIST'),
                    'TAG' => 'input',
                    'TYPE' => 'text',
                ),
                'E:EAutocomplete' => array(
                    'NAME' => Loc::getMessage('U_ORDER_CREATE_HELP_INPUT_TYPE_E_AUTO_COMPLETE'),
                    'TAG' => 'input',
                    'TYPE' => 'text',
                ),
            )
        );
        if(!$userType) {
            return $arTypesProperty['BASE'][$propertyType];
        } else {
            return $arTypesProperty['USER_FIELDS'][$propertyType . ':' . $userType];
        }
    }
    //endregion
    //region Функция возвращает массив свойства инфоблока
    /** Функция возвращает массив свойства инфоблока
     * @param $iblockCode - символьный код инфоблока которому принадлежит свойство
     * @param $propertyCode - символьный код свойства
     * @return mixed - возвращаемый массив
     */
    public function checkProperty($iblockCode) {
        Loc::loadMessages(__FILE__);
        // массив с типами заказов и их свойствами
        $arTypeOfOrder = array(
            'digitization' => array(                            // ОЦИФРОВКА
                'PROPERTY' => array(
                    'TYPE_DIGITIZATION' => array(               // Тип оцифровки
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => array(
                            'SCAN' => array(
                                'SCAN_RESOLUTION',
                                'SCAN_COLOR',
                                'SCAN_EXT'
                            ),
                            'RECOGNITION' => '',
                            'INDEX' => ''
                        ),
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_DIGITIZATION_TYPE'),
                        'TAG' => 'select',
                        'TYPE' => '',
                        'DIRECTORY' => false,
                    ),
                    'DATE' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_DIGITIZATION_DATE'),
                        'TAG' => 'input',
                        'TYPE' => 'date',
                    ),
                    'SCAN_RESOLUTION' => array(                 // Разрешение
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_DIGITIZATION_RESOLUTION'),
                        'TAG' => 'input',
                        'TYPE' => 'text',
                    ),
                    'SCAN_COLOR' => array(                      // Цветность
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_DIGITIZATION_COLOR'),
                        'TAG' => 'select',
                        'TYPE' => '',
                        'DIRECTORY' => false,
                    ),
                    'SCAN_EXT' => array(                         // Формат
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_DIGITIZATION_EXT'),
                        'TAG' => 'input',
                        'TYPE' => 'text'
                    ),
                    'COMMENT' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_COMMENT'),
                        'TAG' => 'textarea',
                        'TYPE' => ''
                    )
                ),
                'REGISTRY' => true,                             // блок с реестром
                'WMS_STATUS' => array(                          // фильтрация по статусам WMS
                    'placed_storage' => true,
                    'placed_operation' => true,
                    //'withdrawn_scan' => false,
                ),
            ),
            'first_placement' => array(                         // Первичное размещение
                'PROPERTY' => array(
                    'DATE' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_FIRST_DATE'),
                        'TAG' => 'input',
                        'TYPE' => 'date',
                    ),
                    'PLACE' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => array(
                            'PICKUP' => array('ADRESS_AUDIT_ROOM'),
                            'DELIVERY' => array('ADRESS')
                        ),
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_FIRST_PLACE'),
                        'TAG' => 'select',
                        'TYPE' => '',
                        'DIRECTORY' => false,
                    ),
                    'ADRESS_CLIENT' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_FIRST_ADRESS_CLIENT'),
                        'TAG' => 'select',
                        'TYPE' => '',
                        'DIRECTORY' => true,
                    ),
                    'ADRESS_AUDIT_ROOM' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_FIRST_ADRESS_AUDIT_ROOM'),
                        'TAG' => 'select',
                        'TYPE' => '',
                        'DIRECTORY' => false,
                    ),
                    'COMMENT' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_COMMENT'),
                        'TAG' => 'textarea',
                        'TYPE' => ''
                    )
                ),
                'REGISTRY' => true,
                'WMS_STATUS' => array(),
            ),
            'delivery' => array(                                // Временный вывоз со склада
                'PROPERTY' => array(
                    'DATE' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_DELIVERY_DATE'),
                        'TAG' => 'input',
                        'TYPE' => 'date',
                    ),
                    'PLACE' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => array(
                            'PICKUP' => array('ADRESS_AUDIT_ROOM'),
                            'DELIVERY' => array('ADRESS_CLIENT')
                        ),
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_DELIVERY_PLACE'),
                        'TAG' => 'select',
                        'TYPE' => '',
                        'DIRECTORY' => false,
                    ),
                    'ADRESS_CLIENT' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_DELIVERY_ADRESS_CLIENT'),
                        'TAG' => 'select',
                        'TYPE' => '',
                        'DIRECTORY' => true,
                    ),
                    'ADRESS_AUDIT_ROOM' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_DELIVERY_ADRESS_AUDIT_ROOM'),
                        'TAG' => 'select',
                        'TYPE' => '',
                        'DIRECTORY' => false,
                    ),
                    'COMMENT' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_COMMENT'),
                        'TAG' => 'textarea',
                        'TYPE' => ''
                    )
                ),
                'REGISTRY' => true,                             // блок с реестром
                'WMS_STATUS' => array(                          // фильтрация по статусам WMS
                    'placed_storage' => true,
                    'placed_operation' => true,
                    //'delivered' => false,
                )
            ),
            'irreversible' => array(                                // Безвозвратное иъятие
                'PROPERTY' => array(
                    'DATE' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_DELIVERY_DATE'),
                        'TAG' => 'input',
                        'TYPE' => 'date',
                    ),
                    'PLACE' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => array(
                            'PICKUP' => array('ADRESS_AUDIT_ROOM'),
                            'DELIVERY' => array('ADRESS_CLIENT')
                        ),
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_DELIVERY_PLACE'),
                        'TAG' => 'select',
                        'TYPE' => '',
                        'DIRECTORY' => false,
                    ),
                    'ADRESS_CLIENT' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_DELIVERY_ADRESS_CLIENT'),
                        'TAG' => 'select',
                        'TYPE' => '',
                        'DIRECTORY' => true,
                    ),
                    'ADRESS_AUDIT_ROOM' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_DELIVERY_ADRESS_AUDIT_ROOM'),
                        'TAG' => 'select',
                        'TYPE' => '',
                        'DIRECTORY' => false,
                    ),
                    'COMMENT' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_COMMENT'),
                        'TAG' => 'textarea',
                        'TYPE' => ''
                    )
                ),
                'REGISTRY' => true,                             // блок с реестром
                'WMS_STATUS' => array(                          // фильтрация по статусам WMS
                    'placed_storage' => true,
                    'placed_operation' => true,
                    //'delivered' => false,
                )
            ),
            'reception_of_units' => array(                      // Возврат на склад
                'PROPERTY' => array(
                    'DATE' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_REC_DATE'),
                        'TAG' => 'input',
                        'TYPE' => 'date',
                    ),
                    'PLACE' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => array(
                            'PICKUP' => array('ADRESS_AUDIT_ROOM'),
                            'DELIVERY' => array('ADRESS_CLIENT')
                        ),
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_DELIVERY_PLACE'),
                        'TAG' => 'select',
                        'TYPE' => '',
                        'DIRECTORY' => false,
                    ),
                    'ADRESS_CLIENT' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_DELIVERY_ADRESS_CLIENT'),
                        'TAG' => 'select',
                        'TYPE' => '',
                        'DIRECTORY' => true,
                    ),
                    'ADRESS_AUDIT_ROOM' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_DELIVERY_ADRESS_AUDIT_ROOM'),
                        'TAG' => 'select',
                        'TYPE' => '',
                        'DIRECTORY' => false,
                    ),
                    'COMMENT' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_COMMENT'),
                        'TAG' => 'textarea',
                        'TYPE' => ''
                    )
                ),
                'REGISTRY' => true,                             // блок с реестром
                'WMS_STATUS' => array(                          // фильтрация по статусам WMS
                    'delivered' => true,
                )
            ),
            'destruction' => array(                             // Уничтожение
                'PROPERTY' => array(
                    'DATE' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_DESTRUCT_DATE'),
                        'TAG' => 'input',
                        'TYPE' => 'date',
                    ),
                    'COMMENT' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_COMMENT'),
                        'TAG' => 'textarea',
                        'TYPE' => ''
                    )
                ),
                'REGISTRY' => true,                             // блок с реестром
                'WMS_STATUS' => array(                          // фильтрация по статусам WMS
                    'placed_storage' => true,
                    'placed_operation' => true,
                    //'destructed' => false,
                )
            ),
            'checkout_out_expert' => array(                     // Выезд эксперта
                'PROPERTY' => array(
                    'DATE' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_CHECKOUT_DATE'),
                        'TAG' => 'input',
                        'TYPE' => 'date',
                    ),
                    'ADRESS_CLIENT' => array(
                        'REQUIRED' => true,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_CHECKOUT_ADRESS_CLIENT'),
                        'TAG' => 'select',
                        'TYPE' => '',
                        'DIRECTORY' => true,
                    ),
                    'COMMENT' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_COMMENT'),
                        'TAG' => 'textarea',
                        'TYPE' => ''
                    ),
                ),
                'REGISTRY' => false,                             // блок с реестром
                'WMS_STATUS' => array()                          // фильтрация по статусам WMS

            ),
            'supplies' => array(                                 // Доставка расходных материалов
                'PROPERTY' => array(
                    'DATE' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_SUPL_DATE'),
                        'TAG' => 'input',
                        'TYPE' => 'date',
                    ),
                    'TYPE_PRODUCT' => array(                                // Тип расходного материала
                        'REQUIRED' => true,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_SUPL_TYPE'),
                        'TAG' => 'select',
                        'TYPE' => '',
                        'DIRECTORY' => false,
                    ),
                    'COUNT_PRODUCT' => array(                               // Количество
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_SUPL_COUNT'),
                        'TAG' => 'input',
                        'TYPE' => 'text',
                        'DIRECTORY' => false,
                    ),
                    'ADRESS_CLIENT' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_SUPL_ADRESS_CLIENT'),
                        'TAG' => 'select',
                        'TYPE' => '',
                        'DIRECTORY' => true,
                    ),
                    'COMMENT' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_COMMENT'),
                        'TAG' => 'textarea',
                        'TYPE' => ''
                    )
                ),
                'REGISTRY' => false,                             // блок с реестром
                'WMS_STATUS' => array()                          // фильтрация по статусам WMS
            ),
            'arbitrarily' => array(                             // Произвольно
                'PROPERTY' => array(
                    'DATE' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_ARB_DATE'),
                        'TAG' => 'input',
                        'TYPE' => 'date',
                    ),
                    'COMMENT' => array(
                        'REQUIRED' => false,
                        'EXTERNAL' => false,
                        'DEPENDENCE' => false,
                        'LANG' => Loc::getMessage('U_HELP_PROPERTY_COMMENT'),
                        'TAG' => 'textarea',
                        'TYPE' => ''
                    )
                ),
                'REGISTRY' => false,                             // блок с реестром
                'WMS_STATUS' => array(),                         // фильтрация по статусам WMS


            ),
        );
        return $arTypeOfOrder[$iblockCode];
    }
    //endregion
    //region Возвращает массив значений свойства
    /**  Возвращает массив значений свойства
     * @param $iblockID - id инфоблока которому принадлежит свойство
     * @param $propertyID - id свойства типа список
     * @return array
     */
    public function getPropertyList($iblockID, $propertyID) {
        self::includeModule();
        $arResult = array();
        $arOrderProperty = array('sort' => 'asc', 'name' => 'asc');
        $arFilterProperty = array('IBLOCK_ID' => $iblockID);
        $rsProperty = CIBlockProperty::GetPropertyEnum($propertyID, $arOrderProperty, $arFilterProperty);
        while($obProperty = $rsProperty->Fetch()) {
            $arResult[] = array(
                'ID' => $obProperty['ID'],
                'VALUE' => $obProperty['VALUE'],
                'XML_ID' => $obProperty['XML_ID'],
                'SORT' => $obProperty['SORT'],
            );
        }
        return $arResult;
    }
    //endregion
    //region Проверка на вхождение в те свойства которые не должны выводиться с помощью ajax (являются общими для всех заказов)
    /** Проверка на вхождение в те свойства которые не должны выводиться с помощью ajax (являются общими для всех заказов)
     * @param $code - код свойства
     * @return bool
     */
    public function checkCodeForShow($code) {
        $arHide = array('COMMENT', 'STATUS', 'UNITS');
        if(in_array($code, $arHide)) {
            return true;
        } else {
            return false;
        }
    }
    //endregion
    //region Подключение необходимых модулей
    /**
     * Подключение необходимых модулей
     */
    public function includeModule() {
        Loader::includeModule('iblock');
        Loader::includeModule('highloadblock');
    }
    //endregion
    //region Возвращает следующий рабочий день
    /** Возвращает следующий рабочий день
     * @return false|string
     */
    public function getNextWorkDay() {
        $d = strtotime("+1 day");

        $ndFirst = date('d.m.Y', $d);

        $nextDay = date("D", $d);
        //region Если следующий день суббота
        if($nextDay == 'Sat') {
            $d = strtotime("+3 day");
        }
        //endregion
        //region Если следующий день воскресенье
        elseif($nextDay == 'Sun') {
            $d = strtotime("+2 day");
        }
        //endregion
        $arDate = array(
            'MONTH' => date('m', $d),
            'DAY' => date('d', $d),
            'YEAR' => date('Y', $d)
        );
        return $arDate;
    }
    //endregion
    //region Возвращает объект для работы с HL адресов
    /** Возвращает объект для работы с HL адресов
     * @return \Bitrix\Main\Entity\DataManager
     */
    public function getDataClassAdress() {
        $idBlockAdress = self::getIdHLBlock(HL_BLOCK_CODE_ADRESS_ALIAS);
        $dataClassAdress = self::getDataClass($idBlockAdress);
        return $dataClassAdress;
    }
    //endregion
    //region Возвращает объект для работы с HL блоком по его ID
    /** Возвращает объект для работы с HL блоком по его ID
     * @param $idHLBlock
     * @return \Bitrix\Main\Entity\DataManager
     */
    private function getDataClass($idHLBlock) {
        self::includeModule();
        //$rsHLBlock = HL\HighloadBlockTable::getList(array('filter' => array('NAME' => HL_BLOCK_CODE_ADRESS_ALIAS)))->fetchRaw();
        $hlblock = HL\HighloadBlockTable::getById($idHLBlock)->fetch();
        $dataClass = HL\HighloadBlockTable::compileEntity($hlblock)->getDataClass();
        return $dataClass;
    }
    //endregion
    //region Возвращает id HL блока
    /** Возвращает id HL блока
     * @param $code - код HL блока
     * @return mixed
     */
    public function getIdHLBlock($code) {
        $connection = Application::getConnection();
        $sql = "SELECT ID,NAME FROM b_hlblock_entity WHERE NAME='" . $code . "';";
        $rsHLBlock = $connection->query($sql)->fetch();
        return $rsHLBlock['ID'];
    }
    //endregion
}



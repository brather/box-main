<?php
$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_S'] = 'Строка';
$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_N'] = 'Число';
$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_L'] = 'Список';
$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_F'] = 'Файл';
$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_E'] = 'Привязка к элементам';
$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_G'] = 'Привязка к разделам';

$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_S_HTML'] = 'HTML/текст';
$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_S_VIDEO'] = 'Видео';
$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_S_DATE'] = 'Дата';
$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_S_DATE_TIME'] = 'Дата/Время';
$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_S_SASDPALETTE'] = 'Палитра';
$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_S_MAP_YANDEX'] = 'Привязка к Яндекс.Карте';
$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_S_MAP_GOOGLE'] = 'Привязка к карте Google Maps';
$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_S_USER_ID'] = 'Привязка к пользователю';
$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_S_TOPIC_ID'] = 'Привязка к теме форума';
$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_S_FILE_MAN'] = 'Привязка к файлу (на сервере)';
$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_S_ELEMENT_XML_ID'] = 'Привязка к элементам по XML_ID';
$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_S_SASDCHECKBOX'] = 'Простой чекбокс';
$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_S_DIRECTORY'] = 'Справочник';

$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_G_SECTION_AUTO'] = 'Привязка к разделам с автозаполнением';

$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_N_SASDSECTION'] = 'Привязка к собственной секции';
$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_N_SASDCHECKBOX_NUM'] = 'Простой чекбокс (число)';
$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_N_SEQUENCE'] = 'Счетчик';

$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_E_SKU'] = 'Привязка к товарам (SKU)';
$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_E_LIST'] = 'Привязка к элементам в виде списка';
$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_E_AUTO_COMPLETE'] = 'Привязка к элементам с автозаполнением';

$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_PLACEHOLDER_S'] = 'текст...';
$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_PLACEHOLDER_N'] = 'число...';

$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_PLACEHOLDER_S_DATE'] = 'выберите дату';
$MESS['U_ORDER_CREATE_HELP_INPUT_TYPE_PLACEHOLDER_S_DATE_TIME'] = 'выберите дату и время';

$MESS['U_HELP_PROPERTY_CONTRACT'] = 'Договор: ';
$MESS['U_HELP_PROPERTY_COMMENT'] = 'Комментарий: ';

$MESS['U_HELP_PROPERTY_DIGITIZATION_DATE'] = 'Дата оцифровки: ';
$MESS['U_HELP_PROPERTY_DIGITIZATION_UNITS'] = 'ЕУ на оцифровку: ';
$MESS['U_HELP_PROPERTY_DIGITIZATION_TYPE'] = 'Тип оцифровки: ';
$MESS['U_HELP_PROPERTY_DIGITIZATION_RESOLUTION'] = 'Разрешение: ';
$MESS['U_HELP_PROPERTY_DIGITIZATION_COLOR'] = 'Цветность: ';
$MESS['U_HELP_PROPERTY_DIGITIZATION_EXT'] = 'Расширение файла: ';

$MESS['U_HELP_PROPERTY_FIRST_DATE'] = 'Дата для забора ЕУ: ';
$MESS['U_HELP_PROPERTY_FIRST_PLACE'] = 'Место приема: ';
$MESS['U_HELP_PROPERTY_FIRST_ADRESS_CLIENT'] = 'Забор из офиса: ';
$MESS['U_HELP_PROPERTY_FIRST_ADRESS_AUDIT_ROOM'] = 'Комната аудита: ';

$MESS['U_HELP_PROPERTY_DELIVERY_DATE'] = 'Дата вывоза: ';
$MESS['U_HELP_PROPERTY_DELIVERY_PLACE'] = 'Место выдачи: ';
$MESS['U_HELP_PROPERTY_DELIVERY_ADRESS_CLIENT'] = 'Доставка по адресу: ';
$MESS['U_HELP_PROPERTY_DELIVERY_ADRESS_AUDIT_ROOM'] = 'Самовывозом из офиса: ';

$MESS['U_HELP_PROPERTY_REC_DATE'] = 'Дата возврата: ';
$MESS['U_HELP_PROPERTY_REC_PLACE'] = 'Место возврата: ';
$MESS['U_HELP_PROPERTY_REC_ADRESS_CLIENT'] = 'Вывоз курьером из: ';
$MESS['U_HELP_PROPERTY_REC_ADRESS_AUDIT_ROOM'] = 'Адрес склада: ';

$MESS['U_HELP_PROPERTY_DESTRUCT_DATE'] = 'Дата уничтожения: ';

$MESS['U_HELP_PROPERTY_CHECKOUT_DATE'] = 'Дата выезда эксперта: ';
$MESS['U_HELP_PROPERTY_CHECKOUT_ADRESS_CLIENT'] = 'Выезд по адресу: ';

$MESS['U_HELP_PROPERTY_SUPL_DATE'] = 'Дата доставки: ';
$MESS['U_HELP_PROPERTY_SUPL_TYPE'] = 'Тип расходников: ';
$MESS['U_HELP_PROPERTY_SUPL_COUNT'] = 'Количество расходников: ';
$MESS['U_HELP_PROPERTY_SUPL_ADRESS_CLIENT'] = 'Доставка по адресу: ';

$MESS['U_HELP_PROPERTY_ARB_DATE'] = 'Дата выполнения заказа: ';
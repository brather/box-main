<?php
$MESS['C_ORDER_CREATE_MESS_SUCCESS'] = 'Your order has been successfully added to the system. All orders can be viewed in the section ';
$MESS['C_ORDER_CREATE_MESS_SUCCESS_URI_TEXT'] = '&laquo;Orders and inquiries&raquo;';
$MESS['C_ORDER_CREATE_MESS_ERROR'] = 'Error: ';
$MESS['C_ORDER_CREATE_MESS_ERROR_ORDER_EXIST'] = 'Error: Order is exist!';
$MESS['C_ORDER_CREATE_MESS_ERROR_EMPTY_UNITS'] = 'Unspecified any accounting units of the registry. Please select units from the registry for your order.';


$MESS['C_ORDER_CREATE_ORDER'] = 'Order ';
$MESS['C_ORDER_CREATE_ORDER_FROM'] = ' from ';
$MESS['C_ORDER_CREATE_ORDER_OF'] = ' of contract ';

$MESS['C_ORDER_CODE_DIGITIZATION'] = 'digitization';
$MESS['C_ORDER_CODE_DELIVERY'] = 'delivery to client';
$MESS['C_ORDER_CODE_RECEPTION_OF_UNITS'] = 'reception to stock';
$MESS['C_ORDER_CODE_DESTRUCTION'] = 'destruction';
$MESS['C_ORDER_CODE_CHECKOUT_OUT_EXPERT'] = 'checkout of expert';
$MESS['C_ORDER_CODE_SUPPLIES'] = 'delivery supplies';
$MESS['C_ORDER_CODE_ARBITRARILY'] = 'arbitrarily';
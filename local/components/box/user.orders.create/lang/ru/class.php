<?php
$MESS['C_ORDER_CREATE_MESS_SUCCESS'] = 'Ваш заказ был успешно добавлен в систему. Весь список заказов Вы можете просмотреть в разделе ';
$MESS['C_ORDER_CREATE_MESS_SUCCESS_URI_TEXT'] = '&laquo;Заказы и запросы&raquo;';
$MESS['C_ORDER_CREATE_MESS_ERROR'] = 'Ошибка: ';
$MESS['C_ORDER_CREATE_MESS_ERROR_ORDER_EXIST'] = 'Ошибка: Заказ уже сформирован!';
$MESS['C_ORDER_CREATE_MESS_ERROR_EMPTY_UNITS'] = 'Не выбрано ни одной единицы учета (ЕУ) из реестра. Пожалуйста, выберите ЕУ из реестра для Вашего заказа.';

$MESS['C_ORDER_CREATE_ORDER'] = 'Формируемый заказ ';
$MESS['C_ORDER_CREATE_ORDER_FROM'] = ' от ';
$MESS['C_ORDER_CREATE_ORDER_OF'] = ' по договору ';

$MESS['C_ORDER_CODE_DIGITIZATION'] = 'на оцифровку';
$MESS['C_ORDER_CODE_DELIVERY'] = 'на временный вывоз со склада';
$MESS['C_ORDER_CODE_RECEPTION_OF_UNITS'] = 'на возврат на склад';
$MESS['C_ORDER_CODE_DESTRUCTION'] = 'на уничтожение';
$MESS['C_ORDER_CODE_CHECKOUT_OUT_EXPERT'] = 'на выезд эксперта';
$MESS['C_ORDER_CODE_SUPPLIES'] = 'на доставку расходных материалов';
$MESS['C_ORDER_CODE_ARBITRARILY'] = 'произвольный';
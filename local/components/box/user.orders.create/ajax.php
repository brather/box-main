<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 26.04.2017
 * Time: 11:16
 */

define('STOP_STATISTICS', true);
set_time_limit(0);

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use EME\ApplicationDestruction;
use EME\AuditRoom;
use EME\DepartureExpert;
use EME\Digitizing;
use EME\IncomeMu;
use EME\IssueMu;
use EME\Order;
use EME\Request;
use EME\ShippingSupplies;
use EME\WMS;
use EME\OrderData;

Loc::loadLanguageFile(__FILE__);

$rq = Application::getInstance()->getContext()->getRequest();

/* Выбросим если это не AJAX  и не POST запрос */
if (!$rq->isAjaxRequest() && !$rq->isPost()) die();

global $USER;

/* Так же выбросим если это не авторизированный пользователь */
if (!$USER->IsAuthorized()) die();

Loader::includeModule('iblock');

$arPost = $rq->getPostList()->toArray();

$order = new OrderData();

/* Constantly values from Order. Values not from POST body. */
$order->dateTimeOrder = date('c');
$order->clientId = OrderData::getClientId((int)$rq->getPost('contract'));

/* If clientId not integer return SOAP_ERROR for "Empty request" */
if (!$order->clientId) die(json_encode(array('SOAP_ERROR' => Loc::getMessage( "SOAP_ERROR_EMPTY" ))));

function toBool($s){return mb_strtolower($s) === "true"; };

foreach ($arPost as $key => $value) {

    switch ($key) {
        case 'orderNumber'  : $order->orderNumber   = (string)$value; break;
        case 'orderType'    :
            $order->orderType     = Order::getTypeOrder($value);
            break;
        case 'orderName'    : $order->orderName     = (string)$value; break;
        case 'listMesUnit'  : $order->listMeasUnit  = Order::getUnitsFromOrder((int)$rq->getPost('orderNumber')); break;
        case 'comment'      : $order->comment       = $value; break;
        case 'digitization'   : // is array | todo checked this data values
            $order->digitizing = new Digitizing();

            $order->digitizing->caseCount = 0; // todo Default 0, в форме нет такого значения
            $order->digitizing->pageCount = 0; // todo Default 0, в форме нет такого значения

            $order->digitizing->isScan = toBool($arPost['isScan']);
            $order->digitizing->isRecognition = toBool($arPost['isRecognition']);
            $order->digitizing->isIndex = toBool($arPost['isIndex']);

            if ($order->digitizing->isScan === true) {
                $order->digitizing->optionsScan = $arPost['optionsScan'];
            }
            break;
        case 'issueMu'      : // break if empty or false
            if (!$value) break;

            $order->issueMu = new IssueMu();
            $order->issueMu->placeIssue   = (string)$rq->getPost('placeIssue');
            $order->issueMu->dateIssue    = WMS::getXMLDateTime($rq->getPost('dateIssue'));
            $order->issueMu->addressIssue = IssueMu::getPlaceIssue((string)$rq->getPost('addressIssue'));
            if ($arPost['orderType']== 'irreversible') // Флаг для безвозвратного изъятия
                $order->issueMu->isUnitDecommission = true;
            break;
        case 'incomeMu'    : // break if empty or false
            if (!$value) break;

            $order->incomeMu = new IncomeMu();
            $order->incomeMu->qtyMu           = (int)$rq->getPost('qtyMu');
            $order->incomeMu->locationReceive = (string)$rq->getPost('locationReceive');
            $order->incomeMu->addressReceive  = (string)$rq->getPost('addressReceive');
            $order->incomeMu->dateReceive     = WMS::getXMLDateTime($rq->getPost('dateReceive'));
            break;
        case 'applicationDestruction' :  // is array | todo checked this data values
            $order->applicationDestruction = new ApplicationDestruction();
            foreach ( $value as $sKey => $sValue ) {

                switch ($sKey) {
                    case 'dateDestruction' : $order->applicationDestruction->dateDestruction = $sValue; break;
                    default: break;
                }
            } break;
        case 'departureExpert' : // is array | todo checked this data values
            $order->departureExpert = new DepartureExpert();
            foreach ( $value as $sKey => $sValue ) {

                switch ($sKey) {
                    case 'dateDeparture' : $order->departureExpert->dateDeparture = $sValue; break;
                    case 'addressDeparture' : $order->departureExpert->addressDeparture = (string)$sValue; break;
                    default: break;
                }
            } break;
        case 'shippingSupplies' : // is array | todo checked this data values
            $order->shippingSupplies = new ShippingSupplies();
            foreach ( $value as $sKey => $sValue ) {

                switch ($sKey) {
                    case 'listMaterial' : $order->shippingSupplies->listMaterial = (array)$sValue; break;
                    case 'dateShipping' : $order->shippingSupplies->dateShipping = $sValue; break;
                    case 'addressDeparture' : $order->shippingSupplies->addressDeparture = (string)$sValue; break;
                    default: break;
                }
            } break;
        case 'auditRoom' : // is array | todo checked this data values
            $order->auditRoom = new AuditRoom();
            foreach ( $value as $sKey => $sValue ) {

                switch ($sKey) {
                    case 'dateTimeAudience' : $order->shippingSupplies->dateTimeAudience = $sValue; break;
                    default: break;
                }
            } break;

        default: break;
    }
}

$client = new WMS(SOAP_TEST_MODE);
$request = new Request();
$request->orderData = $order;
$res = $client->LoadOrder($request);

$arJson = array();

if ($res->LoadOrderResult->state > 0) {
/**
 * EST	Превышено время ожидания ответа от WMS
 * ONE	Передаваемый номер заказа уже существует в БД WMS
 * UNF	В БД WMS не найдена ЕУ
 * ANF	Не заполнена структура для соответствующего типа заказа
 */
    switch ($res->LoadOrderResult->descriptionError) {
        case 'EST' : $arJson['SOAP_ERROR'] = Loc::getMessage("SOAP_ERROR_EST"); break;
        case 'ONE' : $arJson['SOAP_ERROR'] = Loc::getMessage( "SOAP_ERROR_ONE" ); break;
        case 'UNF' : $arJson['SOAP_ERROR'] = Loc::getMessage( "SOAP_ERROR_UNF" ); break;
        case 'ANF' : $arJson['SOAP_ERROR'] = Loc::getMessage( "SOAP_ERROR_ANF" ); break;
        default:     $arJson['SOAP_ERROR'] = Loc::getMessage( "SOAP_ERROR_EMPTY" ); break;
    }
}
else {

    $arJson['SOAP_RESPONSE'] = true;
}

die(json_encode($arJson));
<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Iblock\IblockTable,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Application;

class UserOrdersCreateComponent extends CBitrixComponent {
    //region Задание свойств класса
    private $page = 'template';                 // подключаемый шаблон
    private $prefix = 'F_UNIT_';                // префикс для наименования инпутов в форме
    private $arPost = array();                  // массив $_POST
    private $arGet = array();                   // массив $_GET
    private $orderClass = '';                   // класс для обработки заказа
    //endregion
    //region Подготовка входных параметров компонента
    /** Подготовка входных параметров компонента
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams) {
        return parent::onPrepareComponentParams($arParams);
    }
    //endregion
    //region Подключение файлов перевода
    /**
     * Подключение файлов перевода
     */
    public function onIncludeComponentLang() {
        Loc::loadMessages(__FILE__);
    }
    //endregion
    //region Выполнение компонента
    public function executeComponent() {
        $this->includeModule();                                 // Подключение модулей
        $this->getRequest();                                    // Получение данных request
        $this->getIBlockIdOrder();                              // Устанавливает свойству $iblockOrderID id инфоблока заказов
        $this->setUserId();                                     // Уточнение id пользователя
        $this->getContractsUser();                              // Возвращает список договоров пользователя
        $this->getListTypeOrder();                              // Возвращает список типов заказа
        $this->getOrder();
        if($this->arPost[$this->prefix . 'FORM'] == 'Y') {
            $this->createOrder();                               // Создание заказа
        }
        $this->checkOrder();
        $this->includeComponentTemplate($this->page);           // Подключение шаблона
    }
    //endregion
    //region Подключаем список модулей
    /**
     * Подключаем список модулей
     */
    private function includeModule() {
        Loader::includeModule('iblock');
    }
    //endregion
    //region Получение данных request
    /**
     * Получение данных request
     */
    private function getRequest() {
        $request = Application::getInstance()->getContext()->getRequest();
        $this->arGet = $request->getQueryList()->toArray();
        $this->arPost = $request->getPostList()->toArray();
    }
    //endregion
    //region Устанавливает свойству $iblockOrderID id инфоблока заказов
    /**
     * Устанавливает свойству $iblockOrderID id инфоблока заказов
     */
    private function getIBlockIdOrder() {
        $this->arResult['IBLOCK_ID_ORDERS'] = $this->getIBlockID($this->arParams['IBLOCK_CODE_ORDERS']);
    }
    //endregion
    //region Уточнение id пользователя
    /**
     * Уточнение id пользователя
     */
    private function setUserId() {
        if(!empty($this->arParams['USER_ID'])) {
            $this->arResult['USER_ID'] = $this->arParams['USER_ID'];
        } else {
            global $USER;
            $this->arResult['USER_ID'] = $USER->GetID();
        }
    }
    //endregion
    //region Возвращает список договоров пользователя
    /**
     * Возвращает список договоров пользователя
     */
    private function getContractsUser() {
        // Если присутствует параметр с кодами групп которым принадлежит польователь и он не является суперпользователем
        if(!empty($this->arParams['USER_GROUPS'])
            && !in_array(U_GROUP_CODE_CLIENT_S_USER, $this->arParams['USER_GROUPS'])) {
            $arFilterClients = array(
                'IBLOCK_CODE' => $this->arParams['IBLOCK_CODE_CLIENTS'],
                'PROPERTY_PROP_CLIENT' => $this->arParams['USER_ID']
            );
            $arSelectClients = array('ID', 'IBLOCK_ID', 'PROPERTY_PROP_REGISTRY');
            $rsClients = CIBlockElement::GetList(array('ID' => 'ASC'), $arFilterClients, false, false, $arSelectClients);
            $arRegistryList = array();
            while($obClients = $rsClients->Fetch()) {
                foreach($obClients['PROPERTY_PROP_REGISTRY_VALUE'] as $idRegistry) {
                    if(!in_array($idRegistry, $arRegistryList)) {
                        $arRegistryList[] = $idRegistry;
                    }
                }
            }
            $this->arResult['LIST_REGISTRY'] = $arRegistryList;
            $arFilterContracts = array(
                'IBLOCK_CODE' => $this->arParams['IBLOCK_CODE_CONTRACTS'],
                'PROPERTY_PROP_ID_REGISTRY' => $arRegistryList
            );
        } else {
            $arFilterContracts = array(
                'IBLOCK_CODE' => $this->arParams['IBLOCK_CODE_CONTRACTS'],
                'PROPERTY_PROP_SUPER_USER' => $this->arResult['USER_ID'],
                'ACTIVE' => 'Y'
            );
        }
        $arOrderContracts = array('NAME' => 'ASC');
        $arSelectContracts = array('ID', 'NAME', 'IBLOCK_ID', 'PROPERTY_PROP_ID_REGISTRY');
        $rsContracts = CIBlockElement::GetList($arOrderContracts, $arFilterContracts, false, false, $arSelectContracts);
        $arRegistryList = array();
        while($obContracts = $rsContracts->Fetch()) {
            $this->arResult['LIST_CONTRACTS'][$obContracts['ID']] = array(
                'ID' => $obContracts['ID'],
                'NAME' => $obContracts['NAME'],
                'CODE' => $obContracts['CODE'],
                'PROPERTY_PROP_ID_REGISTRY' => $obContracts['PROPERTY_PROP_ID_REGISTRY_VALUE'],
            );
            foreach($obContracts['PROPERTY_PROP_ID_REGISTRY_VALUE'] as $idRegistry) {
                if(!in_array($idRegistry, $arRegistryList)) {
                    $arRegistryList[] = $idRegistry;
                }
            }
        }
        $this->arResult['LIST_REGISTRY'] = $arRegistryList;
    }
    //endregion
    //region Устанавливает список типов заказа
    /**
     * Устанавливает список типов заказа
     */
    private function getListTypeOrder() {
        $this->arResult['PREFIX'] = $this->prefix;
        $this->arResult['PATH_TO_HELP'] = $this->getPath() . '/help/';
        $arOrderType = array('SORT' => 'ASC');
        $arFilterType = array(
            'IBLOCK_ID' => $this->arResult['IBLOCK_ID_ORDERS'],
            'CODE' => 'TYPE_ORDER'
            );
        $rsTypeOrder = CIBlockPropertyEnum::GetList($arOrderType, $arFilterType);
        while($obType = $rsTypeOrder->Fetch()) {
            $this->arResult['LIST_ORDER_TYPE'][] = $obType;
        }
    }
    //endregion

    //region Создание заказа
    /**
     * Создание заказа
     */
    private function createOrder() {
        $this->orderClass = new Orders();
        $forming = $this->orderClass->getCurentForming($this->arPost['ORDER_ID']);
        $arServer = Application::getInstance()->getContext()->getServer()->toArray();
        if($forming == 'Y') {
            $arProperties = $this->getPropertyFromIBlock();
            $el = new CIBlockElement;
            $arUpdateOrder = array(
                'MODIFIED_BY' => $this->arResult['USER_ID'],
                'NAME' => $this->arPost[$this->prefix . 'NAME'],
                'PROPERTY_VALUES' => $arProperties
            );
            $res = $el->Update($this->arPost['ORDER_ID'], $arUpdateOrder);
            if($res) {
                $this->arResult['MESSAGE'] = array(
                    'TEXT' => Loc::getMessage('C_ORDER_CREATE_MESS_SUCCESS')
                        . '<a href="' . $this->arParams['SEF_FOLDER'] . '">' . Loc::getMessage('C_ORDER_CREATE_MESS_SUCCESS_URI_TEXT') . '</a>',
                    'TYPE' => 'SUCCESS'
                );
                $this->orderClass->removeItemFromOrder('SAVE', $this->arPost[$this->prefix . 'TYPE_ORDER'], $this->arPost[$this->prefix . 'CONTRACT']);
                //header('Location: ' . $this->arParams['SEF_FOLDER'] . $this->arPost['ORDER_ID'] . '/');
                LocalRedirect('/client/orders/' . $this->arPost['ORDER_ID'] . '/', false, '301');
            } else {
                $this->arResult['MESSAGE'] = array(
                    'TEXT' => Loc::getMessage('C_ORDER_CREATE_MESS_ERROR') . $el->LAST_ERROR,
                    'TYPE' => 'ERROR'
                );
            }
        } else {
            $this->arResult['MESSAGE'] = array(
                'TEXT' => Loc::getMessage('C_ORDER_CREATE_MESS_ERROR_ORDER_EXIST'),
                'TYPE' => 'ERROR'
            );
            header('Location: ' . $arServer['HTTP_ORIGIN'] . $this->arParams['SEF_FOLDER'] . $this->arPost['ORDER_ID'] . '/');
        }
    }
    //endregion
    //region Возвращает ID инфоблока по его коду
    /** Возвращает ID инфоблока по его коду
     * @param $code - символьный код инфоблока
     * @return mixed - id инфоблока
     */
    private function getIBlockID($code) {
        $arIBlock = IblockTable::getList(array(
            'filter' => array('CODE' => $code),
            'select' => array('ID')
        ))->fetch();
        return $arIBlock['ID'];
    }
    //endregion
    //region Сопоставляет свойства инфоблока и даные от формы
    /**
     * Сопоставляет свойства инфоблока и даные от формы
     */
    private function getPropertyFromIBlock() {
        $arReturn = array();
        $iblockID = $this->arResult['IBLOCK_ID_ORDERS'];
        $orderId = $this->arPost['ORDER_ID'];
        $arOrderProperty = array('sort' => 'asc');
        $arFilterProperty = array('IBLOCK_ID' => $iblockID);
        $rsProperty = CIBlockProperty::GetList($arOrderProperty, $arFilterProperty);
        //$orderClass = new Orders();

        while($obProperty = $rsProperty->Fetch()) {
            $dPost = $this->prefix . $obProperty['CODE'];
            if(!empty($this->arPost[$dPost])) {
                //region Определяет свойство, что заказ не формирующийся
                if($obProperty['CODE'] == 'FORMING') {
                    $arProperty = $this->orderClass->getValuesForming();
                    $arReturn['FORMING'] = $arProperty['N']['ID'];
                }
                //endregion
                //region Определяет запрос от пользователя
                elseif($obProperty['CODE'] == 'REQUEST') {
                    $arProperty = $this->orderClass->getValuesRequest();
                    $arReturn['REQUEST'] = $arProperty[$this->arPost[$dPost]]['ID'];
                }
                //endregion
                //region Тип заказа
                elseif($obProperty['CODE'] == 'TYPE_ORDER') {
                    $arProperty = $this->orderClass->getValuesTypeOrder();
                    $arReturn['TYPE_ORDER'] = $arProperty[$this->arPost[$dPost]]['ID'];
                }
                //endregion
                //region ЕУ
                elseif($obProperty['CODE'] == 'UNITS') {
                    $arReturn['UNITS'] = $this->orderClass->getListUnitsFromOrder($orderId);
                }
                //endregion
                //region Список
                elseif($obProperty['PROPERTY_TYPE'] == 'L') {
                    $arProperty = $this->getPropertyEnum($iblockID, $obProperty['CODE']);
                    $arReturn[$obProperty['CODE']] = $arProperty[$this->arPost[$dPost]];
                }
                //endregion
                //region Справочник
                elseif($obProperty['USER_TYPE'] == 'directory') {
                    $arReturn[$obProperty['CODE']] = $this->arPost[$dPost];
                }
                //endregion
                //region Дата оформления
                elseif($obProperty['CODE'] == 'DATE_OF_ISSUE') {
                    global $DB;
                    $arReturn['DATE_OF_ISSUE'] = date($DB->DateFormatToPHP(CSite::GetDateFormat('FULL')), time());
                }
                //endregion
                //region Дата
                elseif($obProperty['CODE'] == 'DATE') {
                    $format = 'SHORT';
                    if($obProperty['USER_TYPE'] == 'DateTime') {
                        $format = 'FULL';
                    }
                    $lang = Application::getInstance()->getContext()->getLanguage();
                    $date = ConvertTimeStamp(strtotime($this->arPost[$dPost]), $format, $lang);
                    $arReturn['DATE'] = $date;
                }
                //endregion
                //region Остальное
                else {
                    $arReturn[$obProperty['CODE']] = $this->arPost[$dPost];
                }
                //endregion
            }
            else {
                //region Определяет свойство, что заказ не формирующийся
                if($obProperty['CODE'] == 'FORMING') {
                    $arProperty = $this->orderClass->getValuesForming();
                    $arReturn['FORMING'] = $arProperty['N']['ID'];
                }
                //endregion
                //region Дата оформления
                elseif($obProperty['CODE'] == 'DATE_OF_ISSUE') {
                    global $DB;
                    $arReturn['DATE_OF_ISSUE'] = date($DB->DateFormatToPHP(CSite::GetDateFormat('FULL')), time());
                }
                //endregion
            }
        }

        return $arReturn;
    }
    //endregion
    //region Возвращает массив со списком значений свойства типа список
    /** Возвращает массив со списком значений свойства типа список
     * @param $iblockID - ID инфоблока
     * @param $propertyCode - код свойства
     * @return array - массив в котором ключом является XML_ID, а значением ID
     */
    private function getPropertyEnum($iblockID, $propertyCode) {
        $arReturn = array();
        $arOrderProperty = array('sort' => 'asc');
        $arFilterProperty = array('IBLOCK_ID' => $iblockID);
        $rsProperty = CIBlockProperty::GetPropertyEnum($propertyCode, $arOrderProperty, $arFilterProperty);
        while($obProperty = $rsProperty->Fetch()) {
            $arReturn[$obProperty['XML_ID']] = $obProperty['ID'];
        }
        return $arReturn;
    }
    //endregion
    //region Возвращает массив с id инфоблока и id элемента из данных arPost
    /** Возвращает массив с id инфоблока и id элемента из данных arPost
     * @param $dataUnit - строка вида UNIT_ID-#element_id#-IBLOCK_ID-#iblock_id#
     * @return array
     */
    private function getArUnit($dataUnit) {
        $preffixUnitID = 'UNIT_ID-';
        $preffixIBlockID = '-IBLOCK_ID-';
        $nUnitData = str_replace($preffixUnitID, '', $dataUnit);

        $unitID = substr($nUnitData, 0, strpos($nUnitData, $preffixIBlockID));
        $iblockID = substr($dataUnit, strpos($dataUnit, $preffixIBlockID) + strlen($preffixIBlockID));
        return $arReturn = array('IBLOCK_ID' => $iblockID, 'UNIT_ID' => $unitID);
    }
    //endregion
    //region Проверяет наличие уже формируемых заказов и ссылок на них
    /**
     * Проверяет наличие уже формируемых заказов и ссылок на них
     */
    private function checkOrder() {
        $orderClass = new Orders();
        $arListContracts = array_keys($this->arResult['LIST_CONTRACTS']);
        $this->arResult['LIST_FORM_ORDERS'] = $orderClass->getListFormingOrders($arListContracts);
        if(count($this->arResult['LIST_FORM_ORDERS']) > 0) {
            foreach($this->arResult['LIST_FORM_ORDERS'] as $iOrder => $arOrder) {
                $this->arResult['LIST_FORM_ORDERS'][$iOrder]['NAME'] = Loc::getMessage('C_ORDER_CREATE_ORDER')
                    . '<b>' . $arOrder['NAME'] . '</b>' . Loc::getMessage('C_ORDER_CREATE_ORDER_OF')
                    . $this->arResult['LIST_CONTRACTS'][$arOrder['PROPERTY_CONTRACT']]['NAME'];
                $this->arResult['LIST_FORM_ORDERS'][$iOrder]['URL'] = $this->arParams['SEF_FOLDER']
                    . 'create/?'
                    //. 'F_UNIT_FILTER=Y'
                    //. '&F_UNIT_CONTRACT=' . $arOrder['PROPERTY_CONTRACT']
                    //. '&F_UNIT_TYPE_ORDER=' . $arOrder['PROPERTY_TYPE_ORDER']
                    //. '&'
                    . 'ORDER_ID=' . $arOrder['ID'];
            }
        }
    }
    //endregion
    //region Если переход осуществляется на страницу черновика с уже имеющимся id заказа / запроса то возвращаем его свойства
    /**
     * Если переход осуществляется на страницу черновика с уже имеющимся id заказа / запроса то возвращаем его свойства
     */
    private function getOrder() {
        $this->arResult['ORDER'] = array();
        if(!empty($this->arGet['ORDER_ID'])) {
            $arOrder = array('ID' => 'ASC');
            $arFilterOrder = array(
                'IBLOCK_CODE' => $this->arParams['IBLOCK_CODE_ORDERS'],
                'ID' => $this->arGet['ORDER_ID']
            );
            $arSelectOrder = array(
                'ID',
                'NAME',
                'IBLOCK_ID',
                'PROPERTY_UNITS',
                'PROPERTY_COUNT_OF_UNITS',
                'PROPERTY_TYPE_ORDER',
                'PROPERTY_CONTRACT'
            );
            $obOrder = CIBlockElement::GetList($arOrder, $arFilterOrder, false, false, $arSelectOrder)->Fetch();
            $existOrder = false;
            if($obOrder) {
                $this->arResult['ORDER'] = array(
                    'ID' => $obOrder['ID'],
                    'NAME' => $obOrder['NAME'],
                    'IBLOCK_ID' => $obOrder['IBLOCK_ID'],
                    'PROPERTY_UNITS' => $obOrder['PROPERTY_UNITS_VALUE'],
                    'PROPERTY_COUNT_OF_UNITS' => $obOrder['PROPERTY_COUNT_OF_UNITS_VALUE'],
                    'PROPERTY_CONTRACT' => $obOrder['PROPERTY_CONTRACT_VALUE'],
                );
                foreach($this->arResult['LIST_ORDER_TYPE'] as $iType => $arType) {
                    if($arType['ID'] == $obOrder['PROPERTY_TYPE_ORDER_ENUM_ID']) {
                        $this->arResult['ORDER']['PROPERTY_TYPE_ORDER'] = $arType['XML_ID'];
                        break;
                    }
                }
                $existOrder = true;
            }
            if(!$this->checkContractUser($this->arResult['ORDER']['PROPERTY_CONTRACT']) || !$existOrder) {
                $arServer = Application::getInstance()->getContext()->getServer()->toArray();
                header('Location: ' . $arServer['HTTP_ORIGIN'] . $this->arParams['SEF_FOLDER']);
            }
        }
    }
    //endregion
    //region Проверяет договор, к которому относится заказ, на возможность обработки пользователем
    /** Проверяет договор, к которому относится заказ, на возможность обработки пользователем
     * @param $contractId - id договора
     * @return bool
     */
    private function checkContractUser($contractId) {
        $sUserId = $this->arParams['USER_ID'];
        // Если пользователь не входит в группу суперпользователей
        if(!in_array(U_GROUP_CODE_CLIENT_S_USER, $this->arParams['USER_GROUPS'])) {
            $arOrderClient = array('ID' => 'ASC');
            $arFilterClient = array('IBLOCK_CODE' => $this->arParams['IBLOCK_CODE']);
            $arSelectClient = array('ID', 'NAME', 'IBLOCK_ID', 'PROPERTY_PROP_CLIENTS_OF_SUPERUSER');
            $rsClient = CIBlockElement::GetList($arOrderClient, $arFilterClient, false, false, $arSelectClient);
            if($obClient = $rsClient->Fetch()) {
                $sUserId = $obClient['PROPERTY_PROP_CLIENTS_OF_SUPERUSER_VALUE'];
            }
        }
        $arOrderContract = array('ID' => 'ASC');
        $arFilterContract = array('ID' => $contractId, 'PROPERTY_PROP_SUPER_USER' => $sUserId);
        $arSelectContract = array('ID', 'NAME', 'IBLOCK_ID');
        $rsContract = CIBlockElement::GetList($arOrderContract, $arFilterContract, false, false, $arSelectContract);
        if($obContract = $rsContract->Fetch()) {
            return true;
        } else {
            return false;
        }
    }
    //endregion
}
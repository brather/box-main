<?php
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
require_once ($_SERVER['DOCUMENT_ROOT'] . $_SESSION['AR_RESULT']['PATH_TO_HELP'] . 'help.php');

use \Bitrix\Main\Application,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Highloadblock as HL;

Loc::loadLanguageFile(__FILE__);

$context = Application::getInstance()->getContext();
$docRoot = $context->getServer()->getDocumentRoot();
$request = $context->getRequest();
$arPost = $request->getPostList()->toArray();

$arGet = $_SESSION['AR_RESULT']['GET'];
/*foreach($arGet as $cGet => $vGet) {
    $arGet[$cGet] = iconv('utf-8', 'cp1251', $vGet);
}*/
//$arPost = $_GET;

Loader::includeModule('iblock');

$iInput = 1;                                        // Количество блоков с input сразу показываемых пользователю
$iInputToggle = 1;                                  // Количество блоков с input скрытых от пользователя
$html = '';                                         // Сразу показываемый список свойств
$htmlToggle = '';                                   // Список свойств временно скрытый от пользователя
$htmlRowTextarea = '';                              // html блока с текстовым полем

//region Если запрос формы пришел с указанием ID Черновика
if (isset($arGet['ORDER_ID']) && $arGet['ORDER_ID'] > 0)
{
    $db = CIBlockElement::GetProperty(6, (int)$arGet['ORDER_ID']);
    $arOrderProps = array();

    while ($arOrderProp = $db->Fetch())
    {
        $arOrderProps[ $arOrderProp['CODE'] ] = $arOrderProp;
    }
    unset($db, $arOrderProp);
}
//endregion

if(!empty($arPost['TYPE_ID']))
{
    /* true если нужно заполнять данными Заказ */
    $putValue = isset($arOrderProps) && count($arOrderProps) > 0;
    $orderHelp = new UserOrdersCreateHelp();
    $dataClass = $orderHelp->getDataClassAdress();
    $arOrderType = $orderHelp->checkProperty($arPost['TYPE_ID']);
    if(!empty($arOrderType)) {
        $arFieldsToggle = array();                          // массив с кодами свойств которые вначале скрыты от пользователя
        $countProperty = count($arOrderType['PROPERTY']);   // количество свойств типа заказа
        foreach($arOrderType['PROPERTY'] as $cProperty => $arProperty) {
            //region Определяем обязательность к заполнению свойства
            $required = '';
            if($arProperty['REQUIRED']) {
                $required = ' required data-required="Y"';
            }
            //endregion
            //region Если есть свойства зависящие от данного то заведем их в массив
            $clsDependence = ''; // класс определяющий есть ли у свойства внутренние зависимости
            if(!empty($arProperty['DEPENDENCE'])) {
                $clsDependence = ' js-dependence';
                //region если зависимостей внутренних нет, то заполняем массив для обязательных полей для конкретного свойства
                if($arProperty['EXTERNAL']) {
                    foreach($arProperty['DEPENDENCE'] as $cDep) {

                    }
                }
                //endregion
                //region если зависимости внутренние имеются то заполним массив с ними
                else {
                    foreach($arProperty['DEPENDENCE'] as $vProperty => $arDependenceField) {
                        foreach($arDependenceField as $cField) {
                            $arFieldsToggle[$cField] = $vProperty;
                        }
                    }
                }
                //endregion
            }
            //endregion
            //region Устанавливаем значения для блоков по умочанию
            $clsHidden = '';        // класс для скрытия свойства
            $dDepencive = '';       // параметр определяющий блок который скрыт
            $htmlTmp = '';          // временный html
            $iTmp = $iInput;        // номер блока
            $showText = true;       // параметр отвечающий за расположение input в блоке и показ текста описания
            //endregion
            //region Если текущее свойство уже является зависимым от другого
            if(!empty($arFieldsToggle[$cProperty])) {
                $iTmp = $iInputToggle;
                $clsHidden = ' hidden';
                $dDepencive = ' data-dependence="' . $arFieldsToggle[$cProperty] . '"';
            }
            //endregion
            //region Создание временного html
            if($arProperty['TYPE'] == 'hidden' || $arProperty['TAG'] == 'textarea') {
                $showText = false;
            }
            else {
                if($iTmp == 1) {
                    $htmlTmp .= '<div class="form__row ajax-data">';
                }
                $htmlTmp .= '
                    <div class="form__row__col _size6' . $clsHidden . '"' . $dDepencive . '>
                        <div class="form__row__name">'
                    . $arProperty['LANG'] .
                    '</div>';
            }
            if($arProperty['TAG'] == 'input') {
                //region Если тип input дата то определим доступное календарное число для заказа
                $addClass = '';
                $typeInput = $arProperty['TYPE'];
                if($arProperty['TYPE'] == 'date') {
                    $typeInput = 'text';
                    $addClass = ' js-datepicker';
                }
                //endregion
                $htmlTmp .= '<input type="' . $typeInput . '" 
                             ' . $required . ' 
                             value="'. ((isset($arOrderProps[$cProperty])) ? $arOrderProps[$cProperty]['VALUE'] : "") .'" 
                             class="form__field' . $addClass . '" 
                             name="' . $arPost['PREFIX'] . $cProperty . '" 
                             placeholder="' . Loc::getMessage('U_ORDER_CREATE_AJAX_PLACEHOLDER_' . $cProperty) . '">';
            }
            elseif($arProperty['TAG'] == 'select') {
                //region Если свойство является справочником
                if($arProperty['DIRECTORY']) {
                    //region Возвращаем id организации по названию договора который выбрал пользователь
                    $arFilterContract = array('IBLOCK_CODE' => IBLOCK_CODE_CONTRACTS, 'ID' => $arPost['CONTRACT']);
                    $arSelectContract = array('NAME', 'IBLOCK_ID', 'ID', 'PROPERTY_PROP_COMPANY');
                    $arContract = CIBlockElement::GetList(array(), $arFilterContract, false, false, $arSelectContract)->Fetch();
                    $companyID = $arContract['PROPERTY_PROP_COMPANY_VALUE'];
                    //endregion
                    //region вернем адреса компании из highload блока
                    $arFilterAdress = array('UF_COMPANY' => $companyID);
                    $arAliasAdress = $dataClass::getList(array(
                        'filter' => $arFilterAdress,
                        'order'  => array('UF_NAME' => 'ASC')
                    ));
                    //endregion
                    //region Сформируем html спика
                    $htmlTmp .= '<select name="' . $arPost['PREFIX'] . $cProperty . '" ' . $required . ' id="prop-' . $cProperty . '">
                                    <option value="">' . Loc::getMessage('U_ORDER_CREATE_AJAX_EMPTY') . '</option>';
                    while ($arAlias = $arAliasAdress->fetch()) {
                        $htmlTmp .= '<option value="' . $arAlias['UF_XML_ID'] . '" ';
                        $htmlTmp .= (isset($arOrderProps[$cProperty]) && $arOrderProps[$cProperty]['VALUE'] == $arAlias['UF_XML_ID']) ? 'selected' :"";
                        $htmlTmp .='>' . $arAlias['UF_NAME'] . '</option>';
                    }
                    $htmlTmp .= '</select>';
                    //endregion
                }
                //region Если свойство обычный список
                else {
                    $htmlTmp .= '<select class="' . $clsDependence . '" name="' . $arPost['PREFIX'] . $cProperty . '" ' . $required . ' id="prop-' . $cProperty . '">
                                    <option value="">' . Loc::getMessage('U_ORDER_CREATE_AJAX_EMPTY') . '</option>';
                    $arPropertyList = $orderHelp->getPropertyList($_SESSION['AR_RESULT']['IBLOCK_ID_ORDERS'], $cProperty);
                    foreach($arPropertyList as $iProperty => $arProp) {
                        $htmlTmp .= '<option value="' . $arProp['XML_ID'] . '" ';
                        $htmlTmp .= (isset($arOrderProps[$cProperty]) && $arOrderProps[$cProperty]['VALUE_XML_ID'] == $arProp['XML_ID']) ? 'selected' :"";
                        $htmlTmp .= '>' . $arProp['VALUE'] . '</option>';
                    }
                    $htmlTmp .= '</select>';
                }
                //endregion
                //endregion
            }
            elseif($arProperty['TAG'] == 'textarea') {
                $htmlRowTextarea .= '
                    <div class="form__row__col _size3">
                        <div class="form__row__name">' . $arProperty['LANG'] . '</div>
                        <textarea ' . $required . ' 
                                    class="form__field' . $addClass . '" 
                                    name="' . $arPost['PREFIX'] . $cProperty . '" 
                                    cols="30" 
                                    rows="5"
                                    placeholder="' . Loc::getMessage('U_ORDER_CREATE_AJAX_PLACEHOLDER_' . $cProperty) . '">' . ( (isset($arOrderProps[$cProperty]) && $arOrderProps[$cProperty]['VALUE'])? $arOrderProps[$cProperty]['VALUE'] : "" ) . '</textarea>
                    </div>';
            }
            if($showText) {
                $htmlTmp .= '</div>';
                if($iTmp % $_SESSION['AR_PARAMS']['COUNT_INPUT_IN_ROW'] == 0 || $countProperty == $iTmp) {
                    $htmlTmp .= '</div>';
                }
                if($iTmp % $_SESSION['AR_PARAMS']['COUNT_INPUT_IN_ROW'] == 0 && $countProperty > $iTmp) {
                    $htmlTmp .= '<div class="form__row ajax-data">';
                }
                if($clsHidden) {
                    $iInputToggle++;
                } else {
                    $iInput++;
                }
            }
            if($clsHidden) {
                $htmlToggle .= $htmlTmp;
            } else {
                $html .= $htmlTmp;
            }
            //endregion
        }
    }?>
    <div class="__js-contain">
        <div class="form__row">
            <?= $html ?>
        </div>
        <div class="form__row" id="html-hide">
            <?= $htmlToggle ?>
        </div>
        <div class="form__row">
            <?= $htmlRowTextarea ?>
        </div>
    </div>
    <div class="registry-list form__row">
        <?if($arOrderType['REGISTRY']) {
            $requireUnits = 'Y';
            $listRegistry = array();
            if(!empty($_SESSION['AR_RESULT']['LIST_REGISTRY'])) {
                $listRegistry = $_SESSION['AR_RESULT']['LIST_REGISTRY'];
            }
            $selectedUnits = array();
            if(!empty($_SESSION['AR_RESULT']['SELECTED_UNITS'])) {
                $selectedUnits = $_SESSION['AR_RESULT']['SELECTED_UNITS'];
            }
            global $APPLICATION;
            $APPLICATION->IncludeComponent(
                'box:user.registry.detail',
                'order.create',
                array(
                    'USER_ID' => $_SESSION['AR_RESULT']['USER_ID'],
                    'SEF_FOLDER' => $_SESSION['AR_PARAMS']['SEF_FOLDER'] . 'create/',
                    'CURRENT_PATH' => $_SESSION['AR_PARAMS']['CURRENT_PATH'] . 'create/',
                    'VARIABLES' => array(
                        'ID' => $_SESSION['AR_RESULT']['CONTRACTS'][$arPost['CONTRACT']]
                    ),
                    'PATH_TO_FILES' => '',
                    'LIST_REGISTRY' => $listRegistry,
                    'PREFIX_FOR_INPUT' => $_SESSION['AR_RESULT']['PREFIX_FOR_INPUT'],
                    'COUNT_IN_ROW_PARAMS' => $_SESSION['AR_PARAMS']['COUNT_INPUT_IN_ROW'],
                    'COUNT_ITEM_ON_PAGE' => $_SESSION['AR_PARAMS']['COUNT_ITEM_ON_PAGE'],
                    'NAV_TEMPLATE' => $_SESSION['AR_PARAMS']['NAV_TEMPLATE'] . '.order',
                    'NAV_ADD_OPTIONS' => array(),
                    'GET_PARAMS' => $_SESSION['AR_RESULT']['GET'],
                    'SET_SORT_FILTER' => array(
                        'F_UNIT_FILTER' => 'Y',
                        'F_UNIT_TYPE_ORDER' => $arPost['TYPE_ID'],
                        'F_UNIT_CONTRACT' => $arPost['CONTRACT'],
                    ),
                    'GET_ALL_EU_WITH_WMS' => true, // Получить все ЕУ в не зависимости от установленного статуса WMS
                    //'OPTION_FOR_FILTER' => $arOrderType['WMS_STATUS'],
                    'F_UNIT_TYPE_ORDER' => $arPost['TYPE_ID'],
                    'F_UNIT_CONTRACT' => $arPost['CONTRACT'],
                    //'CODE_OPTIONS_TO_SHOW' => array(
                    //    'IBLOCK_NAME',
                    //    'NAME',
                        //'PROPERTY_PROP_SSCC',
                        //'PROPERTY_PROP_WMS_STATUS',
                    //),
                    'OPTIONS_TO_UNIT' => array(                                 // Опции для работы с ЕУ реестра
                        'DOWNLOAD' => false,                                    // Скачать
                        'VIEW' => false,                                        // Просмотреть
                        'EDIT' => false,                                        // Редактировать
                        'PRINT' => false,                                       // Распечатать
                        'DELETE' => false                                       // Удалить
                    ),
                    'SELECTED_UNITS' => $selectedUnits                        // массив с ЕУ которые необходимо исключить из подборки
                ));
        } else {
            $requireUnits = 'N';
        }?>
        <input type="hidden" name="<?= $arPost['PREFIX'] ?>REQUIRE_UNITS" value="<?= $requireUnits ?>" />
    </div>
<?}
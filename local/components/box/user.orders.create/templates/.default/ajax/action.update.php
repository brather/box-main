<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 24.03.2017
 * Time: 14:41
 */
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use Bitrix\Main\Application;
$rq = Application::getInstance()->getContext()->getRequest();

if (!$rq->isAjaxRequest())  die(json_encode(array('error' => 'Bad request')));
if (!$rq->isPost())         die(json_encode(array('error' => 'Bad method')));

$orderClass     = new Orders();
$arPost         = $rq->getPostList()->toArray();
$orderID        = (int)$arPost['ORDER_ID'];
$tmpOrderID     = $orderID;
$orderName      = $arPost['ORDER_NAME'];
$jsonRes        = array();

if ($orderID <= 0)          die(json_encode(array('error' => 'Invalid input values')));

// Order update, create draft order
$resUpdate = $orderClass->updateOrderAndDraft($arPost);

if ($resUpdate === false)
    $resUpdate = ['success' => 'N', 'msg' => 'Not fields to update'];
if ($resUpdate !== false && $resUpdate !== true)
    $resUpdate = ['success' => 'N', 'msg' => $resUpdate];

die(json_encode( $resUpdate ));
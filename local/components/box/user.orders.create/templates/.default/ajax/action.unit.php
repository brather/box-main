<?php
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use Bitrix\Main\Application;

$arPost = Application::getInstance()->getContext()->getRequest()->getPostList()->toArray();

function getUnitID($string) {
    $preffixUnitID = 'UNIT_ID-';
    $preffixIBlockID = '-IBLOCK_ID-';
    $nUnitData = str_replace($preffixUnitID, '', $string);
    $unitID = substr($nUnitData, 0, strpos($nUnitData, $preffixIBlockID));
    return $unitID;
}

$orderClass = new Orders();
$orderID = $arPost['ORDER_ID'];
$tmpOrderID = $orderID;
$orderName = $arPost['ORDER_NAME'];
// Если не в черновике заказа, то создаем заказ
if($orderID == 0) {
    if(empty($orderName)) {
        $orderName = $arPost['CONTRACT'] . '_' . date('dmyHis') . '_' . substr(strtoupper($arPost['TYPE_ORDER']), 0, 3);
    }
    $orderID = $orderClass->createOrder($orderName, $arPost['CONTRACT'], $arPost['TYPE_ORDER'], $arPost['ORDER_REQ']);
}

$arResult = array(
    'ADD' => 'N',
    'REMOVE' => 'N',
    'ORDER_NAME' => $orderName,
    'ORDER_ID' => $orderID
);

/* Был создан черновик */
if ($tmpOrderID == 0 && $tmpOrderID != $orderID )
    $arResult['ADD'] = 'Y';

$arUnitsID = array();
$actionUnits = '';

foreach($arPost['UNIT'] as $unitString => $action) {
    if($actionUnits == '') {
        $actionUnits = $action;
    }
    $arUnitsID[] = getUnitID($unitString);
}

if($actionUnits == 'ADD') {
    // Добавляем ЕУ в заказ
    $orderClass->addUnitToOrder($orderID, $arUnitsID);
    $arResult['ADD'] = 'Y';
} elseif($actionUnits == 'REMOVE') {
    $resAction = $orderClass->removeUnitsFromOrder($orderID, $arUnitsID);
    $arResult['REMOVE'] = $resAction;
}

die(json_encode($arResult));
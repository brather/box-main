<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

use \Bitrix\Main\Application;

$request = Application::getInstance()->getContext()->getRequest();
$arGet = $request->getQueryList()->toArray();

$_SESSION['AR_RESULT'] = array(
    'USER_ID' => $arResult['USER_ID'],
    'PREFIX_FOR_INPUT' => $arResult['PREFIX'],
    'PATH_TO_HELP' => $arResult['PATH_TO_HELP'],
    'GET' => $arGet,
    'IBLOCK_ID_ORDERS' => $arResult['IBLOCK_ID_ORDERS'],
    'LIST_REGISTRY' => $arResult['LIST_REGISTRY'],
    'SELECTED_UNITS' => $arResult['ORDER']['PROPERTY_UNITS']
);
$countItemOnPage = $arParams['COUNT_ITEM_ON_PAGE'];
if(!empty($arGet['COUNT_ON_PAGE']) && $arGet['COUNT_ON_PAGE'] != $countItemOnPage) {
    $countItemOnPage = $arGet['COUNT_ON_PAGE'];
}
$_SESSION['AR_PARAMS'] = array(
    'SEF_FOLDER' => $arParams['SEF_FOLDER'],
    'CURRENT_PATH' => $arParams['CURRENT_PATH'],
    'VARIABLES' => $arParams['VARIABLES'],
    'COUNT_ITEM_ON_PAGE' => $countItemOnPage,
    'NAV_TEMPLATE' => $arParams['NAV_TEMPLATE'],
    'COUNT_INPUT_IN_ROW' => $arParams['COUNT_INPUT_IN_ROW'],
);


foreach($arResult['LIST_CONTRACTS'] as $idContract => $arContract) {
    $_SESSION['AR_RESULT']['CONTRACTS'][$arContract['ID']] = $arContract['PROPERTY_PROP_ID_REGISTRY'];
}

//region Функция проверки необходимости установки значения из POST данных
/** Функция проверки необходимости установки значения из POST данных
 * @param $key - ключ для post данных проверяемого значения на проставление
 * @param $messageType - тип сообщения о добавлении
 * @return string
 */
function checkSetPostValue($key, $messageType) {
    $result = '';
    $request = Application::getInstance()->getContext()->getRequest();
    $arPost = $request->getPostList()->toArray();
    if(!empty($arPost[$key]) && $messageType == 'ERROR') {
        $result = $arPost[$key];
    }
    return $result;
}
//endregion
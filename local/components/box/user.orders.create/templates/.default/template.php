<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application;
global $APPLICATION;

$context = Application::getInstance()->getContext();
$request = $context->getRequest();
$arGet = $request->getQueryList()->toArray();
$arPost = $request->getPostList()->toArray();
$arServer = $context->getServer()->toArray();

$messageType = '';
if(!empty($arResult['MESSAGE'])) {
    $messageType = $arResult['MESSAGE']['TYPE'];
}
$orderId = 0;
if(!empty($arGet['ORDER_ID'])) {
    $orderId = $arGet['ORDER_ID'];
}
$flagToShowFormingOrders = false;?>

<div class="page__frame">
    <?if(!empty($arParams['HTML_PAGE_TITLE'])) {?>
        <?= urldecode($arParams['HTML_PAGE_TITLE']) ?>
    <?}?>
    <div class="order">
        <div class="order__col" id="contents">
            <div class="scrollbar-outer">
                <h2><?= Loc::getMessage('U_CREATE_ORDER_TITLE') ?><?= ($orderId > 0) ? " №" . $orderId : ""?></h2>
                <?if(!empty($arResult['LIST_FORM_ORDERS']) && $flagToShowFormingOrders) {?>
                    <?// Если формируемых заявок больше чем 1 или на данный момент не открыта ссылка с текущей одной заявкой
                    if(count($arResult['LIST_FORM_ORDERS']) == 1
                        && $arGet['F_UNIT_CONTRACT'] != $arResult['LIST_FORM_ORDERS'][0]['CONTRACT']
                        //&& $arGet['F_UNIT_TYPE_ORDER'] != $arResult['LIST_FORM_ORDERS'][0]['TYPE']
                        || count($arResult['LIST_FORM_ORDERS']) > 1
                        || count($arResult['LIST_FORM_ORDERS']) == 1 && empty($arGet)) {?>
                        <div class="form">
                            <div class="form__row">
                                <ul class="form__row__orders">
                                    <?foreach($arResult['LIST_FORM_ORDERS'] as $arOrder) {
                                        if($arGet['ORDER_ID'] != $arOrder['ID']) {?>
                                            <li><a href="<?= $arOrder['URL'] ?>"><?= $arOrder['NAME'] ?></a></li>
                                        <?}?>
                                    <?}?>
                                </ul>
                            </div>
                        </div>
                    <?}?>
                <?}?>
                <?$postfix = '';
                if(!empty($arServer['REDIRECT_QUERY_STRING'])){
                    $postfix = '?' . $arServer['REDIRECT_QUERY_STRING'];
                }?>
                <form action="<?= $arParams['SEF_FOLDER'] ?>create/<?= $postfix ?>" class="form"  method="post" name="createOrder">
                    <input type="hidden" name="ORDER_ID" value="<?= $orderId ?>" id="order-id">
                    <?if(!empty($arResult['MESSAGE'])) {?>
                        <div class="create-mess _size3 bgd-<?= strtolower($messageType) ?>">
                            <?= $arResult['MESSAGE']['TEXT'] ?>
                        </div>
                    <?}?>
                    <?$orderReq = 'N';
                    if(!empty($arParams['USER_GROUPS'])
                        && !in_array(U_GROUP_CODE_CLIENT_S_USER, $arParams['USER_GROUPS'])) {
                        $orderReq = 'Y';
                    }?>
                    <input type="hidden" name="<?= $arResult['PREFIX'] ?>REQUEST" value="<?= $orderReq ?>" id="order-request">
                    <input type="hidden" name="<?= $arResult['PREFIX'] ?>FORM" value="Y" />
                    <input type="hidden" name="<?= $arResult['PREFIX'] ?>STATUS" value="formation" />
                    <div class="form__row">
                        <div class="form__row__col _size6">
                            <?if(count($arResult['LIST_CONTRACTS']) == 1) {?>
                                <div class="form__row__name">
                                    <?$keys = array_keys($arResult['LIST_CONTRACTS']);
                                    $arContractUser = $arResult['LIST_CONTRACTS'][$keys[0]];
                                    echo Loc::getMessage('U_CREATE_ORDER_FORM_CONTRACT');?>
                                </div>
                                <div class="contract-title">
                                    <?= $arContractUser['NAME'] ?>
                                </div>
                                <input type="hidden"
                                       name="<?= $arResult['PREFIX'] ?>CONTRACT"
                                       value="<?= $arContractUser['ID'] ?>"
                                       id="order-contract">
                            <?} else {?>
                                <div class="form__row__name">
                                    <?= Loc::getMessage('U_CREATE_ORDER_FORM_CONTRACT_NAME') ?>
                                </div>
                                <select class="js-dependence-contracts"
                                        name="<?= $arResult['PREFIX'] ?>CONTRACT"
                                        id="order-contract">
                                    <option value=""><?= Loc::getMessage('U_CREATE_ORDER_FORM_VALUE_EMPTY') ?></option>
                                    <?
                                    foreach($arResult['LIST_CONTRACTS'] as $idContract => $arContract) {?>
                                        <?$select = '';
                                        if($arGet[$arResult['PREFIX'] . 'CONTRACT'] == $arContract['ID']
                                            || ($messageType == 'ERROR' && $arPost[$arResult['PREFIX'] . 'CONTRACT'] == $arContract['ID'])
                                            || count($arResult['LIST_CONTRACTS']) == 1
                                            || $arResult['ORDER']['PROPERTY_CONTRACT_VALUE'] == $arContract['ID']
                                            || $arGet['ORDER_ID'] && $arResult['ORDER']['PROPERTY_CONTRACT'] == $idContract) {
                                            $select = ' selected';
                                        }?>
                                        <option value="<?= $arContract['ID'] ?>" <?= $select ?>>
                                            <?= $arContract['NAME'] ?>
                                        </option>
                                    <?}?>
                                </select>
                            <?}?>
                        </div>
                        <div class="form__row__col hidden _size6 js-order-contract">
                            <div class="form__row__name">
                                <label for="order-name"><?= Loc::getMessage('U_CREATE_ORDER_FORM_ORDER_TITLE') ?></label>
                            </div>
                            <?if(!empty($arResult['ORDER']['NAME'])) {
                                $nameOrder = $arResult['ORDER']['NAME'];
                            } else {
                                $nameOrder = checkSetPostValue($arResult['PREFIX'] . 'NAME', $messageType);
                            }?>
                            <input type="text"
                                   value="<?= $nameOrder ?>"
                                   class="form__field"
                                   name="<?= $arResult['PREFIX'] ?>NAME"
                                   id="order-name">
                        </div>
                        <div class="form__row__col hidden _size6 js-order-contract">
                            <div class="form__row__name">
                                <?= Loc::getMessage('U_CREATE_ORDER_FORM_ORDER_TYPE') ?>
                            </div>
                            <select name="<?= $arResult['PREFIX'] ?>TYPE_ORDER"
                                    id="order-type"
                                    data-ajax="<?= $this->GetFolder() ?>" required>
                                <option value=""><?= Loc::getMessage('U_CREATE_ORDER_FORM_VALUE_EMPTY') ?></option>
                                <?foreach($arResult['LIST_ORDER_TYPE'] as $iIBlock => $arIBlock) {?>
                                    <?$select = '';
                                    if($arGet[$arResult['PREFIX'] . 'TYPE_ORDER'] == $arIBlock['XML_ID']
                                        || ($messageType == 'ERROR' && $arPost[$arResult['PREFIX'] . 'TYPE'] == $arIBlock['XML_ID'])
                                        || $arResult['ORDER']['PROPERTY_TYPE_ORDER'] == $arIBlock['XML_ID']) {
                                        $select = ' selected';
                                    }
                                    if($arIBlock['XML_ID'] != 'first_placement') {?>
                                        <option data-title="<?= $arIBlock['VALUE'] ?>"
                                                value="<?= $arIBlock['XML_ID'] ?>"<?= $select ?>>
                                            <?= $arIBlock['VALUE'] ?>
                                        </option>
                                    <?}?>
                                <?}?>
                            </select>
                        </div>
                    </div>
                    <div id="js-contain"></div>
                    <div class="form__row">
                        <div class="btn-group">
                            <button type="submit" name="create" class="btn btn-disabled" disabled="disabled"><?= Loc::getMessage('U_CREATE_ORDER_FORM_ORDER_BUTTON') ?></button>
                            <span class="btn _white action-save" data-href="/client/orders/drafts/"><?= Loc::getMessage('U_CREATE_ORDER_FORM_ORDER_BUTTON_SAVE') ?></span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

$(document).ready(function() {
    //region Задание переменных
    var ids = {
            content     : '#contents',
            select      : '#order-type',
            fullpage    : '#vertical',
            contain     : '#js-contain',
            contract    : '#order-contract',
            iname       : '#order-name',
            irequest    : '#order-request',
            otable      : '#order-table',
            iorderid    : '#order-id'
        },
        cls = {
            ajaxdata    : '.ajax-data',
            row         : '.form__row',
            container   : '.__js-contain',
            selectbox   : {
                contain : '.jq-selectbox',
                list    : '.jq-selectbox__dropdown',
                active  : '.selected'
            },
            form        : {
                select  : '.js-dependence',
                hide    : 'hidden',
                cont    : '.js-order-contract'
            },
            scroll      : {
                order   : '.order__col'
            },
            page        : {
                title   : '.page__title',
                blkorder: '.order'
            },
            registry    : {
                block   : '.registry-list'
            },
            checkbox    : {
                all     : '.js-table-checkbox-all',
                unit    : '.js-table-checkbox'
            }
        },
        heightTitle = $(ids.fullpage).find(cls.page.title).outerHeight(),
        marginOrder = getNumberFromString($(ids.fullpage).find(cls.page.blkorder).css('margin-top')),
        height = getPageSize().pageHeight - heightTitle - marginOrder,
        flagShowOrder = 0;
    //endregion

    //region Установка значения max-height для scrollbar
    $(cls.scroll.order).children('div').css('max-height', height);
    $(window).on('resize', function(){
        height = getPageSize().pageHeight - heightTitle - marginOrder;
        $(cls.scroll.order).children('div').css('max-height', height);
    });
    //endregion

    //region Выбор договора
    if($(ids.contract).val() != '') {
        loadTypeOrders($(ids.contract));
    }
    $(ids.contract).on('change', function(e){
        loadTypeOrders($(this));
    });
    //endregion

    //region Функция показа / скрытия списка с типом заказов
    function loadTypeOrders(obj) {
        var $this = $(obj),
            fClass = cls.form.cont,
            curVal = $(ids.select).val();
        if($this.val() != '') {
            if($this.val() != flagShowOrder) {
                flagShowOrder = $this.val();
                $(ids.content).find(fClass).removeClass(cls.form.hide);
                sendAjaxFromSelect($(ids.select));
            }
        } else {
            if(flagShowOrder) {
                flagShowOrder = false;
                $(ids.content).find(fClass).addClass(cls.form.hide);
                $(ids.select).val('').trigger('refresh');
                if(curVal != '') {
                    sendAjaxFromSelect($(ids.select));
                }
            }
        }
    }
    //endregion

    function sendWithAllowStatus(codeStatus){
        if (codeStatus === undefined) return false;
        var allowStatus = {
            arbitrarily: true,
            supplies: true,
            checkout_out_expert: true
        };

        return (allowStatus[codeStatus] === true);
    }

    //region Загрузка в форму через ajax блоков для заполнения
    $(ids.select).on('change', function(e){

        if (sendWithAllowStatus(this.value) === false){
            $('form[name=createOrder] button[name=create]')
                .attr('disabled', 'disabled')
                .addClass('btn-disabled');
        } else {
            $('form[name=createOrder] button[name=create]')
                .removeAttr('disabled')
                .removeClass('btn-disabled');
        }
        sendAjaxFromSelect($(this));
    });
    //endregion

    //region Функция отправки данных из select через ajax запрос
    function sendAjaxFromSelect(select) {
        var $this = select,
            url = $this.data('ajax') + '/ajax/get.form.php',
            prefix = 'F_UNIT_',
            clsTemp = 'temp-required',
            data = {
                CONTRACT        : $(ids.contract).val(),
                PREFIX          : prefix,
                TYPE_ID         : $this.val()
            },
            jsBContainer = $(ids.contain);

        $(ids.content).find('.' + clsTemp).attr('required', false).removeClass(clsTemp);
        startLoader(ids.content);
        jsBContainer.html('');

        if($this.val()) {
            $.ajax({
                method  : 'post',
                data    : data,
                url     : url,
                success : function(d) {
                    stopLoader();
                    jsBContainer.html(d);
                    $('select').styler();
                    $(document).find('.js-datepicker').each(function(i,y){
                        var dateMin = new Date(),
                            weekDays = AddWeekDays(2);

                        dateMin.setDate(dateMin.getDate() + weekDays);
                        $(y).datepicker({
                            inline              : true,
                            beforeShowDay       : noWeekendsOrHolidays,
                            dateFormat          : "dd.mm.yy",
                            firstDay            : 1,
                            showAnim            : "slide",
                            changeFirstDay      : false,
                            minDate             : dateMin,
                            showOtherMonths     : true,
                            selectOtherMonths   : true,
                            dayNamesShort       : $.datepicker.regional[ "ru" ].dayNamesShort,
                            dayNames            : $.datepicker.regional[ "ru" ].dayNames,
                            monthNamesShort     : $.datepicker.regional[ "ru" ].monthNamesShort,
                            monthNames          : $.datepicker.regional[ "ru" ].monthNames
                        });
                    });
                    toggleUnit({}, true);

                    if ($('select#prop-PLACE').val())
                        $('select#prop-PLACE').trigger('change');
                }
            });
        } else {
            stopLoader();
        }
    }
    //endregion

    //region Переключение между инпутами которые скрыты
    $(document).on('change', 'select' + cls.form.select, function(e){
        var $this = $(this),
            container = $(ids.contain),
            val = $this.val(),
            tmp = {
                act : 'js-temp',
                cur : 'tmp-current-' + Math.random().toFixed()
            };
        //region Установим временный класс для всех select зависящих от текущего
        $this.find('option').each(function(i,y) {
            if($(y).val() != '-') {
                var blkTmp = container.find('div[data-dependence="' + $(y).val() + '"]');
                blkTmp.addClass(tmp.cur);
                if(!blkTmp.hasClass(tmp.act)) {
                    blkTmp.find('select').attr('required', false);
                }
            }
        });
        //endregion

        //region Если есть уже активный блок select
        var blkActive = container.find('.' + tmp.act + '.' + tmp.cur),
            bShow = container.find('.' + tmp.cur + '[data-dependence="' + val + '"]'),
            dRequired = bShow.find('select').data('required'),
            activeDependence = blkActive.data('dependence');
        if(blkActive.length > 0) {
            // если устанавливаемое значение пустое / равное тире / неравное значению активного блока с select
            if(!val || val == '-' || activeDependence != val) {
                // отключаем обязательность у внутреннего селекта
                if(blkActive.find('select').is('required')) {
                    blkActive.find('select').attr('required', false).styler();
                }
                blkActive.addClass(cls.form.hide).removeClass(tmp.act);
                if(dRequired == 'Y') {
                    bShow.find('select').attr('required', true);
                }
                bShow.addClass(tmp.act).removeClass(cls.form.hide);
            }
        } else {
            if(bShow.length > 0) {
                if(dRequired == 'Y') {
                    bShow.find('select').attr('required', true);
                }
                bShow.addClass(tmp.act).removeClass(cls.form.hide);
            }
        }
        //endregion
        container.find('.' + tmp.cur).removeClass(tmp.cur);
    });
    //endregion

    //region Выделение всех строк таблицы
    $(document).on('click', '.js-table-checkbox-all', function(){
        var table = $(this).closest('table'),
            objsend = {};
        table.find('.js-table-checkbox').each(function(i,y){
            var value = $(y).val();
            if($(y)[0].checked) {
                objsend[value] = 'ADD';
            } else {
                objsend[value] = 'REMOVE';
            }
        });
        toggleUnit(objsend);
    });
    //endregion

    //region Выделение строки таблицы
    $(document).on('click', '.js-table-checkbox', function(){
        var value = $(this).val(),
            objsend = {};
        if($(this)[0].checked) {
            objsend[value] = 'ADD';
        } else {
            objsend[value] = 'REMOVE';
        }
        toggleUnit(objsend);
    });
    //endregion

    //region Сохрание частичного заказа
    $(document.forms['createOrder']).on('change', function () {
        /* Отменяем внесения изменений если ORDER_ID = 0 */
        if ( $(ids.iorderid).val() == 0 ) return false;

        var data = {},
            notEmpty = function (e) {
                return (e.value !== '');
            };

        //var checkboxCheched = {};
        $.each(this, function (v, o) {
            switch (o.type){
                case 'hidden':
                case 'text':
                case 'select-one':
                case 'textarea':
                    if (notEmpty(o))
                        data[o.name] = o.value;
                    break;
            }
        });

        updateOrder(data);
    });
    //endregion

    //region Событие нажатия на "Сохранить"
    $('span.action-save').on('click', function () {
        var dataHref = $(this).attr('data-href');
        startLoader($(ids.content));
        $(document.forms['createOrder']).trigger('change');
        setTimeout(function () {
            $('#blk-loader').find('i').removeClass('fa-pulse fa-spinner').addClass('fa-check');
            setTimeout(function () {
                stopLoader();
                $.confirm({
                    animation           : 'top',
                    closeAnimation      : 'bottom',
                    theme               : 'light',
                    backgroundDismiss   : true,
                    title               : "Сохранено как Черновик",
                    content: "Ваш заказ [<b>"+$(ids.iname).val()+"</b>] сохранен в разделе \"Черновики\". Вы можете закрыть эту страницу и вернуться к оформлению заказа позже. Для оформления заказа в работу необходимо нажать кнопку <b>\"Создать и отправить\"</b>",
                    buttons             : {
                        ok : {
                            keys    : ['enter', 'esc', 'space'],
                            text    : 'Понятно',
                            action  : function () {
                                window.location.href = dataHref;
                            }
                        }
                    }
                });
            }, 500)
        }, 500);
    });
    //endregion

    /* region Отправка данных для добавления в заказ пользователя параметров ЕУ */
    function toggleUnit(value, hideLoader) {
        var url = $(ids.select).data('ajax') + '/ajax/action.unit.php',
            data = {
                UNIT        : value,
                CONTRACT    : $(ids.contract).val(),
                TYPE_ORDER  : $(ids.select).val(),
                ORDER_ID    : $(ids.iorderid).val(),
                ORDER_REQ   : $(ids.irequest).val(),
                ORDER_NAME  : $(ids.iname).val()
            };
        $.ajax({
            method  : 'post',
            url     : url,
            dataType: 'json',
            data    : data,
            beforeSend: function () {
                if (!(hideLoader === true))
                    startLoader($(ids.otable));
            },
            success : function(d){
                var dataObj = d;

                /* Если заказ только что создан, подменим Заголовок */

                if ((data.ORDER_ID * 1)  === 0 && dataObj.ORDER_ID > 0) {

                    var url = location.pathname;

                    if (location.search === "") {
                        url += "?ORDER_ID=" + dataObj.ORDER_ID;
                    } else {
                        if (location.search.search(/ORDER_ID/i) === -1) {
                            url += location.search + "&ORDER_ID=" + dataObj.ORDER_ID;
                        } else {
                            url += location.search;
                        }
                    }

                    window.history.pushState({}, "", url);

                    $('#contents h2').text($('#contents h2').text() + " №" + dataObj.ORDER_ID);
                }

                $('#filter_eu').attr('action', '/client/orders/create/?ORDER_ID=' + dataObj.ORDER_ID);
                $('#filter_eu_reset').attr('href', '/client/orders/create/?F_UNIT_FILTER=Y&ORDER_ID=' + dataObj.ORDER_ID);

                if(dataObj.ADD == 'Y') {
                    $(ids.iorderid).val(dataObj.ORDER_ID);
                    $(ids.iname).val(dataObj.ORDER_NAME);

                } /*else if(dataObj.REMOVE == 'DELETE') {
                    $(ids.iorderid).val('');
                    $(ids.iname).val('');
                }*/

                /*if ((dataObj.REMOVE == 'UPDATE' || dataObj.REMOVE == 'N') && !$.isEmptyObject(data.UNIT)){
                        $('form[name=createOrder] button[name=create]')
                            .removeAttr('disabled')
                            .removeClass('btn-disabled');
                }*/



                checkFormBeforeCreateAndSend();

                if (!(hideLoader === true))
                    stopLoader();
            }
        });
    }
    //endregion

    //region  Обновление заказа в черновики
    function updateOrder(fieldsOrder){
        var success = true;

        $.ajax({
            url: $(ids.select).data('ajax') + '/ajax/action.update.php',
            data: fieldsOrder,
            method: 'post',
            dataType: 'json',
            beforeSend: function () {},
            success: function (json) {
                if (json !== true)
                    console.debug(json.error);

                checkFormBeforeCreateAndSend();

                return success;
            },
            error: function (xhr, st, msg) {
                console.error(st, msg);

                return false;
            }
        });
    }
    //endregion

    //region Проверка состояния формы для активации кнопки "Создать и отправить"
    function checkFormBeforeCreateAndSend() {
        // Attr disabled not present, button is active, return true
        //if ($('button[name=create]').attr('disabled') === undefined) return true;

        var doSave = true,
            requiredValues = {
            // false - не проверяем
            // true - обязательно
                F_UNIT_REQUEST      : false,
                F_UNIT_FORM         : false,
                F_UNIT_STATUS       : false, // Спорный вопрос ! (formation)

                ORDER_ID            : true,
                F_UNIT_TYPE_ORDER   : true,
                F_UNIT_CONTRACT     : true,
                F_UNIT_NAME         : true,
                F_UNIT_DATE         : true,
                F_UNIT_PLACE        : true,

                F_UNIT_ADRESS_CLIENT        : 'F_UNIT_ADRESS_AUDIT_ROOM',   // сравнительные
                F_UNIT_ADRESS_AUDIT_ROOM    : 'F_UNIT_ADRESS_CLIENT',       // сравнительные

                'F_UNIT_UNITS[]'    : [false]
            };

        //region Else check from values that enabled button
        // if Type order "delivery"
        if (document.getElementById('order-type').value === 'delivery' || document.getElementById('order-type').value === "") {

            $(document.forms['createOrder']).find('input, select').each(function (i) {

                if (doSave === true) {

                    if (requiredValues[this.name] === true) { // Boolev type is true

                        doSave = this.value.length > 0;
                    } else if (typeof (requiredValues[this.name]) === "string") { // String type

                        doSave = (this.value.length > 0 || document.getElementsByName(requiredValues[this.name])[0].value.length > 0 )
                    } else if (typeof (requiredValues[this.name]) === "object") { // Object type
                        if (this.checked === true) {
                            doSave = this.checked;
                            requiredValues[this.name] = [true]
                        }
                    } // All the rest is continue procedure
                }
            });

            if (doSave === true)
                doSave = requiredValues['F_UNIT_UNITS[]'][0];
        }
        //endregion

        if (doSave === true)
            $('form[name=createOrder] button[name=create]').removeAttr('disabled').removeClass('btn-disabled');
        else
            $('form[name=createOrder] button[name=create]').attr('disabled', 'disabled').addClass('btn-disabled');
    }
    //endregion

    checkFormBeforeCreateAndSend();

    $('button[name="create"]').on('click', function () {
        $(document).find('input[name=F_UNIT_STATUS]').val('processing');
    });

    $(document.forms.createOrder).on('submit', function(e) {
        var c        = e.currentTarget,
            pData = {},
            preData = {},
            mapArr   = {
                F_UNIT_CONTRACT:      "contract",
                ORDER_ID:             "orderNumber",
                F_UNIT_TYPE_ORDER:    "orderType",
                F_UNIT_NAME:          "orderName",
                F_UNIT_COMMENT:       "comment",
                F_UNIT_PLACE:         "placeIssue",
                F_UNIT_DATE:          "dateIssue",
                F_UNIT_ADRESS_CLIENT: "addressIssue"
            };

        var getDataForm = function(form) {
            var $form = $(form)[0],
                len = $form.length,
                returnData = {};

            for(var i = 0; i < len; i++) {
                returnData[$form[i].name] = $form[i].value;
            }

            return returnData;
        };

        $(c).serializeArray().map(function (v) {
            if (mapArr[v.name] !== undefined)
                pData[mapArr[v.name]] = v.value;

            if (v.name === 'F_UNIT_TYPE_ORDER') {
                switch (v.value) {
                    case 'digitization':
                        pData.digitization = true;
                        pData.listMesUnit = true;

                        var dF = getDataForm(c);
                        pData.isScan        = dF.F_UNIT_TYPE_DIGITIZATION === "SCAN";
                        pData.isRecognition = dF.F_UNIT_TYPE_DIGITIZATION === "RECOGNITION";
                        pData.isIndex       = dF.F_UNIT_TYPE_DIGITIZATION === "INDEX";
                        if (pData.isScan === true) {
                            pData.optionsScan = dF.F_UNIT_SCAN_COLOR+":"+dF.F_UNIT_SCAN_EXT+":"+dF.F_UNIT_SCAN_RESOLUTION;
                        }
                        /** Добавить когда будет необходимость входные параметры formatCaseIdentifier, optionsIndex, caseCount, pageCount */
                        break;
                    case 'first_placement':
                        pData.incomeMu = true;
                        break;
                    case 'irreversible':
                    case 'delivery':
                        pData.issueMu = true;
                        pData.listMesUnit = true;
                        break;
                    case 'destruction':
                        pData.applicationDestruction = true;
                        pData.listMesUnit = true;
                        break;
                    case 'checkout_out_expert': pData.departureExpert = true; break;
                    case 'supplies': pData.shippingSupplies = true; break;
                    default: break;
                }
            }
        });
        console.log(pData);
        $.ajax({
            url: '/local/components/box/user.orders.create/ajax.php',
            method: 'POST',
            dataType: 'json',
            data: pData,
            success: function(d){
                if (d.SOAP_ERROR !== undefined)
                    $.confirm(d.SOAP_ERROR);
                else if (d.SOAP_RESPONSE === undefined) {
                    $.confirm(d);
                } else {
                    if (confirm('Continue'))
                        c.submit();
                }
            }
        });

        return false;
    });

    onLoadTableOrder = function() {

        var unitOnPage = $('span[data-ui-id]'),
            obCodes = {},
            IBLOCK_ID = unitOnPage[0].dataset.iblockId;

        for (var i = 0; i < unitOnPage.length; i++) {
            obCodes[i] =unitOnPage[i].dataset.uiId;
        }

        $.ajax({
            url: '/local/components/box/user.registry.detail/templates/design.list/ajax/wmsstatus.php',
            data: {
                UI_IDS: obCodes,
                IBLOCK_ID: IBLOCK_ID
            },
            method: 'POST',
            dataType: 'json',
            success: function(myJson) {
                unitOnPage.map(function(k, v) {
                    $(v).removeClass('fa fa-icon fa-spin fa-spinner');
                    if (myJson[v.dataset.uiId].SOAP_ERROR) {
                        $(v).addClass('frn-danger').text(myJson[v.dataset.uiId].SOAP_ERROR);
                    } else {
                        $(v).text(myJson[v.dataset.uiId].SOAP_RESPONSE);
                    }
                });
            },
            error: function(xhs, st, msg){
                console.error(st, msg);
                unitOnPage.map(function(i,v){
                    $(v).removeClass('fa fa-icon fa-spin fa-spinner').addClass('frn-danger').text('Ошибка ! Попробуйте позже.');
                });
            }
        });
    };
});

//region Получаем для каждого ЕУ свой статус
/**
 *  Здесь понадобиться дополнительная проверка сравнивая
 *  тип заказа что бы понимать какие ЕУ можно отдавать на
 *  выбор с корректным статусом
 **/
function loaderStatusFromWMS(self) {
    var $this = $(self);
    $.ajax({
        url: '/local/components/box/user.registry.detail/templates/design.list/ajax/wmsstatus.php',
        data: {
            UI_ID: self.dataset.uiId,
            IBLOCK_ID: self.dataset.iblockId
        },
        method: 'POST',
        dataType: 'json',
        beforeSend: function() {
            $this.parents('tr').find('td.js-checkbox-td label.checkbox').hide();
        },
        success: function(myJson) {
            $this.removeClass('fa fa-icon fa-spin fa-spinner');
            if (myJson.SOAP_ERROR) {
                $this.parents('tr').addClass('not-available');
                $this.addClass('frn-danger').text(myJson.SOAP_ERROR);
            } else {
                $this.parents('tr').find('td.js-checkbox-td label.checkbox').show();
                $this.text(myJson.SOAP_RESPONSE);
            }
        },
        error: function(xhs, st, msg){
            console.debug(st, msg);
            $this.removeClass('fa fa-icon fa-spin fa-spinner').addClass('frn-danger').text('Ошибка ! Попробуйте позже.');
        }
    });
}
//endregion
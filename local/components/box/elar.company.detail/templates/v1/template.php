<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 07.04.2017
 * Time: 16:39
 */

use Bitrix\Main\Localization\Loc;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $APPLICATION;

$APPLICATION->SetTitle( Loc::getMessage( "E_COMPANY_PAGE_TITLE" ) . $arResult['COMPANY']['NAME']);
?>

<div class="content__item">
    <div class="page__title">
        <h1><?= Loc::getMessage( "E_COMPANY_BODY_TITLE" )?> — <?= $arResult['COMPANY']['NAME']?>, (ID <?= $arResult['COMPANY']['ID']?>)</h1>
    </div>
    <div class="scrollbar-outer">
        <div class="content__item__frame">
            <div class="form__table">
                <div class="form__table__col">
                    <div class="clearfix">&nbsp;</div>
                    <? foreach ($arResult['COMPANY']['PROPERTIES'] as $code => $prop) { ?>
                        <article>
                            <strong><?= $prop['NAME']?></strong>
                            <p><?= ($prop['VALUE']) ? $prop['VALUE'] : "—" ?></p>
                        </article>
                    <? } //endforeach ?>
                    <div class="clearfix">&nbsp;</div>
                    <h2><?= Loc::getMessage( "H2_PLACE_OF_DELIVERY" )?></h2>
                    <? foreach ($arResult['PLACE_OF_DELIVERY'] as $id => $place) { ?>
                        <article>
                            <strong><?= $place['UF_NAME']?></strong>
                            <p><?= Loc::getMessage( "PLACE_OF_DELIVERY_ADRESS" )?><?= $place['STR_ADDRESS'] ?></p>
                            <p><?= Loc::getMessage( "PLACE_OF_DELIVERY_PHONE" )?><?= $place['UF_CONTACT_PERSON_PH'] ?></p>
                            <p><strong><?= Loc::getMessage(
                                        "PLACE_OF_DELIVERY_CONTACT_PERSON"
                                    )?></strong><?= $place['UF_CONTACT_PERSON'] ?></p>
                        </article>
                    <? } //endforeach ?>
                </div>
                <div class="form__table__col">
                    <div class="clearfix">&nbsp;</div>
                    <h2><?= Loc::getMessage( "H2_CLIENTS" )?></h2>
                    <table class="table">
                        <thead>
                        <tr>
                            <th><?= Loc::getMessage( "CLIENTS_ID" )?></th>
                            <th><?= Loc::getMessage( "CLIENTS_FIO" )?></th>
                            <th><?= Loc::getMessage( "CLIENTS_POSITION" )?></th>
                            <th><?= Loc::getMessage( "CLIENTS_LOGIN" )?></th>
                            <th><?= Loc::getMessage( "CLIENTS_ACTIVE" )?></th>
                            <th><?= Loc::getMessage( "CLIENTS_EMAIL" )?></th>
                            <th><?= Loc::getMessage( "CLIENTS_PHONE" )?></th>
                            <th><?= Loc::getMessage( "CLIENTS_LAST_LOGIN" )?></th>
                        </tr>
                        </thead>
                        <tbody >
                        <? foreach ($arResult['CONTRACT'] as $contr) :?>
                            <? foreach ($contr['PROPERTIES']['PROP_SUPER_USER'] as $user) :?>
                                <? $u = CUser::GetByID($user)->Fetch(); ?>
                                <tr>
                                    <td ><?= $u['ID'] ?></td>
                                    <td ><?= $u['LAST_NAME'] ." ". $u['NAME'] ." ". $u['SECOND_NAME']?></td>
                                    <td ><?= $u['WORK_POSITION'] ?></td>
                                    <td ><?= $u['LOGIN'] ?></td>
                                    <td ><?= ($u['ACTIVE'] == 'Y') ? 'Да' : 'Нет'; ?></td>
                                    <td ><?= $u['EMAIL'] ?></td>
                                    <td ><?= $u['PERSONAL_MOBILE'] ?></td>
                                    <td ><?= $u['LAST_LOGIN'] ?></td>
                                </tr>
                            <? endforeach; ?>
                        <? endforeach; ?>
                        </tbody>
                    </table>
                    <div class="clearfix">&nbsp;</div>
                    <h2><?= Loc::getMessage( "H2_ORDERS_HISTORY" )?></h2>
                    <table class="table">
                        <thead>
                        <tr>
                            <th><?= Loc::getMessage( "OH_NUMBER_ORDER" )?></th>
                            <th><?= Loc::getMessage( "OH_TYPE_ORDER" )?></th>
                            <th><?= Loc::getMessage( "OH_STATUS_ORDER" )?></th>
                            <th><?= Loc::getMessage( "OH_DATE" )?></th>
                            <th><?= Loc::getMessage( "OH_DATE_OF_ISSUE" )?></th>
                            <th><?= Loc::getMessage( "OH_COMMENT" )?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <? foreach ($arResult['ORDERS'] as $NUM => $ORDER) :?>
                        <tr>
                            <td><a target="_blank" href="/elar/orders/<?= $NUM ?>/">№ <?= $NUM ?></a></td>
                            <td><?= $ORDER['PROPERTY_TYPE_ORDER_VALUE']?></td>
                            <td><?= $ORDER['PROPERTY_STATUS_VALUE']['NAME']?></td>
                            <td><?= $ORDER['PROPERTY_DATE_VALUE']?></td>
                            <td><?= $ORDER['PROPERTY_DATE_OF_ISSUE_VALUE']?></td>
                            <td><?= $ORDER['PROPERTY_COMMENT_VALUE']?></td>
                        </tr>
                        <? endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix">&nbsp;</div>
</div>
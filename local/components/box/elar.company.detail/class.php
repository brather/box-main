<?php
use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Application;
use Bitrix\Main\Loader;

/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 07.04.2017
 * Time: 16:38
 */

class ElarCompanyDetail extends CBitrixComponent {
    private $rq;
    private $COMPANY_ID;
    private $arStatusOrder;
    private $arTypeOrder;

    public function executeComponent()
    {
        /* GET, POST, REQUEST */
        $this->rq = Application::getInstance()->getContext()->getRequest();

        $this->COMPANY_ID = $this->rq->get('COMPANY_ID');

        Loader::includeModule('iblock');
        Loader::includeModule('highloadblock');

        /* Заполним $arStatusOrder данными */
        $this->_getStatusOrder();

        $this->arResult['COMPANY']              = $this->_getRequisites($this->COMPANY_ID);
        $this->arResult['CONTRACT']             = $this->_getContract($this->COMPANY_ID);
        $this->arResult['ORDERS']               = $this->_getOrders();
        $this->arResult['PLACE_OF_DELIVERY']    = $this->_getPlaceOfDelivery();

        $this->includeComponentTemplate();
    }

    /* Получить реквизиты по организации */
    private function _getRequisites($COMPANY_ID)
    {
        $db = CIBlockElement::GetList(
            array("SORT" => "ASC"),
            array("IBLOCK_ID" => IBLOCK_CODE_COMPANY_ID, "ID" => $COMPANY_ID)
        );

        if ($res = $db->Fetch()) {
            $arCompany = $res;

            $db = CIBlockElement::GetProperty(IBLOCK_CODE_COMPANY_ID, $COMPANY_ID);

            while ($res = $db->Fetch()) {
                //$dbP = CIBlockProperty::GetByID($res['ID'], $res['IBLOCK_ID']);
                //$res['AAA'] = $dbP->Fetch();
                $arCompany['PROPERTIES'][$res['CODE']] = $res;
            }

            /* Получим ФИЗИЧЕКИЙ И ЮРИДИЧЕСКИЙ АДРЕСА организации */
            $db = $this->_getDbAddressClient($COMPANY_ID);

            while ($res = $db->fetch()) {
                $uf = (new CUserFieldEnum)->GetList(array(), array(
                    "ID" => $res["UF_TYPE_ADRESS"]
                ));
                $rsUf = $uf->GetNext();
                $res[$rsUf['XML_ID']] = $rsUf;

                $arCompany['PROPERTIES'][$rsUf['XML_ID']]['NAME'] = $rsUf['VALUE'];
                $arCompany['PROPERTIES'][$rsUf['XML_ID']]['VALUE'] = $this->_makeAddress($res);
            }

        } else {
            /* Redirect to Company List if COMPANY_ID not match */
            LocalRedirect('/elar/company/');
            die();
        }

        return $arCompany;
    }

    private function _getHLDataClass($code)
    {
        $connection = Application::getConnection();
        $sql = "SELECT `ID` FROM b_hlblock_entity WHERE `TABLE_NAME` = '{$code}';";
        $res = $connection->query($sql)->fetch();
        $id = $res['ID'];
        unset($connection,$sql,$res);

        $hlblock = HighloadBlockTable::getById($id)->fetch();
        $dClass = HighloadBlockTable::compileEntity($hlblock);

        return $dClass->getDataClass();
    }

    /* Из знаечний свойств сформируем строку Адреса */
    private function _makeAddress (array $ADDRESS_DATA)
    {
        $str = '';

        if ($ADDRESS_DATA['UF_INDEX'])
            $str .= $ADDRESS_DATA['UF_INDEX'];
        if ($ADDRESS_DATA['UF_COUNTRY'])
            $str .= ", " . $ADDRESS_DATA['UF_COUNTRY'];
        if ($ADDRESS_DATA['UF_REGION'])
            $str .= ", " . $ADDRESS_DATA['UF_REGION'];
        if ($ADDRESS_DATA['UF_TOWN'])
            $str .= ", " . $ADDRESS_DATA['UF_TOWN'];
        if ($ADDRESS_DATA['UF_STREET']) {
            $str .= ", " . $ADDRESS_DATA['UF_STREET'];
            if ($ADDRESS_DATA['UF_HOUSE'])
                $str .= " дом " . $ADDRESS_DATA['UF_HOUSE'];
            if ($ADDRESS_DATA['UF_HOUSING'])
                $str .= " корпус " . $ADDRESS_DATA['UF_HOUSING'];
            if ($ADDRESS_DATA['UF_STRUCT'])
                $str .= " " . $ADDRESS_DATA['UF_STRUCT'];
        }
        if ($ADDRESS_DATA['UF_OFFICE'])
            $str .= ", офис " . $ADDRESS_DATA['UF_OFFICE'];

        return $str;
    }


    /**
     * Получение Контрактов по ID организации
     *
     * @param $ID_COMPANY
     *
     * @return array
     */
    private function _getContract ( $ID_COMPANY) {

        $arResult = array();

        $db = CIBlockElement::GetList(
            array(),
            array("PROPERTY_PROP_COMPANY.ID" => $ID_COMPANY),
            false,
            false,
            array("ID", "NAME", "CODE", "IBLOCK_NAME")
        );

        while ($res = $db->Fetch()) {

            $arResult[$res['ID']] = $res;
            /* Получим свойство значение PROP_SUPER_USER */
            $dbp = CIBlockElement::GetProperty(IBLOCK_CODE_CONTRACTS_ID, $res['ID'], ($by="SORT"), ($order="ASC"), array("CODE" => "PROP_SUPER_USER"));
            while ($resP = $dbp->Fetch()) {
                $arResult[$res['ID']]['PROPERTIES'][$resP['CODE']][$resP['PROPERTY_VALUE_ID']] = $resP['VALUE'];
            }
        }

        return $arResult;
    }

    /**
     * Вернем заказы для <u>конкретной</u> организации
     *
     * @return array
     */
    private function _getOrders( ) {
        /* Для фильтра выберем ID контрактов и используем Логику выборки OR */
        $arFilterContract = array("LOGIC" => "OR");
        foreach ($this->arResult['CONTRACT'] as $contract )
            $arFilterContract[] = array("=PROPERTY_CONTRACT" => (int)$contract['ID']);

        /* Фильтр  по  статусам заказа, все кроме Формирующихся */
        $arFilterStatus = array("LOGIC" => "OR");
        foreach ($this->arStatusOrder as $code => $status)
            $arFilterStatus[] = array("=PROPERTY_STATUS" => $code);

        $db = CIBlockElement::GetList(
            array("ID" => "DESC"),
            array(
                "=IBLOCK_ID" => IBLOCK_CODE_ORDERS_ID,
                $arFilterContract,
                $arFilterStatus,
            ),
            false,false,
            array(
                "ID", "NAME",
                "PROPERTY_DATE_OF_ISSUE",
                "PROPERTY_DATE",
                "PROPERTY_COMMENT",
                "PROPERTY_STATUS",
                "PROPERTY_TYPE_ORDER"
            )
        );

        $arOrders = array();

        while ($res = $db->Fetch()) {
            $res['PROPERTY_STATUS_VALUE'] = $this->arStatusOrder[$res['PROPERTY_STATUS_VALUE']];
            $arOrders[$res['ID']] = $res;
        }

        return $arOrders;
    }


    private function _getStatusOrder () {

        $dClass = $this->_getHLDataClass(HL_BLOCK_TABLE_STATUS_ORDER);

        $rsStatus = $dClass::getList(array(
            'filter' => array("!UF_XML_ID" => "formation"),
            'select' => array('*'),
            'order' => array('UF_SORT' => 'ASC'),
            'limit' => ''
        ));
        while($obStatus = $rsStatus->fetch()) {
            $this->arStatusOrder[$obStatus['UF_XML_ID']] = array(
                'ID' => $obStatus['ID'],
                'NAME' => $obStatus['UF_NAME']
            );
        }
    }

    private function _getTypesOrder () {
        $dClass = $this->_getHLDataClass(HL_BLOCK_TABLE_STATUS_WMS);

        $rsStatus = $dClass::getList(array(
            'filter' => array("!UF_XML_ID" => "formation"),
            'select' => array('*'),
            'order' => array('UF_SORT' => 'ASC'),
            'limit' => ''
        ));
        while($obStatus = $rsStatus->fetch()) {
            $this->arStatusOrder[$obStatus['UF_XML_ID']] = array(
                'ID' => $obStatus['ID'],
                'NAME' => $obStatus['UF_NAME']
            );
        }
    }

    private function _getPlaceOfDelivery() {
        $typePhysicalCode   =  "ADRESS_ACTUAL";
        $addressActualID    = null;
        $arResult           = array();

        $db = (new CUserFieldEnum)->GetList(array(), array(
            "XML_ID" => $typePhysicalCode
        ));

        if ($res = $db->GetNext()) {
            $addressActualID = (int)$res['ID'];
        }

        $db = $this->_getDbAddressClient($this->COMPANY_ID, array("UF_TYPE_ADRESS" => $addressActualID));

        while ($res = $db->fetch()) {
            $arResult[$res['ID']] = $res;
            $arResult[$res['ID']]['STR_ADDRESS'] = $this->_makeAddress($res);
        }

        return $arResult;
    }

    /**
     * @param       $COMPANY_ID
     * @param array $extFilter
     *
     * @return \Bitrix\Main\DB\Result
     */
    private function _getDbAddressClient($COMPANY_ID, $extFilter = array())
    {
        /* Default filter */
        $filter = array('UF_COMPANY' => $COMPANY_ID);

        /* If extFilter is not empty, append values on Filter */
        if (is_array($extFilter) && count($extFilter) > 0 )
            $filter = array_merge($filter, $extFilter);

        $hl = $this->_getHLDataClass( HL_BLOCK_TABLE_ADRESS_ALIAS );
        return $hl::getList(array(
            'filter' => $filter
        ));
    }
}
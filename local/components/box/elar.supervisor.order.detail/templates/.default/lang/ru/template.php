<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 05.04.2017
 * Time: 15:13
 */
$MESS['U_ORDER_DETAIL_T_TITLE_1'] = 'Заказ &laquo;';
$MESS['U_ORDER_DETAIL_PAGE_TITLE'] = 'Заказ - ';
$MESS['U_ORDER_DETAIL_T_TITLE_2'] = '&raquo;';
$MESS['U_ORDER_DETAIL_T_ORDER_NUMB_AND_DATE'] = 'Номер и дата исполнения';
$MESS['SUP_ADRESS_OFICE'] = ", офис ";

$MESS['U_ORDER_DETAIL_T_TABLE_NUMB'] = '№ ';
$MESS['U_ORDER_DETAIL_T_TABLE_ID'] = 'ID ';
$MESS['U_ORDER_DETAIL_T_TABLE_IBLOCK_NAME'] = 'Реестр ';
$MESS['U_ORDER_DETAIL_T_TABLE_NAME'] = 'Название ЕУ ';
$MESS['U_ORDER_DETAIL_COMPANY'] = 'Заказчик (ID)';
$MESS['U_ORDER_USER_AUTHOR'] = 'Оформил заказ';

$MESS['E_INFORMATION_ORDER_TITLE'] = 'Информация по заказу';
$MESS['E_ORDER_EU_COUNT'] = 'Колличество ЕУ';

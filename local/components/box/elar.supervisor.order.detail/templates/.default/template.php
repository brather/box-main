<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 04.04.2017
 * Time: 15:56
 *
 * @var array $arResult
 * @var array $arParams
 *
 * @var CBitrixComponentTemplate $this
 */

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Application;
$arGet = Application::getInstance()->getContext()->getRequest()->getQueryList()->toArray();
global $APPLICATION;

$APPLICATION->SetTitle( Loc::getMessage('U_ORDER_DETAIL_PAGE_TITLE') . $arResult['ORDER']['NAME'] );
?>
<div class="page__frame">
    <div class="page__title">
        <h1><?= Loc::getMessage('U_ORDER_DETAIL_T_TITLE_1') ?><?= $arResult['ORDER']['NAME'] ?><?= Loc::getMessage('U_ORDER_DETAIL_T_TITLE_2') ?></h1>
    </div>
    <div class="order">
        <div class="order__col">
            <div class="scrollbar-outer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-6">
                                <h2><?= Loc::getMessage('E_INFORMATION_ORDER_TITLE')?></h2>
                                <div class="form">
                                <ul class="order__list">
                                    <li>
                                        <div><?= Loc::getMessage('U_ORDER_DETAIL_T_ORDER_NUMB_AND_DATE') ?></div>
                                        <div><?= $arResult['ORDER']['ID'] ?> / <?= $arResult['PROPERTIES']['DATE']['VALUE']?></div>
                                    </li>
                                    <li>
                                        <div><?= $arResult['PROPERTIES']['TYPE_ORDER']['NAME']?></div>
                                        <div><?= $arResult['PROPERTIES']['TYPE_ORDER']['VALUE']?></div>
                                    </li>
                                    <?/* todo Тип ЕУ,  такой сущности нет, как будет, должна разместиться тут */?>
                                    <li>
                                        <div><?= Loc::getMessage("E_ORDER_EU_COUNT")?></div>
                                        <div><?= count($arResult['ROW_SORT']); ?></div>
                                    </li>
                                    <li>
                                        <div><?= Loc::getMessage('U_ORDER_DETAIL_COMPANY')?></div>
                                        <div><?= $arResult['PROPERTIES']['COMPANY'];?> (<a href='<?= $arParams['PATH_COMPANY_ID']?><?= $arResult['PROPERTIES']['COMPANY_ID']['ID']?>/'><?=$arResult['PROPERTIES']['COMPANY_ID']['ID']?></a>)</div>
                                    </li>
                                    <li>
                                        <div><?= Loc::getMessage("U_ORDER_USER_AUTHOR")?></div>
                                        <div><a href='<?= $arParams['PATH_CLIENT']?><?= $arResult['PROPERTIES']['CLIENT']['ID']?>/'><?= $arResult['PROPERTIES']['CLIENT']['VALUE']; ?></a> (<?= $arResult['PROPERTIES']['CLIENT']['ID']; ?>)</div>
                                    </li>
                                    <li>
                                        <div><?= $arResult['PROPERTIES']['DATE_OF_ISSUE']['NAME']?></div>
                                        <div><?= date("m.d.Y", strtotime($arResult['PROPERTIES']['DATE_OF_ISSUE']['VALUE']))?></div>
                                    </li>
                                    <!-- START todo Следующие парметры выводятся не для всех типов заказа -->
                                    <li>
                                        <div>Место и адрес выдачи заказа</div>
                                        <div>-</div>
                                    </li>
                                    <li>
                                        <div>Дата и время доставки заказа</div>
                                        <div><?= date("d.m.Y H:i:s", 1)?></div>
                                    </li>
                                    <li>
                                        <div>Контактное лицо</div>
                                        <div>Сидоров Сидор Сидорович (Кладовщик)</div>
                                    </li>
                                    <!-- END -->
                                    <? /* Адресс организации  todo Где то здесь может пригодиться */
                                        if (true === 'ADRESS_CLIENT'){
                                        $value  = $arProperty['VALUE']['INDEX'] . ", ";
                                        $value .= $arProperty['VALUE']['TOWN'] . ", ";
                                        $value .= $arProperty['VALUE']['STREET'] . ", ";
                                        $value .= $arProperty['VALUE']['HOUSE'];
                                        $value .= (!empty($arProperty['VALUE']['OFFICE'])) ? Loc::getMessage('SUP_ADRESS_OFICE') . $arProperty['VALUE']['OFFICE']: "";
                                        echo $value;
                                        }
                                    ?>
                                </ul>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <? if (count($arResult['ACTUAL_ORDERS']) > 0): ?>
                                <h2>Текущие заказы клиента</h2>
                                <div class="form">
                                    <ul class="order__list">
                                        <? foreach($arResult['ACTUAL_ORDERS'] as $order): ?>
                                            <? if ($order['ID'] == $arResult['ORDER']['ID']): ?>
                                            <li class="this-order">
                                                <div style="width: auto;">№ <?= $order['ID']?></div>
                                            <? else: ?>
                                            <li>
                                                <div style="width: auto;"><a href="<?= $arParams['SEF_FOLDER'] . $order['ID'] ?>/" target="_blank">№ <?= $order['ID']?></a></div>
                                            <? endif; ?>
                                            <div style="width: auto;">
                                                <ul>
                                                    <li><strong><?= $order['NAME']?></strong></li>
                                                    <li>Статус: <?= $arResult['STATUS_LIST'][$order['PROPERTY_STATUS_VALUE']]['UF_NAME']?></li>
                                                    <li>Дата исполнение: <?= $order['PROPERTY_DATE_OF_ISSUE_VALUE']?></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <? endforeach; ?>
                                    </ul>
                                </div>
                                <? endif; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs 12">
                                <div class="form">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th><?= Loc::getMessage('U_ORDER_DETAIL_T_TABLE_NUMB') ?></th>
                                            <?foreach($arResult['ROW_SORT'] as $code => $url) {?>
                                                <th>
                                                    <a href="<?= $url ?>" class="filtr">
                                                        <?= Loc::getMessage('U_ORDER_DETAIL_T_TABLE_' . $code) ?>
                                                    </a>
                                                </th>
                                            <?}?>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?$startIndex = $arResult['START_INDEX_UNIT'];

                                        /* Покажем таблицу данных если в $arResult['LIST_UNITS'] массив данных */
                                        if ($arResult['LIST_UNITS'] !== false)
                                        {
                                            foreach (
                                                $arResult['LIST_UNITS'] as $iUnit => $arUnit
                                            )
                                            { ?>
                                                <tr>
                                                    <td><?= $startIndex ?></td>
                                                    <? foreach ($arParams['SET_SORT_FILTER'] as $code) { ?>
                                                        <td><?= $arUnit[ $code ] ?></td>
                                                        <?
                                                    } ?>
                                                    <? $startIndex++; ?>
                                                </tr>
                                                <?
                                            }
                                        } else { ?>
                                            <tr>
                                                <td align="center" colspan="<?= count ($arResult['ROW_SORT']) + 1?>">ЕУ для этого заказа отсуствуют или не были добавлены.</td>
                                            </tr>
                                        <?}?>
                                        </tbody>
                                    </table>
                                </div>
                                <? /* Только если ЕУ для заказа > 0 , есть смысл отображать пагинацию */?>
                                <? if ($arResult['LIST_UNITS'] !== false):?>
                                    <div class="navigator">
                                        <?= $arResult['NAV_PRINT'] ?>
                                        <div class="pager">
                                            <div class="pager__current">
                                                <span><?= Loc::getMessage('USER_REG_T_COUNT_ITEMS') ?></span>
                                                <form action="<?= $arResult['FORM_PATH_DEFAULT'] ?>" method="GET" class="nav-form">
                                                    <?= getHTMLInputExcludeParam($arGet, 'COUNT_ON_PAGE') ?>
                                                    <input type="text" name="COUNT_ON_PAGE" value="<?= $arResult['COUNT_ON_PAGE'] ?>">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                <? endif; ?>
                            </div>
                        </div>
                </div>
                <div class="clearfix">&nbsp;</div>
            </div>
        </div>
    </div>
</div>
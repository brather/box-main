<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 04.04.2017
 * Time: 15:56
 */

use Bitrix\Highloadblock\HighloadBlockTable;
use \Bitrix\Main\Application,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Loader,
    \Bitrix\Iblock\ElementTable,
    \Bitrix\Highloadblock as HL;

class ElarSupervisorOrderDetail extends CBitrixComponent {

    private $arPost;
    private $dClassStatusOrder;
    private $arAdressList;
    private $arGet;

    private static function getContract( $VALUE )
    {
        $arContract = ElementTable::getList(array(
            'filter' => array('ID' => $VALUE, "IBLOCK_ID" => IBLOCK_CODE_CONTRACTS_ID),
            'select' => array('IBLOCK_ID', 'NAME', 'ID')
        ))->fetch();

        $db = CIBlockElement::GetProperty(IBLOCK_CODE_CONTRACTS_ID, $VALUE);

        while ($res = $db->Fetch()) {

            switch ($res['CODE']) {

                case 'PROP_COMPANY':
                    $company = CIBlockElement::GetByID($res['VALUE'])->Fetch();
                    $arContract['PROPERTY'][$res['CODE']]['VALUE'] = $company['NAME'];
                    $arContract['PROPERTY'][$res['CODE']]['ID'] = $company['ID'];
                    unset($company);
                    break;

                case 'PROP_FILES' :
                    $file = CFile::GetPath($res['VALUE']);
                    $arContract['PROPERTY'][$res['CODE']]['VALUE'] = $file;
                    unset($file);
                    break;

                default :

                    $arContract['PROPERTY'][$res['CODE']]['VALUE'] = $res['VALUE'];
            }
        }

        return $arContract;
    }

    public function executeComponent()
    {
        $this->_includeModule();                                 // Подключение модулей
        $this->_getRequest();                                    // Определяет массив request данных

        $this->_getStatusList();                                 // Возвращаем список статусов из highload блока со статусами
        $this->_getOrder();
        $this->includeComponentTemplate();
    }

    private function _includeModule()
    {
        Loader::includeModule('iblock');
        Loader::includeModule('highloadblock');
    }

    private function _getRequest() {
        $request = Application::getInstance()->getContext()->getRequest();
        $this->arGet = $request->getQueryList()->toArray();
        $this->arPost = $request->getPostList()->toArray();

        unset($request);
    }

    private function _getStatusList()
    {
        if(empty($this->dClassStatusOrder)) {
            $this->dClassStatusOrder = $this->_getHLDataClass($this->arParams['HL_CODE_STATUS']);
        }

        $dataClass = $this->dClassStatusOrder;
        $rsStatus = $dataClass::getList(array(
            'filter' => array(),
            'select' => array('*'),
            'order' => array('UF_SORT' => 'ASC'),
            'limit' => ''
        ));
        while($obStatus = $rsStatus->fetch()) {
            $this->arResult['STATUS_LIST'][$obStatus['UF_XML_ID']] = array(
                'ID' => $obStatus['ID'],
                'UF_NAME' => $obStatus['UF_NAME']
            );
        }
    }

    /**
     * @param $code
     *
     * @return \Bitrix\Main\Entity\DataManager
     */
    private function _getHLDataClass($code)
    {
        $connection = Application::getConnection();
        $sql = "SELECT `ID` FROM b_hlblock_entity WHERE `NAME` = '{$code}';";
        $res = $connection->query($sql)->fetch();
        $id = $res['ID'];
        unset($connection,$sql,$res);

        $hlblock = HighloadBlockTable::getById($id)->fetch();
        $dClass = HighloadBlockTable::compileEntity($hlblock);

        return $dClass->getDataClass();
    }

    private function _getOrder()
    {
        $arFilterOrder = array('ID' => $this->arParams['ORDER_ID']);
        $arSelectOrder = array('ID', 'NAME', 'IBLOCK_ID', 'CREATED_BY');
        $this->arResult['ORDER'] = ElementTable::getList(array(
            'filter' => $arFilterOrder,
            'select' => $arSelectOrder
        ))->fetch();

        /* Получим список актуальных заказов по договору */
        $this->_getActualOrders($this->arResult['ORDER']['CREATED_BY']);

        $u = CUser::GetByID($this->arResult['ORDER']['CREATED_BY'])->Fetch();

        $uID = $u['ID'];
        $clientFullName = $u['LAST_NAME'] . " " . $u['NAME'] . " " . $u['SECOND_NAME'];
        unset($u);

        $this->arResult['PROPERTIES']['CLIENT']['SORT'] = 300;
        $this->arResult['PROPERTIES']['CLIENT']['NAME'] = "Клиент";
        $this->arResult['PROPERTIES']['CLIENT']['PROPERTY_TYPE'] = "H";
        $this->arResult['PROPERTIES']['CLIENT']['VALUE'] = $clientFullName;
        $this->arResult['PROPERTIES']['CLIENT']['ID'] = $uID;

        $arOrderPropertyOrder = array('sort' => 'asc');
        $arFilterPropertyOrder = array(
            'ACTIVE' => 'Y',
            'EMPTY' => 'N'
        );
        $rsPropertyOrder = CIBlockElement::GetProperty(
            $this->arResult['ORDER']['IBLOCK_ID'],
            $this->arParams['ORDER_ID'],
            $arOrderPropertyOrder,
            $arFilterPropertyOrder);
        $iMultiple = 0;
        $previousCode = '';
        while($obPropertyOrder = $rsPropertyOrder->Fetch()) {
            if($previousCode != $obPropertyOrder['CODE']) {
                $this->arResult['PROPERTIES'][$obPropertyOrder['CODE']] = array(
                    'ID' => $obPropertyOrder['ID'],
                    'NAME' => $obPropertyOrder['NAME'],
                    'SORT' => $obPropertyOrder['SORT'],
                    'PROPERTY_TYPE' => $obPropertyOrder['PROPERTY_TYPE'],
                    'XML_ID' => $obPropertyOrder['XML_ID'],
                    'MULTIPLE' => $obPropertyOrder['MULTIPLE'],
                );
                if($obPropertyOrder['PROPERTY_TYPE'] == 'L') {
                    $this->arResult['PROPERTIES'][$obPropertyOrder['CODE']]['VALUE_ENUM'] = $obPropertyOrder['VALUE_ENUM'];
                    $this->arResult['PROPERTIES'][$obPropertyOrder['CODE']]['VALUE_XML_ID'] = $obPropertyOrder['VALUE_XML_ID'];
                    $this->arResult['PROPERTIES'][$obPropertyOrder['CODE']]['VALUE_SORT'] = $obPropertyOrder['VALUE_SORT'];
                }
                $previousCode = $obPropertyOrder['CODE'];
                $iMultiple = 0;
            } else {
                $iMultiple++;
            }

            if($obPropertyOrder['MULTIPLE'] == 'Y') {
                $this->arResult['PROPERTIES'][$obPropertyOrder['CODE']]['VALUE'][$iMultiple] = $obPropertyOrder['VALUE'];
            } else {
                if($obPropertyOrder['CODE'] == 'CONTRACT') {
                    $arContract = self::getContract($obPropertyOrder['VALUE']);

                    /* Данные по договору */
                    $this->arResult['PROPERTIES'][$obPropertyOrder['CODE']]['VALUE'] = $arContract['NAME'];
                    $this->arResult['PROPERTIES'][$obPropertyOrder['CODE']]['ID'] = $arContract['ID'];
                    $this->arResult['PROPERTIES'][$obPropertyOrder['CODE']]['NAME'] = $obPropertyOrder['NAME'];

                    /* Название компании */
                    $this->arResult['PROPERTIES']['COMPANY']    = $arContract['PROPERTY']['PROP_COMPANY']['VALUE'];

                    /* ID компании*/
                    $this->arResult['PROPERTIES']['COMPANY_ID']['VALUE']            = $arContract['PROPERTY']['PROP_COMPANY']['ID'];
                    $this->arResult['PROPERTIES']['COMPANY_ID']['ID']               = $arContract['PROPERTY']['PROP_COMPANY']['ID'];
                    $this->arResult['PROPERTIES']['COMPANY_ID']['NAME']             = "ID заказчика";
                    $this->arResult['PROPERTIES']['COMPANY_ID']['PROPERTY_TYPE']    = "H";

                    /* Файл прикрепленный к договору */
                    $this->arResult['PROPERTIES']['FILES']      = $arContract['PROPERTY']['PROP_FILES']['VALUE'];
                } elseif($obPropertyOrder['CODE'] == 'STATUS') {
                    $this->arResult['PROPERTIES'][$obPropertyOrder['CODE']]['VALUE'] = $this->arResult['STATUS_LIST'][$obPropertyOrder['VALUE']]['UF_NAME'];
                    $this->arResult['PROPERTIES'][$obPropertyOrder['CODE']]['XML_ID'] = $obPropertyOrder['VALUE'];
                }
                else if ( $obPropertyOrder['CODE'] == 'ADRESS_CLIENT' )
                {
                    $this->_getAdressClient(array(
                            "=UF_XML_ID" => $obPropertyOrder['VALUE']
                        )
                    );
                    $this->arResult['PROPERTIES'][$obPropertyOrder['CODE']]['VALUE'] = (count($this->arAdressList) == 1) ? current($this->arAdressList) : $this->arAdressList;
                }
                else {
                    $this->arResult['PROPERTIES'][$obPropertyOrder['CODE']]['VALUE'] = $obPropertyOrder['VALUE'];
                }
            }
        }

        if(!$this->_checkContractUser($this->arResult['PROPERTIES']['CONTRACT']['ID']) || !$this->arResult['ORDER']) {
            $arServer = Application::getInstance()->getContext()->getServer()->toArray();
            header('Location: ' . $arServer['HTTP_ORIGIN'] . $this->arParams['LIST_URL']);
        }
        //todo Возможно и не понадобиться отображать ЕУ в заказе
        $this->arResult['LIST_UNITS'] = $this->_getListUnits($this->arResult['PROPERTIES']['UNITS']['VALUE']);
    }

    /**
     * Сформируем массив данных адреса клиента
     *
     * @param $array
     */
    private function _getAdressClient( $array )
    {
        $arFilter = (count($array) > 0) ? $array : array();

        $dClassAdressClient = $this->_getHLDataClass( $this->arParams['HL_CODE_ADRESS_CLIENT']);

        $rsStatus = $dClassAdressClient::getList(array(
            'filter' => $arFilter,
            'order' => array('UF_SORT' => 'ASC'),
            'limit' => ''
        ));
        while($obStatus = $rsStatus->fetch()) {
            $this->arAdressList[ $obStatus['ID'] ] = array(
                'ID'            => $obStatus['ID'],
                'INDEX'         => $obStatus['UF_INDEX'],
                'REGION'        => $obStatus['UF_REGION'],
                'TOWN'          => $obStatus['UF_TOWN'],
                'STREET'        => $obStatus['UF_STREET'],
                'HOUSE'         => $obStatus['UF_HOUSE'],
                'OFFICE'        => $obStatus['UF_OFFICE'],
            );
        }

        unset($rsStatus, $dClassAdressClient, $idHLBlock);
    }

    private function _checkContractUser( $VALUE_ID )
    {
        $sUserId = $this->arParams['USER_ID'];
        // Если пользователь не входит в группу суперпользователей
        if(!in_array(U_GROUP_CODE_CLIENT_S_USER, $this->arParams['USER_GROUPS'])) {
            $arOrderClient = array('ID' => 'ASC');
            $arFilterClient = array('IBLOCK_CODE' => $this->arParams['IBLOCK_CODE']);
            $arSelectClient = array('ID', 'NAME', 'IBLOCK_ID', 'PROPERTY_PROP_CLIENTS_OF_SUPERUSER');
            $rsClient = CIBlockElement::GetList($arOrderClient, $arFilterClient, false, false, $arSelectClient);
            if($obClient = $rsClient->Fetch()) {
                $sUserId = $obClient['PROPERTY_PROP_CLIENTS_OF_SUPERUSER_VALUE'];
            }
        }
        $arOrderContract = array('ID' => 'ASC');
        $arFilterContract = array('ID' => $VALUE_ID, 'PROPERTY_PROP_SUPER_USER' => $sUserId);
        $arSelectContract = array('ID', 'NAME', 'IBLOCK_ID');
        $rsContract = CIBlockElement::GetList($arOrderContract, $arFilterContract, false, false, $arSelectContract);
        if($obContract = $rsContract->Fetch()) {
            return true;
        } else {
            return false;
        }
    }

    private  function _getActualOrders($USER_ID)
    {
        $db = CIBlockElement::GetList(
            array("ID" => "DESC"),
            array("IBLOCK_ID" => IBLOCK_CODE_ORDERS_ID, "CREATED_BY" => $USER_ID,
                  "PROPERTY_STATUS" => array(
                      'processing', 'execution', 'prepared')
            ),
            false,
            false,
            array("ID", "NAME", "PROPERTY_DATE_OF_ISSUE", "PROPERTY_STATUS")
        );

        while ($res = $db->Fetch()) {
            $this->arResult['ACTUAL_ORDERS'][$res['ID']] = $res;
        }
    }

    //region Возвращает список ЕУ по массиву ID
    /** Возвращает список ЕУ по массиву ID
     * @param $arID - массив ID ЕУ
     * @return array|bool
     */
    private function _getListUnits($arID) {

        if (!is_array($arID) || !$arID ) return false;

        $rq = Application::getInstance()->getContext()->getRequest();
        $get = $rq->getQueryList()->toArray();

        $arUnits = array();
        $arOrderUnits = array('NAME' => 'ASC');
        if(!empty($get['SORT'])) {
            $arOrderUnits = array($get['SORT'] => $get['ORDER']);
        }
        $this->arResult['COUNT_ON_PAGE'] = 10;
        if(!empty($get['COUNT_ON_PAGE']) && is_numeric($get['COUNT_ON_PAGE'])) {
            $this->arResult['COUNT_ON_PAGE'] = $get['COUNT_ON_PAGE'];
        }
        $this->arResult['START_INDEX_UNIT'] = 1;
        if(!empty($get['PAGEN_1'])) {
            $this->arResult['START_INDEX_UNIT'] = ($get['PAGEN_1'] - 1) * $this->arResult['COUNT_ON_PAGE'] + 1;
        }
        $numPage = 1;
        if(!empty($get['PAGEN_1'])) {
            $numPage = $get['PAGEN_1'];
        }
        $arNavParamsUnits = array(
            'nPageSize' => $this->arResult['COUNT_ON_PAGE'],
            'iNumPage' => $numPage
        );

        $arFilterUnits = array('ID' => $arID);
        $arSelectUnits = array('ID', 'NAME', 'IBLOCK_NAME', 'IBLOCK_ID');
        $rsUnits = CIBlockElement::GetList($arOrderUnits, $arFilterUnits, false, $arNavParamsUnits, $arSelectUnits);
        $this->arResult['COUNT_UNITS'] = $rsUnits->SelectedRowsCount();
        if($this->arResult['START_INDEX_UNIT'] > $this->arResult['COUNT_UNITS']) {
            $this->arResult['START_INDEX_UNIT'] = 1;
        }
        $this->arResult['NAV_PRINT'] = $rsUnits->GetPageNavStringEx($navComponentObject, Loc::getMessage('USER_REG_C_PAGES'), $this->arParams['NAV_TEMPLATE']);
        while($obUnits = $rsUnits->Fetch()) {
            $arUnits[] = array(
                'ID' => $obUnits['ID'],
                'NAME' => $obUnits['NAME'],
                'IBLOCK_NAME' => $obUnits['IBLOCK_NAME'],
                'IBLOCK_ID' => $obUnits['IBLOCK_ID'],
            );
        }

        $this->_setSort();
        return $arUnits;
    }

    private function _setSort() {
        $preffixGet = '';

        $rq = Application::getInstance()->getContext()->getRequest();
        $get = $rq->getQueryList()->toArray();

        if(!empty($get)) {
            $iGet = 0;
            foreach($get as $cGet => $vGet) {
                if(!empty($this->arParams['SET_SORT_FILTER'])) {
                    if(!empty($this->arParams['SET_SORT_FILTER'][$cGet])) {
                        continue;
                    }
                }
                if($cGet != 'SORT' && $cGet != 'ORDER') {
                    if($iGet == 0) {
                        $preffixGet .= '?';
                    } else {
                        $preffixGet .= '&';
                    }
                    if(is_array($vGet)) {
                        foreach($vGet as $iVal => $val) {
                            $preffixGet .= $cGet . urlencode('[]') . '=' . urlencode($val);
                            if($iVal + 1 != count($vGet)) {
                                $preffixGet .= '&';
                            }
                        }
                    } else {
                        $preffixGet .= $cGet . '=' . $vGet;
                    }

                    $iGet++;
                }
            }
        }
        if(!empty($this->arParams['SET_SORT_FILTER'])) {
            $iFilter = 0;
            foreach($this->arParams['SET_SORT_FILTER'] as $cCode) {
                $vCode = 'ASC';
                if(!empty($get['SORT']) && $get['SORT'] == $cCode && $get['ORDER'] == 'ASC') {
                    $vCode = 'DESC';
                }
                $prefix = $preffixGet;
                if($iFilter == 0 && $prefix == '') {
                    $prefix .= '?';
                } else {
                    $prefix .= '&';
                }
                $this->arResult['ROW_SORT'][$cCode] = $this->arParams['CURENT_PATH'] . $prefix . 'SORT=' . $cCode . '&ORDER=' . $vCode;
                $iFilter++;
            }
        }
    }
}
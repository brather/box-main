$(document).ready( function() {
    //region Данные
    var $window = $(window),
		$body = $('body'),
		cls = {
    		active              : '_active',
			scrollbar           : '.scrollbar-outer',
			jstable             : '.js-table',
			jstabcheckbox       : '.js-table-checkbox',
			jstabcheckboxall    : '.js-table-checkbox-all',
            maskedinput         : '.js-maskedinput'
		};
    //endregion
	//region splitter
    $('#vertical-s').split({
		orientation: 'vertical',
		limit: 100,
		position: '64%' // if there is no percentage it interpret it as pixels
	});
	$('#horizontal-s').split({
		orientation: 'horizontal',
		limit: 100,
		position: '43%' // if there is no percentage it interpret it as pixels
	});
    //endregion
    //region scrollbar
    $(cls.scrollbar).scrollbar();
    //endregion
    //region form select
    $('select').styler();
    //endregion
    //region checkbox table
    (function(){
        $(document).on('click', cls.jstabcheckbox, (function(){
			var $tr = $(this).closest('tr'),
                $table = $(this).closest(cls.jstable),
                $checkbox = $table.find(cls.jstabcheckbox),
                $checkboxAll = $table.find(cls.jstabcheckboxall),
                checkboxLenght = $checkbox.length,
                $trActive = $table.find('tr.' + cls.active);
			$tr.toggleClass(cls.active);
			if ((checkboxLenght - $trActive.length) == 0) {
				$checkboxAll[0].checked = true;
			} else {
				$checkboxAll[0].checked = false;
			}

		}));
		$(document).on('click', cls.jstabcheckboxall, (function(){
			var state = $(this)[0].checked,
                $table = $(this).closest(cls.jstable),
                $checkbox = $table.find(cls.jstabcheckbox),
                $checkboxAll = $table.find(cls.jstabcheckboxall),
                checkboxLenght = $checkbox.length;
			$checkbox.each(function () {
				var trParent = $(this).closest('tr');
				$(this)[0].checked = state;
				if(state) {
                    trParent.removeClass(cls.active).addClass(cls.active);
				} else {
                    trParent.removeClass(cls.active);
				}

			});
		}));

	})();
    //endregion
    //region Accordion
    var animateSpeed = 300,
        clsA = {
            toggleuri   : '.js-toggle-uri',
    		container	: '.b-container',
            contain 	: '.b-container-block',
            head    	: '.bcb-head',
            body    	: '.bcb-collapse',
            open    	: 'js-open'
        };
    $(document).on('click', clsA.toggleuri, function(e){
    //$(clsA.container).find(clsA.head).find('a').on('click', function(e){
        e.preventDefault();
        var $this = $(this),
            containCur = $this.closest(clsA.contain),
			headCur = $this.closest(clsA.head),
			container = containCur.parent(clsA.container),
            bodyCur = headCur.next(),
            containAct = container.children('.' + clsA.open),
            bodyActive = containAct.children(clsA.body),
            headAct = containAct.children(clsA.head),
            textShow = $this.data('text-toggle'),
            textHide = $this.find('span').html();
        if(containCur.hasClass(clsA.open)) {
            bodyCur.hide(animateSpeed, function(){
                containCur.removeClass(clsA.open);
                if($this.find('span').length > 0 && textShow != '') {
                    $this.data('text-toggle', textHide).find('span').html(textShow);
                }
            });
        } else {
            bodyActive.hide(animateSpeed, function() {
                containAct.removeClass(clsA.open);
                if(headAct.find('a').find('span').length > 0 && headAct.find('a').data('text-toggle') != '') {
                    var textShowAct = headAct.find('a').data('text-toggle'),
                        textHideAct = headAct.find('a').find('span').html();
                    headAct.find('a').data('text-toggle', textHideAct).find('span').html(textShowAct);
                }
            });
            bodyCur.show(animateSpeed, function(){
                containCur.addClass(clsA.open);
                if($this.find('span').length > 0 && textShow != '') {
                    $this.data('text-toggle', textHide).find('span').html(textShow);
                }
            });
        }
    });
    //endregion
    //region Отключение действий на выключенную кнопку
    $(document).on('click', '.btn-disabled', function(e) {
        e.preventDefault();
    });
    //endregion
    //region Блок изменения значений у input type="number"
    var inumber = {
            contain     : '.pager__number',
            navblock    : '.pn-nav',
            navbtn      : '.pn-nav-button',
            navbtnup    : 'pn-nav-up',
            navbtndown  : 'pn-nav-down'
        };
    $(document).on('click', inumber.navbtn, function (e) {
        var $this = $(this),
            tContain = $this.closest(inumber.contain),
            iNumber = tContain.find('input[type="number"]'),
            cValue = iNumber.val() * 1,
            stepINumber = iNumber.attr('step') * 1,
            step = 1,
            minInput = iNumber.attr('min') * 1,
            maxInput = iNumber.attr('max') * 1,
            newVal = 0;

        if(typeof(stepINumber) != "undefined" && !isNaN(stepINumber)) {
            step = stepINumber;
        }
        if($this.hasClass(inumber.navbtnup)) {
            if(typeof(maxInput) != "undefined" && !isNaN(maxInput)) {
                if((cValue + step) >= maxInput) {
                    newVal = maxInput;
                } else {
                    newVal = cValue + step;
                }
            } else {
                newVal = cValue + step;
            }
        }
        else if($this.hasClass(inumber.navbtndown)) {
            if(typeof(minInput) != "undefined" && !isNaN(minInput)) {
                if((cValue - step) <= minInput) {
                    newVal = minInput;
                } else {
                    newVal = cValue - step;
                }
            } else {
                newVal = cValue - step;
            }
        }
        iNumber.val(newVal);
    });
    //endregion
    //region Подключение плагина maskedinput для форм в которых присутствует необходимость ввода номера телефона
    $(document).find(cls.maskedinput).each(function(i,y){
        var m = $(y).data('maskedinput');
        if(m) {
            $(y).mask(m);
        }
    });
    //endregion
    //region Снятие активности с выключенной кнопки
    var clsdisabled = '.js-nav-disabled';
    $(document).on('click', clsdisabled, function(e) {
        e.preventDefault();
    });
    //endregion
    //region Подключение datepicker для input со значением для заполнения даты
    $(document).find('.js-datepicker').each(function(i,y){
        var dateMin = new Date(),
            weekDays = AddWeekDays(2);

        dateMin.setDate(dateMin.getDate() + weekDays);
        $(y).datepicker({
            inline              : true,
            //beforeShowDay       : noWeekendsOrHolidays,
            dateFormat          : "dd.mm.yy",
            firstDay            : 1,
            showAnim            : "slide",
            changeFirstDay      : false,
            //minDate             : dateMin,
            showOtherMonths     : true,
            selectOtherMonths   : true,
            dayNamesShort       : $.datepicker.regional[ "ru" ].dayNamesShort,
            dayNames            : $.datepicker.regional[ "ru" ].dayNames,
            monthNamesShort     : $.datepicker.regional[ "ru" ].monthNamesShort,
            monthNames          : $.datepicker.regional[ "ru" ].monthNames
        });
    });
    //endregion
    //region Очистка полей input
    $(document).on('click', '.js-reset-input', function(e){
        e.preventDefault();
        var $this = $(this),
            input = $this.parent().find('input');
        if(input.val() != '') {
            input.val('');
            $this.animate({ 'opacity' : 0 }, 200);
        }
        input.focus();
    });
    //endregion
    //region Показ / скрытие пароля в форме
    var cpass = {
        contain : '.js-password',
        spass   : 'js-show-pass',
        hpass   : 'js-hide-pass'
    };
    $(document).on('click', '.' + cpass.spass, function(e){
        e.preventDefault();
        var $this = $(this),
            input = $this.closest(cpass.contain).find('input');
        $this.removeClass(cpass.spass).addClass(cpass.hpass);
        input.attr('type', 'text');
    });
    $(document).on('click', '.' + cpass.hpass, function(e){
        e.preventDefault();
        var $this = $(this),
            input = $this.closest(cpass.contain).find('input');
        $this.removeClass(cpass.hpass).addClass(cpass.spass);
        input.attr('type', 'password');
    });
    //endregion
});
//region Функция запуска лоадера
/**
 * Функция запуска лоадера
 * @param block - блок-родитель в котором осуществляется запуск лоадера
 */
function startLoader(block) {
    stopLoader();
    var blkLoader = '<div id="blk-loader" class="blk-milk-shadow"><i class="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom"></i></div>';
    $(block).append(blkLoader);
}
//endregion
//region Функция остановки лоадера
/**
 * Функция остановки лоадера
 */
function stopLoader() {
    $('#blk-loader').animate({ 'opacity' : 0 }, 300, function(){
        $(this).remove();
    });
}
//endregion
//region Функция инициализации события запуска плагина split
/**
 * Функция инициализации события запуска плагина split
 * @param obj - объект, в котором передается параметр указывающий на рабочее пространство которое необходимо разбить
 * @param orientation - направление разделения рабочего пространства (horizontal / vertical)
 * @param limit - площадь в % занимаемого рабочего пространства блоками
 * @param position - % занимаемый первым блоком
 */
function setSplit(obj, orientation, limit, position) {
    $(obj).split({
        orientation: orientation,
        limit: limit,
        position: position
    });
}
//endregion
//region Функция запуска обновления блоков внутри объекта для плагина split
/**
 * Функция запуска обновления блоков внутри объекта для плагина split
 * @param obj - объект на котором вызвано событие split
 * @param orientation - направление разбивки рабочего пространства объекта
 * @param percent - проценты занимаемые первым блоком
 */
function refreshSplit(obj, orientation, percent) {
    var $obj = $(obj),
        cls = {
            vertical : {
                first : '.left_panel',
                second : '.right_panel'
            },
            horizontal : {
                first : '.top_panel',
                second : '.bottom_panel'
            },
            splitter : {
                vertical : 'left',
                horizontal : 'top'
            },
            action  : {
                horizontal  : 'height',
                vertical    : 'width'
            },
            splitcls : {
                horizontal  : '.hsplitter',
                vertical    : '.vsplitter'
            }
        },
        wFirst = $obj.find(cls[orientation].first).css(cls.action[orientation]).replace(/\D+/g, "") * 1,
        wSecond = $obj.find(cls[orientation].second).css(cls.action[orientation]).replace(/\D+/g, "") * 1,
        fullWidth = wFirst + wSecond,
        wFirstNew = Math.floor(fullWidth * percent / 100),
        wSecondNew = Math.floor(fullWidth * (100 - percent) / 100);

    $obj.find(cls[orientation].first).css(cls.action[orientation], wFirstNew);
    $obj.find(cls[orientation].second).css(cls.action[orientation], wSecondNew);
    $obj.find(cls.splitcls[orientation]).css(cls.splitter[orientation], wFirstNew);
}
//endregion
//region функция возвращает размеры страницы и экрана
/**
 * функция возвращает размеры страницы и экрана
 * @returns {{pageWidth: *, pageHeight: *, windowWidth: *, windowHeight: *}}
 */
function getPageSize(){
    var xScroll,
        yScroll,
        windowWidth,
        windowHeight;
    if (window.innerHeight && window.scrollMaxY){
        xScroll = document.body.scrollWidth;
        yScroll = window.innerHeight + window.scrollMaxY;
    }else if(document.body.scrollHeight > document.body.offsetHeight){
        xScroll = document.body.scrollWidth;
        yScroll = document.body.scrollHeight;
    }else if(document.documentElement && document.documentElement.scrollHeight > document.documentElement.offsetHeight){
        xScroll = document.documentElement.scrollWidth;
        yScroll = document.documentElement.scrollHeight;
    }else{
        xScroll = document.body.offsetWidth;
        yScroll = document.body.offsetHeight;
    }

    if (self.innerHeight){
        windowWidth = self.innerWidth;
        windowHeight = self.innerHeight;
    }else if(document.documentElement && document.documentElement.clientHeight){
        windowWidth = document.documentElement.clientWidth;
        windowHeight = document.documentElement.clientHeight;
    }else if(document.body){
        windowWidth = document.body.clientWidth;
        windowHeight = document.body.clientHeight;
    }

    if(yScroll < windowHeight){
        pageHeight = windowHeight;
    }else{
        pageHeight = yScroll;
    }

    if(xScroll < windowWidth){
        pageWidth = windowWidth;
    }else{
        pageWidth = xScroll;
    }
    return {
        'pageWidth' : pageWidth,
        'pageHeight' : pageHeight,
        'windowWidth' : windowWidth,
        'windowHeight' : windowHeight
    };
}
//endregion
//region Функция возвращает число из строки
/**
 * Функция возвращает число из строки
 * @param a - строка из которой необходимо получить число
 * @returns {number}
 */
function getNumberFromString(a) {
    var b = a.replace(/\D/g, '');
    return b * 1;
}
//endregion
//region Функции для блокировки выходных дней в календаре datepicker
function noWeekendsOrHolidays(date) {
    var noWeekend = $.datepicker.noWeekends(date);
    if (noWeekend[0]) {
        return nationalDays(date);
    } else {
        return noWeekend;
    }
}
function nationalDays(date) {
    var natDays = [
        [1, 1, 'uk'],
        [12, 25, 'uk'],
        [12, 26, 'uk']
    ];
    for (i = 0; i < natDays.length; i++) {
        if (date.getMonth() == natDays[i][0] - 1 && date.getDate() == natDays[i][1]) {
            return [false, natDays[i][2] + '_day'];
        }
    }
    return [true, ''];
}
function AddWeekDays(weekDaysToAdd) {
    var daysToAdd = 0,
        mydate = new Date(),
        day = mydate.getDay();
    weekDaysToAdd = weekDaysToAdd - (5 - day);
    if ((5 - day) < weekDaysToAdd || weekDaysToAdd == 1) {
        daysToAdd = (5 - day) + 2 + daysToAdd;
    } else { // (5-day) >= weekDaysToAdd
        daysToAdd = (5 - day) + daysToAdd;
    }
    while (weekDaysToAdd != 0) {
        var week = weekDaysToAdd - 5;
        if (week > 0) {
            daysToAdd = 7 + daysToAdd;
            weekDaysToAdd = weekDaysToAdd - 5;
        } else { // week < 0
            daysToAdd = (5 + week) + daysToAdd;
            weekDaysToAdd = weekDaysToAdd - (5 + week);
        }
    }

    return daysToAdd;
}
//endregion
//region Функция проверки формы перед отправкой для того, чтобы не передавать пустые параметры
function checkSendForm(form) {
    var ffilter = 'form',
        objtype = { 0 : 'input', 1 : 'select' };
    $(ffilter).submit(function(){
        var $this = $(this);
        $.each(objtype, function (itype, ytype) {
            $this.find(ytype).each(function(iteg, yteg) {
                if($(yteg).val() == '') {
                    $(yteg).remove();
                }
            });
        })
    });
}
//endregion
//region Функция показа кнопки очистки поля от значения при наведении на него
/**
 * Функция показа кнопки очистки поля от значения при наведении на него
 * @param blkinput - блок в котором содержится input со значением и ссылка на его очистку
 */
function hoverInput(blkinput) {
    $(blkinput).mouseenter(function(e){
        var $this = $(this),
            jsclean = $this.find('.js-reset-input'),
            input = $this.find('input');
        if(input.val() != 0) {
            $(jsclean).animate({'opacity' : 1}, 200);
        }
    });
    $(blkinput).mouseleave(function(){
        var $this = $(this),
            jsclean = $this.find('.js-reset-input'),
            input = $this.find('input');
        $(jsclean).animate({'opacity' : 0}, 200);
    });
}
//endregion
//region Функция возвращает количество элементов в объекте
/**
 * Функция возвращает количество элементов в объекте
 * @param obj
 * @returns {number}
 */
function countOfOject(obj) {
    var t = typeof(obj),
        i = 0;
    if (t!="object" || obj == null) {
        return 0;
    }
    for (x in obj) {
        i++;
    }
    return i;
}
//endregion
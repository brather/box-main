<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?
use Bitrix\Main\Localization\Loc;
ShowMessage($arParams["~AUTH_RESULT"]);?>
<div class="bx-auth">
    <div class="bx-auth-note">
        <p>
            <?= Loc::getMessage("AUTH_FORGOT_PASSWORD_1") ?>
        </p>
        <hr>
        <p>
            <?= Loc::getMessage("AUTH_GET_CHECK_STRING") ?>
        </p>
    </div>
    <form class="form" name="bform" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
        <?if (strlen($arResult["BACKURL"]) > 0) {?>
            <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
        <?}?>
        <input type="hidden" name="AUTH_FORM" value="Y">
        <input type="hidden" name="TYPE" value="SEND_PWD">

        <table class="data-table bx-forgotpass-table bx-auth-table">
            <tbody>
            <tr>
                <td class="bx-auth-label"><?= Loc::getMessage("AUTH_LOGIN") ?></td>
                <td>
                    <input type="text"
                           name="USER_LOGIN"
                           maxlength="50"
                           class="form__field"
                           value="<?=$arResult["LAST_LOGIN"]?>" />
                </td>
            </tr>
            <tr>
                <td class="bx-auth-label"><?= Loc::getMessage("AUTH_EMAIL") ?></td>
                <td>
                    <input type="text" class="form__field" name="USER_EMAIL" maxlength="255" />
                </td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <td></td>
                <td>
                    <button class="btn" type="submit" name="send_account_info">
                        <?= Loc::getMessage("AUTH_SEND") ?>
                    </button>
                </td>
            </tr>
            </tfoot>
        </table>
        <noindex>
            <p>
                <a href="<?=$arResult["AUTH_AUTH_URL"]?>"><?= Loc::getMessage("AUTH_AUTH") ?></a>
            </p>
        </noindex>
    </form>
</div>
<script type="text/javascript">
    document.bform.USER_LOGIN.focus();
</script>

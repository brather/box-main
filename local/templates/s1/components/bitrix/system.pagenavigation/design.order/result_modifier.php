<?
/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

use Bitrix\Main\Application;

$request = Application::getInstance()->getContext()->getRequest();
$arPost = $request->getPostList()->toArray();
$cGet = json_decode(urldecode($arPost['GET']));
$arGet = (array) $cGet;

if(empty($arGet) && !empty($_SESSION['AR_RESULT']['GET'])) {
    $arGet = $_SESSION['AR_RESULT']['GET'];
}

$prefix = '?';
$inputHidden = '';
if(!empty($arGet)) {
    $iGet = 0;
    foreach ($arGet as $cGet => $vGet) {
        if ($cGet != 'PAGEN_' . $arResult["NavNum"]) {
            if ($iGet != 0) {
                $prefix .= '&';
            }
            $vGet = iconv('utf-8', 'cp1251', $vGet);
            $prefix .= $cGet . '=' . $vGet;
            $inputHidden .= '<input name="' . $cGet . '" value="' . $vGet . '" type="hidden" />';
            $iGet++;
        }
    }
} else {
    $inputHidden = '
        <input type="hidden" name="' . $arPost['PREFIX'] . 'FILTER" value="Y" />
        <input type="hidden" name="' . $arPost['PREFIX'] . 'CONTRACT" value="' . $arPost['CONTRACT'] . '" />
        <input type="hidden" name="COUNT_ON_PAGE" value="' . $_SESSION['AR_PARAMS']['COUNT_ITEM_ON_PAGE'] . '" />
        <input type="hidden" name="' . $arPost['PREFIX'] . 'TYPE_ORDER" value="' . $arPost['TYPE_ID'] . '" />';
    $prefix .= $arPost['PREFIX'] . 'FILTER=Y&' . $arPost['PREFIX'] . 'CONTRACT=' . $arPost['CONTRACT']
        . '&' . $arPost['PREFIX'] . 'TYPE_ORDER=' . $arPost['TYPE_ID']
        . '&COUNT_ON_PAGE=' . $_SESSION['AR_PARAMS']['COUNT_ITEM_ON_PAGE'];
}
$arResult['HIDDEN_INPUT'] = $inputHidden;
$arResult["sUrlPath"] = $_SESSION['AR_PARAMS']['CURRENT_PATH'] . 'create/' . $prefix;
?>
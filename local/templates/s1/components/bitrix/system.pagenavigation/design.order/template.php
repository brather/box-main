<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Application;
$request = Application::getInstance()->getContext()->getRequest();
$arPost = $request->getPostList()->toArray();
$cGet = json_decode(urldecode($arPost['GET']));
$arGet = (array) $cGet;

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");

$navPageNumber = $arResult['NavPageNomer'];
if(empty($arGet['PAGEN_' . $arResult["NavNum"]]) && $arResult['NavPageNomer'] == 1) {
    $navPageNumber = 1;
}

$clsPrev = '';
$clsNext = '';
$prevNumber = $navPageNumber - 1;
$nextNumber = $navPageNumber + 1;

if($navPageNumber == 1) {
    $clsPrev = ' js-nav-disabled';
    $prevNumber = 1;
}
if($navPageNumber == $arResult['NavPageCount'] || $arResult['NavPageCount'] == 1) {
    $clsNext = ' js-nav-disabled';
    $nextNumber = $arResult['NavPageCount'];
}

if($navPageNumber == 1) {
    $prevUrl = $arResult["sUrlPath"];
} else {
    $prevUrl = $arResult["sUrlPath"] . '&' . 'PAGEN_' . $arResult["NavNum"] . '=' . $prevNumber;
}
if($arResult['NavPageCount'] == 1) {
    $nextUrl = $arResult["sUrlPath"];
} else {
    $nextUrl = $arResult["sUrlPath"] . '&' . 'PAGEN_' . $arResult["NavNum"] . '=' . $nextNumber;
}?>
<link href="<?= $this->GetFolder() ?>/style.css" rel="stylesheet" />
<div class="pager">
    <a href="<?= $prevUrl ?>"
       title="<?= Loc::getMessage('NAV_DESIGN_PREV') ?>"
       class="pager__arrow _prev<?= $clsPrev ?>">
        <?= Loc::getMessage('NAV_DESIGN_PREV') ?>
    </a>
    <a href="<?= $nextUrl ?>"
       title="<?= Loc::getMessage('NAV_DESIGN_NEXT') ?>"
       class="pager__arrow _next<?= $clsNext ?>">
        <?= Loc::getMessage('NAV_DESIGN_NEXT') ?>
    </a>
    <div class="pager__current">
        <span><?= Loc::getMessage('NAV_DESIGN_PAGE') ?></span>
        <form action="<?= $arResult["sUrlPath"] ?>" class="nav-form" method="get">
            <?= $arResult['HIDDEN_INPUT'] ?>
            <input type="text"
                   name="PAGEN_<?= $arResult["NavNum"] ?>"
                   value="<?= $navPageNumber ?>">
        </form>
        <span><?= Loc::getMessage('NAV_DESIGN_FROM')?><?= $arResult['NavPageCount'] ?></span>
    </div>
</div>
<?
use Bitrix\Main\Application;

$objRequest = Application::getInstance()->getContext()->getRequest();
$arGet = $objRequest->getQueryList()->toArray();
$prefix = '?';

$arResult['GET_ONLY_PAGER'] = true; // параметр отвечающий за то, что в массиве GET не только PAGEN_
foreach($arGet as $cGet => $vGet) {
    if($cGet != 'PAGEN_' . $arResult["NavNum"]) {
        $arResult['GET_ONLY_PAGER'] = false;
        break;
    }
}

if(!empty($arGet)) {
    $iGet = 0;
    $inputHidden = '';
    foreach($arGet as $cGet => $vGet) {
        if($cGet != 'PAGEN_' . $arResult["NavNum"]) {
            if($iGet != 0) {
                $prefix .= '&';
            }
            if(is_array($vGet)) {
                foreach($vGet as $iVal => $val) {
                    $prefix .= $cGet . urlencode('[]') . '=' . urlencode($val);
                    if($iVal + 1 != count($vGet)) {
                        $prefix .= '&';
                    }
                    $inputHidden .= '<input name="' . $cGet . '[]" value="' . $val . '" type="hidden" />';
                }
            } else {
                $prefix .= $cGet . '=' . $vGet;
                $inputHidden .= '<input name="' . $cGet . '" value="' . $vGet . '" type="hidden" />';
            }
            $iGet++;
        }
    }
    $arResult['HIDDEN_INPUT'] = $inputHidden;
    $arResult['PREFIX_S'] = $prefix;
    if($prefix != '?') {
        $prefix .= '&';
    }

}
$arResult['PREFIX'] = $prefix;
?>
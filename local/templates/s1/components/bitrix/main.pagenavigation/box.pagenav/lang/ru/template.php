<?
$MESS["round_nav_back"] = "Назад";
$MESS["round_nav_forward"] = "Вперед";
$MESS["round_nav_pages"] = "Страницы";
$MESS["round_nav_all"] = "Все";

$MESS['TITLE_COLON'] = ' : ';
$MESS['TITLE_DASH'] = ' - ';
$MESS['TITLE_FROM'] = ' из ';
$MESS['TITLE_VERTICAL_LINE'] = ' | ';
$MESS['TITLE_START'] = 'Начало';
$MESS['TITLE_END'] = 'Конец';
$MESS["TITLE_PREV"] = 'Пред.';
$MESS["TITLE_NEXT"] = 'След.';
$MESS["TITLE_ON_PAGES"] = 'По стр.';
?>
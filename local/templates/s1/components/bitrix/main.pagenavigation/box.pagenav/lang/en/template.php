<?
$MESS["round_nav_back"] = "Back";
$MESS["round_nav_forward"] = "Next";
$MESS["round_nav_pages"] = "Pages";
$MESS["round_nav_all"] = "All";

$MESS['TITLE_COLON'] = ' : ';
$MESS['TITLE_DASH'] = ' - ';
$MESS['TITLE_FROM'] = ' from ';
$MESS['TITLE_VERTICAL_LINE'] = ' | ';
$MESS['TITLE_START'] = 'Start';
$MESS['TITLE_END'] = 'End';
$MESS["TITLE_PREV"] = 'Prev';
$MESS["TITLE_NEXT"] = 'Next';
$MESS["TITLE_ON_PAGES"] = 'Pages';
?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

/** @var PageNavigationComponent $component */
$component = $this->getComponent();
use Bitrix\Main\Localization\Loc;?>

<div class="pagenav-table">
    <font class="text">
        <?= $arParams['TITLE_UNIT'] ?>
        <?= Loc::getMessage('TITLE_COLON') ?>
        <?= $arResult['FIRST_RECORD'] ?>
        <?= Loc::getMessage('TITLE_DASH') ?>
        <?= $arResult['LAST_RECORD'] ?>
        <?= Loc::getMessage('TITLE_FROM') ?>
        <?= $arResult['RECORD_COUNT'] ?><br>
    </font>
    <font class="text">
        <?if ($arResult["CURRENT_PAGE"] > 1) {?>
            <a href="<?= htmlspecialcharsbx($arResult["URL"]) ?>">
                <?= Loc::getMessage('TITLE_START') ?>
            </a>
            <?= Loc::getMessage('TITLE_VERTICAL_LINE') ?>
            <a href="<?= htmlspecialcharsbx($component->replaceUrlTemplate($arResult["CURRENT_PAGE"] - 1)) ?>">
                <?= Loc::getMessage('TITLE_PREV') ?>
            </a>
        <?} else {?>
            <?= Loc::getMessage('TITLE_START') ?><?= Loc::getMessage('TITLE_VERTICAL_LINE') ?><?= Loc::getMessage('TITLE_PREV') ?>
        <?}?>
        <?= Loc::getMessage('TITLE_VERTICAL_LINE') ?>

        <?$page = $arResult["START_PAGE"];
        while($page <= $arResult["END_PAGE"]) {?>
            <?if ($page == $arResult["CURRENT_PAGE"]) {?>
                <b><?= $page ?></b>
            <?} else {?>
                <a href="<?= htmlspecialcharsbx($component->replaceUrlTemplate($page)) ?>">
                    <?= $page ?>
                </a>
            <?}?>
            <?$page++;?>
        <?}?>

        <?= Loc::getMessage('TITLE_VERTICAL_LINE') ?>
        <?if($arResult["CURRENT_PAGE"] < $arResult["PAGE_COUNT"]) {?>
            <a href="<?= htmlspecialcharsbx($component->replaceUrlTemplate($arResult["CURRENT_PAGE"] + 1)) ?>">
                <?= Loc::getMessage('TITLE_NEXT') ?>
            </a>
            <?= Loc::getMessage('TITLE_VERTICAL_LINE') ?>
            <a href="<?= htmlspecialcharsbx($component->replaceUrlTemplate($arResult["PAGE_COUNT"])) ?>">
                <?= Loc::getMessage('TITLE_END') ?>
            </a>
        <?} else {?>
            <?= Loc::getMessage('TITLE_NEXT') ?>
            <?= Loc::getMessage('TITLE_VERTICAL_LINE') ?>
            <?= Loc::getMessage('TITLE_END') ?>
        <?}?>
        <noindex>
            <?= Loc::getMessage('TITLE_VERTICAL_LINE') ?>
            <?if ($arResult["SHOW_ALL"]) {?>
                <?if ($arResult["ALL_RECORDS"]) {?>
                    <a href="<?= htmlspecialcharsbx($arResult["URL"]) ?>" rel="nofollow">
                        <?= Loc::getMessage("TITLE_ON_PAGES") ?>
                    </a>
                <?} else {?>
                    <a href="<?= htmlspecialcharsbx($component->replaceUrlTemplate("all")) ?>" rel="nofollow">
                        <?= Loc::getMessage("round_nav_all") ?>
                    </a>
                <?}?>
            <?}?>
        </noindex>
    </font>
</div>

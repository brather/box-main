<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/** @var array $arParams */
/** @var array $arResult */

use Bitrix\Main\Localization\Loc;?>

<div class="pager">
    <a href="<?= $arResult['PREV_URL'] ?>"
       title="<?= Loc::getMessage('round_nav_back') ?>"
       class="pager__arrow _prev<?= $arResult['PREV_CLASS'] ?>">
        <?= Loc::getMessage('round_nav_back') ?>
    </a>
    <a href="<?= $arResult['NEXT_URL'] ?>"
       title="<?= Loc::getMessage('round_nav_forward') ?>"
       class="pager__arrow _next<?= $arResult['NEXT_CLASS'] ?>">
        <?= Loc::getMessage('round_nav_forward') ?>
    </a>
    <div class="pager__current">
        <span><?= Loc::getMessage('round_nav_page') ?></span>
        <form action="<?= $arResult['URL'] ?>" type="get" class="nav-form" id="box-pagenav">
            <?= $arResult['INPUT_HTML'] ?>
            <input type="text" class="show-input" value="<?= $arResult['CURRENT_PAGE'] ?>">
            <input type="hidden" name="PAGE" value="page-<?= $arResult['CURRENT_PAGE'] ?>">
        </form>
        <span><?= Loc::getMessage('TITLE_FROM') ?><?= $arResult['PAGE_COUNT'] ?></span>
    </div>
</div>

$(document).ready(function() {
    var form = $('#box-pagenav'),
        cls = {
            sinput : '.show-input'
        };
    form.on('submit', function() {
        var $this = $(this),
            sInput = $this.find(cls.sinput),
            inputPage = $this.find('input[name="PAGE"]'),
            valPage = 'page-' + sInput.val();
        inputPage.val(valPage);
    });
});
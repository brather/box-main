<?php
/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

/** @var PageNavigationComponent $component */
use Bitrix\Main\Application;
$arGet = Application::getInstance()->getContext()->getRequest()->getQueryList()->toArray();
$inputHtml = '';
if(!empty($arGet)) {
    foreach($arGet as $cGet => $vGet) {
        if($cGet != 'PAGE') {
            if(is_array($vGet)) {
                foreach($vGet as $iVal => $val) {
                    $inputHtml .= '<input name="' . $cGet . '[]" value="' . $val . '" type="hidden" />';
                }
            } else {
                $inputHtml .= '<input type="hidden" name="' . $cGet . '" value="' . $vGet . '" />';
            }
        }
    }
}
$arResult['INPUT_HTML'] = $inputHtml;

$component = $this->getComponent();
$arResult['PREV_URL'] = htmlspecialcharsbx($component->replaceUrlTemplate($arResult["CURRENT_PAGE"] - 1));
$arResult['NEXT_URL'] = htmlspecialcharsbx($component->replaceUrlTemplate($arResult["CURRENT_PAGE"] + 1));

$arResult['PREV_CLASS'] = '';
if($arResult['CURRENT_PAGE'] == 1) {
    $arResult['PREV_CLASS'] = ' js-nav-disabled';
}
if($arResult['CURRENT_PAGE'] == 2 || $arResult['CURRENT_PAGE'] == 1) {
    $arResult['PREV_URL'] = $arResult['URL'];
}
$arResult['NEXT_CLASS'] = '';
if($arResult['CURRENT_PAGE'] == $arResult['END_PAGE']) {
    $arResult['NEXT_CLASS'] = ' js-nav-disabled';
    $arResult['NEXT_URL'] = htmlspecialcharsbx($component->replaceUrlTemplate($arResult["END_PAGE"]));
}
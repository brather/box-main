<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Application;
$context = Application::getInstance()->getContext();
$arServer = $context->getServer()->toArray();
$menu = new HelpMenu($arResult);
$codeGroups = $menu->getCodeUserGroups();
$arListItems = $menu->getListShowItems();

if (!empty($arResult)){
    $previousParent = false;
    $previousLevel = 0;?>
    <ul class="menu left-menu" id="left-multilevel-menu">
        <?foreach($arListItems as $iItem => $arItem) {?>
            <?$cslSelect = '';
            $clsParentSelect = '';
            $clsSpan = 'span-toggle-hide';
            // Добавленные условия необходимы для того, чтобы у суперпользователя при открытии главной страницы
            // одновременно показывался список реестров с соответствующим активным меню
            if ($arItem["SELECTED"]
                || $arServer['REQUEST_URI'] == '/client/'
                && $arItem['LINK'] == '/client/registry/'
                && in_array(U_GROUP_CODE_CLIENT_S_USER, $codeGroups)) {
                $cslSelect = '_active';
                $clsParentSelect = ' parent-active';
                $clsSpan = 'span-toggle-show';
            }
            if($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel) {
                echo str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));
            }
            $previousLevel = $arItem["DEPTH_LEVEL"];

            if($arItem["IS_PARENT"]) {
                $previousParent = true;?>
                <li class="<?= $cslSelect ?><?= $clsParentSelect ?>">
                    <?if($arListItems[$iItem + 1]['DEPTH_LEVEL'] > $arItem["DEPTH_LEVEL"]) {?>
                        <div>
                            <a href="#" class="subMenuToggle">
                                <span class="<?= $clsSpan ?>"></span>
                            </a>
                    <?}?>
                    <a href="<?=$arItem["LINK"]?>">
                        <?=$arItem["TEXT"]?>
                    </a>
                    <?if($arListItems[$iItem + 1]['DEPTH_LEVEL'] > $arItem["DEPTH_LEVEL"]) {?>
                        </div>
                        <ul class="menu__level2">
                    <?}?>
            <?} else {
                $previousParent = false;
                if ($arItem["PERMISSION"] > "D") {?>
                    <li class="<?= $cslSelect ?>">
                        <a href="<?=$arItem["LINK"]?>">
                            <?=$arItem["TEXT"]?>
                        </a>
                    </li>
                <?} else {?>
                    <?if ($arItem["DEPTH_LEVEL"] == 1){?>
                        <li class="<?= $cslSelect ?>">
                            <a href="" title="<?= Loc::getMessage("MENU_ITEM_ACCESS_DENIED") ?>">
                                <?=$arItem["TEXT"]?>
                            </a>
                        </li>
                    <?} else {?>
                        <li>
                            <a href=""
                               class="denied"
                               title="<?= Loc::getMessage("MENU_ITEM_ACCESS_DENIED") ?>">
                                <?=$arItem["TEXT"]?>
                            </a>
                        </li>
                    <?}?>
                <?}?>
            <?}?>
        <?}?>
    <?if ($previousLevel > 1) {?>
        <?= str_repeat("</ul></li>", ($previousLevel - 1)); ?>
    <?}?>
<?}?>
$(document).ready(function(){
    var cls = {
            menu    : '.menu',
            submenu : '.menu__level2',
            active  : '_active',
            toggle  : '.subMenuToggle',
            sShow   : 'span-toggle-show',
            sHide   : 'span-toggle-hide'
        };

    $(cls.toggle).on('click', function(e) {
        e.preventDefault();
        var $this = $(this),
            item = $this.closest('li'),
            submenu = item.find(cls.submenu),
            list = item.closest(cls.menu);
            if(submenu.length > 0) {
                if(item.hasClass(cls.active)) {
                    var sHide = item.find('.' + cls.sShow);
                    item.removeClass(cls.active);
                } else {
                    var sShow = item.find('.' + cls.sHide);
                    sHide = list.find('.' + cls.active).find('.' + cls.sShow);
                    list.children('.' + cls.active).removeClass(cls.active);
                    item.addClass(cls.active);
                    sShow.removeClass(cls.sHide).addClass(cls.sShow);
                }
                sHide.removeClass(cls.sShow).addClass(cls.sHide);
            }
    });
});
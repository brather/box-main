<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

class HelpMenu {
    //region Свойства класса
    private $listItems = array();
    private $codeGroups = array();
    //endregion
    //region Формирование начальных данных
    /** Формирование начальных данных
     * HelpMenu constructor.
     * @param $arListItems
     */
    public function __construct($arListItems) {
        $this->listItems = $arListItems;
        self::setCodeUserGroups();
    }
    //endregion
    //region Возвращает все дочерние элементы родительского пункта меню (без учета файлов ext)
    /** Возвращает все дочерние элементы родительского пункта меню (без учета файлов ext)
     * @param $key - ключ родительского элемента
     * @return array
     */
    public function getSubItems($key) {
        $arSubItems = array();
        $iList = $key + 1;
        $depthLevelParent = $this->listItems[$key]['DEPTH_LEVEL'];
        while($iList < count($this->listItems)) {
            if($this->listItems[$iList]['DEPTH_LEVEL'] > $depthLevelParent) {
                $arSubItems[] = $this->listItems[$iList];
            } else {
                break;
            }
            $iList++;
        }
        return $arSubItems;
    }
    //endregion
    //region Возвращает массив ссылок подразделов раздела с ключом $key
    /** Возвращает массив ссылок подразделов раздела с ключом $key
     * @param $key - ключ раздела
     * @return array
     */
    public function getSubItemsLink($key) {
        $arSubItemslink = array();
        $iList = $key + 1;
        $depthLevelParent = $this->listItems[$key]['DEPTH_LEVEL'];
        while($iList < count($this->listItems)) {
            if($this->listItems[$iList]['DEPTH_LEVEL'] > $depthLevelParent) {
                $arSubItemslink[] = $this->listItems[$iList]['LINK'];
            } else {
                break;
            }
            $iList++;
        }
        return $arSubItemslink;
    }
    //endregion
    //region Проверяет у родительского пункта меню наличие подразделов с правами доступа, если имеются то проверяет есть ли один который должен быть показан (без учета файлов ext)
    /** Проверяет у родительского пункта меню наличие подразделов с правами доступа, если имеются то проверяет есть ли один который должен быть показан (без учета файлов ext)
     * @param $key - ключ проверяемого элемента
     * @return bool
     */
    public function checkParentShow($key) {
        $arItems = self::getSubItems($key);
        $show = false;
        foreach($arItems as $iItem => $arItem) {
            $param = false;
            foreach($this->codeGroups as $code) {
                if(in_array($code, $arItem['PARAMS']['GROUPS'])) {
                    $param = true;
                    break;
                }
            }
            if(empty($arItem['PARAMS']['GROUPS'])
                || $param) {
                $show = true;
                break;
            }
        }
        return $show;
    }
    //endregion
    //region Формирует массив с кодами групп, которым принадлежит пользователь
    /** Формирует массив с кодами групп, которым принадлежит пользователь
     * @return array
     */
    private function setCodeUserGroups() {
        global $USER;
        $arIdUG = $USER->GetUserGroupArray();
        $arFilterGroups = array('ACTIVE' => 'Y', 'ID' => implode('|', $arIdUG));
        $rsGroups = CGroup::GetList($by = 'id', $order = 'asc', $arFilterGroups);
        while($obGroup = $rsGroups->Fetch()) {
            $this->codeGroups[] = $obGroup['STRING_ID'];
        }
    }
    //endregion
    //region Возвращает массив с кодами групп, которым принадлежит пользователь
    /** Возвращает массив с кодами групп, которым принадлежит пользователь
     * @return array
     */
    public function getCodeUserGroups() {
        return $this->codeGroups;
    }
    //endregion
    public function getListShowItems() {
        $arReturn = array();
        $previousShowParent = true;
        $previousSubItems = array();
        foreach($this->listItems as $iItem => $arItem) {
            if($arItem['IS_PARENT']) {
                $previousSubItems = array();
                $previousShowParent = true;
                if(!empty($arItem['PARAMS']['GROUPS'])) {
                    $previousShowParent = false;
                    foreach($arItem['PARAMS']['GROUPS'] as $iGroup => $nGroup) {
                        if(in_array($nGroup, $this->codeGroups)) {
                            $previousShowParent = true;
                            break;
                        }
                    }
                }
                if($previousShowParent) {
                    $arReturn[] = $arItem;
                } else {
                    $previousSubItems = $this->getSubItemsLink($iItem);
                }
            } else {
                if($previousShowParent) {
                    if(!empty($previousSubItems) && !in_array($arItem['LINK'], $previousSubItems)
                        || empty($previousSubItems)) {
                        $flagShow = true;
                        if(!empty($arItem['PARAMS']['GROUPS'])) {
                            $flagShow = false;
                            foreach($arItem['PARAMS']['GROUPS'] as $iGroup => $nGroup) {
                                if(in_array($nGroup, $this->codeGroups)) {
                                    $flagShow = true;
                                    break;
                                }
                            }
                        }
                        if($flagShow) {
                            $arReturn[] = $arItem;
                        }
                    }
                }
            }
        }
        return $arReturn;
    }
}
<!--[if lt IE 7]>
<html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie10 lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie10 lt-ie9"><![endif]-->
<!--[if IE 9]>
<html class="no-js lt-ie10"><![endif]-->
<!--[if gt IE 9]><!--><html class="no-js"><!--<![endif]-->

<!DOCTYPE html>
<html xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
    <?global $APPLICATION,
             $USER;?>
    <title><?$APPLICATION->ShowTitle()?></title>
    <?$APPLICATION->ShowMeta("description")?>
    <?$APPLICATION->ShowMeta("keywords")?>

    <link rel="shortcut icon" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH?>/i/favicon.ico?<?= rand() ?>" />
    <link rel="icon" type="image/png" href="<?=SITE_TEMPLATE_PATH?>/i/favicon-32x32.png?<?= rand() ?>" sizes="32x32" />
    <link rel="icon" type="image/png" href="<?=SITE_TEMPLATE_PATH?>/i/favicon-16x16.png?<?= rand() ?>" sizes="16x16" />
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic" rel="stylesheet">
    <?
    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/font-awesome.min.css");
    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/jquery.confirm.css");
    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/jquery.splitter.css");
    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/jquery.scrollbar.css");
    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/jquery.formstyler.css");
    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/jquery-ui.min.css");

    $APPLICATION->ShowCSS();
    $APPLICATION->ShowHeadStrings();

    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery-2-1-0.min.js');
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery-ui.min.js');
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/datepicker-ru.js');
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.splitter.js');
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.scrollbar.js');
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.formstyler.min.js');
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.confirm.js');
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.maskedinput.min.js");
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/main.js');
    $APPLICATION->ShowHeadScripts();?>

    <?\Bitrix\Main\Page\Asset::getInstance()->setJsToBody(true);?>
    <script type="text/javascript">
        // <![CDATA[
        document.documentElement.className += " js";
        document.documentElement.className = document.documentElement.className.replace(/(?:^|\s)no-js(?!\S)/, '');
        // ]]>
    </script>
    <?use \Bitrix\Main\Application,
        \Bitrix\Main\Localization\Loc;
    Loc::loadMessages(__FILE__);
    unsetSessionParam();?>
</head>
<body>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<?$arSplitStyle = array('CONTAINER_CLASS' => ' _table', 'PAGE_ID' => '');
$arSectionsNoSplit = array('/cabinet/');
$arServer = Application::getInstance()->getContext()->getServer()->toArray();
$sectExist = false;
foreach ($arSectionsNoSplit as $uri) {
    if(stristr($arServer['REQUEST_URI'], $uri)) {
        $sectExist = true;
        break;
    }
}
if(!$sectExist) {
    $arSplitStyle = array('CONTAINER_CLASS' => '', 'PAGE_ID' => 'vertical');
}?>
<div class="container<?= $arSplitStyle['CONTAINER_CLASS'] ?>">
    <div class="user-panel">
        <?if($USER->isAuthorized()){?>
            <div class="user-panel__top">
                <a href="/"><?= Loc::getMessage('H_PERSONAL_CABINET_TITLE') ?></a>
                <a href="/?logout=yes" class="user-panel__top__exit">
                    <?= Loc::getMessage('H_PERSONAL_CABINET_LOGOUT') ?>
                </a>
            </div>
            <?$APPLICATION->IncludeComponent(
                'box:user.data',
                'menu.title',
                array(
                    'DATA_PROPERTY_FIELDS' => array(
                            'LAST_NAME',
                            'NAME',
                            'SECOND_NAME'
                    )
                )
            );?>
            <? $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "left.favorite",
                array(
                    "ROOT_MENU_TYPE" => "left",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "MENU_CACHE_GET_VARS" => array(),
                    "MAX_LEVEL" => "2",
                    "CHILD_MENU_TYPE" => "left.sub",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                ),
                false
            ); ?>
        <?} else {
            $requestUri = Application::getInstance()->getContext()->getServer()->getRequestUri();
            if($requestUri != '/' && $requestUri != '/?forgot_password=yes') {
                LocalRedirect('/');
            }
        }?>
    </div>
    <div class="page" id="<?= $arSplitStyle['PAGE_ID'] ?>">
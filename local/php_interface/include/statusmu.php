<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.04.2017
 * Time: 17:41
 *
 * Class StatusMu
 * @package EME
 */
namespace EME;

class StatusMu {
    /**
     * @var string $barCode Штрих-код ЕУ
     * @var string $currentStatus Текущий статус ЕУ
     * @var string $dateTimeCurrentStatus Дата и время присвоения текущего статуса в формате XML DateTime
     * @var string $dateTimePlacedStatus Дата и время присвоения статуса «Размещено» в формате XML DateTime
     */
    public $barCode, $currentStatus, $dateTimeCurrentStatus, $dateTimePlacedStatus;
}
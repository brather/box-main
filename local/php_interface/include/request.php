<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.04.2017
 * Time: 17:38
 *
 * Объект стандартного запроса
 *
 * Class Request
 * @package EME
 */
namespace EME;

class Request{
    /**
     * @var string $dateTimeRequest Дата и время запроса в формате XML DateTime
     */
    public $dateTimeRequest;

    public function __construct()
    {
        $this->dateTimeRequest = date('c');
    }

    public function __set( $name, $value )
    {
        $this->$name = $value;
    }

    static function formatXML($xmlString){

        //Format XML to save indented tree rather than one line
        $dom = new \DOMDocument('1.0');
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $dom->loadXML($xmlString);

        return $dom->saveXML();
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.04.2017
 * Time: 17:46
 *
 * Class MeasUnit
 * @package EME
 */
namespace EME;

class MeasUnit{
    /**
     * @var string $barCode Идентификатор ЕУ
     */
    public $barCode;
}
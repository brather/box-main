<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.04.2017
 * Time: 17:39
 *
 * Class Response
 * В нем содержится: Дата и время ответа на запрос, состояние ответа, описание ошибки (если есть).
 *
 * Если во время выполнения запроса будет вызвана ошибка,
 * то в поле dateTimeResponse  будет записана дата и время ответа на запрос,
 * в поле state будет записано 1, а в поле descriptionError будет записан код ошибки.
 * Остальные поля будут пустыми.
 */
namespace EME;

class Response{
    /**
     * @var string $dateTimeResponse Дата и время запроса в формате XML DateTime
     * @var int $state Состояние ответа на запрос. 0 – без ошибок, 1 – есть ошибка
     * @var string $descriptionError Код ошибки:
     *  * Error codes:
     *  EST	- Превышено время ожидания ответа от WMS (по умолчанию 100 сек., можно изменить)
     *  CNF	- В БД WMS не найден клиент
     *  DEF	- Передаваемые параметры не соответствуют установленному формату
     *  UNF	- В БД WMS не найдена ЕУ
     *  ONE	- Передаваемый номер заказа уже существует в БД WMS
     *  ANF	- Не заполнена структура для соответствующего типа заказа
     *  ONF	- В БД WMS не найден заказ
     *  PNF	- В БД WMS не задана стоимость услуги для клиента
     *  FNA	- Невозможно вызвать функцию web-сервиса
     */
    public $dateTimeResponse, $state, $descriptionError;
}

/*----------Response-----------*/
<?php
$MESS['DEFINE_ADRESS_STOCK'] = 'reg. Moscow, Mytishchi, st. Karl Marks, house 5, structure 7';
$MESS['DEFINE_ADRESS_AUDIT_ROOM'] = '127015, Moscow, Bumagniy passage 14, structure 2';
$MESS['DEFINE_SIGNATURE_REG_ITEMS'] = 'Storage units';
$MESS['DEFINE_SIGNATURE_REG_ITEM'] = 'Storage unit';
$MESS['DEFINE_SIGNATURE_REG_ADD_ITEM'] = 'Add storage unit';
$MESS['DEFINE_SIGNATURE_REG_EDIT_ITEM'] = 'Edit storage unit';
$MESS['DEFINE_SIGNATURE_REG_DELETE_ITEM'] = 'Delete storage unit';
<?php
$MESS['DEFINE_ADRESS_STOCK'] = 'Московская обл., Мытищи, ул. Карла Маркса, д. 5, стр. 7';
$MESS['DEFINE_ADRESS_AUDIT_ROOM'] = '127015, Москва, Бумажный проезд 14, стр. 2';
$MESS['DEFINE_SIGNATURE_REG_ITEMS'] = 'Единицы хранения';
$MESS['DEFINE_SIGNATURE_REG_ITEM'] = 'Единица хранения';
$MESS['DEFINE_SIGNATURE_REG_ADD_ITEM'] = 'Добавить единицу хранения';
$MESS['DEFINE_SIGNATURE_REG_EDIT_ITEM'] = 'Изменить единицу хранения';
$MESS['DEFINE_SIGNATURE_REG_DELETE_ITEM'] = 'Удалить единицу хранения';
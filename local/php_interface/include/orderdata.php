<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.04.2017
 * Time: 17:51
 *
 * Class OrderData
 * @package EME
 */
namespace EME;

/**
 * @property IssueMu    issueMu
 * @property IncomeMu   incomeMu
 * @property Digitizing digitizing
 */
class OrderData extends Order
    implements EmeInt {
    /**
     * @var string $clientId Идентификатор клиента
     */
    public $clientId;

    /**
     * Вернет код клиента в системе WMS по ID контракта
     *
     * @param $contractId
     *
     * @return string
     */
    static public function getClientId( $contractId ) {

        \Bitrix\Main\Loader::includeModule('iblock');

        $clientId = '';
        $db = \CIBlockElement::GetProperty(
            IBLOCK_CODE_CONTRACTS_ID,
            $contractId,
            array('NAME' => 'ASC'),
            array('CODE' => PROPERTY_CODE_CONTRACT_CLIENT_CODE)
        );

        if ($res = $db->Fetch()) {
            $clientId = (string)$res['VALUE'];
        }
        return $clientId;
    }


}
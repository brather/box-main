<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.04.2017
 * Time: 17:53
 *
 * Class ClientAddress
 * @package EME
 */
namespace EME;

class ClientAddress extends Client {
    /**
     * @var string $actualAddress Фактический адрес
     * @var array $listContact Список объектов Contact
     */
    public $actualAddress, $listContact;
}
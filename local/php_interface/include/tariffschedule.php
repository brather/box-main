<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.04.2017
 * Time: 17:45
 *
 * Class TariffSchedule
 * @package EME
 */
namespace EME;

class TariffSchedule{
    /**
     * @var string $typeService Тип услуги
     * @var string $paramsService Параметры
     * @var string $unitMeasure Единица измерения
     * @var int $cost Стоимость
     * @var string $comments Комментарии
     */
    public $typeService, $paramsService, $unitMeasure, $cost, $comments;
}
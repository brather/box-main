<?php
IncludeModuleLangFile(__FILE__);

/* Soap WMS test mode*/
define("SOAP_TEST_MODE", false);
define("SOAP_URL", 'http://10.4.131.8:922/WMSWebService.asmx?WSDL');

//region Константы групп пользователей
define('U_GROUP_CODE_SUPER_ADMIN',      'SUPER_ADMIN');
define('U_GROUP_CODE_ALL',              'ALL_USERS');
define('U_GROUP_CODE_ELAR_ADMIN',       'U_ELAR_ADMIN');
define('U_GROUP_CODE_ELAR_SUPERVISOR',  'U_ELAR_SUPERVISOR');
define('U_GROUP_CODE_ELAR_OPERATOR',    'U_ELAR_OPERATOR');
define('U_GROUP_CODE_CLIENT_S_USER',    'U_CLIENT_S_USER');
define('U_GROUP_CODE_CLIENT_USER',      'U_CLIENT_USER');
//endregion

//region Типы инфоблоков
define('IBLOCK_TYPE_REGISTRY',  'registries');
define('IBLOCK_TYPE_LISTS',     'lists');
//endregion

//region Коды информационных блоков
define('IBLOCK_CODE_CLIENTS',   'clients');
define('IBLOCK_CODE_CLIENTS_ID',   2);
define('IBLOCK_CODE_COMPANY',   'company');
define('IBLOCK_CODE_COMPANY_ID',   12);
define('IBLOCK_CODE_CONTRACTS', 'contracts');
define('IBLOCK_CODE_CONTRACTS_ID', 3);
define('IBLOCK_CODE_ORDERS',    'orders');
define('IBLOCK_CODE_ORDERS_ID',    6);
define('IBLOCK_ID_REGISTRIES',    25);
define('IBLOCK_ID_BID', 26);

//endregion

//region Коды HL блоков
define('HL_BLOCK_CODE_ADRESS_ALIAS',    'AdressAliasList');
define('HL_BLOCK_CODE_STATUS_ORDER',    'StatusOfOrder');
define('HL_BLOCK_CODE_STATUS_WMS',      'StatusOfWMS');
//endregion

//region Таблицы HL блоков
define('HL_BLOCK_TABLE_ADRESS_ALIAS',   'adress_alias_list');
define('HL_BLOCK_TABLE_STATUS_ORDER',   'status_of_order');
define('HL_BLOCK_TABLE_STATUS_WMS',     'status_of_wms');
//endregion

//region Подписи к инфоблоку Реестр
define('CAPTION_IBLOCK_REG_ITEMS',          GetMessage('DEFINE_SIGNATURE_REG_ITEMS'));
define('CAPTION_IBLOCK_REG_ITEM',           GetMessage('DEFINE_SIGNATURE_REG_ITEM'));
define('CAPTION_IBLOCK_REG_ADD_ITEM',       GetMessage('DEFINE_SIGNATURE_REG_ADD_ITEM'));
define('CAPTION_IBLOCK_REG_EDIT_ITEM',      GetMessage('DEFINE_SIGNATURE_REG_EDIT_ITEM'));
define('CAPTION_IBLOCK_REG_DELETE_ITEM',    GetMessage('DEFINE_SIGNATURE_REG_DELETE_ITEM'));
//endregion

//region Свойства информационных блоков
define('IBLOCK_REGISTRY_PROP_CLIENT',       'PROP_CLIENT');
define('PROPERTY_CODE_LIST_OF_PERMISSION',  'LIST_PROPERTIES');             // Код свойства для инфоблоков реестра в котором внесены свойства недоступные обычному пользователю
define('PROPERTY_CODE_FORMING_OF_ORDER',    'FORMING');                     // Код свойства инфоблока заказов в котором определяется, формирующийся заказ или нет
define('PROPERTY_CODE_REQUEST',             'REQUEST');                     // Код свойства инфоблока заказов в котором определяется, заказ или запрос сформировал пользователь
define('PROPERTY_CODE_TYPE_OF_ORDER',       'TYPE_ORDER');                  // Код свойства инфоблока заказов определяющий тип заказа / запроса клиента
define('PROPERTY_CODE_UNITS',               'UNITS');                       // Код свойства инфоблока заказов определяющий список ЕУ для создания заказа
define('PROPERTY_CODE_CONTRACT',            'CONTRACT');                    // Код свойства инфоблока заказов определяющий id договора для создания заказа
define('PROPERTY_CODE_COUNT_OF_UNITS',      'COUNT_OF_UNITS');              // Код свойства инфоблока заказов определяющий количество ЕУ в заказе
define('PROPERTY_CODE_STATUS_OF_ORDER',     'STATUS');                      // Код свойства инфоблока заказов определяющий статус заказа
define('PROPERTY_CODE_CONTRACTS_SUSER',     'PROP_SUPER_USER');             // Код свойства инфоблока с договорами "Суперпользователь"
define('PROPERTY_CODE_CLIENTS_SUSER',       'PROP_CLIENTS_OF_SUPERUSER');   // Код свойства инфоблока с клиентами "Суперпользователь"
define('PROPERTY_CODE_CLIENTS_USER',        'PROP_CLIENT');                 // Код свойства инфоблока с клиентами "Пользователь"
define('PROPERTY_CODE_CONTRACT_CLIENT_CODE', 'CLIENT_CODE');                 // Код свойства инфоблока, код клиента в WMS "Контракты"
//endregion

//region Адреса
define('ADRESS_OF_STOCK',       GetMessage('DEFINE_ADRESS_STOCK'));
define('ADRESS_OF_AUDIT_ROOM',  GetMessage('DEFINE_ADRESS_AUDIT_ROOM'));
//endregion
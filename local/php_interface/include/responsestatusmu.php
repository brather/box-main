<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.04.2017
 * Time: 17:40
 *
 * Class ResponseStatusMu
 *
 * @package EME
 */
namespace EME {
    /* @property GetStatusMeasUnitResult GetStatusMeasUnitResult */
    class ResponseStatusMu extends Response
    {
        /**
         * @var array $listStatusMu Список объектов StatusMu
         */
        public $listStatusMu;
    }
}

namespace EME {
    /**
     * @property    integer      state
     * @property    string       descriptionError
     * @property    listStatusMu listStatusMu
     */
    abstract class GetStatusMeasUnitResult{}
}

namespace EME {
    /**
     * @property array|StatusMu     StatusMu
     * */
    abstract class listStatusMu{}
}
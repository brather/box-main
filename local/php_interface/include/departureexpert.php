<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.04.2017
 * Time: 17:56
 *
 * Выезд эксперта
 * Class DepartureExpert
 *
 * @package EME
 */
namespace EME;

class DepartureExpert {

    /**
     * Дата выезда
     *
     * @var string $dateDeparture
     */
    public $dateDeparture;
    /**
     * Адрес выезда (справочник)
     *
     * @var string $addressDeparture
     */
    public $addressDeparture;
}
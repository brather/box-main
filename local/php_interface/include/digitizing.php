<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.04.2017
 * Time: 17:47
 *
 * Оцифровка
 * Class Digitizing
 *
 * @package EME
 */
namespace EME;

class Digitizing {
    /**
     * @var bool $isScan Сканирование
     * @var bool $isRecognition Распознавание
     * @var bool $isIndex Индексирование
     * @property string $optionsScan Параметры (справочник для сканирования)
     * @property string $formatCaseIdentifier Формат идентификатора дела
     * @property int $caseCount Формат идентификатора дела
     * @property int $pageCount Формат идентификатора дела
     * @property string $optionsIndex Формат идентификатора дела
     */

    public $isScan = false;

    public $isRecognition = false;

    public $isIndex = false;

    function __set( $name, $value )
    {
        $this->$name = $value;
    }
}
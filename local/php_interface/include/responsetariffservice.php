<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.04.2017
 * Time: 17:43
 *
 * Class ResponseTariffService
 * @package EME
 */
namespace EME;

class ResponseTariffService extends Response{
    /**
     * @var int $costWork Стоимость работ
     */
    public $costWork;
}
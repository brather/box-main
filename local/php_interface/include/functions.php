<?php
//region Функция возвращает содержимое объекта / массива
/** Функция возвращает содержимое объекта / массива
 * @param $mess - object / array
 */
function p($mess) {
    global $USER;
    //if($USER->isAdmin()) {
        ?><pre style="border-color: rgb(40, 181, 9); color: rgb(40, 181, 9); border-radius: 0; font-size: 14px;"><? print_r($mess) ?></pre><?
    //}
}
//endregion
//region Функция подмены киррилических символов на схожие по написанию латинские
/** Функция подмены киррилических символов на схожие по написанию латинские
 * @param $string - строка для подмены
 * @return mixed - строка с только лишь латинскими буквами
 */
function transliteRuToEnString($string) {
    $resStr = $string;
    $arCode = array(
        'a' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'j', 'з' => 'z',
        'и' => 'i', 'й' => 'i', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r',
        'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sh',
        'ъ' => '', 'ы' => 'e', 'ь' => '', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya',
        'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'E', 'Ж' => 'J', 'З' => 'Z',
        'И' => 'I', 'Й' => 'I', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O', 'П' => 'P', 'Р' => 'R',
        'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C', 'Ч' => 'CH', 'Ш' => 'SH', 'Щ' => 'SH',
        'Ъ' => '', 'Ы' => 'E', 'Ь' => '', 'Э' => 'E', 'Ю' => 'YU', 'Я' => 'YA');
    foreach($arCode as $symbRu => $symbEn) {
        if(stristr($resStr, $symbRu)) {
            $resStr = str_replace($symbRu, $symbEn, $resStr);
        }
    }
    return $resStr;
}
//endregion
//region Скрытие всех символов в строке
/** Скрытие всех символов в строке
 * @param $string
 * @return mixed
 */
function hidePassword($string) {
    $pattern = '/[a-zA-Z\d\,\.\<\>\/\?\;\:\'\"\[\]\{\}\\\|\`\~\!\@\#\$\%\^\&\*\(\)\-\_\+\=]/i';
    $resPassword = preg_replace($pattern, '*', $string);
    return $resPassword;
}
//endregion
//region Проверка email
/** Проверка email
 * @param $email
 * @return bool
 */
function checkEmail($email) {
    $pattern = "/^(|(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6})$/i";
    if(preg_match($pattern, $email)) {
        return true;
    } else {
        return false;
    }
}
//endregion
//region Проверка слова на содержание только лишь букв
/**
 * @param $word
 */
function checkWord($word) {
    $pattern = "/^[A-Za-zА-Яа-я\-]{0,120}$/u";
    if(preg_match($pattern, $word)) {
        return true;
    } else {
        return false;
    }
}
//endregion
//region Проверка наименования компании
/** Проверка наименования компании
 * @param $word
 * @return bool
 */
function checkTitleCompany($word) {
    $pattern = "/^[A-Za-zА-Яа-я0-9\'\"\«]{1}+[0-9A-Za-zА-Яа-я\'\"\«\»\s\-]{0,120}$/u";
    if(preg_match($pattern, $word)) {
        return true;
    } else {
        return false;
    }
}
//endregion
//region Проверка телефона на наличие букв и символов кроме +, -, (, )
/** Проверка телефона на наличие букв и символов кроме +, - (, )
 * @param $phone
 * @return bool
 */
function checkPhone($phone) {
    $pattern = "/[A-Za-z\!\'\@\#\$\%\^\&\*\|\/\;\:\"]/u";
    if(preg_match($pattern, $phone)) {
        return false;
    } else {
        return true;
    }
}
//endregion
//region Функция для очистки строки от символов для использования строки в названии классов
/** Функция для очистки строки от символов для использования строки в названии классов
 * @param $string
 * @return mixed
 */
function checkStringForSetAttr($string) {
    $resStr = transliteRuToEnString($string);
    $resStr = str_replace(' ', '_', $resStr);
    $resStr = preg_replace('/\.+/i', '', $resStr);
    return $resStr;
}
//endregion
//region Функция транслитерации строки из кириллицы в латиницу
/** Функция транслитерации строки из кириллицы в латиницу
 * @param $string
 * @return string
 */
function transliterateTo($string) {
    $resString = $string;
    if(preg_match('/[А-Яа-яЁё]/i', $resString)) {
        $arTransliterate = array(
            'А' => '-A',   'Б' => '-B',   'В' => '-V',   'Г' => '-G',   'Д' => '-D',   'Е' => '-E',
            'Ж' => '-J',   'З' => '-Z',   'И' => '-I',   'Й' => '-IY',  'К' => '-K',   'Л' => '-L',
            'М' => '-M',   'Н' => '-N',   'О' => '-O',   'П' => '-P',   'Р' => '-R',   'С' => '-S',
            'Т' => '-T',   'У' => '-U',   'Ф' => '-F',   'Х' => '-H',   'Ц' => '-TS',  'Ч' => '-CH',
            'Ш' => '-SH',  'Щ' => '-SCH', 'Ъ' => '-IEA', 'Ы' => '-YI',  'Ь' => '-IEU', 'Э' => '-IE',
            'Ю' => '-YU',  'Я' => '-YA',  'а' => '-a',   'б' => '-b',   'в' => '-v',   'г' => '-g',
            'д' => '-d',   'е' => '-e',   'ж' => '-j',   'з' => '-z',   'и' => '-i',   'й' => '-iy',
            'к' => '-k',   'л' => '-l',   'м' => '-m',   'н' => '-n',   'о' => '-o',   'п' => '-p',
            'р' => '-r',   'с' => '-s',   'т' => '-t',   'у' => '-u',   'ф' => '-f',   'х' => '-h',
            'ц' => '-ts',  'ч' => '-ch',  'ш' => '-sh',  'щ' => '-sch', 'ъ' => '-iea', 'ы' => '-yi',
            'ь' => '-ieu', 'э' => '-ie',  'ю' => '-yu',  'я' => '-ya',  'ё' => '-yo',  'Ё' => '-YO'
        );
        $resString = 'trans_' . strtr($resString, $arTransliterate);
    }
    return $resString;
}
//endregion
//region Функция обратной транслитерации строки из латиницы в кириллицу (строка перед этим должна была быть преобразована из кир. в лат.)
/** Функция обратной транслитерации строки из латиницы в кириллицу (строка перед этим должна была быть преобразована из кир. в лат.)
 * @param $string
 * @return mixed=string
 */
function transliterateOut($string) {
    $resString = $string;
    if(stristr($resString, 'trans_')) {
        $arRevert = array(
            '-SCH' => 'Щ', '-sch' => 'щ', '-IEA' => 'Ъ', '-iea' => 'ъ', '-IEU' => 'Ь', '-ieu' => 'ь',

            '-IY' => 'Й',  '-iy' => 'й',  '-TS' => 'Ц',  '-ts' => 'ц',  '-CH' => 'Ч',  '-ch' => 'ч',  '-YO' => 'Ё',  '-yo' => 'ё',
            '-SH' => 'Ш',  '-sh' => 'ш',  '-IE' => 'Э',  '-ie' => 'э',  '-YU' => 'Ю',  '-yu' => 'ю',  '-YA' => 'Я',  '-ya' => 'я',

            '-A' => 'А', '-a' => 'а', '-B' => 'Б', '-b' => 'б', '-V' => 'В', '-v' => 'в', '-G' => 'Г', '-g' => 'г', '-D'=> 'Д',  '-d' => 'д',
            '-E' => 'Е', '-e' => 'е', '-J' => 'Ж', '-j' => 'ж', '-Z' => 'З', '-z' => 'з', '-I' => 'И', '-i' => 'и', '-K' => 'К', '-k' => 'к', '-L' => 'Л',
            '-l' => 'л', '-M' => 'М', '-m' => 'м', '-N' => 'Н', '-n' => 'н', '-O' => 'О', '-o' => 'о', '-P' => 'П', '-p' => 'п', '-R' => 'Р', '-r' => 'р',
            '-S' => 'С', '-s' => 'с', '-T' => 'Т', '-t' => 'т', '-U' => 'У', '-u' => 'у', '-F' => 'Ф', '-f' => 'ф', '-H' => 'Х', '-h' => 'х',
        );
        $resString = str_replace('trans_', '', $resString);
        $resString = strtr($resString, $arRevert);
    }
    return $resString;
}
//endregion
//region Проверяет наличие папки и при ее отсутствии создает ее
/** Проверяет наличие папки и при ее отсутствии создает ее
 * @param $folder - путь к папке
 * @param bool $permission - права доступа (по умолчанию 0777)
 */
function checkFolder($folder, $permission = false) {
    if(!$permission) {
        $permission = 0777;
    }
    if(!file_exists($folder)) {
        mkdir($folder, $permission);
    }
}
//endregion
//region Функция считывания изображения из pdf файла и сохранения его в директорию
/** Функция считывания изображения из pdf файла и сохранения его в директорию
 * @param $file - pdf файл, путь от document_root
 * @param $numberPage - номер страницы для сохранения
 * @param $dirToSave - директория для сохранения файла
 * @param $newName - новое имя
 */
function createImageOutPdf($file, $numberPage, $dirToSave, $newName, $extension = 'jpg') {
    $fileUrl = $file . '[' . $numberPage . ']';
    $obPdf = new Imagick();
    $obPdf->setResolution(112, 112);
    $obPdf->readImage($fileUrl);
    $obPdf->setImageColorspace(255);
    $obPdf->setCompressionQuality(60);
    $obPdf->setImageFormat($extension);
    $obPdf->writeImage($dirToSave . $newName . '.' . $extension);
    $obPdf->clear();
    $obPdf->destroy();
}
//endregion
//region Функция возвращает в строчном виде размер файла с его размером
/** Функция возвращает в строчном виде размер файла с его размером
 * @param $size - размер файла в байтах
 * @param int $round - количество знаков после запятой
 * @return string
 */
function fileSizeToString($size, $round = 2) {
    $sizes = array('B', 'Kb', 'Mb', 'Gb', 'Tb', 'Pb', 'Eb', 'Zb', 'Yb');
    for ($i = 0; $size > 1024 && $i < count($sizes) - 1; $i++) {
        $size /= 1024;
    }
   return round($size, $round) . " " . $sizes[$i];
}
//endregion
//region Очистка пользовательских параметров для сессии которые задаются в некоторых компонентах
/**
 * Очистка пользовательских параметров для сессии которые задаются в некоторых компонентах
 */
function unsetSessionParam() {
    if($_SESSION['AR_RESULT']) {
        unset($_SESSION['AR_RESULT']);
    }
    if($_SESSION['AR_PARAMS']) {
        unset($_SESSION['AR_PARAMS']);
    }
}
//endregion
//region Возвращает html список input исключая параметр $nameExclude
/** Возвращает html список input исключая параметр $nameExclude
 * @param $arCheck - массив с параметрами для создания html
 * @param $exclude - параметр для исключения (принимает значения строки или одномерного массива)
 * @param $prefix - префикс для названия поля у input (необязательный)
 * @return string
 */
function getHTMLInputExcludeParam($arCheck, $exclude, $prefix = false) {
    $html = '';
    foreach($arCheck as $cCheck => $vCheck) {
        if((is_array($exclude) && in_array($cCheck, $exclude))
            || (is_string($exclude) && $cCheck == $exclude)) {
            continue;
        }
        if(is_array($vCheck)) {
            foreach($vCheck as $vSubCheck) {
                $html .= '<input type="hidden" name="' . $prefix . $cCheck . '[]" value="' . $vSubCheck . '" />';
            }
        } else {
            $html .= '<input type="hidden" name="' . $prefix . $cCheck . '" value="' . $vCheck . '" />';
        }
    }
    return $html;
}
//endregion
//region Возвращает строку из массива исключая параметр $exclude
/** Возвращает строку из массива исключая параметр $exclude
 * @param $arCheck - массив в котором находится параметр
 * @param $exclude - параметр для исключения
 * @return mixed
 */
function getStringQueryExcludeParam($arCheck, $exclude) {
    $resString = '?';
    if(!empty($arCheck)) {
        $iCheck = 0;
        unset($arCheck[$exclude]);
        foreach($arCheck as $cCheck => $vCheck) {
            $resString .= $cCheck . '=' . $vCheck;
            $iCheck++;
            if($iCheck != count($arCheck)) {
                $resString .= '&';
            }
        }
    }
    return $resString;
}
//endregion
//region Возвращает параметры для select у которого возможна выборка нескольких значений
/** Возвращает параметры для select у которого возможна выборка нескольких значений
 * @param $count - количество значений
 * @return string
 */
function getSizeMultiSelect($count) {
    if($count > 1) {
        $maxMulti = 3;
        if($count > $maxMulti) {
            $count = $maxMulti;
        }
        return ' multiple size="' . $count . '"';
    } else {
        return 1;
    }
}
//endregion
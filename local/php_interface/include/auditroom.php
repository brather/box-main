<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.04.2017
 * Time: 17:54
 *
 * Class AuditRoom
 * @package EME
 */
namespace EME;

class AuditRoom{
    /**
     * @var string $dateTimeAudience Дата и время посещения
     */
    public $dateTimeAudience;
}
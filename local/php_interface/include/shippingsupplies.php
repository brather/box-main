<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.04.2017
 * Time: 17:56
 */
namespace EME;
/** Доставка расходных материалов
 * Class ShippingSupplies
 *
 * @package EME
 */
class ShippingSupplies {
    /**
     * @var array $listMaterial Список объектов Material
     * @var string $dateShipping Дата доставки
     * @var string $addressDeparture Адрес выезда
     */
    public $listMaterial, $dateShipping, $addressDeparture;
}

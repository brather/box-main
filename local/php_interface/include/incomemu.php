<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.04.2017
 * Time: 17:48
 */
namespace EME;

/** Прием ЕУ
* Class IncomeMu
 * @package EME
*/
class IncomeMu {
    /**
     * @var int $qtyMu Количество ЕУ
     * @var string $locationReceive Место приема (справочник)
     * @var string $addressReceive Адрес приема
     * @var string $dateReceive Дата приема
     */
    public $qtyMu, $locationReceive, $addressReceive, $dateReceive;
}
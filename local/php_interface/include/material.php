<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.04.2017
 * Time: 17:55
 *
 * Class Material
 * @package EME
 */
namespace EME;

class Material{
    /**
     * @var string $accountMaterial Расходный материал
     * @var int $qty Количество
     */
    public $accountMaterial, $qty;
}

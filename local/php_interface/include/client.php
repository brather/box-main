<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.04.2017
 * Time: 17:51
 *
 *
 * Class Client
 * @package EME
 */
namespace EME;

class Client {
    /**
     * @var int $clientId Идентификатор клиента
     */
    public $clientId;
}
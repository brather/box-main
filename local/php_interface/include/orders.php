<?php
//require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

IncludeModuleLangFile(__FILE__);

use Bitrix\Iblock\IblockTable,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader;

/**
 * Class Orders
 * Класс для хранения информации о добавляемых и удаляемых ЕУ (и ЕХ) в массив заказов в сессию:
 * Структура записи:
 * $_SESSION['ORDERS_' . $userID][ $stepOrder ][ $typeOrder ][ $contractID ][ $unitID ]
 * $userID - ID пользователя
 * $stepOrder - этап заказа ( TEMP / SAVE)
 * $typeOrder - код типа заказа (оцифровка, временный вывоз со склада и т.п.)
 * $contractID - ID договора
 * $unitID - ID ЕУ (ЕХ)
 */

class Orders extends CBitrixComponent {
    public $userID;                         // id пользователя
    private $cIBlockOrders = '';            // код инфоблока с заказами
    private $cIBlockContracts = '';         // код инфоблока с договорами
    private $cIBlockClients = '';           // код инфоблока с клиентами
    private $iBlockIdOrders = 0;            // id инфоблока с заказами
    private $arValuesForming = array();     // массив значений свойства "Формирование" инфоблока с заказами
    private $arValuesRequest = array();     // массив значений свойства "Запрос" инфоблока с заказами
    private $arValuesTypeOrder = array();   // массив значений свойства "Тип заказа" инфоблока с заказами
    private $codePropertyForming = '';      // код свойства "Формирование" инфоблока с заказами
    private $codePropertyRequest = '';      // код свойства "Запрос" инфоблока с заказами
    private $codePropertyUnits = '';        // код свойства "ЕУ для заказа" инфоблока с заказами
    private $codePropertyContract = '';     // код свойства "Договор" инфоблока с заказами
    private $codePropertyCountUnits = '';   // код свойства "Количество ЕУ" инфоблока с заказами
    private $codePropertyStatus = '';       // код свойства "Статус заказа" инфоблока с заказами
    private $cPropertyContractsSUser = '';  // код свойства "Супер-пользователь" в инфоблоке с договорами
    private $cPropertyClientsSUser = '';    // код свойства "Супер-пользователь" в инфоблоке с клиентами
    private $cPropertyClietsUser = '';      // код свойства "Пользователь" в инфоблоке с клиентами
    private $arListOrdersForming = array(); // массив со списком уже формирующихся заказов
    private $arListContracts = array();     // массив со списком договоров доступных пользователю
    private $arListUserGroups = array();    // массив со списком групп пользователей

    /**
     * Подключение файлов перевода
     */
    public function onIncludeComponentLang() {
        Loc::loadMessages(__FILE__);
    }
    //endregion

    public function __construct($component = null) {
        parent::__construct($component);
        global $USER;
        $this->userID = $USER->GetID();
        //$this->userID = bitrix_sessid();
        // Если не установлены параметры сессии для добавления ЕУ через список реестров
        if(empty($_SESSION['TMP_ORDERS_' . $this->userID]['FLAG_TYPES'])) {
            self::setDefaultTypeOrder();
        }

        self::includeModule();
        $this->iBlockIdOrders           = self::getIBlockId(IBLOCK_CODE_ORDERS);
        $this->arValuesForming          = self::getPropertyList($this->iBlockIdOrders, PROPERTY_CODE_FORMING_OF_ORDER);
        $this->arValuesRequest          = self::getPropertyList($this->iBlockIdOrders, PROPERTY_CODE_REQUEST);
        $this->arValuesTypeOrder        = self::getPropertyList($this->iBlockIdOrders, PROPERTY_CODE_TYPE_OF_ORDER);
    }

    //region Метод подключает модули необходимые для работы
    /**
     * Метод подключает модули необходимые для работы
     */
    private function includeModule() {
        Loader::includeModule('iblock');
    }
    //endregion
    //region Возвращает id инфоблока по его коду
    /** Возвращает id инфоблока по его коду
     * @param $iblockCode
     * @return mixed
     */
    private function getIBlockId($iblockCode) {
        $arIblock = IblockTable::getList(array(
            'filter' => array('CODE' => $iblockCode),
            'select' => array('ID')
        ))->fetch();
        return $arIblock['ID'];
    }
    //endregion
    //region Возвращает id инфоблока с заказами
    /** Возвращает id инфоблока с заказами
     * @return int|mixed
     */
    public function getIdIBlockOrder() {
        return $this->iBlockIdOrders;
    }
    //endregion
    //region Возвращает из инфоблока заказов массив с вариантами свойств по id инфоблока и по коду свойства
    /** Возвращает из инфоблока заказов массив с вариантами свойств
     * по id инфоблока и по коду свойства
     *
     * @param $iblockID - id инфоблока
     * @param $propertyCode - код свойства которое необходимо вернуть
     * @return array
     */
    private function getPropertyList($iblockID, $propertyCode) {
        $arReturn = array();
        $arOrderProperty = array('SORT' => 'ASC');
        $arFilterProperty = array('IBLOCK_ID' => $iblockID);
        $rsProperty = CIBlockProperty::GetPropertyEnum(
            $propertyCode,
            $arOrderProperty,
            $arFilterProperty);
        while($obProperty = $rsProperty->Fetch()) {
            $arReturn[$obProperty['XML_ID']] = array(
                'VALUE' => $obProperty['VALUE'],
                'ID' => $obProperty['ID'],
                'SORT' => $obProperty['SORT'],
                'XML_ID' => $obProperty['XML_ID']
            );
        }
        return $arReturn;
    }
    //endregion
    //region Добавляет список ЕУ в заказ
    /** Добавляет список ЕУ в заказ
     * @param $typeOrder - xml_id типа заказа
     * @param $contractId - id договора
     * @param $request - xml_id определяющий запрос
     * @param string $forming - xml_id определяющий формируемый запрос
     */
    public function addToOrderForming($typeOrder, $contractId, $request, $forming = 'N') {
        $el = new CIBlockElement;
        $nameOrder = $contractId . '_' . date('dmyHis') . '_' . substr(strtoupper($typeOrder), 0, 3);
        $arUnitsId = array_keys($_SESSION['TMP_ORDERS_' . $this->userID]['LIST_UNITS'][$contractId]);
        $arPropertyValues = array(
            PROPERTY_CODE_FORMING_OF_ORDER => $this->arValuesForming[$forming]['ID'],
            PROPERTY_CODE_REQUEST => $this->arValuesRequest[$request]['ID'],
            PROPERTY_CODE_TYPE_OF_ORDER => $this->arValuesTypeOrder[$typeOrder]['ID'],
            PROPERTY_CODE_UNITS => $arUnitsId,
            PROPERTY_CODE_CONTRACT => $contractId,
            PROPERTY_CODE_COUNT_OF_UNITS => count($arUnitsId),
            PROPERTY_CODE_STATUS_OF_ORDER => 'formation'
        );
        $arLoadOrder = array(
            'NAME' => $nameOrder,
            'IBLOCK_ID' => $this->iBlockIdOrders,
            'ACTIVE' => 'Y',
            'PROPERTY_VALUES' => $arPropertyValues
        );
        $el->Add($arLoadOrder);
    }
    //endregion
    //region Обновление заказа
    public function updateOrderAndDraft( $fields )
    {
        /* проверим входящие данные и сформируем корректный массив */
        $arFields = $this->_checkFieldsBeforeUpdate( $fields );

        /* После проверки вернулся пустой массив */
        if (count($arFields) <= 0) return false;

        $obEl = new CIBlockElement();

        /* обновим отдельно каждое свойство */
        if (count($arFields['PROPERTY_VALUES']) > 0)
        {
            foreach ($arFields['PROPERTY_VALUES'] as $code => $value)
                $obEl->SetPropertyValues($arFields['ID'], 6, $value, $code);
        }

        unset($arFields['PROPERTY_VALUES']);

        /* Не удалось обновить, вернем сообщение ошибки */
        if (!$obEl->Update($arFields['ID'], $arFields))
            return ($obEl->LAST_ERROR);

        /* Удачное изменение данных */
        return true;
    }
    //endregion
    //region Проверка обязательных полей перед обновлением
    private function _checkFieldsBeforeUpdate( $arFields )
    {
        global $DB;

        /* Наличие номера заказа */
        if ( !isset($arFields['ORDER_ID']) || $arFields['ORDER_ID'] === '' || $arFields['ORDER_ID'] === 0 )
            return array();

        $returnFields = array();
        $propertyValues = array();

        foreach ($arFields as $code => $value)
        {
            $code = preg_replace('#F_UNIT_#', '', $code);

            switch ($code):
                /* Отдельная обратка значений не вхожих в свойства */
                case 'ORDER_ID':
                case 'NAME':
                    /* Если ID */
                    if ($code == 'ORDER_ID')
                        $returnFields['ID'] = $value;
                    /* Если NAME */
                    if ($code == 'NAME')
                        $returnFields['NAME'] = $value;
                    /* не спускаемся ниже */
                    continue;
                    break;
                case 'DATE':
                    $propertyValues[$code] = date($DB->DateFormatToPHP(CSite::GetDateFormat()), strtotime($value));
                    continue;
                    break;
                case 'DATE_OF_ISSUE':
                    $propertyValues[$code] = date($DB->DateFormatToPHP(CSite::GetDateFormat()), $value);
                    break;
                case 'REQUEST':
                    $propertyValues[$code] = $this->arValuesRequest[$value]['ID'];
                    break;
            endswitch;

            $prop = $this->getPropertyList($this->iBlockIdOrders, $code);

            /* Тип свойства список */
            if (is_array($prop) && count($prop) > 0 )
            {
                $propertyValues[$code] = $prop[$value]['ID'];
            }
            else
            {
                $prop = CIBlockElement::GetProperty($this->iBlockIdOrders, $arFields['ORDER_ID'], array(), array('CODE' => $code))->Fetch();
                if (is_array($prop) && count($prop) > 0)
                    $propertyValues[$code] = $value;
            }
        }

        if (count($returnFields) > 0)
        {
            $returnFields['IBLOCK_ID'] = $this->iBlockIdOrders;
            $returnFields['IBLOCK_ID'] = $this->iBlockIdOrders;


            if (count($propertyValues) > 0)
            {
                // todo Hardcode FORMING status
                $propertyValues['FORMING'] = $this->arValuesForming['Y']['ID'];
                $returnFields['PROPERTY_VALUES'] = $propertyValues;
            }
        }

        return $returnFields;
    }
    ////endregion
    //region Добавляет в заказ список ЕУ
    /** Добавляет в заказ список ЕУ
     * @param $orderId - id заказа в который необходимо провести добавление
     * @param $contractId - id договора
     * @param $request - xml_id определяющий запрос
     * @param string $forming - xml_id определяющий формируемый запрос
     */
    public function addUnitToFormingOrder($orderId, $contractId) {
        $flag = false;
        $arUnitsId = array_keys($_SESSION['TMP_ORDERS_' . $this->userID]['LIST_UNITS'][$contractId]);
        $arUnitExistInOrder = $this->getListUnitsFromOrder($orderId);
        foreach($arUnitsId as $idUnit) {
            if(!in_array($idUnit . '', $arUnitExistInOrder)) {
                $arUnitExistInOrder[] = $idUnit;
                $flag = true;
            }
        }
        if($flag) {
            $this->setUnitsListToOrder($orderId, $arUnitExistInOrder);
        }
    }
    //endregion
    //region Определяет список договоров доступных пользователю
    /**
     * Определяет список договоров доступных пользователю
     */
    private function setListContractsUser() {
        $arListContracts = array();
        $sUserId = '';
        // Если клиент принадлежит группе суперпользователей
        if($this->checkSUser()) {
            $sUserId = $this->userID;
        }
        // Если клинет обычный пользователь
        else {
            $arOrderClients = array('ID' => 'ASC');
            $arFilterClients = array('IBLOCK_CODE' => IBLOCK_CODE_CLIENTS, 'PROPERTY_' . PROPERTY_CODE_CLIENTS_USER => $this->userID);
            $arSelectClients = array('ID', 'NAME', 'IBLOCK_ID', 'PROPERTY_' . PROPERTY_CODE_CLIENTS_SUSER);
            $arClients = CIBlockElement::GetList($arOrderClients, $arFilterClients, false, false, $arSelectClients)->Fetch();
            $sUserId = $arClients['PROPERTY_' . PROPERTY_CODE_CLIENTS_SUSER . '_VALUE'];
        }

        $arOrderContracts = array('ID' => 'ASC');
        $arFilterContracts = array(
            'IBLOCK_CODE' => IBLOCK_CODE_CONTRACTS,
            'PROPERTY_' . PROPERTY_CODE_CONTRACTS_SUSER => $sUserId
        );
        $arSelectContracts = array('ID', 'NAME', 'IBLOCK_ID');
        $rsContracts = CIBlockElement::GetList($arOrderContracts, $arFilterContracts, false, false, $arSelectContracts);
        while($obContracts = $rsContracts->Fetch()) {
            $arListContracts[$obContracts['ID']] = $obContracts['NAME'];
        }
        $this->arListContracts = $arListContracts;
    }
    //endregion
    //region Возвращает список договоров с которыми может работать пользователь
    /** Возвращает список договоров с которыми может работать пользователь
     * @return array
     */
    public function getListContracts() {
        if(empty($this->arListContracts)) {
            self::setListContractsUser();
        }
        return $this->arListContracts;
    }
    //endregion
    //region Возвращает список заказов находящихся на стадии формирования
    /** Возвращает список заказов находящихся на стадии формирования
     * @param $contractList - список договоров пользователя
     * @param string $request - параметр определяет возвращать ли список запросов (Y - возвращает запросы от пользователей, 'N' или '' то вернет заказы)
     * @return array
     */
    public function getListFormingOrders($contractList = array(), $request = '') {
        $arReturn = array();
        $arOrderOrders = array('ID' => 'ASC');
        $arFilterOrders = array(
            'IBLOCK_CODE' => IBLOCK_CODE_ORDERS,
            'PROPERTY_' . PROPERTY_CODE_FORMING_OF_ORDER . '_VALUE' => $this->arValuesForming['Y']['VALUE'],
        );
        $contract = $contractList;
        if(empty($contract)) {
            $contract = $this->getListContracts();
        }
        $arFilterOrders['PROPERTY_' . PROPERTY_CODE_CONTRACT] = $contract;
        if(!empty($request)) {
            $arFilterOrders['PROPERTY_' . PROPERTY_CODE_REQUEST . '_VALUE'] = $this->arValuesRequest[$request]['VALUE'];
        } else {
            $arFilterOrders['!PROPERTY_' . PROPERTY_CODE_REQUEST . '_VALUE'] = $this->arValuesRequest['Y']['VALUE'];
        }
        $arSelectOrders = array(
            'ID',
            'NAME',
            'PROPERTY_' . PROPERTY_CODE_TYPE_OF_ORDER,
            'PROPERTY_' . PROPERTY_CODE_CONTRACT
        );
        $rsOrders = CIBlockElement::GetList($arOrderOrders, $arFilterOrders, false, false, $arSelectOrders);
        $arTypesOfOrder = array();
        foreach($this->arValuesTypeOrder as $xmlType => $arType) {
            $arTypesOfOrder[$arType['ID']] = $xmlType;
        }
        while($obOrders = $rsOrders->Fetch()) {
            $arTempOrder = array(
                'ID' => $obOrders['ID'],
                'NAME' => $obOrders['NAME'],
                'PROPERTY_' . PROPERTY_CODE_TYPE_OF_ORDER => $arTypesOfOrder[$obOrders['PROPERTY_' . PROPERTY_CODE_TYPE_OF_ORDER . '_ENUM_ID']],
                'PROPERTY_' . PROPERTY_CODE_CONTRACT => $obOrders['PROPERTY_' . PROPERTY_CODE_CONTRACT . '_VALUE'],
            );
            if(empty($this->arListOrdersForming[$obOrders['PROPERTY_' . PROPERTY_CODE_CONTRACT . '_VALUE']][$obOrders['ID']])) {
                $this->arListOrdersForming[$obOrders['PROPERTY_' . PROPERTY_CODE_CONTRACT . '_VALUE']][$obOrders['ID']] = $arTempOrder;
            }
            $arReturn[] = $arTempOrder;
        }
        return $arReturn;
    }
    //endregion
    //region Создает заказ
    /** Создает заказ. При успешном выполнении возвращает числовой код заказа, если с ошибкой, соответственно текст ошибки
     * @param $orderName - имя заказа
     * @param $contract - номер договора
     * @param $orderType - код типа заказа
     * @param string $request - тип заказа (N - заказ от суперпользователя, Y - запрос от пользователя)
     * @param string $forming - тип заказа (Y - формирующийся, N - сформированный)
     * @return bool|string
     */
    public function createOrder($orderName, $contract, $orderType, $request = 'N', $forming = 'Y') {
        $el = new CIBlockElement;
        $arProperty = array(
            PROPERTY_CODE_REQUEST => $this->arValuesRequest[$request]['ID'],
            PROPERTY_CODE_FORMING_OF_ORDER => $this->arValuesForming[$forming]['ID'],
            PROPERTY_CODE_CONTRACT => $contract,
            PROPERTY_CODE_TYPE_OF_ORDER => $this->arValuesTypeOrder[$orderType]['ID'],
            PROPERTY_CODE_STATUS_OF_ORDER => 'formation'
        );
        $arLoadOrder = array(
            'NAME' => $orderName,
            'ACTIVE' => 'Y',
            'IBLOCK_ID' => $this->iBlockIdOrders,
            'PROPERTY_VALUES' => $arProperty);
        if($orderID = $el->Add($arLoadOrder)) {
            return $orderID;
        } else {
            return $el->LAST_ERROR;
        }
    }
    //endregion
    //region Добавляет ЕУ в заказ
    /** Добавляет ЕУ в заказ
     * @param $orderId - id заказа
     * @param $unitID - массив с id ЕУ / либо id ЕУ
     */
    public function addUnitToOrder($orderId, $unitID) {
        $arUnitsResult = $this->getListUnitsFromOrder($orderId);
        $flag = false; // параметр отвечающий за то, что были произведены изменения в списке ЕУ
        if(is_array($unitID)) {
            foreach($unitID as $uID) {
                if(!in_array($uID, $arUnitsResult)) {
                    $arUnitsResult[] = $uID;
                    $flag = true;
                }
            }
        } else {
            if(!in_array($unitID, $arUnitsResult)) {
                $arUnitsResult[] = $unitID;
                $flag = true;
            }
        }
        if($flag) {
            $this->setUnitsListToOrder($orderId, $arUnitsResult);
        }
    }
    //endregion
    //region Удаляет ЕУ из формирующегося заказа
    /** Удаляет ЕУ из формирующегося заказа
     * @param $orderId - id заказа
     * @param $unitID - массив / строка содержит в себе id ЕУ
     * @return string - если == UPDATE то значит просто обновил список ЕУ, == UPDATE то удалил полностью весь заказ
     */
    public function removeUnitsFromOrder($orderId, $unitID) {
        $arUnitsResult = $this->getListUnitsFromOrder($orderId);
        $flag = false;
        if(is_array($unitID)) {
            foreach($unitID as $uID) {
                if(in_array($uID, $arUnitsResult)) {
                    $keyUnit = array_search($uID, $arUnitsResult);
                    unset($arUnitsResult[$keyUnit]);
                    $flag = true;
                }
            }
        } else {
            if(in_array($unitID, $arUnitsResult)) {
                $keyUnit = array_search($unitID, $arUnitsResult);
                unset($arUnitsResult[$keyUnit]);
                $flag = true;
            }
        }
        if($flag) {
            // Если количество ЕУ в заказе больше нуля
            if(count($arUnitsResult) > 0) {
                $this->setUnitsListToOrder($orderId, $arUnitsResult);
                return 'UPDATE';
            }
            // Если количество ЕУ в заказе равно нулю
            else {
                // Удаляет весь заказ если убрать последний из UNITов
                //CIBlockElement::Delete($orderId);
                return 'DELETE';
            }
        }
    }
    //endregion
    //region Возвращает текущий список ЕУ у заказа
    /** Возвращает текущий список ЕУ у заказа
     * @param $orderId - id заказа
     * @return array
     */
    public function getListUnitsFromOrder($orderId) {
        $arUnitsResult = array();
        $rsUnits = CIBlockElement::GetProperty(
            $this->iBlockIdOrders,
            $orderId,
            array('name' => 'asc'),
            array('CODE' => PROPERTY_CODE_UNITS));
        while($obUnits = $rsUnits->Fetch()) {
            $arUnitsResult[] = $obUnits['VALUE'];
        }
        return $arUnitsResult;
    }
    //endregion
    //region Обновление списка ЕУ в свойстве заказа
    /** Обновление списка ЕУ в свойстве заказа
     * @param $orderId - id заказа
     * @param $unitsList - список ЕУ для обновления
     */
    private function setUnitsListToOrder($orderId, $unitsList) {
        CIBlockElement::SetPropertyValuesEx(
            $orderId,
            $this->iBlockIdOrders,
            array(PROPERTY_CODE_UNITS => $unitsList));
    }
    //endregion
    //region Установка свойства "Формирующийся" для заказа
    /** Установка свойства "Формирующийся" для заказа
     * @param $orderId - id инфоблока заказа
     * @param $statusForming - Y / N
     */
    public function setStatusForming($orderId, $statusForming) {
        CIBlockElement::SetPropertyValuesEx(
            $orderId,
            $this->iBlockIdOrders,
            array(PROPERTY_CODE_FORMING_OF_ORDER => $this->arValuesForming[$statusForming]['ID']));
    }
    //endregion
    //region Возвращает массив значений для типа заказа
    /** Возвращает массив значений для типа заказа
     * @return array
     */
    public function getValuesTypeOrder() {
        return $this->arValuesTypeOrder;
    }
    //endregion
    //region Возвращает массив со значениями свойств определяющего запрос
    /** Возвращает массив со значениями свойств определяющего запрос
     * @return array
     */
    public function getValuesRequest() {
        return $this->arValuesRequest;
    }
    //endregion
    //region Возвращает массив со значениями свойства определяющего формирование заказа
    /** Возвращает массив со значениями свойства определяющего формирование заказа
     * @return array
     */
    public function getValuesForming() {
        return $this->arValuesForming;
    }
    //endregion
    //region Возвращает текущий статус свойства "Формирующийся"
    /** Возвращает текущий статус свойства "Формирующийся"
     * @param $orderId - id заказа
     * @return mixed
     */
    public function getCurentForming($orderId) {
        $rsForming = CIBlockElement::GetProperty(
            $this->iBlockIdOrders,
            $orderId,
            array('name' => 'asc'),
            array('CODE' => PROPERTY_CODE_FORMING_OF_ORDER));
        if($obForming = $rsForming->Fetch()) {
            return $obForming['VALUE_XML_ID'];
        }

        return false;
    }
    //endregion
    //region Формирует html на странице списка ЕУ
    /** Формирует html на странице списка ЕУ
     * @param $contract - id договора
     * @return string
     */
    public function getHTMLFromContract($contract) {
        $resHTML = '';
        // количество договоров, у ЕУ выделенных пользователем
        $countOrders = self::getCountContracts();
        // Количество договоров с добавленными ЕУ больше одного
        if($countOrders > 1) {
            $resHTML .= '
                <div class="control__actions">
                    <div class="form__row__name"></div>
                    <div class="form__row__col__item">
                        <a href="#" class="js-btn-reset">' . Loc::getMessage('BUTTON_RESET') .'</a>
                    </div>
                    <div class="form__row__col__item _size4 frn-danger">
                        ' . Loc::getMessage('ERROR_FEW_CONTRACTS') . '
                    </div>                   
                </div>';
            self::setFlag(false);
        }
        // Договор с добавленными ЕУ один
        elseif($countOrders == 1) {
            $countUnitsOfContract = self::getCountUnitsOfContract($contract);
            $flag = self::checkFlag();
            // Количество ЕУ в договоре больше чем 1
            if($countUnitsOfContract > 1 && !$flag) {
                $resHTML .= '
            <div class="control__actions">
                <div class="form__row__name"></div>
                <div class="form__row__col__item">
                    <a href="#" class="js-btn-reset">' . Loc::getMessage('BUTTON_RESET') .'</a>
                </div>
                <div class="form__row__col__item _size2 frn-danger">
                    ' . Loc::getMessage('ERROR_EXIST_STATUS_UNIT') . '
                </div>                
            </div>';
            }
            // ЕУ в договоре 1
            else {
                $arTypesOrder = self::getTypesUnit($contract);
                if(!$arTypesOrder['NEW_ORDER'] && !$arTypesOrder['FORM_ORDER']) {
                    $resHTML .= '
                        <div class="control__actions">
                            <div class="form__row__name"></div>                    
                            <div class="form__row__col__item">
                                <a href="#" class="js-btn-reset">' . Loc::getMessage('BUTTON_RESET') .'</a>
                            </div>
                            <div class="form__row__col__item _size5 frn-danger">
                                ' . Loc::getMessage('ERROR_FORMING_ORDER') . '
                            </div>
                        </div>';
                }
                else {
                    // Если есть типы заказов для создания нового заказа
                    $htmlNewOrder = '';
                    $htmlFormOrder = '';
                    if($arTypesOrder['NEW_ORDER']) {
                        $htmlNewOrder .= '
                            <div class="control__actions">
                                <div class="form__row__name">' . Loc::getMessage('CREATE_NEW_ORDER') . '</div>
                                <div class="form__row__col__item _size2">
                                    <select name="F_UNIT_NEW_ORDER" id="new-order" class="js-new-order">
                                        <option value="">' . Loc::getMessage('EMPTY') . '</option>';
                    }
                    if($arTypesOrder['FORM_ORDER']) {
                        $htmlFormOrder .= '
                            <div class="control__actions">
                                <div class="form__row__name">' . Loc::getMessage('ADD_TO_ORDER') . '</div>
                                <div class="form__row__col__item _size2">
                                    <select name="F_UNIT_ADD_TO_ORDER" id="add-to-order" class="js-add-to-order">
                                        <option value="">' . Loc::getMessage('EMPTY') . '</option>';
                        $arListOrderForming = $this->arListOrdersForming[$contract];
                        if(empty($arListOrderForming)) {
                            $arListOrderForming = $this->getListFormingOrders($contract);
                        }
                        foreach($arListOrderForming as $iOrder => $arOrder) {
                            $codeOfTypeOrder = $arOrder['PROPERTY_' . PROPERTY_CODE_TYPE_OF_ORDER];
                            $htmlFormOrder .= '<option class="tooltip-option" value="' . $arOrder['ID'] . '" title="' . Loc::getMessage('ORDER_TYPE_' . strtoupper($codeOfTypeOrder)) . '">
                                ' . $arOrder['NAME'] . '
                                </option>';
                        }
                    }
                    foreach($arTypesOrder['LIST_TYPES'] as $typeOrder => $arVariablesType) {
                        $disabled = '';
                        if(!$arVariablesType['ENABLE']) {
                            $disabled = ' disabled';
                        }
                        if($arVariablesType['ALLOW']) {
                            $htmlNewOrder .= '<option value="' . $typeOrder . '"' . $disabled . '>' . Loc::getMessage('ORDER_TYPE_' . strtoupper($typeOrder)) . '</option>';
                        } elseif($arTypesOrder['FORM_ORDER']) {
                            //$htmlFormOrder .= '<option value="' . $typeOrder . '"' . $disabled . '>' . Loc::getMessage('ORDER_TYPE_' . strtoupper($typeOrder)) . '</option>';
                        }
                    }
                    if($arTypesOrder['NEW_ORDER']) {
                        $htmlNewOrder .= '
                                </select>
                            </div>
                            <div class="form__row__col__item">
                                <button type="button" class="btn js-btn-order btn-disabled">' . Loc::getMessage('CREATE') .'</button>
                            </div>
                        </div>';
                    }
                    if($arTypesOrder['FORM_ORDER']) {
                        $htmlFormOrder .= '
                                </select>
                            </div>
                            <div class="form__row__col__item">
                                <button type="button" class="btn js-btn-order btn-disabled">' . Loc::getMessage('ADD') .'</button>
                            </div>
                        </div>';
                    }
                    $resHTML .= '<div class="control__actions">
                                <div class="form__row__col__item">
                                    <a href="#" class="js-btn-reset">' . Loc::getMessage('BUTTON_RESET') .'</a>
                                </div>
                            </div>' . $htmlNewOrder . $htmlFormOrder;
                }
            }
        }
        return $resHTML;
    }
    //endregion
    //region Проверяет принадлежность пользователя к группе суперпользователи (true - принадлежит, false - не принадлежит)
    /**
     * Проверяет принадлежность пользователя к группе суперпользователи (true - принадлежит, false - не принадлежит)
     */
    public function checkSUser() {
        if(empty($this->arListUserGroups)) {
            $this->arListUserGroups = self::getUserGroups();
        }
        if(in_array(U_GROUP_CODE_CLIENT_S_USER, $this->arListUserGroups)) {
            return true;
        } else {
            return false;
        }
    }
    //endregion
    //region Возвращает код группы которой принадлежит пользователь
    /**
     * Возвращает код группы которой принадлежит пользователь
     */
    private function getUserGroups() {
        $res = CUser::GetUserGroupList($this->userID);
        $arUserGroupID = array();
        while ($arGroup = $res->Fetch()){
            $arUserGroupID[] = $arGroup['GROUP_ID'];
        }
        $arFilterGroups = array('ACTIVE' => 'Y', 'ID' => implode('|', $arUserGroupID));
        $rsGroups = CGroup::GetList($by = 'SORT', $order = 'ASC', $arFilterGroups);
        $arUserGroupCode = array();
        while($obGroup = $rsGroups->Fetch()) {
            $arUserGroupCode[] = $obGroup['STRING_ID'];
        }
        return $arUserGroupCode;
    }
    //endregion

    /*-------------------- Функционал c добавлением через сессию --------------------*/

    //region Добавляет элемент в заказ
    /** Добавляет элемент в заказ
     * @param $stepOrder - этап заказа (TEMP / SAVE)
     * @param $typeOrder - код типа заказа (оцифровка, выезд и т.п.)
     * @param $idItem - id ЕУ реестра
     * @param $arItem - дополнительный массив с данными ЕУ
     */
    public function addItemToOrder($stepOrder, $typeOrder, $contractID, $idItem) {
        $_SESSION['ORDERS_' . $this->userID][$stepOrder][$contractID][$typeOrder][] = $idItem;
    }
    //endregion
    //region Удаление ЕУ из заказа
    /** Удаление ЕУ из заказа
     * @param $stepOrder - этап заказа
     * @param $typeOrder - код типа заказа
     * @param $contractID - ID договора
     * @param $idItem - ID ЕУ (или ЕХ)
     */
    public function removeItemFromOrder($stepOrder, $typeOrder, $contractID, $idItem = false) {
        if($idItem) {
            $keyItem = array_search($idItem, $_SESSION['ORDERS_' . $this->userID][$stepOrder][$contractID][$typeOrder]);
            unset($_SESSION['ORDERS_' . $this->userID][$stepOrder][$contractID][$typeOrder][$keyItem]);
        }
        unset($_SESSION['ORDERS_' . $this->userID][$stepOrder][$contractID][$typeOrder]);
        if(empty($_SESSION['ORDERS_' . $this->userID][$stepOrder][$contractID])) {
            unset($_SESSION['ORDERS_' . $this->userID][$stepOrder][$contractID]);
            if(empty($_SESSION['ORDERS_' . $this->userID][$stepOrder])) {
                unset($_SESSION['ORDERS_' . $this->userID][$stepOrder]);
            }
        }
    }
    //endregion
    //region Сохранение временного заказа
    /** Сохранение временного заказа
     * @param $typeOrder - код типа заказа
     * @param $contractID - id договора
     */
    public function saveTempOrder($typeOrder, $contractID) {
        if(!empty($_SESSION['ORDERS_' . $this->userID]['TEMP'][$contractID][$typeOrder])) {
            $saveArray = array_merge($_SESSION['ORDERS_' . $this->userID]['SAVE'][$contractID][$typeOrder],
                $_SESSION['ORDERS_' . $this->userID]['TEMP'][$contractID][$typeOrder]);
            $_SESSION['ORDERS_' . $this->userID]['SAVE'][$contractID][$typeOrder] = $saveArray;
        }
        self::removeItemFromOrder('TEMP', $typeOrder, $contractID);
    }
    //endregion
    //region Проверка наличия ЕУ в добавленных этапах заказа
    /** Проверка наличия ЕУ в добавленных этапах заказа
     * @param $typeOrder - код типа заказа
     * @param $contractID - ID договора
     * @param $idItem - ID ЕУ (или ЕХ)
     * @return bool
     */
    public function checkUnit($typeOrder, $contractID, $idItem) {
        $arSSave = $_SESSION['ORDERS_' . $this->userID]['SAVE'][$contractID][$typeOrder];
        $arSTemp = $_SESSION['ORDERS_' . $this->userID]['TEMP'][$contractID][$typeOrder];

        if(!in_array($idItem, $arSSave) && !in_array($idItem, $arSTemp)) {
            return false;
        } else {
            return true;
        }
    }
    //endregion
    //region Возвращает список ЕУ добавленных пользователем
    /** Возвращает список ЕУ добавленных пользователем
     * @param $typeOrder - код типа заказа
     * @param $contractID - id договора
     * @return mixed
     */
    public function getListUnits($typeOrder, $contractID) {
        self::saveTempOrder($typeOrder, $contractID);
        return $_SESSION['ORDERS_' . $this->userID]['SAVE'][$contractID][$typeOrder];
    }
    //endregion
    /*-------------------- Функционал для страницы со списком реестров --------------------*/
    //region Установка значений массива типов заказа по умолчанию
    /**
     * Установка значений массива типов заказа по умолчанию
     */
    public function setDefaultTypeOrder() {
        $_SESSION['TMP_ORDERS_' . $this->userID]['FLAG'] = true;
        // По умолчанию доступны все типы заказов
        $_SESSION['TMP_ORDERS_' . $this->userID]['FLAG_TYPES'] = array(
            'digitization'          => true,       // Оцифровка
            'delivery'              => true,       // Временный вывоз со склада
            'reception_of_units'    => true,       // Возврат на склад
            'destruction'           => true,       // Заявка на уничтожение
            'arbitrarily'           => true,       // Произвольно
        );
        if($_SESSION['TMP_ORDERS_' . $this->userID]['FLAG_TYPES']) {

        }
    }
    //endregion
    //region Возвращает массив со списком типов заказов разрешенных для статуса WMS ЕУ
    /** Возвращает массив со списком типов заказов разрешенных для статуса WMS ЕУ
     * @param $ufXmlId - код статуса WMS ЕУ
     * @param $nonArray - параметр определяющий возвращать массив с типами заказов (true) при булевое значение
     * @return array|bool
     */
    public function checkPermission($ufXmlId, $nonArray = false) {
        $arTypesOrder = array(
            'digitization'          => false,       // Оцифровка
            'delivery'              => false,       // Временный вывоз со склада
            'reception_of_units'    => false,       // Возврат на склад
            'destruction'           => false,       // Заявка на уничтожение
            'arbitrarily'           => false,       // Произвольно
        );
        // Массив с разрешенными для создания заказов статусами WMS
        $arPermitOrder = array(
            'placed_storage',                               // Размещено на хранение (Складирование)
            'delivered' => array('reception_of_units'),     // Доставлено клиенту (Заказ операций над ЕУ или ее содержимым)
            'placed_operation'                              // Размещено на хранение (Заказ операций над ЕУ или ее содержимым)
        );
        $arPermitOrder = array(
            'placed_storage' => array('digitization', 'delivery', 'destruction', 'arbitrarily'),
            'delivered' => array('reception_of_units'),
            'placed_operation' => array('digitization', 'delivery', 'destruction', 'arbitrarily'),
        );
        // Если статус передали
        if(!empty($ufXmlId)) {
            // Если статус входит в массив со списком разрешенных статусов
            if(isset($arPermitOrder[$ufXmlId]) || in_array($ufXmlId, $arPermitOrder)) {
                if($nonArray) {
                    return true;
                } else {
                    foreach($arTypesOrder as $cOType => $vOType) {
                        if(is_array($arPermitOrder[$ufXmlId]) && in_array($cOType, $arPermitOrder[$ufXmlId])) {
                            $arTypesOrder[$cOType] = true;
                        } else {
                            $_SESSION['TMP_ORDERS_' . $this->userID]['FLAG_TYPES'][$cOType] = false;
                        }
                    }
                    return $arTypesOrder;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    //endregion
    //region Возвращает ID пользователя
    /** Возвращает ID пользователя
     * @return null
     */
    public function getUserID() {
        return $this->userID;
    }
    //endregion
    //region Добавление в общий список всех выделенных пользователем ЕУ (возможно в виде массива редставление данных)
    /** Добавление в общий список всех выделенных пользователем ЕУ
     * @param $contractID - ID договора
     * @param $unitID - ID ЕУ
     * @param $statusWMS - Статус WMS ЕУ
     */
    public function addUnitToList($contractID, $unitID, $statusWMS) {
        if(is_array($contractID)) {
            foreach($contractID as $contract => $arUnit) {
                foreach($arUnit as $unID) {
                    self::addUnit($contract, $unID, $unitID[$unID]);
                }
            }
        } else {
            self::addUnit($contractID, $unitID, $statusWMS);
        }
    }
    //endregion
    //region Добавление в общий список всех выделенных пользователем ЕУ
    /** Добавление в общий список всех выделенных пользователем ЕУ
     * @param $contractID - ID договора
     * @param $unitID - ID ЕУ
     * @param $statusWMS - Статус WMS ЕУ
     */
    private function addUnit($contractID, $unitID, $statusWMS) {
        $flagUnit = self::checkPermission($statusWMS, true);
        $_SESSION['TMP_ORDERS_' . $this->userID]['LIST_UNITS'][$contractID][$unitID] = array(
            'STATUS' => $statusWMS,
            'FLAG' => $flagUnit
        );
    }
    //endregion
    //region Удаление из общего списка ЕУ
    /** Удаление из общего списка ЕУ
     * @param $contractID - ID договора или массив с договорами
     * @param $unitID - ID ЕУ или массив ЕУ
     */
    public function removeUnitToList($contractID, $unitID) {
        if(is_array($contractID)) {
            foreach($contractID as $contract => $arUnit) {
                foreach ($arUnit as $unID) {
                    self::removeUnit($contract, $unID);
                }
            }
        } else {
            self::removeUnit($contractID, $unitID);
        }

    }
    //endregion
    //region Удаление ЕУ
    /** Удаление ЕУ
     * @param $contractID - ID договора
     * @param $unitID - ID ЕУ
     */
    private function removeUnit($contractID, $unitID) {
        unset($_SESSION['TMP_ORDERS_' . $this->userID]['LIST_UNITS'][$contractID][$unitID]);
        self::setFlagAfterRemoveUnit();
        if(empty($_SESSION['TMP_ORDERS_' . $this->userID]['LIST_UNITS'][$contractID])) {
            unset($_SESSION['TMP_ORDERS_' . $this->userID]['LIST_UNITS'][$contractID]);
            self::setDefaultTypeOrder();
        }
    }
    //endregion
    //region Возвращает количество договоров (учитываются только те в которых имеются ЕУ)
    /** Возвращает количество договоров (учитываются только те в которых имеются ЕУ)
     * @return int
     */
    public function getCountContracts() {
        $countContracts = 0;
        if(count($_SESSION['TMP_ORDERS_' . $this->userID]['LIST_UNITS']) > 0) {
            foreach($_SESSION['TMP_ORDERS_' . $this->userID]['LIST_UNITS'] as $arContract) {
                if(count($arContract) > 0) {
                    $countContracts++;
                }
            }
        }
        return $countContracts;
    }
    //endregion
    //region Возвращает количество ЕУ выделенных в списке реестра по одному договору
    /** Возвращает количество ЕУ выделенных в списке реестра по одному договору
     * @param $contract - ID договора
     * @return int
     */
    public function getCountUnitsOfContract($contract) {
        return count($_SESSION['TMP_ORDERS_' . $this->userID]['LIST_UNITS'][$contract]);
    }
    //endregion
    //region Проверка разрешения на добавление ЕУ в заказ из списка реестра
    /** Проверка разрешения на добавление ЕУ в заказ из списка реестра
     * @return mixed
     */
    public function checkFlag() {
        return $_SESSION['TMP_ORDERS_' . $this->userID]['FLAG'];
    }
    //endregion
    //region Устанавливает параметр определяющий разрешение на добавление ЕУ в заказ из списка реестров
    /** Устанавливает параметр определяющий разрешение на добавление ЕУ в заказ из списка реестров
     * @param $flag - true / false
     */
    public function setFlag($flag) {
        $_SESSION['TMP_ORDERS_' . $this->userID]['FLAG'] = $flag;
    }
    //endregion
    //region Установка значения параметра отвечающего за разрешение выводить список типов заказов после удаления ЕУ
    /**
     * Установка значения параметра отвечающего за разрешение выводить список типов заказов после удаления ЕУ
     */
    public function setFlagAfterRemoveUnit() {
        if(empty($_SESSION['TMP_ORDERS_' . $this->userID]['LIST_UNITS'])) {
            self::setDefaultTypeOrder();
        } else {
            if(count($_SESSION['TMP_ORDERS_' . $this->userID]['LIST_UNITS']) > 1) {
                self::setFlag(false);
            } else {
                self::checkAllowStatus();
                //self::setFlag($showTypes);
            }
        }
    }
    //endregion
    //region Сброс всех добавленных ЕУ со страницы списка реестров
    /**
     * Сброс всех добавленных ЕУ со страницы списка реестров
     */
    public function resetUnits() {
        unset($_SESSION['TMP_ORDERS_' . $this->userID]['LIST_UNITS']);
        self::setDefaultTypeOrder();
    }
    //endregion
    //region Возвращает типы заказов которые доступны для данного договора
    /** Возвращает типы заказов которые доступны для данного договора
     * @param $contract - ID договора
     * @return array
     */
    public function getTypesUnit($contract) {
        $arTypesOrder = array(
            'digitization',         // Оцифровка
            'delivery',             // Временный вывоз со склада
            'reception_of_units',   // Возврат на склад
            'destruction',          // Заявка на уничтожение
            'arbitrarily',          // Произвольно
        );
        //$_SESSION['TMP_ORDERS_' . $this->userID]['FLAG_TYPES']
        $arReturn = array();
        //$arSession = $_SESSION['ORDERS_' . $this->userID]['SAVE'];
        $arListOrderForming = $this->arListOrdersForming[$contract];
        if(empty($arListOrderForming)) {
            $arListOrderForming = self::getListFormingOrders($contract);
        }
        $arListOrderTypes = array();
        foreach($arListOrderForming as $iOrder => $arOrder) {
            $arListOrderTypes[$arOrder['PROPERTY_' . PROPERTY_CODE_TYPE_OF_ORDER]] = $arOrder;
        }

        $arReturn['NEW_ORDER'] = true;
        $arReturn['FORM_ORDER'] = false;
        foreach($arTypesOrder as $typeOrder) {
            // отвечает за то, что будет ли включенным элемент типа заказа в списке "добавить к новому заказу"
            $arReturn['LIST_TYPES'][$typeOrder]['ENABLE'] = $_SESSION['TMP_ORDERS_' . $this->userID]['FLAG_TYPES'][$typeOrder];
            // отвечает за вывод типа заказа в списке "добавить к новому заказу"
            $arReturn['LIST_TYPES'][$typeOrder]['ALLOW'] = true;

            //$arReturn['LIST_TYPES'][$typeOrder]['ALLOW'] = true;
            //$arReturn['LIST_TYPES'][$typeOrder]['ENABLE'] = true;

            // Заказ с типом $typeOrder по договору $contract формируется
            if(!empty($arListOrderTypes[$typeOrder])) {
                // Если заказ с таким типом уже формируется то запрещаем формировать еще один с ним же
                //$arReturn['LIST_TYPES'][$typeOrder]['ALLOW'] = false;
                if($arReturn['LIST_TYPES'][$typeOrder]['ENABLE']) {
                    $arReturn['FORM_ORDER'] = true;
                }
            }
            // Заказ с типа $typeOrder по договору $contract не формируется
            else {
                //$arReturn['LIST_TYPES'][$typeOrder]['ALLOW'] = true;
                if($arReturn['LIST_TYPES'][$typeOrder]['ENABLE']) {
                    $arReturn['NEW_ORDER'] = true;
                }
            }
        }
        return $arReturn;
    }
    //endregion
    //region Проверка добавления ЕУ в списке реестров
    /** Проверка добавления ЕУ в списке реестров
     * @param $contract - ID договора
     * @param $unitID - ID ЕУ
     * @return bool
     */
    public function checkExistUnit($contract, $unitID) {
        if(!empty($_SESSION['TMP_ORDERS_' . $this->userID]['LIST_UNITS'][$contract][$unitID]['STATUS'])
            || $_SESSION['TMP_ORDERS_' . $this->userID]['LIST_UNITS'][$contract][$unitID]['FLAG'] === false) {
            return true;
        } else {
            return false;
        }
    }
    //endregion
    //region Проверяет на наличие в списке ЕУ элемента у которого статус не разрешает выводить список типов заказа
    /** Проверяет на наличие в списке ЕУ элемента у которого статус не разрешает выводить список типов заказа
     * @return bool
     */
    public function checkAllowStatus() {
        $arPermitOrder = array(
            'placed_storage' => array(
                'digitization',
                'delivery',
                'destruction',
                'arbitrarily'
            ),
            'delivered' => array('reception_of_units'),
            'placed_operation' => array(
                'digitization',
                'delivery',
                'destruction',
                'arbitrarily'
            ),
        );
        $showTypes = true;
        $arStatus = array();
        foreach($_SESSION['TMP_ORDERS_' . $this->userID]['LIST_UNITS'] as $contractID => $arContract) {
            if($showTypes) {
                foreach($arContract as $unitID => $arUnit) {
                    if(empty($arPermitOrder[$arUnit['STATUS']]) || !$arUnit['FLAG']) {
                        $showTypes = false;
                        break;
                    } else {
                        if(empty($arStatus)) {
                            $arStatus = $arPermitOrder[$arUnit['STATUS']];
                        } else {
                            $arStatus = array_intersect($arStatus, $arPermitOrder[$arUnit['STATUS']]);
                            if(empty($arStatus)) {
                                $showTypes = false;
                                break;
                            }
                        }
                    }
                }
            } else {
                break;
            }
        }
        if(!empty($arStatus) && $showTypes) {
            foreach($_SESSION['TMP_ORDERS_' . $this->userID]['FLAG_TYPES'] as $cStatus => $vStatus) {
                if(in_array($cStatus, $arStatus)) {
                    $_SESSION['TMP_ORDERS_' . $this->userID]['FLAG_TYPES'][$cStatus] = true;
                } else {
                    $_SESSION['TMP_ORDERS_' . $this->userID]['FLAG_TYPES'][$cStatus] = false;
                }
            }
        }
        self::setFlag($showTypes);
        return $showTypes;
    }
    //endregion
    //region Возвращает первый договор
    /** Возвращает первый договор
     * @return mixed
     */
    public function getFirstContract() {
        $keys = array_keys($_SESSION['TMP_ORDERS_' . $this->userID]['LIST_UNITS']);
        return $keys[0];
    }
    //endregion
    //region Добавление ЕУ к оформляемым заказам на странице создания заказа
    /** Добавление ЕУ к оформляемым заказам на странице создания заказа
     * @param $typeOrder - тип заказа
     */
    public function addUnitsFromListRegistry($typeOrder) {
        $contractID = self::getFirstContract();
        foreach($_SESSION['TMP_ORDERS_' . $this->userID]['LIST_UNITS'][$contractID] as $unitID => $arUnit) {
            self::addItemToOrder('SAVE', $typeOrder, $contractID, $unitID);
        }
    }
    //endregion
    //region Проверяет наличие ЕУ в сессии пользователя
    /** Проверяет наличие ЕУ в сессии пользователя
     * @return bool
     */
    public function checkExistUnits() {
        if(!empty($_SESSION['TMP_ORDERS_' . $this->userID]['LIST_UNITS'])) {
            return true;
        } else {
            return false;
        }
    }
    //endregion
    //region Проверяет наличие ЕУ в сессии пользователя в блоке создания заказа
    /** Проверяет наличие ЕУ в сессии пользователя в блоке создания заказа
     * @param $contractID - ID договора
     * @param $typeOrder - код типа заказа
     * @return bool
     */
    public function checkExistUnitsByOrder($contractID, $typeOrder) {
        if(!empty($_SESSION['ORDERS_' . $this->userID]['SAVE'][$contractID][$typeOrder])) {
            return true;
        } else {
            return false;
        }
    }
    //endregion
    //region Возвращает массив ID ЕУ отмеченных пользователем
    /** Возвращает массив ID ЕУ отмеченных пользователем
     * @return array
     */
    public function getListCheckUnitID() {
        $arUnits = array();
        foreach($_SESSION['TMP_ORDERS_' . $this->userID]['LIST_UNITS'] as $contractID => $arContract) {
            foreach($arContract as $idUnit => $arUnit) {
                $arUnits[] = $idUnit;
            }
        }
        return $arUnits;
    }
    //endregion
    //region Возвращает массив ID ЕУ отмеченных пользователем в блоке создания заказа
    /** Возвращает массив ID ЕУ отмеченных пользователем в блоке создания заказа
     * @param $contractID - ID договора
     * @param $typeOrder - код типа заказа
     * @return mixed
     */
    public function getListCheckUnitIDByOrders($contractID, $typeOrder) {
        $arUnits = $_SESSION['ORDERS_' . $this->userID]['SAVE'][$contractID][$typeOrder];
        return $arUnits;
    }
    //endregion
    //region Проверяет наличие у пользователя уже имеющихся для создания заказов
    /** Проверяет наличие у пользователя уже имеющихся для создания заказов
     * @return bool
     */
    public function checkOrdersInSession() {
        if(!empty($_SESSION['ORDERS_' . $this->userID]['SAVE'])) {
            return $_SESSION['ORDERS_' . $this->userID]['SAVE'];
        } else {
            return false;
        }
    }
    //endregion
}
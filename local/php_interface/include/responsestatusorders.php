<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.04.2017
 * Time: 17:42
 *
 * Class ResponseStatusOrders
 * @package EME
 */
namespace EME;

/**
 * @property GetStatusOrderResult GetStatusOrderResult
 */
class ResponseStatusOrders extends Response{
    /**
     * @var array $listStatusOrder Список объектов StatusOrder
     */
    public $listStatusOrder;
}
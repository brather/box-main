<?php

namespace EME;

//use Bitrix\Iblock\IblockTable,
//    Bitrix\Main\Loader;

/**
 * Class WMS
 * @package EME
 */
class WMS {
    public $soap;
    public $testMode;
    private $logging = false;

    public function __construct($testMode = false) {
        $options = array(
            'soap_version' => SOAP_1_2,
            'exceptions' => true,
            'trace' => 1,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'connection_timeout' => 1
        );
        $this->testMode = $testMode;
        $this->soap = new \SoapClient(SOAP_URL, $options);
    }

    //region Загрузка данных клиента в WMS
    /** Загрузка данных клиента в WMS
     * @param object ClientData
     * @return Response
     */
    public function LoadClientData($clientData) {
        $Request = new Request();
        $Request->clientData = $clientData;
        $Response = $this->soap->LoadClientData($Request);
        return $Response;
    }
    //endregion
    //region Загрузка данных о физических адресах клиента в WMS
    /** Загрузка данных о физических адресах клиента в WMS
     * @param object ClientAddress
     * @return Response
     * Возможные ошибки:
     * EST	Превышено время ожидания ответа от WMS
     * CNF	В БД WMS не найден клиент
     * DEF	Передаваемые параметры не соответствуют установленному формату
     */
    public function LoadPhysicalAddressClient($clientAddress) {
        $Request = new Request();
        $Request->clientAddress = $clientAddress;
        $Response = $this->soap->LoadPhysicalAddressClient($Request);
        return $Response;
    }
    //endregion
    //region Выгрузка данных о статусах всех ЕУ из WMS для клиента
    /** Выгрузка данных о статусах всех ЕУ из WMS для клиента
     * @param string $clientId
     * @return ResponseStatusMu|null
     */
    public function GetStatusMeasUnitByClient($clientId){
        $ResponseStatusMu = null;
        $Request = new Request();
        $Request->clientId = $clientId;
        try {
            $ResponseStatusMu = $this->soap->GetStatusMeasUnitByClient($Request);
        } catch (\SoapFault $e) {
            $this->_logError($e->getFile() . " " . $e->getLine());
            $this->_logError($e->getMessage());
        }
        return $ResponseStatusMu;
    }
    //endregion
    //region Выгрузка данных о статусе одной ЕУ из WMS
    /** Выгрузка данных о статусе одной ЕУ из WMS
     * @param string $muCode - Идентификатор ЕУ
     * @return ResponseStatusMu|null
     */
    public function GetStatusMeasUnit(array $muCode){
        $ResponseStatusMu = null;

        $Request = new Request();
        $Request->listMuCode = array();
        foreach ($muCode as $code) {
            $MuCode = new MuCode();
            $MuCode->Code = $code;
            $Request->listMuCode[] = $MuCode;
        }

        $Request->dateTimeRequest = $this->getXMLDateTime();
        try {
            $ResponseStatusMu = $this->soap->GetStatusMeasUnit($Request);
        } catch (\SoapFault $e) {

            $this->_logError($e->getFile() . " " . $e->getLine());
            $this->_logError($e->getMessage());
        }

        return $ResponseStatusMu;
    }
    //endregion
    //region Выгрузка данных о количестве ЕУ и ЕХ клиента из WMS
    /** Выгрузка данных о количестве ЕУ и ЕХ клиента из WMS
     * @param string $clientId
     * @return ResponseQtyMSUnit
     */
    public function GetQtyMSUnitClient($clientId){
        $Request = new Request();
        $Request->clientId = $clientId;
        $ResponseQtyMSUnit = $this->soap->GetQtyMSUnitClient($Request);
        return $ResponseQtyMSUnit;
    }
    //endregion
    //region Загрузка данных заказа в WMS
    /** Загрузка данных заказа в WMS
     * @param object OrderData
     * @return Response
     */
    public function LoadOrder(Request $orderData){

        $Response = $this->soap->LoadOrder($orderData);
        //$rq = $this->soap->__getLastRequest();
        //$rs = $this->soap->__getLastResponse();

        return $Response;
    }
    //endregion
    //region Выгрузка данных о статусах заказов из WMS (все заказы по клиенту)
    /** Выгрузка данных о статусах заказов из WMS (все заказы по клиенту)
     * @param string $clientId
     * @return ResponseStatusOrders - В нем содержится: Дата и время ответа на запрос, состояние ответа, описание ошибки (если есть), список номеров заказов и их статусы.
     */
    public function GetStatusOrdersByClient($clientId){
        $Request = new Request();
        $Request->clientId = $clientId;
        $ResponseStatusOrders = $this->soap->GetStatusOrdersByClient($Request);
        return $ResponseStatusOrders;
    }
    //endregion
    //region Выгрузка данных о статусе заказа из WMS
    /** Выгрузка данных о статусе заказа из WMS
     * @param string $orderNumber
     * @return ResponseStatusOrders - В нем содержится: Дата и время ответа на запрос, состояние ответа, описание ошибки (если есть), список номеров заказов и их статусы.
     */
    public function GetStatusOrder($orderNumber){
        if ($this->testMode) {
            $ResponseStatusOrders = new ResponseStatusOrders;
            $ResponseStatusOrders->dateTimeResponse = $this->getXMLDateTime();
            $ResponseStatusOrders->state = rand(0, 1);
            if($ResponseStatusOrders->state) {
                $arError = array('EST', 'UNF');
                $ResponseStatusOrders->descriptionError = $arError[rand(0, 1)];
            } else {
                $ResponseStatusOrders->descriptionError = null;
            }
            $StatusOrder = new StatusOrder;
            $StatusOrder->orderNumber = $orderNumber;
            $arOrderTypes = array('digitizing', 'issueMu', 'incomeMu', 'applicationDestruction', 'departureExpert', 'shippingSupplies');
            $StatusOrder->orderType = $arOrderTypes[rand(0, 5)];
            $StatusOrder->currentStatus = rand(0, 6);

            $ResponseStatusOrders->listStatusOrder[] = $StatusOrder;
        } else {
            $Request = new Request();
            $Request->orderNumber = $orderNumber;
            $ResponseStatusOrders = $this->soap->GetStatusOrder($Request);
        }
        return $ResponseStatusOrders;
    }
    //endregion
    //region Выгрузка данных о тарифе по услуге из WMS
    /** Выгрузка данных о тарифе по услуге из WMS
     * @param string $clientId
     * @param string $orderType - Тип заказа
     * @return ResponseTariffService - В нем содержится: Дата и время ответа на запрос, состояние ответа, описание ошибки (если есть), стоимость работ.
     */
    public function GetDataTariffService($clientId, $orderType){
        $Request = new Request();
        $Request->clientId = $clientId;
        $Request->orderType = $orderType;
        $ResponseTariffService  = $this->soap->GetDataTariffService($Request);
        return $ResponseTariffService;
    }
    //endregion
    //region Выгрузка данных об индивидуальной тарифной сетке клиента из WMS
    /** Выгрузка данных об индивидуальной тарифной сетке клиента из WMS
     * @param string $clientId
     * @return ResponseTariffSchedule - В нем содержится: Дата и время ответа на запрос, состояние ответа, описание ошибки (если есть), список состоящий из: тип услуги, параметры, единица измерения, стоимость, комментарии.
     */
    public function GetTariffScheduleClient($clientId){
        $Request = new Request();
        $Request->clientId = $clientId;
        $ResponseTariffSchedule  = $this->soap->GetTariffScheduleClient($Request);
        return $ResponseTariffSchedule;
    }

    static public function getXMLDateTime($dt = null)
    {
        if ($dt === null)
            return date('Y-m-d\TH:i:s');
        else
            return date('Y-m-d\TH:i:s', strtotime($dt));
    }
    //endregion

    private function _logError ($msg) {

        if ($this->logging === true) {

            if (file_exists($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR .  'log_soap_error_' . date("d_m_Y"))) {

                file_put_contents($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'log_soap_error_' . date("d_m_Y"), "[". date(DATE_ATOM)."] ". $msg . "\n", FILE_APPEND | LOCK_FILE);
            } else {

                file_put_contents($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'log_soap_error_' . date("d_m_Y"), "[". date(DATE_ATOM)."] ". $msg . "\n");
            }
        }
    }
}












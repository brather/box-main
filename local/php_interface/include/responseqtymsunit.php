<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.04.2017
 * Time: 17:42
 *
 * Class ResponseQtyMSUnit
 * @package EME
 */
namespace EME;

class ResponseQtyMSUnit extends Response {
    /**
     * @var int $qtyMeasUnit Количество ЕУ
     * @var int $qtyStorageUnit Количество ЕХ
     */
    public $qtyMeasUnit, $qtyStorageUnit;
}
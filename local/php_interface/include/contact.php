<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.04.2017
 * Time: 17:53
 *
 * Class Contact
 * @package EME
 */
namespace EME;

class Contact{
    /**
     * @var string $contactFullName ФИО контактного лица
     * @var string $position Должность
     * @var string $phoneService Телефон служебный
     * @var string $phoneMobile Телефон сотовый
     * @var string $email Электронная почта
     * @var int $isMainResponsible Признак главного ответственного («1» - главный, «-1» - нет)
     */
    public $contactFullName, $position, $phoneService, $phoneMobile, $email, $isMainResponsible;
}
<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.04.2017
 * Time: 17:54
 *
 * Class StatusOrder
 * @package EME
 */
namespace EME;

class StatusOrder extends Order{
    /**
     * @var string $currentStatus Текущий статус заказа
     * @var string $dateTimeCurrentStatus Дата и время присвоения текущего статуса в формате XML DateTime
     */
    public $currentStatus, $dateTimeCurrentStatus;
}
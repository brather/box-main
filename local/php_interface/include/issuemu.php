<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.04.2017
 * Time: 17:48
 */
namespace EME;
use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Application;
use Bitrix\Main\Loader;

/** Выдача ЕУ
 * Class IssueMu
 *
 * @package EME
*/
class IssueMu {
    /**
     * @var string $placeIssue Место выдачи
     * @var string $dateIssue Дата выдачи в формате XML DateTime
     * @var string $addressIssue Адрес выдачи
     */
    public $placeIssue;

    public $dateIssue;

    public $addressIssue;

    public $isUnitDecommission = false;

    /**
     * Возращает сформированную строку адреса, либо null
     * @param $code
     *
     * @return null|string
     */
    static public function getPlaceIssue ($code) {

        $filter = array('UF_XML_ID' => (string)$code);

        if (empty($filter['UF_XML_ID'])) return null;

        $connection = Application::getConnection();
        $sql = "SELECT `ID` FROM b_hlblock_entity WHERE `TABLE_NAME` = '" . HL_BLOCK_TABLE_ADRESS_ALIAS ."';";
        $res = $connection->query($sql)->fetch();
        $id = $res['ID'];
        unset($connection,$sql,$res);

        Loader::includeModule('highloadblock');

        $hlblock = HighloadBlockTable::getById($id)->fetch();
        $dClass = HighloadBlockTable::compileEntity($hlblock);

        $hl = $dClass->getDataClass();

        $db =  $hl::getList(array(
            'filter' => $filter
        ));

        if ($res = $db->fetch()) {
            $str = '';

            if ($res['UF_INDEX'])
                $str .= $res['UF_INDEX'];
            if ($res['UF_COUNTRY'])
                $str .= ", " . $res['UF_COUNTRY'];
            if ($res['UF_REGION'])
                $str .= ", " . $res['UF_REGION'];
            if ($res['UF_TOWN'])
                $str .= ", " . $res['UF_TOWN'];
            if ($res['UF_STREET']) {
                $str .= ", " . $res['UF_STREET'];
                if ($res['UF_HOUSE'])
                    $str .= " дом " . $res['UF_HOUSE'];
                if ($res['UF_HOUSING'])
                    $str .= " корпус " . $res['UF_HOUSING'];
                if ($res['UF_STRUCT'])
                    $str .= " " . $res['UF_STRUCT'];
            }
            if ($res['UF_OFFICE'])
                $str .= ", офис " . $res['UF_OFFICE'];

            unset($db, $res, $hl, $dClass, $hlblock);
            return $str;
        }

        return null;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 15.06.2017
 * Time: 17:51
 */
namespace EME {
    /**
     * @property string          state
     * @property listStatusOrder listStatusOrder
     * @property string          descriptionError
     */
    interface GetStatusOrderResult{};
}

namespace EME {
    /**
     * @property StatusOrders StatusOrders
     */
    interface listStatusOrder{};
}

namespace EME {
    /**
     * @property string currentStatus
     */
    interface StatusOrders{};
}
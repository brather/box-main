<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.04.2017
 * Time: 17:45
 *
 * Class Order
 * @package EME
 */
namespace EME;

class Order
    implements EmeInt {
    /**
     * @var string $orderNumber Номер заказа
     * @var string $dateTimeOrder Дата и время формирования в формате XML DateTime
     * @var string $orderType Тип заказа
     * @var string $orderName Название заказа
     * @var array $listMeasUnit Список объектов MeasUnit
     * @var string $comment Комментарий
     * @var Digitizing Оцифровка (заполняется, если тип заказа = Оцифровка)
     * @var IssueMu Выдача ЕУ (заполняется, если тип заказа = Выдача ЕУ)
     * @var IncomeMu Прием ЕУ (заполняется, если тип заказа = Прием ЕУ)
     * @var ApplicationDestruction Прием ЕУ (заполняется, если тип заказа = Заявка на уничтожение)
     * @var DepartureExpert Прием ЕУ (заполняется, если тип заказа = Выезд эксперта)
     * @var ShippingSupplies Прием ЕУ (заполняется, если тип заказа = Доставка расходных материалов)
     * @var AuditRoom Комната аудита
     */
    public $orderNumber;

    public $dateTimeOrder;

    public $orderType;

    public $orderName;

    public function __set( $name, $value )
    {
        $this->$name = $value;
    }

    /**
     * Вернет значение Типа заказа по его коду
     *
     * @param null|string $typeOrderCode
     *
     * @return string|bool
     */
    static public function getTypeOrder($typeOrderCode = null) {

        //\Bitrix\Main\Loader::includeModule('iblock');

        /* Get IBLOCK_ID from its CODE*/
        /*$arIblock = \Bitrix\Iblock\IblockTable::getList(array(
            'filter' => array('CODE' => IBLOCK_CODE_ORDERS),
            'select' => array('ID')
        ))->fetch();
        $IBLOCK_ID = (int) $arIblock['ID'];
        unset($arIblock);

        $arOrderProperty = array( 'SORT' => 'ASC' );
        $arFilterProperty = array( 'IBLOCK_ID' => $IBLOCK_ID );
        if ($typeOrderCode !== null && is_string($typeOrderCode) && mb_strlen($typeOrderCode) > 0)
            $arFilterProperty['CODE'] = $typeOrderCode;

        $rsProperty = \CIBlockProperty::GetPropertyEnum(
            PROPERTY_CODE_TYPE_OF_ORDER,
            $arOrderProperty,
            $arFilterProperty
        );

        if ( $obProperty = $rsProperty->Fetch() ) {
            return $obProperty['VALUE'];
        }*/

        //todo Полная чушь, НО - когда WMS смодет принимать orderType наш, передать ему нужно коды и соответствия
        $arProp = array(
            'digitization' => 'Оцифровка',
            'first_placement' => 'Прием ЕУ',
            'delivery' => 'Выдача ЕУ',
            //'reception_of_units' => '',
            'destruction' => 'Заявка на уничтожение',
            'checkout_out_expert' => 'Выезд эксперта',
            'supplies' => 'Доставка расходных материалов',
            'arbitrarily' => 'Произвольный',
            'irreversible' => 'Выдача ЕУ',
        );
        if (!$typeOrderCode || !$arProp[$typeOrderCode])
            return false;

        return $arProp[$typeOrderCode];

    }

    /**
     * Возвразщает все ЕУ этого заказа
     *
     * @param int $orderId
     *
     * @return array
     */
    static public function getUnitsFromOrder ($orderId) {

        \Bitrix\Main\Loader::includeModule('iblock');

        $arUnits = array();
        $rsUnits = \CIBlockElement::GetProperty(
            IBLOCK_CODE_ORDERS_ID,
            $orderId,
            array('NAME' => 'ASC'),
            array('CODE' => PROPERTY_CODE_UNITS)
        );

        while($obUnits = $rsUnits->Fetch()) {
            $mu = new \EME\MeasUnit();
            $eu = \CIBlockElement::GetList(
                array("SORT" => "ASC"),
                array("IBLOCK_ID" => IBLOCK_ID_REGISTRIES, "ID" => (int)$obUnits['VALUE']),
                false, false,
                array("ID", "IBLOCK_ID", "PROPERTY_PROP_SSCC")
            )->Fetch();

            $mu->barCode = $eu['PROPERTY_PROP_SSCC_VALUE'];
            $arUnits[] = $mu;
        }
        return $arUnits;
    }
}
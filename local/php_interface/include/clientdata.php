<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.04.2017
 * Time: 17:52
 *
 * Class ClientData
 * @package EME
 */
namespace EME;

class ClientData extends Client {
    /**
     * @var string $shortNameOrganization Краткое наименование организации
     * @var string $fullNameOrganization Полное наименование организации
     * @var string $fax Факс
     * @var string $INN ИНН
     * @var string $KPP КПП
     * @var string $legalAddress Юридический адрес
     * @var int $isActiveOrganization Статус организации («1» – активна, «-1» – не активна)
     * @var bool $isNewClient Новый клиент («true» - новый клиент, «false» - обновить данные клиента)
     */
    public $shortNameOrganization, $fullNameOrganization, $fax, $INN, $KPP, $legalAddress, $isActiveOrganization, $isNewClient;
}
<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.04.2017
 * Time: 17:50
 */

namespace EME;

/**
 * Заявка на уничтожение
 * Class ApplicationDestruction
 *
 * @package EME
 */
class ApplicationDestruction{
    /**
     * @var string $dateDestruction Дата уничтожения в формате XML DateTime
     */
    public $dateDestruction;
}
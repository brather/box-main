<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 25.04.2017
 * Time: 17:44
 *
 * Class ResponseTariffSchedule
 * @package EME
 */
namespace EME;

class ResponseTariffSchedule extends Response{
    /**
     * @var array $listTariffSchedule Список объектов TariffSchedule
     */
    public $listTariffSchedule;
}
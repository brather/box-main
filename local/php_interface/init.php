<?php

//error_reporting(E_ALL);
error_reporting(E_PARSE && E_ERROR);
ini_set('display_errors', 1);
use Bitrix\Main\Application;
use Bitrix\Main\Loader;

$documentRoot = Application::getInstance()->getContext()->getServer()->getDocumentRoot();
define('DOCUMENT_ROOT', $documentRoot);

require_once(DOCUMENT_ROOT . '/local/php_interface/include/constants.php');
require_once(DOCUMENT_ROOT . '/local/php_interface/include/functions.php');
require_once(DOCUMENT_ROOT . '/local/php_interface/include/orders.php');

/**
 * Автозагрузка Классов EME для работы с WMS
 */
Loader::registerAutoLoadClasses(null, array(
    'EME\WMS'                           => '/local/php_interface/include/wms.php',
    'EME\EmeInt'                        => '/local/php_interface/include/emeint.php',
    'EME\ApplicationDestruction'        => '/local/php_interface/include/applicationdestruction.php',
    'EME\AuditRoom'                     => '/local/php_interface/include/auditroom.php',
    'EME\Client'                        => '/local/php_interface/include/client.php',
    'EME\ClientAddress'                 => '/local/php_interface/include/clientaddress.php',
    'EME\ClientData'                    => '/local/php_interface/include/clientdata.php',
    'EME\Contact'                       => '/local/php_interface/include/contact.php',
    'EME\DepartureExpert'               => '/local/php_interface/include/departureexpert.php',
    'EME\Digitizing'                    => '/local/php_interface/include/digitizing.php',
    'EME\IncomeMu'                      => '/local/php_interface/include/incomemu.php',
    'EME\IssueMu'                       => '/local/php_interface/include/issuemu.php',
    'EME\Material'                      => '/local/php_interface/include/material.php',
    'EME\MeasUnit'                      => '/local/php_interface/include/measunit.php',
    'EME\Order'                         => '/local/php_interface/include/order.php',
    'EME\OrderData'                     => '/local/php_interface/include/orderdata.php',
    'EME\Response'                      => '/local/php_interface/include/response.php',
    'EME\Request'                       => '/local/php_interface/include/request.php',
    'EME\ResponseQtyMSUnit'             => '/local/php_interface/include/responseqtymsunit.php',
    'EME\ResponseStatusMu'              => '/local/php_interface/include/responsestatusmu.php',
    'EME\ResponseStatusOrders'          => '/local/php_interface/include/responsestatusorders.php',
    'EME\ResponseTariffSchedule'        => '/local/php_interface/include/responsetariffschedule.php',
    'EME\ResponseTariffService'         => '/local/php_interface/include/responsetariffservice.php',
    'EME\ShippingSupplies'              => '/local/php_interface/include/shippingsupplies.php',
    'EME\StatusMu'                      => '/local/php_interface/include/statusmu.php',
    'EME\StatusOrder'                   => '/local/php_interface/include/statusorder.php',
    'EME\TariffSchedule'                => '/local/php_interface/include/tariffschedule.php',
    'EME\MuCode'                        => '/local/php_interface/include/mucode.php',
));

AddEventHandler('main', 'OnBuildGlobalMenu', 'OnBuildGlobalMenuHandler');

function OnBuildGlobalMenuHandler(&$aGlobalMenu, &$aModuleMenu) {
    /* Добавим в меню Админпанели ссылку на импорт Реестра */
    foreach ($aModuleMenu as &$menu) {
        if ($menu['parent_menu'] == 'global_menu_content' && $menu['section'] == 'iblock') {
            $menu['items'][] = array(
                'text' => 'Импорт реестра',
                'url' => 'iblock_custom_import.php?lang=ru',
                'module_id' => 'iblock',
                'sort' => 10
            );
        }
    }
}
if (!function_exists('_jR')) {
    function _jR($data, $err = false) {
        header('Content-Type: application/json');
        if ($err) {
            header('HTTP/1.0 404 Not Found');
            header('Status: 404 Not Found');
            $data = ['error' => $data];
        }
        die(json_encode($data));
    };
}


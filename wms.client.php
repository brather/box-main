<?php
/**
 * Created by PhpStorm.
 * User: atereshchenko
 * Date: 01.02.2017
 * Time: 16:12
 */

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

header("Content-Type: text/html; charset=utf-8");
header('Cache-Control: no-store, no-cache');
header('Expires: ' . date('r'));

ini_set('display_errors', 1);
error_reporting(E_ALL & ~E_NOTICE);

// создаем объект для отправки на сервер
$req = new EME\ClientData;
$req->clientId = "0001";
$req->shortNameOrganization = "shortName";
$req->fullNameOrganization = "fullName";
$req->fax = "fax";
$req->INN = "INN";
$req->KPP = "KPP";
$req->legalAddress = "legalAddress";
$req->isActiveOrganization = 1;
$req->isNewClient = true;


$clientAddress = new EME\ClientAddress;
$contac1 = new EME\Contact;
$contac1->contactFullName = "Строка	ФИО контактного лица";
$contac1->position = "Строка	Должность";
$contac1->phoneService = "Телефон служебный";
$contac1->phoneMobile = "Телефон сотовый";
$contac1->email = "Электронная почта";
$contac1->isMainResponsible = 1;

$contac2 = new EME\Contact;
$contac2->contactFullName = "Строка	ФИО контактного лица #2";
$contac2->position = "Строка	Должность";
$contac2->phoneService = "Телефон служебный";
$contac2->phoneMobile = "Телефон сотовый";
$contac2->email = "Электронная почта";
$contac2->isMainResponsible = -1;

$clientAddress->clientId = 'ELR';
$clientAddress->actualAddress = 'Фактический адрес N1';
$clientAddress->listContact = array($contac1, $contac2);


$orderData = new EME\OrderData;
$measUnit1 = new EME\MeasUnit;
$measUnit1->barCode = '000000000000000642';
$measUnit2 = new EME\MeasUnit;
$measUnit2->barCode = '000000000000000659';
/*
$applicationDestruction = new EME\ApplicationDestruction;
$applicationDestruction->dateDestruction = date('c', mktime(0,0,0, 02, 22, 2018));
*/
$digitizing = new EME\Digitizing;
$digitizing->isScan = true;
$digitizing->isRecognition = false;
$digitizing->isIndex = false;
$digitizing->optionsScan = "Параметры";
//$digitizing->formatCaseIdentifier =  "справочник для сканирования";
$digitizing->caseCount = 2;
$digitizing->pageCount = 2;
//$digitizing->optionsIndex = "справочник для индекирования";

$orderData->clientId = "ELR";
$orderData->orderNumber = "12";
$orderData->dateTimeOrder = date('c');
$orderData->orderType = "Оцифровка";
$orderData->orderName = "Заказ №5";
$orderData->listMeasUnit = array($measUnit1, $measUnit2);
$orderData->comment = "Комментарий";
$orderData->digitizing = $digitizing;
//$orderData->applicationDestruction = $applicationDestruction;


$client = new EME\WMS(false);

try {
    echo "<pre>\n";
    //var_dump($client->GetStatusMeasUnit('000000000000009560'));
    //var_dump($client->LoadPhysicalAddressClient($clientAddress));
    //var_dump($client->LoadClientData($req));
    //$client->GetStatusMeasUnitByClient("ELR");
    //$client->GetQtyMSUnitClient("ELR");
    $client->LoadOrder($orderData);
    //$client->GetTariffScheduleClient("01");
    //var_dump($client->GetStatusOrder($req));

    print "Запрос :\n".format($client->soap->__getLastRequest()) ."\n";
    print "Ответ:\n".format($client->soap->__getLastResponse())."\n";
    echo "\n</pre>\n";
} catch (SoapFault $exception) {
    echo $exception;
}

/**
 * formats the xml output readable
 *
 * @param $xmlString instance of SimpleXmlObject to pretty-print
 * @return string of indented xml-elements
 */
function format($xmlString){

    //Format XML to save indented tree rather than one line
    $dom = new DOMDocument('1.0');
    $dom->preserveWhiteSpace = false;
    $dom->formatOutput = true;
    $dom->loadXML($xmlString);


    return $dom->saveXML();
}
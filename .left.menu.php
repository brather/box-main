<?php
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$aMenuLinks = array(
    Array(
        Loc::getMessage('L_MENU_ITEM_ELAR'),
        "elar/",
        Array(),
        Array(),
        ""
    ),
    Array(
        Loc::getMessage('L_MENU_ITEM_CLIENT'),
        "client/",
        Array(),
        Array(),
        ""
    ),
    Array(
        Loc::getMessage('L_MENU_ITEM_IMPORT'),
        "import/",
        Array(),
        Array()
    )
);
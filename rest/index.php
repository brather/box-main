<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 13.06.2017
 * Time: 13:07
 *
 * POST     - Приемка ЕУ из контекста
 * GET      - Возврат статусов
 */
use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Application;
use Bitrix\Main\Loader;

define('STOP_STATISTICS', true);
set_time_limit(0);

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

/* Обертка для Json_encode */
if (!function_exists('_jR')) {
    function _jR($data, $err = false) {
        header('Content-Type: application/json');
        if ($err) {
            header('HTTP/1.0 404 Not Found');
            header('Status: 404 Not Found');
            $data = ['error' => $data];
        }
        die(json_encode($data));
    };
}

$rq = Application::getInstance()->getContext()->getRequest();

/* Тело запроса */
$getData = $rq->getQueryList()->toArray();

if (($rq->isPost() || strtolower($_SERVER['REQUEST_METHOD']) === 'put') && !$getData ) {
    //Receive the RAW post data.
    $content = trim(file_get_contents("php://input"));
    //Attempt to decode the incoming RAW post data from JSON.
    $getData = json_decode($content, true);
}

/* В Запросе нет токена */
if (!$getData['token']) _jR('Token is empty', true);

/* Проверка токена */
Loader::includeModule('iblock');
Loader::includeModule('highloadblock');

/* Получим пользователя по токену */
$db = CIBlockElement::GetList(['ID' => 'DESC'],
    ['PROPERTY_API_KEY' => $getData['token'], 'IBLOCK_ID' => IBLOCK_CODE_CLIENTS_ID],
    false, false, ['ID', 'PROPERTY_PROP_CLIENT']);
$res = $db->Fetch();
/* Такого токена не существует, вернем ошибку */
if (!$res) _jR('Bad token', true);

$CLIENT_ID = $res['ID'];
$CLIENT_USER_ID = $res['PROPERTY_PROP_CLIENT_VALUE'];

/* Cache WMS Status */
$cache = new CPHPCache();
$st = [];
$cTime = 86400;
$cDir = "status_wms";
if ($cache->InitCache($cTime, md5(HL_BLOCK_CODE_STATUS_WMS), $cDir)) {
    $c = $cache->GetVars();
    $st = $c['WMS_STATUS'];
    unset($c);
}
if ($cache->StartDataCache($cTime, md5(HL_BLOCK_CODE_STATUS_WMS), $cDir)) {
    $hlblock = HighloadBlockTable::getById(8)->fetch();
    $entity = HighloadBlockTable::compileEntity($hlblock);
    $obClass = $entity->getDataClass();

    $rsStatus = $obClass::getList(array(
        'select' => array('ID', 'UF_WMS_NUM', 'UF_XML_ID'),
        'order' => array('UF_SORT' => 'ASC'),
        'limit' => ''
    ));

    while($obStatus = $rsStatus->fetch()) {
        $st[$obStatus['UF_XML_ID']] = array(
            'ID' => $obStatus['ID'],
            'WMS_CODE' => $obStatus['UF_WMS_NUM']
        );
    }

    $cache->EndDataCache(array('WMS_STATUS' => $st));
}
/* End cache WMS Status */

/* Данные по договору, вытяним ID реестра */
$db = CIBlockElement::GetList([],
    ['IBLOCK_ID' => IBLOCK_CODE_CONTRACTS_ID, 'PROPERTY_PROP_SUPER_USER' => $CLIENT_USER_ID],
    false, false,
    ['ID', 'PROPERTY_PROP_ID_REGISTRY']);
$contract = $db->Fetch();

/* Метод получения статусов ЕУ (GET) */
if (!$rq->isPost() && strtolower($_SERVER['REQUEST_METHOD']) === 'get') {
    /* Когда нуждны конкретные ЕУ $getData['sscc'] */
    if (is_array($getData['sscc']) && count($getData['sscc'])) {
        if ($contract) {
            /* Выберем из реестра нужные ЕУ */
            $db = CIBlockElement::GetList([],
                ['IBLOCK_ID' => $contract['PROPERTY_PROP_ID_REGISTRY_VALUE'][0], 'PROPERTY_PROP_SSCC' => $getData['sscc']],
                false, false,
                ['PROPERTY_PROP_SSCC', 'PROPERTY_PROP_WMS_STATUS']);
            $ret = [];
            while ($res = $db->Fetch()) {
                $ret[$res['PROPERTY_PROP_SSCC_VALUE']] = $st[$res['PROPERTY_PROP_WMS_STATUS_VALUE']]['WMS_CODE'];
            }
            /* Если результат пустой и возвращать нечего, значит таких ЕУ нет в базе */
            if (!$ret) _jR('SSCC is wrong data', true);
            else _jR($ret);
        }
    } /* Когда нужно отдать все ЕУ по контракту */
    else if ($rq->get('all') === 'y') {
        /* Выберем из реестра нужные ЕУ */
        $db = CIBlockElement::GetList([],
            ['IBLOCK_ID' => $contract['PROPERTY_PROP_ID_REGISTRY_VALUE'][0]],
            false, false,
            ['PROPERTY_PROP_SSCC', 'PROPERTY_PROP_WMS_STATUS', 'NAME']);
        $ret = [];
        while ($res = $db->Fetch()) {
            $ret[$res['PROPERTY_PROP_SSCC_VALUE']] = [
                0 => $res['NAME'],
                1 => $st[$res['PROPERTY_PROP_WMS_STATUS_VALUE']]['WMS_CODE'],
            ];
        }
        /* Если результат пустой и возвращать нечего, значит таких ЕУ нет в базе */
        if (!$ret) _jR('No SSCC on registries', true);
        else _jR($ret);
    } else { /* Вернем ошибку потмоу что не было запрошено ни одного ЕУ */
        _jR('List SSCC is empty', true);
    }
}

/* Обрабатывать только метод с Content-Type application/json */
if (strcasecmp($_SERVER['CONTENT_TYPE'], 'application/json') === 0) {
    /* Метод добавления ЕУ в реестр (POST)  application/json */
    if ($rq->isPost()) {

        /* В запросе есть sscc список, добавим их в БД*/
        if (is_array($getData['sscc']) && count($getData['sscc'])) {

            $arBarCode = $arTmp = [];
            foreach ($getData['sscc'] as $sscc)
                $arBarCode[] = $sscc['barCode'];

            $db = CIBlockElement::GetList([],
                ['IBLOCK_ID' => (int)$contract['PROPERTY_PROP_ID_REGISTRY_VALUE'][0], 'PROPERTY_PROP_SSCC' => $arBarCode],
                false, false, ['ID', 'PROPERTY_PROP_SSCC']);

            while ($tmp = $db->Fetch()){
                $arTmp[$tmp['PROPERTY_PROP_SSCC_VALUE']] =  $tmp['ID'];
            }
            unset($db, $tmp, $arBarCode);

            $el = new CIBlockElement();
            $cntAdd = 0;
            foreach ($getData['sscc'] as $sscc ) {
                if ($arTmp[$sscc['barCode']]) continue;

                $arElement = [
                    "MODIFIED_BY"    => $CLIENT_USER_ID,
                    'IBLOCK_SECTION_ID' => false,
                    'IBLOCK_ID' => (int)$contract['PROPERTY_PROP_ID_REGISTRY_VALUE'][0],
                    'NAME' => $sscc['name'],
                    'PROPERTY_VALUES' => [
                        "PROP_SSCC" => $sscc['barCode']
                    ],
                    'ACTIVE' => "Y",
                ];

                if ($el->Add($arElement)) $cntAdd++;
            }
            $result =['cntAdd' => $cntAdd];
            if (count($getData['sscc']) > $cntAdd)
                $result['msg'] = "Some SSCC is already in Portal Data Base";

            _jR($result);

        } /* Список sscc есть, но он пустой, ответим ошибкой */
        else if (is_array($getData['sscc']) && count($getData['sscc']) === 0) {
            _jR('List SSCC is empty', true);
        }
    }

    /* Метод добавления завявки от Контекста (PUT) application/json */
    if (strtolower($_SERVER['REQUEST_METHOD']) === 'put') {
        /* Обработаем полученный список Заявок и добавим их в систему */
        if (is_array($getData['bid']) && count($getData['bid'])) {
            $el = new CIBlockElement();

            $arElement = [
                'IBLOCK_ID' => IBLOCK_ID_BID,
                'MODIFIED_BY' => $CLIENT_USER_ID,
                'IBLOCK_SECTION_ID' => false,
                'NAME' => 'Заявка от (' . date('d.m.Y') . ')',
                'ACTIVE' => "Y",
                'PROPERTY_VALUES' => [
                    'DOCS' => "",
                    'SSCC' => [],
                    'CLIENT' => $CLIENT_ID,
                    'TYPE' => "",
                    'FULL_NAME' => "",
                    'POSITION' => "",
                    'COMMENT' => ""
                ]
            ];

            /* Свойство DOCS и SSCC */
            $barCodes = [];
            foreach ($getData['bid']['sscc'] as $barCode => $docs){
                if (is_array($docs) && count($docs))
                    foreach ($docs as $doc)
                        $arElement['PROPERTY_VALUES']['DOCS'] .= $doc . "; ";

                $barCodes[] = $barCode . "";
            }
            $arElement['PROPERTY_VALUES']['DOCS'] = rtrim($arElement['PROPERTY_VALUES']['DOCS'], "; ");

            $db = CIBlockElement::GetList([],
                [
                    'IBLOCK_ID' => (int)$contract['PROPERTY_PROP_ID_REGISTRY_VALUE'][0],
                    'PROPERTY_PROP_SSCC' => $barCodes
                ],
                false, false,
                ['ID']
            );

            while ( $res = $db->Fetch()){
                $arElement['PROPERTY_VALUES']['SSCC'][] = $res['ID'];
            }

            if (count($arElement['PROPERTY_VALUES']['SSCC']) == 0) {
                _jR('SSCC not find in DB, sorry.', true);
            }

            /* Свойство TYPE */
            $arElement['PROPERTY_VALUES']['TYPE'] = $getData['bid']['typeBid'];

            /* Свойство FULL_NAME */
            $arElement['PROPERTY_VALUES']['FULL_NAME'] = $getData['bid']['fullName'];

            /* Свойство POSITION */
            $arElement['PROPERTY_VALUES']['POSITION'] = $getData['bid']['position'];

            /* Свойство COMMENT */
            $arElement['PROPERTY_VALUES']['COMMENT'] = $getData['bid']['comment'];

            if ($ID = $el->Add($arElement)) {
                _jR(['idBid' => $ID]);
            } else {
                _jR($el->LAST_ERROR, true);
            }

        } /* Если список bid пустой, сообщим ошибкой */
        else if (is_array($getData['bid']) && count($getData['bid']) === 0) {
            _jR('Bid list is empty', true);
        }
    }
}

/* Ни одно условие не сработало. */
_jR('What do you mean?', true);
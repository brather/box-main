<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");?>
<? use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);?>
<? global $USER;?>
<? if(!$USER->isAuthorized()){?>
    <div class="container-column">
        <?$APPLICATION->AuthForm(
            array(),
            false,
            false,
            'N',
            false
        );?>
    </div>
<?} else {?>
    <?$APPLICATION->IncludeComponent(
        'box:user.set.section',
        '',
        array(
            'GROUPS_URL' => array(
                U_GROUP_CODE_ELAR_OPERATOR => '/elar/',
                U_GROUP_CODE_CLIENT_S_USER => '/client/',
                U_GROUP_CODE_CLIENT_USER => '/client/',
                U_GROUP_CODE_ELAR_ADMIN => '/elar/',
                U_GROUP_CODE_ELAR_SUPERVISOR => '/elar/',
                U_GROUP_CODE_SUPER_ADMIN => '/',
            )
        )
    );?>
<?}?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
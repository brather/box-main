<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 30.03.2017
 * Time: 10:55
 */

use Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);

$aMenuLinks = array(
    Array(
        Loc::getMessage( "E_MENU_ORDERS_DIGITIZED" ),
        "digitization/",
        Array(),
        Array('GROUPS' => array(U_GROUP_CODE_ELAR_SUPERVISOR)),
        ""
    ),
    Array(
        Loc::getMessage( "E_MENU_ORDERS_INOUT" ),
        "delivery/",
        Array(),
        Array('GROUPS' => array(U_GROUP_CODE_ELAR_SUPERVISOR)),
        ""
    ),
    Array(
        Loc::getMessage( "E_MENU_ORDERS_PLACEMENT" ),
        "placement/",
        Array(),
        Array('GROUPS' => array(U_GROUP_CODE_ELAR_SUPERVISOR)),
        ""
    ),
    Array(
        Loc::getMessage( "E_MENU_ORDERS_DESTRUCTION" ),
        "destruction/",
        Array(),
        Array('GROUPS' => array(U_GROUP_CODE_ELAR_SUPERVISOR)),
        ""
    ),
    Array(
        Loc::getMessage( "E_MENU_ORDERS_CHECKOUT_EXPERT" ),
        "checkout_out_expert/",
        Array(),
        Array('GROUPS' => array(U_GROUP_CODE_ELAR_SUPERVISOR)),
        ""
    ),
    Array(
        Loc::getMessage( "E_MENU_ORDERS_SUPPLIES" ),
        "supplies/",
        Array(),
        Array('GROUPS' => array(U_GROUP_CODE_ELAR_SUPERVISOR)),
        ""
    ),
    Array(
        Loc::getMessage( "E_MENU_ORDERS_ARBITRARY" ),
        "arbitrarily/",
        Array(),
        Array('GROUPS' => array(U_GROUP_CODE_ELAR_SUPERVISOR)),
        ""
    ),
);
<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 30.03.2017
 * Time: 10:54
 */
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

global $APPLICATION;

$APPLICATION->IncludeComponent(
    "box:elar.supervisor", // Component name
    ".default", // Component template
    array(
        // Component Params
        "COMPONENT_TEMPLATE_FILE" => 'template',                    // Имя шаблона по-умолчанию
        "NAV_TEMPLATE" => 'design',
        "SEF_MODE" => "Y",                                         // ЧПУ включен
        "SEF_FOLDER" => "/elar/orders/",                            // Корневая директория
        "SEF_URL_TEMPLATES" => array(                               // Параметры ЧПУ
            "detail" => "#ID#/",                                    // Детальная страница заказа (#ID# - id инфоблока реестра)
        ),
        "CACHE_TYPE" => "N",     // тип кеша, на время разработки N
        "SET_SORT_FILTER" => array(
            'ID',
            'IBLOCK_NAME',
            'NAME',
        ),
    ),
    false
);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
<?php
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$aMenuLinks = array(
    Array(
        Loc::getMessage('L_MENU_ITEM_FAVORITE'),
        "/",
        Array(),
        Array('GROUPS' => array(U_GROUP_CODE_SUPER_ADMIN)),
        ""
    ),
    Array(
        Loc::getMessage('L_MENU_ITEM_ORDERS'),
        "orders/",
        Array(),
        Array('GROUPS' => array(U_GROUP_CODE_ELAR_SUPERVISOR, "IS_PARENT" => true, "DEPTH_LEVEL" => 1)),
        ""
    ),
    Array(
        Loc::getMessage('L_MENU_ITEM_CLIENTS'),
        "users/",
        Array(),
        Array('GROUPS' => array(U_GROUP_CODE_ELAR_SUPERVISOR, "IS_PARENT" => true, "DEPTH_LEVEL" => 1)),
        ""
    ),
    Array(
        Loc::getMessage('L_MENU_ITEM_COMPANY'),
        "company/",
        Array(),
        Array('GROUPS' => array(U_GROUP_CODE_ELAR_SUPERVISOR, "IS_PARENT" => true, "DEPTH_LEVEL" => 1)),
        ""
    ),
);

return $aMenuLinks;
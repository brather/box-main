<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 06.04.2017
 * Time: 11:59
 */
require ($_SERVER['DOCUMENT_ROOT'] . "/bitrix/header.php");

global $APPLICATION;

$APPLICATION->SetTitle("Список организаций");

$APPLICATION->IncludeComponent("box:elar.company.list", ".default",
    array(
        "CACHE_TYPE" => "N",
        "CACHE_TIME" => 86400,
        "NAV_TEMPLATE" => 'design',
        "COMPANY_ON_PAGE" => 20,
        "SEF_FOLDER" => "/elar/company/"
    )
);

require ($_SERVER['DOCUMENT_ROOT'] . "/bitrix/footer.php");
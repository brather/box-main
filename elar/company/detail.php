<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 06.04.2017
 * Time: 13:25
 */

require ($_SERVER['DOCUMENT_ROOT'] . "/bitrix/header.php");

global $APPLICATION;

$APPLICATION->IncludeComponent("box:elar.company.detail", "v1",
    array(
        "CACHE_TYPE" => "N",
    )
);

require ($_SERVER['DOCUMENT_ROOT'] . "/bitrix/footer.php");
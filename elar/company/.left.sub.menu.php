<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 07.04.2017
 * Time: 16:50
 */
use Bitrix\Main\Localization\Loc;

Loc::loadLanguageFile(__FILE__);

$aMenuLinks = array(
    Array(
        Loc::getMessage( "E_USER_MENU_CREATE_USER" ),
        "create/",
        Array(),
        Array('GROUPS' => array(U_GROUP_CODE_ELAR_SUPERVISOR), "DEPTH_LEVEL" => 2),
        ""
    ),
);
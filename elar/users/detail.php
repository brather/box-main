<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

/** urlrewrite.php
 *
 * "CONDITION" => "#^/elar/clients/([^/]*)(/?)#",
 * "RULE" => "CLIENT_ID=$1",
 * "ID" => "box:elar.client.detail",
 * "PATH" => "/elar/clients/detail.php",
 */

global $APPLICATION;

$APPLICATION->IncludeComponent("box:elar.user.detail", ".default", array(
    "CACHE_TYPE" => "N",
));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
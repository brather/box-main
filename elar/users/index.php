<?php
/**
 * Created by PhpStorm.
 * User: AChechel
 * Date: 06.04.2017
 * Time: 11:58
 */

require ($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

global $APPLICATION;

$APPLICATION->IncludeComponent("box:elar.user.list", ".default",
    array(
        "CACHE_TYPE" => "N",
        "IBLOCK_ID" => IBLOCK_CODE_CLIENTS_ID,
        "USERS_ON_PAGE" => 20,
        "NAV_TEMPLATE" => "design" // на примере проверить у user.order.list
    )
);

require ($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
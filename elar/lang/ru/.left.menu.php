<?php /* @var array $MESS */
$MESS['L_MENU_ITEM_ORDERS'] = 'Заказы';
$MESS['L_MENU_ITEM_CLIENTS'] = 'Пользователи';
$MESS['L_MENU_ITEM_COMPANY'] = 'Организации';
$MESS['L_MENU_ITEM_REQ'] = 'Запросы';
$MESS['L_MENU_ITEM_REGISTRY'] = 'Реестр';
$MESS['L_MENU_ITEM_CIRCULATION'] = 'Обращения';
$MESS['L_MENU_ITEM_CABINET'] = 'Кабинет';
$MESS['L_MENU_ITEM_FAVORITE'] = 'Главная';
<? /* @var array $MESS */
$MESS["E_MENU_ORDERS_DIGITIZED"] = "Оцифровка";
$MESS["E_MENU_ORDERS_INOUT"] = "Выдача";
$MESS["E_MENU_ORDERS_PLACEMENT"] = "Прием";
$MESS["E_MENU_ORDERS_DESTRUCTION"] = "Заявка на уничтожение";
$MESS["E_MENU_ORDERS_CHECKOUT_EXPERT"] = "Выезд эксперта";
$MESS["E_MENU_ORDERS_SUPPLIES"] = "Доставка расходных материалов";
$MESS["E_MENU_ORDERS_ARBITRARY"] = "Произвольный заказ";

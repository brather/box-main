<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
<div class="page__frame">
    <div class="page__title">
        <h1><?= Loc::getMessage('404_TITLE') ?></h1>
    </div>
    <div class="order">
        <div class="order__col">
            <h2><?= Loc::getMessage('404_DESCRIPTION') ?></h2>
        </div>
    </div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
